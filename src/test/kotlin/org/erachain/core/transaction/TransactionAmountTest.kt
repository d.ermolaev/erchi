﻿package org.erachain.core.transaction

//import org.junit.jupiter.api.Test
//import org.junit.Assert.assertTrue
import erchi.core.account.PrivateKeyAccount
import org.erachain.controller.Controller
import org.erachain.core.BlockChain
import org.erachain.core.account.Account
import org.erachain.core.block.GenesisBlock
import org.erachain.core.crypto.Crypto
import org.erachain.core.exdata.exLink.ExLink
import org.erachain.core.item.assets.AssetCls
import org.erachain.core.item.assets.AssetVenture
import org.erachain.core.wallet.Wallet
import org.erachain.dapp.DAPP
import org.erachain.database.IDB
import org.erachain.datachain.DCSet
import org.erachain.ntp.NTP
import org.erachain.settings.Settings
import org.erachain.utils.SimpleFileVisitorForRecursiveFolderDeletion
import org.junit.Assert.*
import org.junit.Test
import org.slf4j.LoggerFactory
import java.io.File
import java.math.BigDecimal
import java.nio.file.Files

internal class TransactionAmountTest {

    var exLink: ExLink? = null
    var DAPP: DAPP? = null

    var ERM_KEY = AssetCls.ERA_KEY
    var FEE_KEY = AssetCls.FEE_KEY

    var TESTED_DEAL = intArrayOf(
        Transaction.FOR_NETWORK,
        Transaction.FOR_DB_RECORD,
    )
    var TESTED_DBS = intArrayOf(
        IDB.DBS_MAP_DB
    )

    var dbRef = 0L
    var FEE_POWER = 1.toByte()
    var assetReference = ByteArray(64)
    var timestamp: Long = 0
    var itemAppData: ByteArray? = null
    var txFlags = 0L
    var cntrl: Controller? = null

    //CREATE KNOWN ACCOUNT
    var seed = Crypto.getInstance().digest("tes213sdffsdft".toByteArray())
    var privateKey = Crypto.getInstance().createKeyPair(seed).a
    var maker = PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, 1))
    var maker_1 = PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, 2))
    var recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW")
    var amount = BigDecimal.valueOf(10).setScale(BlockChain.AMOUNT_DEFAULT_SCALE)
    var data = "test123!".toByteArray()
    var isText = byteArrayOf(1)
    var encrypted = byteArrayOf(0)
    var ERA_KEY = AssetCls.ERA_KEY
    var flags = 0L
    var feePow: Byte = 0

    var asset: AssetCls? = null
    var assetMovable: AssetCls? = null
    var key: Long = 0

    private val icon = byteArrayOf(1, 3, 4, 5, 6, 9) // default value
    private val image = byteArrayOf(4, 11, 32, 23, 45, 122, 11, -45) // default value

    //CREATE EMPTY MEMORY DATABASE
    private var dcSet: DCSet? = null
    private var gb: GenesisBlock? = null
    private var bchain: BlockChain? = null

    // INIT ASSETS
    private fun init(dbs: Int) {
        val tempDir = File(Settings.getInstance().dataTempDir)
        LOGGER.info(" ********** open DBS: $dbs \n IN: ${tempDir.absolutePath}")
        try {
            Files.walkFileTree(tempDir.toPath(), SimpleFileVisitorForRecursiveFolderDeletion())
        } catch (e: Throwable) {
        }
        dcSet = DCSet.createEmptyHardDatabaseSet(dbs)
        cntrl = Controller.getInstance()
        timestamp = NTP.getTime()
        cntrl!!.initBlockChain(dcSet)
        bchain = cntrl!!.getBlockChain()
        BlockChain.ALL_VALID_BEFORE = 0
        gb = bchain!!.genesisBlock

        // FEE FUND
        maker.setLastTimestamp(longArrayOf(gb!!.timestamp, 0), dcSet)
        maker.changeBalance(dcSet, false, ERA_KEY,
            BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE),
            false, false)
        maker.changeBalance(dcSet, false, FEE_KEY,
            BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE),
            false, false)
        maker_1.setLastTimestamp(longArrayOf(gb!!.timestamp, 0), dcSet)
        asset = AssetVenture(1, maker, "aasdasd", icon, image, "asdasda", 8, 50000L)
        // set SCALABLE assets ++
        asset!!.setReference(Crypto.getInstance().digest(asset!!.toBytes(false)), dbRef)
        asset!!.insertToMap(dcSet, 0L)
        key = asset!!.key
        assetMovable = AssetVenture(0, maker, "movable", icon, image, "...", 8, 500L)
        assetMovable!!.setReference(Crypto.getInstance().digest(assetMovable!!.toBytes(false)), dbRef)
    }

    @Test
    fun scaleTest() {
        System.setProperty("probe", "1")
        val bbb = 31
        assertEquals("11111", Integer.toBinaryString(bbb))
        assertEquals("10000000", Integer.toBinaryString(128))
        ///assertEquals(128.toByte(), -128.toByte())
    }

    @Test
    fun scaleTestNewParse() {
        val amount = BigDecimal("12.00000000123");

        // WRITE AMOUNT
        val amountBytes = amount.unscaledValue().toByte()


    }


    @Test
    fun parse() {

        for (forDeal in TESTED_DEAL) {
            LOGGER.info(" ********** open DBS: $forDeal")

            var sendTX = TransactionAmount(
                maker, exLink, DAPP, FEE_POWER,
                recipient,
                Account.BALANCE_POS_OWN,
                ERM_KEY,
                amount, "headdd",
                null,
                isText,
                encrypted,
                timestamp
            )

            sendTX.sign(maker, forDeal)
            assertTrue(sendTX.isSignatureValid)

            var bytes = sendTX.toBytes(forDeal)
            assertEquals(bytes.size, sendTX.getDataLength(forDeal))
            var sendTX_2: TransactionAmount
            try {
                sendTX_2 = TransactionFactory.getInstance().parse(bytes, forDeal) as TransactionAmount
            } catch (e: Exception) {
                LOGGER.error(e.message, e)
                fail("ERROR: ${e.message}")
                return
            }

            /////////////////////
            sendTX = TransactionAmount(
                maker, exLink, DAPP, FEE_POWER,
                recipient,
                Account.BALANCE_POS_OWN,
                ERM_KEY,
                amount, "headdd",
                data,
                isText,
                encrypted,
                timestamp
            )

            sendTX.sign(maker, forDeal)
            assertTrue(sendTX.isSignatureValid)

            bytes = sendTX.toBytes(forDeal)
            assertEquals(bytes.size, sendTX.getDataLength(forDeal))
            try {
                sendTX_2 = TransactionFactory.getInstance().parse(bytes, forDeal) as TransactionAmount
            } catch (e: Exception) {
                LOGGER.error(e.message, e)
                fail("ERROR: ${e.message}")
                return
            }

            assertEquals(
                String(sendTX.getDataMessage()), String(
                    sendTX_2.getDataMessage()
                )
            )
            assertEquals(sendTX.getCreator(), sendTX_2.getCreator())
            assertEquals(sendTX.getRecipient(), sendTX_2.getRecipient())
            assertEquals(sendTX.getKey(), sendTX_2.getKey())
            assertEquals(sendTX.getAmount(), sendTX_2.getAmount().setScale(BlockChain.AMOUNT_DEFAULT_SCALE))
            assertEquals(sendTX.isEncrypted, sendTX_2.isEncrypted)
            assertEquals(sendTX.isText(), sendTX_2.isText())
            sendTX_2.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1)
            assert(sendTX_2.isSignatureValid)
        }
    }

    @Test
    fun toBytes() {
    }

    @Test
    fun validate() {
        for (dbs in TESTED_DBS) {
            try {
                init(dbs)

                val sendTX = TransactionAmount(
                    maker, exLink, DAPP, FEE_POWER,  //	ATFunding
                    recipient,
                    Account.BALANCE_POS_OWN,
                    ERM_KEY,
                    amount, "headdd",
                    data,
                    isText,
                    encrypted,
                    timestamp
                )
                sendTX.sign(maker, Transaction.FOR_NETWORK)
                sendTX.setDC(dcSet, Transaction.FOR_NETWORK, BlockChain.SKIP_INVALID_SIGN_BEFORE, 1)
                assertTrue(sendTX.isSignatureValid)
                try {
                    assertEquals(
                        sendTX.isValid(Transaction.FOR_NETWORK, txFlags),
                        Transaction.VALIDATE_OK
                    )
                } catch (e: TxException) {
                    throw RuntimeException(e)
                }
                sendTX.process(gb, Transaction.FOR_NETWORK)
                assertEquals(
                    BigDecimal.valueOf(1).subtract(sendTX.getFee())
                        .setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(FEE_KEY, dcSet)
                )
                assertEquals(
                    BigDecimal.valueOf(90).setScale(BlockChain.AMOUNT_DEFAULT_SCALE),
                    maker.getBalanceUSE(ERM_KEY, dcSet)
                )
                assertEquals(
                    BigDecimal.valueOf(10).setScale(BlockChain.AMOUNT_DEFAULT_SCALE),
                    recipient.getBalanceUSE(ERM_KEY, dcSet)
                )
                val rawMessageTransactionV3 = sendTX.toBytes(Transaction.FOR_NETWORK)
                val dd = sendTX.getDataLength(Transaction.FOR_NETWORK)
                assertEquals(
                    rawMessageTransactionV3.size.toLong(), sendTX.getDataLength(
                        Transaction.FOR_NETWORK
                    ).toLong()
                )
                var messageTransactionV3_2: TransactionAmount? = null
                try {
                    messageTransactionV3_2 = TransactionFactory.getInstance()
                        .parse(rawMessageTransactionV3, Transaction.FOR_NETWORK) as TransactionAmount
                } catch (e: Exception) {
                    LOGGER.error(e.message, e)
                }
                assertEquals(
                    String(sendTX.getDataMessage()), String(
                        messageTransactionV3_2!!.getDataMessage()
                    )
                )
                assertEquals(sendTX.getCreator(), messageTransactionV3_2.getCreator())
                assertEquals(sendTX.getRecipient(), messageTransactionV3_2.getRecipient())
                assertEquals(sendTX.getKey(), messageTransactionV3_2.getKey())
                assertEquals(
                    sendTX.getAmount(),
                    messageTransactionV3_2.getAmount().setScale(BlockChain.AMOUNT_DEFAULT_SCALE)
                )
                assertEquals(sendTX.isEncrypted, messageTransactionV3_2.isEncrypted)
                assertEquals(sendTX.isText(), messageTransactionV3_2.isText())
                messageTransactionV3_2.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1)
                assert(messageTransactionV3_2.isSignatureValid)
            } finally {
                dcSet!!.close()
            }
        }
    }

    @Test
    fun noAmount() {
        println("(byte) 128 : " + Integer.toBinaryString(128.toByte().toInt()))
        println("(byte)-128 : " + Integer.toBinaryString(-128.toByte().toInt()))
        var rSend = TransactionAmount(
            maker, null, null, feePow, recipient, Account.BALANCE_POS_OWN, 1L, amount,
            "", null, isText, encrypted, 1L
        )
        println(
            """
    prop1: ${Integer.toBinaryString(rSend.getTypeBytes()[2].toInt())}
    prop2: ${Integer.toBinaryString(rSend.getTypeBytes()[3].toInt())}
    """.trimIndent()
        )
        println("propView: " + rSend.viewProperies())
        rSend = TransactionAmount(
            maker, null, null, feePow, recipient, Account.BALANCE_POS_OWN, 0L, null,
            "", null, isText, encrypted, 1L
        )
        println(
            """
    prop1: ${Integer.toBinaryString(rSend.getTypeBytes()[2].toInt())}
    prop2: ${Integer.toBinaryString(rSend.getTypeBytes()[3].toInt())}
    """.trimIndent()
        )
        println("propView: " + rSend.viewProperies())
    }

    @Test
    fun processAction() {
    }

    /*
    @get:Test
    val dataLength: Int
        get() {
            return 10
        }

    @get:Test
    val isValid: Int
        get() {
            return 1
        }

     */

    @Test
    fun makeItemsKeys() {
    }

    @Test
    fun processBody() {
    }

    @Test
    fun orphanBody() {
    }

    companion object {
        var LOGGER = LoggerFactory.getLogger(CancelOrderTransaction::class.java.name)
    }
}