﻿package org.erachain.core.transaction

import org.erachain.controller.Controller
import org.erachain.core.BlockChain
import org.erachain.core.account.Account
import org.erachain.core.block.GenesisBlock
import org.erachain.datachain.DCSet
import org.erachain.ntp.NTP
import org.erachain.settings.Settings
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.jupiter.api.fail
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import kotlin.experimental.and
import kotlin.experimental.inv

class RCalculatedTest {

    var logger = LoggerFactory.getLogger(RCalculatedTest::class.java.name)

    private val seqNo: Long = 0
    var timestamp = NTP.getTime()
    var shortAcc = ByteArray(Account.ADDRESS_LENGTH)

    var recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW")
    var amount = BigDecimal.valueOf(10).setScale(BlockChain.AMOUNT_DEFAULT_SCALE)
    var title = "probe title"
    var tags = "gen"
    var message = "test123!"
    var dcSet: DCSet? = null
    var chain: BlockChain? = null
    var gb: GenesisBlock? = null

    //@Before
    fun init() {
        Settings.genesisStamp = Settings.DEFAULT_MAINNET_STAMP
        DCSet.reCreateDBinMEmory(false, false)
        dcSet = DCSet.getInstance()
        try {
            chain = BlockChain(dcSet)
        } catch (e1: Exception) {
        }
        gb = chain!!.genesisBlock
        Controller.getInstance().blockChain = chain

        if (false) {
            val maskNeg: Byte = -128
            // RESET 7 bit
            val typeBytes = byteArrayOf(100, 0, 0, 0)
            typeBytes[2] = typeBytes[2] and maskNeg.inv()
            println(typeBytes[2] > 0)
            println(typeBytes[2] < 0)
            println((typeBytes[2] and maskNeg).toString(radix = 2))

            println(maskNeg.toString(radix = 2))
            println(maskNeg.inv().toString(radix = 2))
            println((typeBytes[2] and maskNeg.inv()).toString(radix = 2))
        }

    }

    var TESTED_DEAL = intArrayOf(
        Transaction.FOR_NETWORK,
        Transaction.FOR_DB_RECORD
    )

    @Test
    fun parse() {

        for (forDeal in TESTED_DEAL) {
            println("*** DEAL: $forDeal ***")

            var calculated1 = RCalculated(
                recipient, Transaction.FEE_KEY, amount,
                BlockChain.MESS_FORGING, tags, message,
                Transaction.makeDBRef(1234, 0), 123L
            )

            var raw = calculated1.toBytes(forDeal)
            assertEquals(raw.size, calculated1.getDataLength(forDeal))
            var calculated2: RCalculated? = null
            try {
                calculated2 = TransactionFactory.getInstance().parse(raw, forDeal) as RCalculated
            } catch (e: Exception) {
                logger.error(e.message, e)
            }

            calculated2!!

            assertEquals(calculated1.creator, calculated2.creator)
            assertEquals(calculated1.recipient.address, calculated2.recipient.address)
            assertEquals(Transaction.FEE_KEY, calculated2.key)
            assertEquals(calculated1.amount, calculated2.amount.setScale(10).setScale(BlockChain.AMOUNT_DEFAULT_SCALE))
            assertEquals(calculated1.title, calculated2.title)
            assertEquals(calculated1.tags, calculated2.tags)
            assertArrayEquals(calculated1.dataMessage, calculated2.dataMessage)


            calculated1 = RCalculated(
                recipient, Transaction.FEE_KEY, amount,
                BlockChain.MESS_FORGING,
                Transaction.makeDBRef(1234, 0), 123L
            )

            raw = calculated1.toBytes(forDeal)
            assertEquals(raw.size, calculated1.getDataLength(forDeal))
            try {
                calculated2 = TransactionFactory.getInstance().parse(raw, forDeal) as RCalculated
            } catch (e: Exception) {
                logger.error(e.message, e)
            }

            calculated2!!

            assertEquals(calculated1.creator, calculated2.creator)
            assertEquals(calculated1.recipient.address, calculated2.recipient.address)
            assertEquals(Transaction.FEE_KEY, calculated2.key)
            assertEquals(calculated1.amount, calculated2.amount.setScale(BlockChain.AMOUNT_DEFAULT_SCALE))
            assertEquals(calculated1.title, calculated2.title)
            assertEquals(calculated1.tags, calculated2.tags)
            assertArrayEquals(calculated1.dataMessage, calculated2.dataMessage)

            try {
                RCalculated(
                    recipient, Transaction.RIGHTS_KEY, null,
                    title, timestamp,
                    seqNo
                )
                fail { "wrong " }
            } catch (e: AssertionError) {
                // good
            }

        }
    }

    @Test
    fun toBytesBody() {
        init()
        val gsend = gb!!.getTransaction(2)
        gsend.toBytes(Transaction.FOR_NETWORK)
    }

}