﻿package org.erachain.core.transaction

import erchi.core.account.PrivateKeyAccount
import org.erachain.controller.Controller
import org.erachain.core.BlockChain
import org.erachain.core.account.PublicKeyAccount
import org.erachain.core.block.GenesisBlock
import org.erachain.core.crypto.Crypto
import org.erachain.core.exdata.exLink.ExLink
import org.erachain.core.item.assets.AssetCls
import org.erachain.core.item.assets.AssetVenture
import org.erachain.database.IDB
import org.erachain.datachain.DCSet
import org.erachain.ntp.NTP
import org.erachain.settings.Settings
import org.erachain.utils.SimpleFileVisitorForRecursiveFolderDeletion
import org.junit.Assert
import org.junit.Test
//import org.junit.jupiter.api.Test
import org.mapdb.Fun.Tuple2
import org.mapdb.Fun.Tuple5
import org.slf4j.LoggerFactory
import java.io.File
import java.math.BigDecimal
import java.nio.file.Files
import java.util.*

internal class CancelOrderTransactionTest {
    var exLink: ExLink? = null

    var TESTED_FOR_PARSE = intArrayOf(
        Transaction.FOR_NETWORK,
        Transaction.FOR_DB_RECORD,
    )
    var TESTED_DBS = intArrayOf(
        IDB.DBS_MAP_DB
    )

    var dbRef = 0L
    var FEE_KEY = AssetCls.FEE_KEY
    var FEE_POWER = 1.toByte()
    var assetReference = ByteArray(64)
    var timestamp: Long = 0
    var itemAppData: ByteArray? = null
    var txFlags = 0L
    var cntrl: Controller? = null

    //CREATE KNOWN ACCOUNT
    var seed = Crypto.getInstance().digest("tes213sdffsdft".toByteArray())
    var privateKey = Crypto.getInstance().createKeyPair(seed).a
    var maker = PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey)
    var seed_1 = Crypto.getInstance().digest("tes213sdffsdft_1".toByteArray())
    var privateKey_1 = Crypto.getInstance().createKeyPair(seed_1).a
    var maker_1 = PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey_1)
    var asset: AssetCls? = null
    var assetMovable: AssetCls? = null
    var key: Long = 0
    var rsend: PublicKeyAccount? = null
    var balance5: Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>>? =
        null
    private val icon = byteArrayOf(1, 3, 4, 5, 6, 9) // default value
    private val image = byteArrayOf(4, 11, 32, 23, 45, 122, 11, -45) // default value

    //CREATE EMPTY MEMORY DATABASE
    private var dcSet: DCSet? = null
    private var gb: GenesisBlock? = null
    private var bchain: BlockChain? = null

    // INIT ASSETS
    private fun init(dbs: Int) {
        val tempDir = File(Settings.getInstance().dataTempDir)
        LOGGER.info(" ********** open DBS: $dbs \n IN: ${tempDir.absolutePath}")
        try {
            Files.walkFileTree(tempDir.toPath(), SimpleFileVisitorForRecursiveFolderDeletion())
        } catch (e: Throwable) {
        }
        dcSet = DCSet.createEmptyHardDatabaseSet(dbs)
        cntrl = Controller.getInstance()
        timestamp = NTP.getTime()
        cntrl!!.initBlockChain(dcSet)
        bchain = cntrl!!.getBlockChain()
        BlockChain.ALL_VALID_BEFORE = 0
        gb = bchain!!.genesisBlock

        // FEE FUND
        maker.setLastTimestamp(longArrayOf(gb!!.timestamp, 0), dcSet)
        maker.changeBalance(
            dcSet,
            false,
            FEE_KEY,
            BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE),
            false,
            false
        )
        maker_1.setLastTimestamp(longArrayOf(gb!!.timestamp, 0), dcSet)
        asset = AssetVenture(1, maker, "aasdasd", icon, image, "asdasda", 8, 50000L)
        // set SCALABLE assets ++
        asset!!.setReference(Crypto.getInstance().digest(asset!!.toBytes(false)), dbRef)
        asset!!.insertToMap(dcSet, 0L)
        key = asset!!.key
        assetMovable = AssetVenture(0, maker, "movable", icon, image, "...", 8, 500L)
        assetMovable!!.setReference(Crypto.getInstance().digest(assetMovable!!.toBytes(false)), dbRef)
    }

    @Test
    fun parse() {

        for (forDeal in TESTED_FOR_PARSE) {
            LOGGER.info(" ********** open DBS: $forDeal")

            //CREATE UPDATE ORDER
            val tx = CancelOrderTransaction(maker, ByteArray(64), FEE_POWER, timestamp)
            tx.sign(maker, forDeal)
            Assert.assertTrue(tx.isSignatureValid)

            //CONVERT TO BYTES
            var rawTX = tx.toBytes(forDeal)

            //CHECK DATA LENGTH
            Assert.assertEquals(rawTX.size, tx.getDataLength(forDeal))

            val parsedTX: CancelOrderTransaction
            try {
                //PARSE FROM BYTES
                parsedTX =
                    TransactionFactory.getInstance().parse(rawTX, forDeal) as CancelOrderTransaction
            } catch (e: Exception) {
                LOGGER.error(e.message, e)
                Assert.fail("Exception while parsing transaction.")
                return
            }

            //CHECK SIGNATURE
            Assert.assertTrue(Arrays.equals(tx.getSignature(), parsedTX.getSignature()))

            //CHECK ISSUER
            Assert.assertEquals(tx.getCreator().address, parsedTX.getCreator().address)

            //CHECK REFERENCE
            //assertEquals((long)tx.getReference(), (long)parsedTX.getReference());

            //CHECK TIMESTAMP
            Assert.assertEquals(tx.getTimestamp(), parsedTX.getTimestamp())
            Assert.assertTrue(Arrays.equals(tx.orderSignature, parsedTX.orderSignature))

            //PARSE TRANSACTION FROM WRONG BYTES
            rawTX = ByteArray(tx.getDataLength(forDeal))
            try {
                //PARSE FROM BYTES
                TransactionFactory.getInstance().parse(rawTX, forDeal)

                //FAIL
                Assert.fail("this should throw an exception")
            } catch (e: Exception) {
                //EXCEPTION IS THROWN OK
            }
        }
    }

    @Test
    fun toBytes() {
    }

    /*
    @get:Test
    val dataLength: Int
        get() {
            return 10
        }

    @get:Test
    val isValid: Int
        get() {
            return 1
        }

     */

    @Test
    fun makeItemsKeys() {
    }

    @Test
    fun processBody() {
    }

    @Test
    fun orphanBody() {
    }

    companion object {
        var LOGGER = LoggerFactory.getLogger(CancelOrderTransaction::class.java.name)
    }
}