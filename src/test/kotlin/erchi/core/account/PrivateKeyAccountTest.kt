﻿package erchi.core.account

import org.erachain.core.crypto.Base58
import org.erachain.core.crypto.Crypto
import org.erachain.core.wallet.Wallet
import org.erachain.settings.Settings
import org.junit.Assert
import org.junit.Test
import org.slf4j.LoggerFactory
import java.io.File
import java.security.SecureRandom

internal class PrivateKeyAccountTest {

    val crypto = Crypto.getInstance()

    @Test
    fun getPrivateKey() {
        val tempDir = File(Settings.getInstance().dataTempDir)
        LOGGER.info(" ********** IN: ${tempDir.absolutePath}")
        println(" ********** IN: ${tempDir.absolutePath}")
    }

    @Test
    fun get58() {
        println("Test PRIVATE KEY")

        var privKeyBytes = Wallet.makeAccountSeedByNonce(crypto.digest("TTT".toByteArray()), 1)
        var privkey = PrivateKeyAccount(Crypto.ED25519_SYSTEM, privKeyBytes)

        Assert.assertEquals(privkey.system, Crypto.ED25519_SYSTEM)

        Assert.assertEquals(privkey.addressFull,     "acc:4YuDWoBZ6LpmZxyR1Nky82orpyvH")
        Assert.assertEquals(privkey.publicKey58Full, "pub:WzQAN6tVwMb7MJYFXvyESLyWm9HE39gbcHbCadDQeaHGD")
        Assert.assertEquals(privkey.getSeed58(), "SECRET:Wk68sLERnrvGgv8oYdBufNhpQk5SjucEd3SbrQy5QwbfA")


    }

    /**
     * Проверка работы по СИДУ из мобилки
     */
    @Test
    fun mobilePrivateKey() {
        val privKyeMobi = "5BzAsmeoXwNzzWdeR253vxLXv7qqutHg952CbFsj1iqaBf3AeswgE9JjzEu78ddi516imb1FaR78gbf54812i7Fe"
        val privKyeMobiByte = Base58.decode(privKyeMobi, Crypto.SIGNATURE_LENGTH)
        val privAccount = PrivateKeyAccount(Crypto.ED25519_SYSTEM, privKyeMobiByte)
        Assert.assertEquals("249wR1G2X5udjMcyRCKVtgcQmVbq39jC35YpjHkLM8b2", Base58.encode(privAccount.publicKey))
        Assert.assertEquals("4HjAyj8agGmLf2YmpJ4fDWLeweDA", privAccount.address)
        val signature = Crypto.getInstance().sign(privAccount, privKyeMobiByte)
        Assert.assertEquals(true, Crypto.getInstance().verify(privAccount.publicKey, signature, privKyeMobiByte))
    }

    @Test
    fun nodePrivateKey() {
        val random = SecureRandom()
        val seed = ByteArray(32)
        random.nextBytes(seed)
        val privAccount = PrivateKeyAccount(Crypto.ED25519_SYSTEM, seed)
        val signature = Crypto.getInstance().sign(privAccount, seed)
        Assert.assertEquals(true, Crypto.getInstance().verify(privAccount.publicKey, signature, seed))
    }

    companion object {
        var LOGGER = LoggerFactory.getLogger(PrivateKeyAccountTest::class.java.name)
    }

}