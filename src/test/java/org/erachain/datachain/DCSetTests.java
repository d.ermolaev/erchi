package org.erachain.datachain;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.core.BlockChain;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetVenture;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.item.persons.PersonHuman;
import org.erachain.core.transaction.*;
import org.erachain.dbs.rocksDB.utils.ConstantsRocksDB;
import org.erachain.ntp.NTP;
import org.erachain.settings.Settings;
import org.erachain.utils.SimpleFileVisitorForRecursiveFolderDeletion;
import org.junit.Test;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple5;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class DCSetTests {

    int[] TESTED_DBS = new int[]{DCSet.DBS_MAP_DB, DCSet.DBS_ROCK_DB, DCSet.DBS_NATIVE_MAP, DCSet.DBS_MAP_DB_IN_MEM, DCSet.DBS_FAST};

    static Logger LOGGER = LoggerFactory.getLogger(DCSetTests.class.getName());
    private final byte[] icon = new byte[0]; // default value
    private final byte[] image = new byte[0]; // default value
    private final byte[] ownerSignature = new byte[Crypto.SIGNATURE_LENGTH];
    byte[] assetReference = new byte[Crypto.SIGNATURE_LENGTH];
    Long releaserReference = null;
    byte[] itemAppData = null;
    long txFlags = 0L;
    BigDecimal BG_ZERO = BigDecimal.ZERO.setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
    long ERM_KEY = Transaction.RIGHTS_KEY;
    long FEE_KEY = Transaction.FEE_KEY;
    //long ALIVE_KEY = StatusCls.ALIVE_KEY;
    byte FEE_POWER = (byte) 1;
    byte[] personReference = new byte[Crypto.SIGNATURE_LENGTH];
    long timestamp = NTP.getTime();
    Long last_ref;
    boolean asPack = false;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    PersonCls personGeneral;
    PersonCls person;
    long personKey = -1;
    IssuePersonRecord issuePersonTransaction;
    //int version = 0; // without signs of person
    int version = 1; // with signs of person
    //CREATE EMPTY MEMORY DATABASE
    private DCSet dcSet;
    private GenesisBlock gb;

    // INIT PERSONS
    @SneakyThrows
    private void init(int dbs) {

        LOGGER.info("\n\n *** DBS_TEST: " + dbs);

        try {
            // NEED DELETE RocksDB file !!!
            File tempDir = new File(Settings.getInstance().getDataChainPath() + ConstantsRocksDB.ROCKS_DB_FOLDER);
            Files.walkFileTree(tempDir.toPath(), new SimpleFileVisitorForRecursiveFolderDeletion());
        } catch (Throwable e) {
        }

        dcSet = DCSet.createEmptyDatabaseSet(dbs);

        BlockChain chain = new BlockChain(dcSet);
        gb = chain.getGenesisBlock();

        last_ref = gb.getTimestamp();

        // GET RIGHTS TO CERTIFIER
        byte gender = 1;
        long birthDay = timestamp - 12345678;
        personGeneral = new PersonHuman(maker, "Ermolaev Dmitrii Sergeevich as certifier", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, null,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, ownerSignature);

        GenesisIssuePersonRecord genesis_issue_person = new GenesisIssuePersonRecord(personGeneral, null, null, null);
        genesis_issue_person.setDC(dcSet);
        genesis_issue_person.process(gb, Transaction.FOR_NETWORK);
        GenesisCertifyPersonRecord genesis_certify = new GenesisCertifyPersonRecord(maker, 0L, null, null, null);
        genesis_certify.setDC(dcSet);
        genesis_certify.process(gb, Transaction.FOR_NETWORK);

        maker.setLastTimestamp(new long[]{last_ref, 0}, dcSet);
        maker.changeBalance(dcSet, true, ERM_KEY, BigDecimal.valueOf(1000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        maker.changeBalance(dcSet, true, FEE_KEY, BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);

        person = new PersonHuman(maker, "Ermolaev Dmitrii Sergeevich", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, null,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, ownerSignature);

        //CREATE ISSUE PERSON TRANSACTION
        issuePersonTransaction = new IssuePersonRecord(maker, null, false, person, FEE_POWER, timestamp);

    }

    @SneakyThrows
    @Test
    public void databaseFork() {

        for (int dbs : TESTED_DBS) {

            init(dbs);

            issuePersonTransaction.sign(maker, Transaction.FOR_NETWORK);
            issuePersonTransaction.setDC(dcSet);
            issuePersonTransaction.process(gb, Transaction.FOR_NETWORK);

            issuePersonTransaction = new IssuePersonRecord(maker, null, false, person, FEE_POWER, timestamp++);
            issuePersonTransaction.sign(maker, Transaction.FOR_NETWORK);
            issuePersonTransaction.setDC(dcSet);
            issuePersonTransaction.process(gb, Transaction.FOR_NETWORK);

            issuePersonTransaction = new IssuePersonRecord(maker, null, false, person, FEE_POWER, timestamp++);
            issuePersonTransaction.sign(maker, Transaction.FOR_NETWORK);
            issuePersonTransaction.setDC(dcSet);
            issuePersonTransaction.process(gb, Transaction.FOR_NETWORK);


            //assertEquals(dcSet.getItemPersonMap().keySet().toString(), "");
            //assertEquals(dcSet.getItemPersonMap().getValuesAll().toString(), "");
            //CREATE FORK
            DCSet fork = dcSet.fork(this.toString());

            issuePersonTransaction = new IssuePersonRecord(maker, null, false, person, FEE_POWER, timestamp++);
            issuePersonTransaction.sign(maker, Transaction.FOR_NETWORK);
            issuePersonTransaction.setDC(dcSet);
            issuePersonTransaction.process(gb, Transaction.FOR_NETWORK);

            issuePersonTransaction = new IssuePersonRecord(maker, null, false, person, FEE_POWER, timestamp++);
            issuePersonTransaction.sign(maker, Transaction.FOR_NETWORK);
            issuePersonTransaction.setDC(dcSet);
            issuePersonTransaction.process(gb, Transaction.FOR_NETWORK);

            //assertEquals(PersonCls.getItem(fork, ItemCls.PERSON_TYPE, 1).getDBMap(fork).keySet().toString(), "");

            //SET BALANCE
            dcSet.getAssetBalanceMap().put(seed, 1L, new Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>>
                    (new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ONE, BigDecimal.ONE),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ONE, BigDecimal.ONE),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ONE, BigDecimal.ONE),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ONE, BigDecimal.ONE),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ONE, BigDecimal.ONE)
                    ));

            //CHECK VALUE IN DB
            assertEquals(BigDecimal.ONE, dcSet.getAssetBalanceMap().get(seed, 1L).a.b);

            //CHECK VALUE IN FORK
            assertEquals(BigDecimal.ONE, fork.getAssetBalanceMap().get(seed, 1L).a.b);

            //SET BALANCE IN FORK
            fork.getAssetBalanceMap().put(seed, 1L, new Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>>
                    (
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.TEN, BigDecimal.TEN),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.TEN, BigDecimal.TEN),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.TEN, BigDecimal.TEN),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.TEN, BigDecimal.TEN),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.TEN, BigDecimal.TEN)
                    ));

            //CHECK VALUE IN DB
            assertEquals(BigDecimal.ONE, dcSet.getAssetBalanceMap().get(seed, 1L).a.b);

            //CHECK VALUE IN FORK
            assertEquals(BigDecimal.TEN, fork.getAssetBalanceMap().get(seed, 1L).a.b);

            //CREATE SECOND FORK
            DCSet fork2 = fork.fork(this.toString());

            //SET BALANCE IN FORK2
            fork2.getAssetBalanceMap().put(seed, 1L, new Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>>
                    (
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ZERO, BigDecimal.ZERO),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ZERO, BigDecimal.ZERO),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ZERO, BigDecimal.ZERO),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ZERO, BigDecimal.ZERO),
                            new Tuple2<BigDecimal, BigDecimal>(BigDecimal.ZERO, BigDecimal.ZERO)
                    ));

            //CHECK VALUE IN DB
            assertEquals(BigDecimal.ONE, dcSet.getAssetBalanceMap().get(seed, 1L).a.b);

            //CHECK VALUE IN FORK
            assertEquals(BigDecimal.TEN, fork.getAssetBalanceMap().get(seed, 1L).a.b);

            //CHECK VALUE IN FORK
            assertEquals(BigDecimal.ZERO, fork2.getAssetBalanceMap().get(seed, 1L).a.b);

            dcSet.close();

        }
    }

    @Test
    public void databaseAssets() {

        for (int dbs : TESTED_DBS) {

            init(dbs);

            ItemAssetMap dbMap = dcSet.getItemAssetMap();
            Collection<ItemCls> assets = dbMap.values();
            for (ItemCls asset : assets) {
                AssetCls aa = (AssetCls) asset;
                LOGGER.info("ASSET - " + asset.getKey() + " : " + asset.getName()
                        + " : " + aa.getQuantity()
                        + " - " + aa.getReference().length
                        + ": " + aa.getReference());
                //db.add(asset);
            }

            dbMap.incrementPut(dbMap.get(1L));
            LOGGER.info("keys " + dbMap.keySet());

            dcSet.close();

        }
    }

    @SneakyThrows
    @Test
    public void databaseAssetsAddGet() {

        for (int dbs : TESTED_DBS) {

            init(dbs);

            AssetCls asset = new AssetVenture(0, maker, "test", icon, image, "strontje", 8, 50000L);
            Transaction issueAssetTransaction = new IssueAssetTransaction(maker, null, asset, FEE_POWER, timestamp);
            issueAssetTransaction.sign(maker, Transaction.FOR_NETWORK);
            issueAssetTransaction.setDC(dcSet);
            issueAssetTransaction.process(gb, Transaction.FOR_NETWORK);

            ItemAssetMap assetDB = dcSet.getItemAssetMap();
            Collection<ItemCls> assets = assetDB.values();
            for (ItemCls asset_2 : assets) {
                AssetCls aa = (AssetCls) asset_2;
                LOGGER.info(aa.toString() + " getQuantity " + aa.getQuantity());
            }

            dcSet.close();

        }
    }
}
