package org.erachain.core.item.assets;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.transaction.IssueAssetTransaction;
import org.erachain.core.transaction.Transaction;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

public class AssetUniqueSeriesCopyTest {

    private final byte[] icon = "qwe qweiou sdjk fh".getBytes(StandardCharsets.UTF_8);
    private final byte[] image = "qwe qweias d;alksd;als dajd lkasj dlou sdjk fh".getBytes(StandardCharsets.UTF_8);
    int forDeal = Transaction.FOR_NETWORK;
    long dbRef = 1111L;
    byte feePow = 0;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("tes213sdffsdft".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);

    @Test
    public void parse() {

        boolean iconAsURL = true;
        int iconType = AssetCls.MEDIA_TYPE_VIDEO;
        boolean imageAsURL = true;
        int imageType = AssetCls.MEDIA_TYPE_AUDIO;
        Long startDate = System.currentTimeMillis();
        Long stopDate = null;
        AssetUnique origAsset = new AssetUnique(AssetCls.AS_INSIDE_ASSETS, maker, "String name", null, null, "String description");

        IssueAssetTransaction issueTX = new IssueAssetTransaction(maker, origAsset, feePow, System.currentTimeMillis());
        issueTX.sign(maker, Transaction.FOR_NETWORK);

        AssetVenture assetVenture = new AssetVenture(AssetCls.AS_INSIDE_ASSETS, maker, "copy", icon, image, "...", 8, 10L);


        AssetUniqueSeriesCopy assetCopy = AssetUniqueSeriesCopy.makeCopy(issueTX, assetVenture, 1001, 10, 2);

        //CONVERT TO BYTES
        byte[] rawAsset = assetCopy.toBytes(false);

        //CHECK DATA LENGTH - 157
        assertEquals(rawAsset.length, assetCopy.getDataLength(false));

        try {
            //PARSE FROM BYTES
            AssetCls parsedAsset = AssetFactory.getInstance().parse(0, rawAsset, false);

            //CHECK INSTANCE
            assertTrue(parsedAsset instanceof AssetUniqueSeriesCopy);

            //CHECK ISSUER
            assertEquals(assetCopy.getAuthor().getAddress(), parsedAsset.getAuthor().getAddress());

            assertEquals(assetCopy.getName(), parsedAsset.getName());

            assertEquals(assetCopy.getDescription(), parsedAsset.getDescription());

            assertEquals(assetCopy.getQuantity(), parsedAsset.getQuantity());


        } catch (Exception e) {
            fail("Exception while parsing transaction.");
        }

    }
}