package org.erachain.core.item.assets;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.controller.Controller;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.exdata.exLink.ExLinkAddress;
import org.erachain.core.transaction.Transaction;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

public class AssetVentureTest {

    private final byte[] icon = "qwe qweiou sdjk fh".getBytes(StandardCharsets.UTF_8);
    private final byte[] image = "qwe qweias d;alksd;als dajd lkasj dlou sdjk fh".getBytes(StandardCharsets.UTF_8);
    int forDeal = Transaction.FOR_NETWORK;
    long dbRef = 1111L;
    Controller cntrl;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("tes213sdffsdft".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);

    @Test
    public void parse() {

        boolean iconAsURL = true;
        int iconType = AssetCls.MEDIA_TYPE_VIDEO;
        boolean imageAsURL = true;
        int imageType = AssetCls.MEDIA_TYPE_AUDIO;
        Long startDate = System.currentTimeMillis();
        Long stopDate = null;
        ExLinkAddress[] awards = null;

        AssetVenture assetVenture = new AssetVenture(AssetCls.AS_OUTSIDE_GOODS, maker, maker, "movable", "tags",
                iconType, iconAsURL, icon, imageType, imageAsURL, image,
                "...", startDate, stopDate,
                awards, false, false,
                8, 10L);

        //CONVERT TO BYTES
        byte[] rawAsset = assetVenture.toBytes(false);

        //CHECK DATA LENGTH - 157
        assertEquals(rawAsset.length, assetVenture.getDataLength(false));

        AssetCls parsedAsset;
        try {
            //PARSE FROM BYTES
            parsedAsset = AssetFactory.getInstance().parse(0, rawAsset, false);

        } catch (Exception e) {
            fail("Exception while parsing transaction. " + e.getMessage());
            return;
        }

        //CHECK INSTANCE
        assertTrue(parsedAsset instanceof AssetVenture);

        //CHECK ISSUER
        assertEquals(assetVenture.getAuthor().getAddress(), parsedAsset.getAuthor().getAddress());

        assertEquals(assetVenture.getDescription(), parsedAsset.getDescription());

        assertEquals(assetVenture.getQuantity(), parsedAsset.getQuantity());

        assertEquals(assetVenture.isAnonimDenied(), parsedAsset.isAnonimDenied());


    }

    @Test
    public void toBytes() {
    }

    @Test
    public void getDataLength() {
    }
}