package org.erachain.core.block;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.BlockGenerator;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.crypto.Base58;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.transaction.GenesisTransferAssetTransaction;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TransactionFactory;
import org.erachain.core.wallet.Wallet;
import org.erachain.database.IDB;
import org.erachain.datachain.DCSet;
import org.erachain.settings.Settings;
import org.junit.Test;
import org.mapdb.Fun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class GenesisBlockTest {
    static Logger LOGGER = LoggerFactory.getLogger(GenesisBlockTest.class.getName());
    long ERM_KEY = Transaction.RIGHTS_KEY;
    long FEE_KEY = Transaction.FEE_KEY;
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount generator = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    Account recipient = generator;
    Account recipient2 = new PublicKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, "test1"));

    Transaction payment;
    //CREATE EMPTY MEMORY DATABASE
    List<Transaction> gbTransactions;
    private Controller cntrl;
    private DCSet db;
    private BlockChain blockChain;
    private GenesisBlock gb;
    private BlockGenerator blockGenerator;

    private void init() {

        Settings.CHECK_BUGS = 7;

        db = DCSet.createEmptyDatabaseSet(0);
        cntrl = Controller.getInstance();
        cntrl.initBlockChain(db);
        blockChain = cntrl.getBlockChain();
        gb = blockChain.getGenesisBlock();

    }

    @SneakyThrows
    @Test
    public void parseGenesisBlock() {

        gb = new GenesisBlock();

        assertEquals(gb.getDataLength(), 0);
        //CONVERT TO BYTES
        byte[] rawBlock = gb.toBytes();
        //CHECK
        assertEquals(rawBlock.length, 0);

        Block parsedBlock = null;
        try {
            //PARSE FROM BYTES
            parsedBlock = new Block(rawBlock, true);
            fail("Exception must be here");
        } catch (Exception e) {
            // all good
        }

        for (Transaction tx : gb.transactions) {
            byte[] bytes = tx.toBytes(Transaction.FOR_NETWORK);
            assertEquals(bytes.length, tx.getDataLength(Transaction.FOR_NETWORK));
            Transaction txParse = TransactionFactory.getInstance().parse(bytes, Transaction.FOR_NETWORK);
            assertArrayEquals(tx.getSignature(), txParse.getSignature());
            assertTrue(txParse.isSignatureValid());
        }

        for (Transaction tx : gb.transactions) {
            byte[] bytes = tx.toBytes(Transaction.FOR_DB_RECORD);
            assertEquals(bytes.length, tx.getDataLength(Transaction.FOR_DB_RECORD));
            Transaction txParse = TransactionFactory.getInstance().parse(bytes, Transaction.FOR_DB_RECORD);
            assertArrayEquals(tx.getSignature(), txParse.getSignature());
            assertTrue(txParse.isSignatureValid());
        }

    }

    @SneakyThrows
    @Test
    public void validateSignatureGenesisBlock() {

        Settings.CHECK_BUGS = 10;

        db = DCSet.createEmptyDatabaseSet(0);
        gb = new GenesisBlock();

        //CHECK IF SIGNATURE VALID
        LOGGER.info("getGeneratorSignature[" + gb.getSignature().length
                + "] : " + Base58.encode(gb.getSignature()));

        assertTrue(gb.isSignatureValid());
        assertEquals(0, gb.isValid(db, false));

        //ADD a GENESIS TRANSACTION for invalid SIGNATURE
        List<Transaction> transactions = gb.getTransactions();
        transactions.add(new GenesisTransferAssetTransaction(
                Account.makeFromOld("7R2WUFaS7DF2As6NKz13Pgn9ij4sFw6ymZ"), 1L, BigDecimal.valueOf(1), null, null, null));
        gb.setTransactionsForTests(transactions);

        // SIGNATURE invalid
        assertFalse(gb.isSignatureValid());

    }

    @SneakyThrows
    @Test
    public void validateAfterProcess() {

        Settings.CHECK_BUGS = 10;

        db = DCSet.createEmptyDatabaseSet(0);
        gb = new GenesisBlock();

        LOGGER.info("getGeneratorSignature[" + gb.getSignature().length
                + "] : " + Base58.encode(gb.getSignature()));

        assertTrue(gb.isSignatureValid());
        assertEquals(Block.INVALID_NONE, gb.isValid(db, false));

        gb.process(db, true);

        // invalid
        assertEquals(Block.INVALID_BLOCK_VERSION, gb.isValid(db, false));

    }

    @SneakyThrows
    @Test
    public void validateGenesisBlock() {

        db = DCSet.createEmptyDatabaseSet(IDB.DBS_MAP_DB);
        gb = new GenesisBlock();

        //CHECK IF VALID
        assertEquals(Block.INVALID_NONE, gb.isValid(db, false));

        //ADD INVALID GENESIS TRANSACTION
        List<Transaction> transactions = gb.getTransactions();
        transactions.add(new GenesisTransferAssetTransaction(
                Account.makeFromOld("7R2WUFaS7DF2As6NKz13Pgn9ij4sFw6ymZ"), 1L, BigDecimal.valueOf(-1000), null, null, null));
        gb.setTransactionsForTests(transactions);

        //CHECK IF INVALID
        assertEquals(Block.INVALID_BLOCK_VERSION, gb.isValid(db, false));

        //CREATE NEW BLOCK
        gb = new GenesisBlock();

        //CHECK IF VALID
        assertEquals(Block.INVALID_NONE, gb.isValid(db, false));

        //PROCESS
        try {
            gb.process(db, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //CHECK IF INVALID
        // AFTER processing ONE - it must be INVALID
        assertEquals(Block.INVALID_BLOCK_VERSION, gb.isValid(db, false));
    }

    @SneakyThrows
    @Test
    public void processGenesisBlock() {

        //init();
        db = DCSet.createEmptyDatabaseSet(IDB.DBS_MAP_DB);
        gb = new GenesisBlock();

        //CHECK VALID
        assertTrue(gb.isSignatureValid());
        assertEquals(Block.INVALID_NONE, gb.isValid(db, false));

        //PROCESS BLOCK
        try {
            gb.process(db, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Account recipient1 = Account.makeFromOld("73CcZe3PhwvqMvWxDznLAzZBrkeTZHvNzo");
        Account recipient2 = Account.makeFromOld("7FUUEjDSo9J4CYon4tsokMCPmfP4YggPnd");

        //CHECK LAST REFERENCE GENERATOR
        assertEquals(recipient1.getLastTimestamp(db)[0], 0);
        assertEquals(recipient2.getLastTimestamp(db)[0], 0);

        //CHECK BALANCE RECIPIENT 1
        assertEquals(0, recipient1.getBalanceUSE(ERM_KEY, db).compareTo(BigDecimal.valueOf(0)));
        assertEquals(0, recipient1.getBalanceUSE(FEE_KEY, db).compareTo(BigDecimal.valueOf(0.0)));

        //CHECK BALANCE RECIPIENT2
        assertEquals(0, recipient2.getBalanceUSE(ERM_KEY, db).compareTo(BigDecimal.valueOf(0)));
        assertEquals(0, recipient2.getBalanceUSE(FEE_KEY, db).compareTo(BigDecimal.valueOf(0.0)));

        int height = gb.getHeight() + 1;
        Fun.Tuple3<Integer, Integer, Integer> forgingData = recipient1.getForgingData(db, height);
        assertEquals(-1, (int) forgingData.a);

        forgingData = recipient2.getForgingData(db, height);
        assertEquals(-1, (int) forgingData.a);

        //ORPHAN BLOCK
        try {
            gb.orphan(db);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        assertEquals(0L, recipient1.getLastTimestamp(db)[0]);
        assertEquals(0L, recipient2.getLastTimestamp(db)[0]);

        //CHECK BALANCE RECIPIENT 1
        assertEquals(recipient1.getBalanceUSE(ERM_KEY, db), BigDecimal.valueOf(0));
        assertEquals(recipient1.getBalanceUSE(FEE_KEY, db), BigDecimal.valueOf(0));
        //CHECK BALANCE RECIPIENT 2
        assertEquals(0, recipient2.getBalanceUSE(ERM_KEY, db).compareTo(BigDecimal.valueOf(0)));
        assertEquals(0, recipient2.getBalanceUSE(FEE_KEY, db).compareTo(BigDecimal.valueOf(0)));

    }
}
