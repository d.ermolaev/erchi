package org.erachain.core.block;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.BlockGenerator;
import org.erachain.core.account.Account;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.transaction.*;
import org.erachain.datachain.DCSet;
import org.erachain.ntp.NTP;
import org.junit.Ignore;
import org.junit.Test;
import org.mapdb.Fun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BlockTests {
    static Logger LOGGER = LoggerFactory.getLogger(BlockTests.class.getName());
    long ERM_KEY = Transaction.RIGHTS_KEY;
    long FEE_KEY = Transaction.FEE_KEY;
    byte FEE_POWER = (byte) 0;
    byte[] assetReference = new byte[Crypto.SIGNATURE_LENGTH];
    long timestamp = NTP.getTime();
    long flags = 0L;
    boolean forDB = true;
    List<Transaction> transactions = new ArrayList<Transaction>();
    Fun.Tuple2<List<Transaction>, Integer> orderedTransactions = new Fun.Tuple2<>(new ArrayList<Transaction>(), 0);
    byte[] transactionsHash = new byte[Crypto.HASH_LENGTH];
    byte[] atBytes = new byte[0];
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount generator = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    Account recipient = Account.makeFromOld("7F9cZPE1hbzMT21g96U8E1EfMimovJyyJ7");
    Transaction payment;
    //CREATE EMPTY MEMORY DATABASE
    List<Transaction> gbTransactions;
    private Controller cntrl;
    private DCSet db;
    private BlockChain blockChain;
    private GenesisBlock gb;
    private BlockGenerator blockGenerator;

    @SneakyThrows
    private void init() {

        db = DCSet.createEmptyDatabaseSet(0);
        cntrl = Controller.getInstance();
        cntrl.initBlockChain(db);
        blockChain = cntrl.getBlockChain();
        gb = blockChain.getGenesisBlock();
        //gb.process(db);
        try {
            //blockChain = new BlockChain(db);
        } catch (Exception e) {
        }

        blockGenerator = new BlockGenerator(db, blockChain, false);
        //gb = blockChain.getGenesisBlock();
        gbTransactions = gb.getTransactions();

        generator.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, db);
        generator.changeBalance(db, false, Account.BALANCE_POS_OWN, ERM_KEY, BigDecimal.valueOf(1000), false, false);
        generator.changeBalance(db, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1000), false, false); // need for payments
    }

    private void initTrans(List<Transaction> transactions, long timestamp) {
        payment = new TransactionAmount(generator, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100), timestamp);
        payment.sign(generator, Transaction.FOR_NETWORK);
        transactions.add(payment);

    }


    @Test
    public void parseBlock() {

        init();

        try {
            gb.process(db, false);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        //CREATE KNOWN ACCOUNT
        byte[] seed = Crypto.getInstance().digest("test".getBytes());
        byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
        PrivateKeyAccount generator = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);

        //PROCESS GENESIS TRANSACTION TO MAKE SURE GENERATOR HAS FUNDS
        //Transaction transaction = new GenesisTransaction(generator, BigDecimal.valueOf(1000), NTP.getTime());
        //transaction.process(databaseSet, false);
        generator.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, db);
        generator.changeBalance(db, false, Account.BALANCE_POS_OWN, ERM_KEY, BigDecimal.valueOf(1000), false, false);
        generator.changeBalance(db, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1000), false, false);


        //GENERATE NEXT BLOCK
        Block block = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);

        //////////// EMPTY BLOCK
        //CONVERT TO BYTES
        byte[] rawBlock = block.toBytes();

        Block parsedBlock = null;
        try {
            //PARSE FROM BYTES
            parsedBlock = new Block(rawBlock, true);
        } catch (Exception e) {
            fail("Exception while parsing transaction. " + e.getMessage());
        }

        //CHECK INSTANCE
        assertFalse(parsedBlock instanceof GenesisBlock);

        //CHECK SIGNATURE
        assertArrayEquals(block.getSignature(), parsedBlock.getSignature());

        //CHECK GENERATOR
        assertEquals(block.getCreator().getAddress(), parsedBlock.getCreator().getAddress());

        //CHECK REFERENCE
        assertArrayEquals(block.getReference(), parsedBlock.getReference());

        //CHECK TRANSACTIONS COUNT
        assertEquals(block.getTransactionCount(), parsedBlock.getTransactionCount());

        //CHECK REFERENCE
        assertArrayEquals(block.getTransactionsHash(), parsedBlock.getTransactionsHash());

        //PARSE TRANSACTION FROM WRONG BYTES
        rawBlock = new byte[50];

        try {
            //PARSE FROM BYTES
            new Block(rawBlock, true);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }

        /////////////////////////////
        //FORK
        DCSet fork = db.fork(this.toString());

        //GENERATE PAYMENT 1
        Account recipient = Account.makeFromOld("7F9cZPE1hbzMT21g96U8E1EfMimovJyyJ7");
        long timestamp = block.getTimestamp();
        Transaction payment1 = new TransactionAmount(generator, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100), timestamp);
        payment1.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        payment1.sign(generator, Transaction.FOR_NETWORK);
        try {
            assertEquals(Transaction.VALIDATE_OK, payment1.isValid(Transaction.FOR_NETWORK, flags));
        } catch (org.erachain.core.transaction.TxException e) {
            throw new RuntimeException(e);
        }

        //payment1.process(fork);
        transactions = new ArrayList<Transaction>();
        transactions.add(payment1);


        //GENERATE PAYMENT 2
        Account recipient2 = Account.makeFromOld("7AfGz1FJ6tUnxxKSAHfcjroFEm8jSyVm7r");
        Transaction payment2 = new TransactionAmount(generator, FEE_POWER, recipient2, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100), timestamp);
        payment2.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        payment2.sign(generator, Transaction.FOR_NETWORK);
        try {
            assertEquals(Transaction.VALIDATE_OK, payment2.isValid(Transaction.FOR_NETWORK, flags));
        } catch (org.erachain.core.transaction.TxException e) {
            throw new RuntimeException(e);
        }

        transactions.add(payment2);

        //ADD TRANSACTION SIGNATURE
        block.setTransactionsForTests(transactions);
        block.sign(generator);
        assertTrue(block.isSignatureValid());

        //CONVERT TO BYTES
        rawBlock = block.toBytes();

        parsedBlock = null;
        try {
            //PARSE FROM BYTES
            parsedBlock = new Block(rawBlock, true);
        } catch (Exception e) {
            fail("Exception while parsing transaction. " + e.getMessage());
        }

        //CHECK INSTANCE
        assertFalse(parsedBlock instanceof GenesisBlock);

        //CHECK SIGNATURE
        assertArrayEquals(block.getSignature(), parsedBlock.getSignature());

        //CHECK GENERATOR
        assertEquals(block.getCreator().getAddress(), parsedBlock.getCreator().getAddress());

        //CHECK REFERENCE
        assertArrayEquals(block.getReference(), parsedBlock.getReference());

        //CHECK TRANSACTIONS COUNT
        assertEquals(block.getTransactionCount(), parsedBlock.getTransactionCount());

        //CHECK REFERENCE
        assertArrayEquals(block.getTransactionsHash(), parsedBlock.getTransactionsHash());

        //PARSE TRANSACTION FROM WRONG BYTES
        rawBlock = new byte[50];

        try {
            //PARSE FROM BYTES
            new Block(rawBlock, true);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }
    }

    ////////////////
    @Test
    public void validateSignatureBlock() {

        init();
        try {
            gb.process(db, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        //PROCESS GENESIS TRANSACTION TO MAKE SURE GENERATOR HAS FUNDS
        //Transaction transaction = new GenesisTransaction(generator, BigDecimal.valueOf(1000), NTP.getTime());
        //transaction.process(databaseSet, false);

        //GENERATE NEXT BLOCK
        //BigDecimal genBal = generator.getGeneratingBalance(db);
        BlockGenerator blockGenerator = new BlockGenerator(db, null, false);
        Block newBlock = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);
        newBlock.sign(generator);

        ////ADD TRANSACTION SIGNATURE
        ///newBlock.makeTransactionsHash();

        //CHECK IF SIGNATURE VALID
        assertTrue(newBlock.isSignatureValid());

        //INVALID TRANSACTION HASH
        Transaction payment = new TransactionAmount(generator, FEE_POWER, generator, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1), timestamp);
        payment.sign(generator, Transaction.FOR_NETWORK);
        transactions.add(payment);

        // SET TRANSACTIONS to BLOCK
        newBlock.setTransactionsForTests(transactions);

        //CHECK IF SIGNATURE INVALID
        assertFalse(newBlock.isSignatureValid());

        //INVALID GENERATOR SIGNATURE
        //newBlock = BlockFactory..create(newBlock.getVersion(), newBlock.getReference(), generator, new byte[Crypto.HASH_LENGTH], new byte[0]);
        newBlock = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);
        newBlock.sign(generator);
        newBlock.setTransactionsForTests(transactions);

        ///CHECK IF SIGNATURE INVALID
        assertFalse(newBlock.isSignatureValid());

        //VALID TRANSACTION SIGNATURE
        newBlock = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);

        //ADD TRANSACTION
        Account recipient = Account.makeFromOld("7F9cZPE1hbzMT21g96U8E1EfMimovJyyJ7");
        long timestamp = newBlock.getTimestamp();
        payment = new TransactionAmount(generator, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100), timestamp);
        payment.sign(generator, Transaction.FOR_NETWORK);

        transactions = new ArrayList<Transaction>();
        transactions.add(payment);

        //ADD TRANSACTION SIGNATURE
        newBlock.setTransactionsForTests(transactions);

        //CHECK VALID TRANSACTION SIGNATURE
        assertFalse(newBlock.isSignatureValid());

        newBlock.sign(generator);
        //CHECK VALID TRANSACTION SIGNATURE
        assertTrue(newBlock.isSignatureValid());

        //INVALID TRANSACTION SIGNATURE
        newBlock = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);

        //ADD TRANSACTION
        payment = new TransactionAmount(generator, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(200), NTP.getTime(), payment.getSignature());
        transactions = new ArrayList<Transaction>();
        transactions.add(payment);

        //ADD TRANSACTION SIGNATURE
        newBlock.setTransactionsForTests(transactions);
        newBlock.sign(generator);

        //CHECK INVALID TRANSACTION SIGNATURE
        try {
            assertEquals(Block.INVALID_BLOCK_WIN, newBlock.isValid(db, false));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        // BUT valid HERE
        assertTrue(newBlock.isSignatureValid());
    }

    @Test
    public void validateBlock() {
        init();

        //CREATE KNOWN ACCOUNT
        byte[] seed = Crypto.getInstance().digest("test".getBytes());
        byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
        PrivateKeyAccount generator = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);

        Transaction transaction;

        //PROCESS GENESIS TRANSACTION TO MAKE SURE GENERATOR HAS FUNDS
		/*
		transaction = new GenesisTransaction(generator, BigDecimal.valueOf(1000), NTP.getTime());
		transaction.process(databaseSet);
		 */

        // (issuer, recipient, 0l, bdAmount, timestamp)
        // need add VOLUME for generating new block - 0l asset!
        transaction = new GenesisTransferAssetTransaction(generator,
                ERM_KEY, BigDecimal.valueOf(100000), null, null, null);
        transaction.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            transaction.process(gb, Transaction.FOR_NETWORK);
        } catch (TxException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
        transaction = new GenesisTransferAssetTransaction(generator,
                FEE_KEY, BigDecimal.valueOf(1000), null, null, null);
        transaction.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            transaction.process(gb, Transaction.FOR_NETWORK);
        } catch (TxException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        //GENERATE NEXT BLOCK
        //BigDecimal genBal = generator.getGeneratingBalance(db);
        BlockGenerator blockGenerator = new BlockGenerator(db, null, false);
        Block newBlock = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);

        // SET WIN VALUE and TARGET
        newBlock.makeHeadMind(db);

        //CHECK IF VALID
        try {
            assertEquals(true, newBlock.isValid(db, false));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        //CHANGE REFERENCE
        ////Block invalidBlock = BlockFactory..create(newBlock.getVersion(), new byte[128], newBlock.getCreator(), transactionsHash, atBytes);
        Block invalidBlock = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);

        invalidBlock.setReferenceForTests(new byte[Block.SIGNATURE_LENGTH]);
        invalidBlock.sign(generator);

        //CHECK IF INVALID
        try {
            assertEquals(false, invalidBlock.isValid(db, false));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        //VRON NUMBER
        invalidBlock = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);
        //CHECK IF INVALID
        try {
            assertEquals(false, invalidBlock.isValid(db, false));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        //ADD INVALID TRANSACTION
        invalidBlock = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);
        Account recipient = Account.makeFromOld("7F9cZPE1hbzMT21g96U8E1EfMimovJyyJ7");
        long timestamp = newBlock.getTimestamp();
        Transaction payment = new TransactionAmount(generator, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(-100), timestamp);
        payment.sign(generator, Transaction.FOR_NETWORK);

        transactions = new ArrayList<Transaction>();
        transactions.add(payment);

        //ADD TRANSACTION SIGNATURE
        invalidBlock.setTransactionsForTests(transactions);
        invalidBlock.sign(generator);

        //CHECK IF INVALID
        try {
            assertEquals(false, invalidBlock.isValid(db, false));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        //ADD GENESIS TRANSACTION
        invalidBlock = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);

        //transaction = new GenesisTransaction(generator, BigDecimal.valueOf(1000), newBlock.getTimestamp());
        transaction = new GenesisIssueAssetTransaction(GenesisBlock.makeAsset(Transaction.RIGHTS_KEY), null, null, null);
        transactions.add(transaction);

        //ADD TRANSACTION SIGNATURE
        invalidBlock.setTransactionsForTests(transactions);
        invalidBlock.sign(generator);

        //CHECK IF INVALID
        try {
            assertEquals(false, invalidBlock.isValid(db, false));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }


    @Ignore
    //TODO actualize the test
    @Test
    public void processBlock() {

        init();
        // already processed gb.process(db);

        //CREATE KNOWN ACCOUNT
        byte[] seed = Crypto.getInstance().digest("test".getBytes());
        byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
        PrivateKeyAccount generator = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);

        //PROCESS GENESIS TRANSACTION TO MAKE SURE GENERATOR HAS FUNDS for generate
        //Transaction transaction = new GenesisTransaction(generator, BigDecimal.valueOf(1000), NTP.getTime());
        //transaction.process(databaseSet, false);
        generator.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, db);
        generator.changeBalance(db, false, Account.BALANCE_POS_OWN, ERM_KEY, BigDecimal.valueOf(100000), false, false);
        generator.changeBalance(db, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1000), false, false);

        //GENERATE NEXT BLOCK
        Block block = blockGenerator.generateNextBlock(generator, gb,
                orderedTransactions,
                1000, 1000L, 1000L);

        //FORK
        DCSet fork = db.fork(this.toString());

        //GENERATE PAYMENT 1
        Account recipient1 = Account.makeFromOld("7JU8UTuREAJG2yht5ASn7o1Ur34P1nvTk5");
        // TIMESTAMP for org.erachain.records make lower
        long timestamp = block.getTimestamp() - 1000;
        Transaction payment1 = new TransactionAmount(generator, FEE_POWER, recipient1, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100), timestamp++);
        payment1.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        payment1.sign(generator, Transaction.FOR_NETWORK);
        try {
            assertEquals(Transaction.VALIDATE_OK, payment1.isValid(Transaction.FOR_NETWORK, flags));
        } catch (org.erachain.core.transaction.TxException e) {
            throw new RuntimeException(e);
        }

        try {
            payment1.process(block, Transaction.FOR_NETWORK);
        } catch (TxException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        transactions.add(payment1);

        //GENERATE PAYMENT 2
        Account recipient2 = Account.makeFromOld("7G1G45RX4td59daBv6PoN84nAJA49NZ47i");
        Transaction payment2 = new TransactionAmount(generator, FEE_POWER, recipient2, Account.BALANCE_POS_OWN, ERM_KEY,
                BigDecimal.valueOf(10), timestamp++);
        payment2.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        payment2.sign(generator, Transaction.FOR_NETWORK);
        try {
            assertEquals(Transaction.VALIDATE_OK, payment2.isValid(Transaction.FOR_NETWORK, flags));
        } catch (org.erachain.core.transaction.TxException e) {
            throw new RuntimeException(e);
        }

        transactions.add(payment2);

        //ADD TRANSACTION SIGNATURE
        block.setTransactionsForTests(transactions);

        ////generator.setLastForgingData(db, block.getHeightByParent(db));
        generator.setForgingData(db, block.getHeight(), payment2.getAmount().intValue());
        //block.setCalcGeneratingBalance(db);
        block.sign(generator);

        //CHECK VALID
        assertTrue(block.isSignatureValid());
        try {
            assertEquals(true, block.isValid(db, false));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        //PROCESS BLOCK
        try {
            block.process(db, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //CHECK BALANCE GENERATOR
        assertEquals(generator.getBalanceUSE(ERM_KEY, db), BigDecimal.valueOf(100990));
        //assertEquals(generator.getBalanceUSE(FEE_KEY, db), BigDecimal.valueOf(900.00009482));
        assertEquals(generator.getBalanceUSE(FEE_KEY, db), BigDecimal.valueOf(1900.0000));

        //CHECK LAST REFERENCE GENERATOR
        assertEquals(generator.getLastTimestamp(db)[0], (long) payment2.getTimestamp());

        //CHECK BALANCE RECIPIENT 1
        assertEquals(recipient1.getBalanceUSE(ERM_KEY, db), BigDecimal.valueOf(0));
        assertEquals(recipient1.getBalanceUSE(FEE_KEY, db), BigDecimal.valueOf(100));

        //CHECK LAST REFERENCE RECIPIENT 1
        //assertEquals((long)recipient1.getLastReference(db), (long)payment1.getTimestamp());
        assertEquals(recipient1.getLastTimestamp(db)[0], 0L);

        //CHECK BALANCE RECIPIENT2
        assertEquals(recipient2.getBalanceUSE(ERM_KEY, db), BigDecimal.valueOf(10));
        assertEquals(recipient2.getBalanceUSE(FEE_KEY, db), BigDecimal.valueOf(0));

        //CHECK LAST REFERENCE RECIPIENT 2
        assertNotEquals(recipient2.getLastTimestamp(db)[0], (long) payment2.getTimestamp());

        //CHECK TOTAL FEE
        assertEquals(block.blockHead.totalFee, BigDecimal.valueOf(0.00065536));

        //CHECK TOTAL TRANSACTIONS
        assertEquals(2, block.getTransactionCount());

        //CHECK LAST BLOCK
        assertArrayEquals(block.getSignature(), db.getBlockMap().last().getSignature());


        ////////////////////////////////////
        //ORPHAN BLOCK
        //////////////////////////////////
        try {
            block.orphan(db);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //CHECK BALANCE GENERATOR
        assertEquals(generator.getBalanceUSE(FEE_KEY, db), BigDecimal.valueOf(2000));

        //CHECK LAST REFERENCE GENERATOR
        assertEquals(generator.getLastTimestamp(db)[0], gb.getTimestamp());

        //CHECK BALANCE RECIPIENT 1
        assertEquals(recipient1.getBalanceUSE(FEE_KEY, db), BigDecimal.valueOf(0));

        //CHECK LAST REFERENCE RECIPIENT 1
        assertNotEquals(recipient1.getLastTimestamp(db), payment1.getTimestamp());

        //CHECK BALANCE RECIPIENT 2
        assertEquals(0, recipient2.getBalanceUSE(FEE_KEY, db).compareTo(BigDecimal.valueOf(0)));

        //CHECK LAST REFERENCE RECIPIENT 2
        assertEquals(recipient2.getLastTimestamp(db)[0], 0);

        //CHECK LAST BLOCK
        assertArrayEquals(gb.getSignature(), db.getBlockMap().last().getSignature());
    }
}
