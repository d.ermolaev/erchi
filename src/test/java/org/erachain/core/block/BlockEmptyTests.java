package org.erachain.core.block;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.wallet.Wallet;
import org.erachain.database.IDB;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.TransactionFinalMapImpl;
import org.erachain.dbs.IteratorCloseable;
import org.erachain.ntp.NTP;
import org.erachain.settings.Settings;
import org.junit.Test;
import org.mapdb.Fun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BlockEmptyTests {
    static Logger LOGGER = LoggerFactory.getLogger(BlockEmptyTests.class.getName());
    long ERM_KEY = Transaction.RIGHTS_KEY;
    long FEE_KEY = Transaction.FEE_KEY;
    byte FEE_POWER = (byte) 0;
    byte[] assetReference = new byte[Crypto.SIGNATURE_LENGTH];
    long timestamp = NTP.getTime();
    long flags = 0L;
    boolean forDB = true;
    Fun.Tuple2<List<Transaction>, Integer> orderedTransactions = new Fun.Tuple2<>(new ArrayList<Transaction>(), 0);
    byte[] transactionsHash = new byte[Crypto.HASH_LENGTH];
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount generator = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    Account recipient = Account.makeFromOld("7F9cZPE1hbzMT21g96U8E1EfMimovJyyJ7");
    //CREATE EMPTY MEMORY DATABASE
    private Controller cntrl;
    private DCSet db;
    private BlockChain blockChain;
    private GenesisBlock gb;

    @SneakyThrows
    private void init() {

        db = DCSet.createEmptyDatabaseSet(IDB.DBS_MAP_DB);
        cntrl = Controller.getInstance();
        cntrl.initBlockChain(db);
        blockChain = cntrl.getBlockChain();
        gb = blockChain.getGenesisBlock();

        generator.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, db);
        generator.changeBalance(db, false, ERM_KEY, BigDecimal.valueOf(1000), false, false);
        generator.changeBalance(db, false, FEE_KEY, BigDecimal.valueOf(1000), false, false); // need for payments
    }

    @Test
    public void parseHeadBlock() {

        //GENERATE NEW HEAD
        Block.BlockHead head = new Block.BlockHead(1, new byte[Block.REFERENCE_LENGTH], generator, 100,
                new byte[Block.TRANSACTIONS_HASH_LENGTH], new byte[Block.SIGNATURE_LENGTH], 23, 10000,
                45000, 33000, 567554563654L, 3456, 12, 200);
        byte[] raw = head.toBytes();
        assertEquals(raw.length, Block.BlockHead.BASE_LENGTH);


        Block.BlockHead parsedHead = null;
        try {
            //PARSE FROM BYTES
            parsedHead = Block.BlockHead.parse(raw);
        } catch (Exception e) {
            fail("Exception while parsing transaction. " + e.getMessage());
        }

        //CHECK SIGNATURE
        assertArrayEquals(head.signature, parsedHead.signature);

        //CHECK GENERATOR
        assertEquals(head.creator.getAddress(), parsedHead.creator.getAddress());

        //CHECK HEIGHT
        assertEquals(head.heightBlock, parsedHead.heightBlock);

        //CHECK BASE TARGET
        assertEquals(head.forgingBalance, parsedHead.forgingBalance);

        //CHECK FEE
        assertEquals(head.totalFee, parsedHead.totalFee);
        assertEquals(head.emittedFee, parsedHead.emittedFee);
        assertEquals(head.size, parsedHead.size);

    }

    @Test
    public void parseEmptyBlock() {

        //CREATE KNOWN ACCOUNT
        byte[] seed = Crypto.getInstance().digest("test".getBytes());
        byte[] reference = Crypto.getInstance().createKeyPair(seed).getA();
        PrivateKeyAccount generator = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);

        //////////// EMPTY BLOCK
        Block block = new Block(1, reference, generator, 123,
                15000, 1000L, 1000L);
        block.sign(generator);

        //CONVERT TO BYTES
        byte[] rawBlock = block.toBytes();

        Block parsedBlock = null;
        try {
            //PARSE FROM BYTES
            parsedBlock = new Block(rawBlock, true);
        } catch (Exception e) {
            fail("Exception while parsing transaction. " + e.getMessage());
        }

        //CHECK INSTANCE
        assertFalse(parsedBlock instanceof GenesisBlock);

        //CHECK SIGNATURE
        assertArrayEquals(block.getSignature(), parsedBlock.getSignature());

        //CHECK GENERATOR
        assertEquals(block.getCreator().getAddress(), parsedBlock.getCreator().getAddress());

        //CHECK REFERENCE
        assertArrayEquals(block.getReference(), parsedBlock.getReference());

        //CHECK TRANSACTIONS COUNT
        assertEquals(block.getTransactionCount(), parsedBlock.getTransactionCount());

        //CHECK REFERENCE
        assertArrayEquals(block.getTransactionsHash(), parsedBlock.getTransactionsHash());

        //PARSE TRANSACTION FROM WRONG BYTES
        rawBlock = new byte[50];

        try {
            //PARSE FROM BYTES
            new Block(rawBlock, true);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }

    }

    ////////////////
    @Test
    public void validateSignatureEmptyBlock() {

        byte[] seed = Crypto.getInstance().digest("test".getBytes());
        byte[] reference = Crypto.getInstance().createKeyPair(seed).getA();
        PrivateKeyAccount generator = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);

        Block newBlock = new Block(1, reference, generator, 123,
                15000, 1000L, 1000L);
        newBlock.sign(generator);


        //CHECK IF SIGNATURE VALID
        assertTrue(newBlock.isSignatureValid());

        reference[3]++;

        //CHECK IF SIGNATURE INVALID
        assertFalse(newBlock.isSignatureValid());

    }

    @Test
    public void validateEmptyBlock() {

        init();

        Block newBlock = new Block(1, gb.getSignature(), generator, db.getBlockMap().size() + 1,
                15000, 1000L, 1000L);

        // SET WIN VALUE and TARGET
        newBlock.makeHeadMind(db);

        //CHECK IF VALID
        try {
            assertEquals(Block.INVALID_NONE, newBlock.isValid(db, false));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

    }

    @Test
    public void processEmptyBlock() {

        init();

        byte[] reference = Crypto.getInstance().createKeyPair(seed).getA();
        PrivateKeyAccount generator2 = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, 2));

        Block block = new Block(1, reference, generator2, 123,
                15000, 1000L, 1000L);

        block.sign(generator);
        //CHECK INVALID
        assertFalse(block.isSignatureValid());

        block.sign(generator2);
        //CHECK VALID
        assertTrue(block.isSignatureValid());

        //FORK
        DCSet fork = db.fork(this.toString());

        //GENERATE PAYMENT 1
        Account recipient1 = Account.makeFromOld("7JU8UTuREAJG2yht5ASn7o1Ur34P1nvTk5");
        // TIMESTAMP for org.erachain.records make lower
        long timestamp = block.getTimestamp() - 1000;

        int forgingVBalance = 100000;

        try {
            assertEquals(Block.INVALID_BRANCH, block.isValid(fork, false));

            // VALID REF BUT INVALID HEIGHT
            reference = fork.getBlockMap().getLastBlockSignature();
            block = new Block(1, reference, generator2, 123,
                    15000, 1000L, 1000L);

            block.sign(generator2);
            assertTrue(block.isSignatureValid());
            assertEquals(Block.INVALID_HEIGHT, block.isValid(fork, false));

            block = new Block(1, reference, generator2, 2,
                    1500, 1000L, 1000L);
            block.sign(generator2);
            assertTrue(block.isSignatureValid());
            assertEquals(Block.INVALID_BLOCK_WIN, block.isValid(fork, false));

            // ADD FORGING BALANCE
            generator2.changeBalance(fork, false, ERM_KEY, BigDecimal.valueOf(forgingVBalance), false, false);
            assertEquals(Block.INVALID_BLOCK_WIN, block.isValid(db, false));
            assertEquals(Block.INVALID_BLOCK_WIN, block.isValid(fork, false));

            // SET ACC LAST TIME
            //generator2.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, fork);
            generator2.setForgingData(fork, 1, forgingVBalance);

            assertEquals(Block.INVALID_NONE, block.isValid(fork, false));

        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        //PROCESS BLOCK
        try {
            block.process(fork, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //CHECK TOTAL TRANSACTIONS
        assertEquals(0, block.getTransactionCount());

        //CHECK BALANCE GENERATOR
        assertEquals(generator2.getBalanceUSE(ERM_KEY, fork), BigDecimal.valueOf(forgingVBalance));
        assertEquals(generator2.getBalanceUSE(FEE_KEY, fork), BigDecimal.valueOf(0.0007).setScale(8));

        ///assertEquals(recipient1.getLastTimestamp(fork)[0], 0L);

        //CHECK TOTAL FEE
        assertEquals(block.blockHead.totalFee, 70000);


        //CHECK LAST BLOCK
        assertArrayEquals(block.getSignature(), fork.getBlockMap().last().getSignature());


        ////////////////////////////////////
        //ORPHAN BLOCK
        //////////////////////////////////
        try {
            block.orphan(fork);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //CHECK BALANCE GENERATOR
        assertEquals(generator2.getBalanceUSE(FEE_KEY, fork).setScale(0), BigDecimal.valueOf(0).setScale(0));
        //BigDecimal.valueOf(2000));

        //CHECK LAST REFERENCE GENERATOR
        assertEquals(generator2.getLastTimestamp(fork)[0], gb.getTimestamp());

        //CHECK BALANCE RECIPIENT 1
        assertEquals(recipient1.getBalanceUSE(FEE_KEY, fork), BigDecimal.valueOf(0));

        //CHECK LAST BLOCK
        assertArrayEquals(gb.getSignature(), fork.getBlockMap().last().getSignature());
    }

    @SneakyThrows
    @Test
    public void processEmptyWrite() {

        Settings.NET_MODE = Settings.NET_MODE_TEST;
        Settings.getInstance();

        assertTrue(BlockChain.TEST_MODE);

        init();


        int stop = 100;
        for (int i = 1; i < stop; i++) {

            PrivateKeyAccount maker = Wallet.makePrivateKey(seed, i);
            Block block = new Block(1, db.getBlockMap().getLastBlockSignature(),
                    Wallet.makePrivateKey(seed, i), db.getBlocksHeadsMap().size() + 1,
                    11003, 11002L, 11001L);

            block.sign(maker);
            assertTrue(block.isSignatureValid());

            block.process(db, false);

        }

        for (int i = 1; i < stop; i++) {

            Block block = db.getBlockMap().get(i);

            byte[] raw = block.toBytes();

        }

        TransactionFinalMapImpl map = db.getTransactionFinalMap();
        IteratorCloseable<Long> iter = map.getIterator();
        while (iter.hasNext()) {
            Long key = iter.next();
            Transaction tx = map.get(key);
            tx.toBytes(Transaction.FOR_DB_RECORD);
        }

    }

}
