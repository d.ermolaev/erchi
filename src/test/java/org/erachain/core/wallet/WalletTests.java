package org.erachain.core.wallet;

import org.erachain.core.crypto.Crypto;
import org.erachain.database.wallet.DWSet;
import org.erachain.database.wallet.SecureWalletDatabase;
import org.erachain.datachain.DCSet;
import org.erachain.settings.Settings;
import org.erachain.utils.SimpleFileVisitorForRecursiveFolderDeletion;
import org.junit.Test;
import org.mapdb.DB;

import javax.ws.rs.WebApplicationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.SecureRandom;

import static org.junit.Assert.*;

public class WalletTests {

    String password = "test";
    Wallet wallet;
    boolean create;
    //CREATE EMPTY MEMORY DATABASE

    // INIT NOTES
    // тест полный всех - не успевает удалить файлы и валится createWallet тест - в ручную один он проходит
    private synchronized void init() {

        // delete \dataWallet\wallet.dat
        File walletFile = new File(Settings.getInstance().getDataWalletPath(), "wallet.dat");
        if (walletFile.getParentFile().exists())
            try {
                Files.walkFileTree(walletFile.getParentFile().toPath(),
                        new SimpleFileVisitorForRecursiveFolderDeletion());
            } catch (Throwable e1) {
                System.out.println(e1.getMessage());
            }


        // delete \walletKeys\wallet.s.dat.p
        if (Settings.SECURE_WALLET_FILE.getParentFile().exists())
            try {
                Files.walkFileTree(Settings.SECURE_WALLET_FILE.getParentFile().toPath(),
                        new SimpleFileVisitorForRecursiveFolderDeletion());
            } catch (Throwable e1) {
                System.out.println(e1.getMessage());
            }

        DCSet.reCreateDBinMEmory(false, false);
        DCSet dcSet = DCSet.getInstance();


        // make anew \walletKeys\wallet.s.dat.p
        SecureWalletDatabase secureDatabase = new SecureWalletDatabase(password);

        wallet = new Wallet(dcSet, false, false);
        create = wallet.create(secureDatabase, Crypto.getInstance()
                .digest(password.getBytes()), 10, false, false);
    }


    @Test
    public void createWalletRecover() {
        // B8CtbrUiwHV79oaJfzzxRyVjYFbEfBVuU5PmGf2TFQng
        // Added account #1 42DVwqJEXBE6YtBQCTQihtS9cdUj for nonce 0 nonceLocal: 1
        // Added account #2 3kUtJxHxFmxVE8ZnT7h1sVnwbpLM for nonce 1 nonceLocal: 2
        // Added account #3 4DTJTzFhX65qZzxNZLkLJeLuSLaT for nonce 2 nonceLocal: 3
        // Added account #4 3saojeTBn2k29LcfUBXuWmdSFcKD for nonce 3 nonceLocal: 4
        // Added account #5 4LChE1qDvMp9jRZXbGUvjvwJKWGP for nonce 4 nonceLocal: 5
        // Added account #6 3mU4MydjYUKy9uJPje99CWDpGqzZ for nonce 5 nonceLocal: 6
        // Added account #7 3ozTrCddLqRkULFYExDqv8kskd9w for nonce 6 nonceLocal: 7
        // Added account #8 4VuUdcKG1fikKaRMRy9KCCEdGnMv for nonce 7 nonceLocal: 8
        // Added account #9 3pQMydSWphfV85wMJGRDZT7SHkuE for nonce 8 nonceLocal: 9
        // Added account #10 4U9D4Leyj7ybGb6DK2oskiMEfbsq for nonce 9 nonceLocal: 10
        // Added account #14 3sJhrLo4u9AnBDkQaLrYVJ5ZmEvp for nonce 13 nonceLocal: 14

        byte[] seed = Crypto.getInstance().digest(("111" + password).getBytes());

        String walletKeysPath = Settings.getInstance().getWalletKeysPath();
        File keysFile = new File(walletKeysPath, "wallet.s.dat");
        try {
            Files.walkFileTree(keysFile.getParentFile().toPath(),
                    new SimpleFileVisitorForRecursiveFolderDeletion());
        } catch (Throwable e) {
        }
        File walletFile = new File(Settings.getInstance().getDataWalletPath(), "wallet.dat");
        try {
            Files.walkFileTree(walletFile.getParentFile().toPath(),
                    new SimpleFileVisitorForRecursiveFolderDeletion());
        } catch (IOException e) {
        }

        DCSet.reCreateDBinMEmory(false, false);
        DCSet dcSet = DCSet.getInstance();
        wallet = new Wallet(dcSet, false, false);
        boolean res = wallet.create(seed, password, 23, false, "walletKeysTEST", false, false);

    }

    @Test
    public void createWallet() {
        //CREATE DATABASE
        //CREATE WALLET
        init();

        //CHECK CREATE
        assertTrue(create);

        //CHECK VERSION
        assertEquals(DWSet.CURRENT_VERSION, wallet.getVersion());

        //CHECK ADDRESSES
        assertEquals(10, wallet.getAccounts().size());

        //CHECK PRIVATE KEYS
        assertEquals(10, wallet.getprivateKeyAccounts().size());

        //CHECK LAST BLOCKS
        assertNotNull(wallet.getLastBlocks(50));

        //CHECK LAST TRANSACTIONS
        assertNotNull(wallet.getLastTransactions(100));
    }

    @Test
    public void lockUnlock() {

        //CREATE DATABASE
        //CREATE WALLET
        init();

        //CHECK UNLOCKED
        assertTrue(wallet.isUnlocked());

        //LOCK
        wallet.lock();

        //CHECK LOCKED
        assertFalse(wallet.isUnlocked());

        //CHECK ACCOUNTS
        assertEquals(0, wallet.getprivateKeyAccounts().size());

        //UNLOCK by close DNS
        wallet.unlock(password);

        //CHECK UNLOCKED
        assertTrue(wallet.isUnlocked());

        //CHECK ACCOUNTS
        assertEquals(10, wallet.getprivateKeyAccounts().size());
    }

    @Test
    public void decodeSeed() {

        String seedStr = "WALLET:EBCEeamuj9d9vBDG8Ci1FwNhqRdeJTfh6HJDjaNg98w";
        assertEquals(seedStr, Wallet.encodeSeed(Wallet.decodeSeed(seedStr)));

        try {
            Wallet.decodeSeed("WW:we");
            fail("wrong");
        } catch (WebApplicationException e) {
        }

        SecureRandom random = new SecureRandom();
        byte[] seed = new byte[32];
        random.nextBytes(seed);

        assertTrue(Wallet.encodeSeed(seed).startsWith("WALLET:"));

    }
}
