package org.erachain.core.account;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.core.crypto.Base58;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.wallet.Wallet;
import org.junit.Test;

import java.security.SecureRandom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PrivateKeyAccountTest {

    /**
     * Проверка работы по SEED мобилки
     */
    @Test
    public void mobilePrivateKey() {
        String privKyeMobi = "5BzAsmeoXwNzzWdeR253vxLXv7qqutHg952CbFsj1iqaBf3AeswgE9JjzEu78ddi516imb1FaR78gbf54812i7Fe";
        byte[] privKyeMobiByte = Base58.decode(privKyeMobi, Crypto.SIGNATURE_LENGTH);
        PrivateKeyAccount privAccount = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privKyeMobiByte);
        assertEquals("249wR1G2X5udjMcyRCKVtgcQmVbq39jC35YpjHkLM8b2", Base58.encode(privAccount.publicKey));
        assertEquals("7583vPjLhbuSjSVykLnS1i2SjS8KjYPCi7", privAccount.address);

        byte[] signature = Crypto.getInstance().sign(privAccount, privKyeMobiByte);
        assertTrue(Crypto.getInstance().verify(privAccount.getPublicKey(), signature, privKyeMobiByte));

    }

    @Test
    public void nodePrivateKey() {
        SecureRandom random = new SecureRandom();
        byte[] seed = new byte[32];
        random.nextBytes(seed);

        PrivateKeyAccount privAccount = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, seed);
        byte[] signature = Crypto.getInstance().sign(privAccount, seed);
        assertTrue(Crypto.getInstance().verify(privAccount.getPublicKey(), signature, seed));

    }

    /**
     * WALLET:Cko8dLZ4Y4PSYSb5mQy1YmbGLuNaxJXQTxSsdA9TbBSe
     * 1 = acc:3u4FHXg1CWLtS22jdY7WfgtGZogt
     * pub:WyuUydN2rXLQkPgdfSAEuKECxEUFm4Q6zdXZ9Vtq9wCir
     * <p>
     * 2 = acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4
     * pub:WxZSKMqoYEkH3jD8sCshghVVuKspGDfL4H5ZSNLanHBVL
     * <p>
     * 3 = acc:4AnAKr8zqNu6pr4QiBgQPVcPLeyh
     * pub:WxZSKMqoYEkH3jD8sCshghVVuKspGDfL4H5ZSNLanHBVL
     * <p>
     * 4 = acc:431bVw72BAiDXVaG1reiDWphEwg3
     * pub:WxKN32yfeHC4mJAdCmqNLs4ox9zWuDSwQT2PzE4rPiNZD
     * <p>
     * 5 = acc:4DqBtnEMyZM1XLMEPYF6WJbUKQ1f
     * pub:Wq1yD7YkGe1bpExJ8Mz1nKPh4AjMcWo22rmTZCcsEFNyK
     * <p>
     * 6 = acc:4QkVCB9NY8ksA9hLGd8NpAwnVDjw
     * pub:WoAVPxf7Zn33LzBguBKu5BmTDxZ8p5ALK9MbvQr1XN7rY
     */
    @Test
    public void getPrivateKey() {
        String walletSeedStr = "WALLET:Cko8dLZ4Y4PSYSb5mQy1YmbGLuNaxJXQTxSsdA9TbBSe";
        byte[] walletSeed = Wallet.decodeSeed(walletSeedStr);
        byte[] accSeed = Wallet.makeAccountSeedByNonce(walletSeed, 0);
        PrivateKeyAccount priv = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, accSeed);
        assertEquals("3u4FHXg1CWLtS22jdY7WfgtGZogt", priv.getAddress());
        assertEquals("pub:WyuUydN2rXLQkPgdfSAEuKECxEUFm4Q6zdXZ9Vtq9wCir", priv.getPublicKey58Full());

        accSeed = Wallet.makeAccountSeedByNonce(walletSeed, 5);
        priv = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, accSeed);
        assertEquals("4QkVCB9NY8ksA9hLGd8NpAwnVDjw", priv.getAddress());
        assertEquals("pub:WoAVPxf7Zn33LzBguBKu5BmTDxZ8p5ALK9MbvQr1XN7rY", priv.getPublicKey58Full());

    }

    @Test
    public void getKeyPair() {
    }
}