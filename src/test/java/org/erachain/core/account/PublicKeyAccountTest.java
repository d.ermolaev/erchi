package org.erachain.core.account;

import org.erachain.core.crypto.Crypto;
import org.erachain.core.transaction.TxException;
import org.junit.Test;

import static org.junit.Assert.*;

public class PublicKeyAccountTest {

    Crypto crypto = Crypto.getInstance();
    PublicKeyAccount pubKey;

    @Test
    public void equals() {

        byte[] pubKeyBytes = crypto.digest("TEST0123".getBytes());
        pubKey = new PublicKeyAccount(Crypto.ED25519_SYSTEM, pubKeyBytes);

        PublicKeyAccount pub1 = new PublicKeyAccount(pubKey.getPublicKey58());

        pubKeyBytes = crypto.digest("TEST  0123".getBytes());
        PublicKeyAccount pub2 = new PublicKeyAccount(Crypto.ED25519_SYSTEM, pubKeyBytes);

        PublicKeyAccount pub1a = new PublicKeyAccount(pub1.getPublicKey58());

        assertEquals(pub1, pub1a);
        assertNotEquals(pub1, pub2);

        boolean equals = pub1.equals(pub1a);
        assertTrue(equals);
        int hash1 = pub1.hashCode();
        int hash1a = pub1a.hashCode();
        assertEquals(hash1, hash1a);

        int hash2 = pub2.hashCode();
        assertNotEquals(hash1, hash2);

        Account acc1 = new Account(pub1.getAddress());
        Account acc2 = new Account(pub2.getAddress());

        Account acc1a = new Account(pub1.getAddress());


        assertEquals(acc1, acc1a);
        assertNotEquals(acc1, acc2);

        assertEquals(acc1, acc1a);
        assertNotEquals(acc1, acc2);

        assertEquals(acc1.hashCode(), acc1a.hashCode());

        assertNotEquals(acc1.hashCode(), acc2.hashCode());


    }

    @Test
    public void makeForDApp() {
        pubKey = PublicKeyAccount.makeForDApp(crypto.digest("TTT".getBytes()));
        assertTrue(pubKey.isDAppOwned());

        assertEquals(pubKey.getAddress(), "APPxBNFH9AnyEYnjZ1NxY8AurW");
        assertEquals(pubKey.getAddressFull(), "acc:APPxBNFH9AnyEYnjZ1NxY8AurW");

        assertEquals(pubKey.getPublicKey58(), "Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE");

        assertEquals(pubKey.getPublicKey58Full(), "pub:Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE");


    }

    @Test
    public void parse() {

        try {
            pubKey = PublicKeyAccount.parse("pub:Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE");
        } catch (TxException e) {
            throw new RuntimeException(e);
        }
        assertEquals(pubKey.getPublicKey58(), "Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE");
        assertEquals(pubKey.getPublicKey58Full(), "pub:Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE");
        assertEquals(pubKey.getSystem(), Crypto.DAPP_SYSTEM);

    }

    @Test
    public void isValid() {
        assertTrue(PublicKeyAccount.isValidPublicKey("erc:pub:Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE"));
        assertTrue(PublicKeyAccount.isValidPublicKey("pub:Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE"));
        assertTrue(PublicKeyAccount.isValidPublicKey("Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE"));

        assertFalse(PublicKeyAccount.isValidPublicKey("wer:pub:Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE"));
        assertFalse(PublicKeyAccount.isValidPublicKey("acc:Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cE"));

        assertFalse(PublicKeyAccount.isValidPublicKey("Pt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZg"));
        assertFalse(PublicKeyAccount.isValidPublicKey("4YuDWoBZ6LPt1wxfy75cEFvDUUf7F2fTjHcUR1xLnPZgi81urgv7cEpmZxyR1Nky82orpy1qeqeH"));

    }
}