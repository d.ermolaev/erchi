package org.erachain.core.account;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.core.BlockChain;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.transaction.GenesisTransferAssetTransaction;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;
import org.erachain.core.wallet.Wallet;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.ItemAssetBalanceMap;
import org.junit.Test;
import org.mapdb.Fun;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.junit.Assert.*;

public class AccountTest {

    Crypto crypto = Crypto.getInstance();
    DCSet db;
    GenesisBlock gb;
    Account account;

    void init() {
        db = DCSet.createEmptyHardDatabaseSet(0);
        gb = new GenesisBlock();

        try {
            gb.process(db, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @SneakyThrows
    @Test
    public void setLastTimestamp() {
        init();

        account = ((GenesisTransferAssetTransaction) gb.getTransactions().get(100)).getRecipient();
        long[] time = account.getLastTimestamp(db);

        // MAKE COPY hoe hasCode() check
        Account newAccount = new Account(account.getAddress());
        Fun.Tuple3<Integer, Integer, Integer> point = newAccount.getLastForgingData(db);
        assertNotNull(point);
        long[] reference = newAccount.getLastTimestamp(db);
        assertNotNull(reference);
    }

    @Test
    public void appAcc() {

        // 23, 83, 118
        Account account = Account.makeFromOld("APPxGgH8c63VrrgEw1wQA4Dno1JuPLTsWe");
        assertTrue(account.isDAppOwned());

        account = PublicKeyAccount.makeForDApp(crypto.digest("APPCGgH8c63VrrgEw1wQA4Dno1JuPLTsWe".getBytes(StandardCharsets.UTF_8)));
        assertEquals(account.getAddress(), "APPBcJaUSzwgK7tKZy3ZwRiivz9DiEizW4");
        assertTrue(account.isDAppOwned());


    }

    @Test
    public void getBalance() {
        init();

        /// Tuple2[323980.18401546, 323980.18401546] //
        Account account = Account.makeFromOld("76ACGgH8c63VrrgEw1wQA4Dno1JuPLTsWe");

        Fun.Tuple5 balance5 = account.getBalance(db, Transaction.RIGHTS_KEY);
        assertEquals(((BigDecimal) ((Fun.Tuple2) balance5.a).a).intValue(), 323980);
        assertEquals(((BigDecimal) ((Fun.Tuple2) balance5.a).b).intValue(), 331417);

        Fun.Tuple2 balance = account.getBalance(db, Transaction.RIGHTS_KEY, 1);

        assertEquals(((BigDecimal) balance.a).intValue(), 323980);
        assertEquals(((BigDecimal) balance.b).intValue(), 331417);

        Fun.Tuple2 balance2 = account.getBalance(db, Transaction.RIGHTS_KEY, 1);

        assertEquals(((BigDecimal) balance2.a).intValue(), 323980);
        assertEquals(((BigDecimal) balance2.b).intValue(), 331417);

        ItemAssetBalanceMap map = db.getAssetBalanceMap();
        map.clear();

        Fun.Tuple2 balance3 = account.getBalance(db, Transaction.RIGHTS_KEY, 1);

        assertEquals(((BigDecimal) balance3.a).intValue(), 0);
        assertEquals(((BigDecimal) balance3.b).intValue(), 331417);

    }

    @Test
    public void tryMakeAccount() {
        byte[] bytes = new byte[20];
        bytes[0] |= Account.DAPP_ADDRESS_VERSION_MASK;
        account = new Account(bytes);
        assertTrue(account.getAddress().length() > Account.ADDRESS_LENGTH);

        byte[] ppk;
        PrivateKeyAccount accountPPK;
        Fun.Tuple2<Account, String> result;

        if (false) {
            // VERY LONG TESTING!

            // норм все пашет
            for (byte i = 0; i < 255; i++) {
                bytes[10] = i;
                for (int k = 0; k < 1000; k++) {
                    ppk = Wallet.makeAccountSeedByNonce(bytes, k);
                    accountPPK = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, ppk);
                    result = Account.tryMakeAccount(accountPPK.getAddress());
                    assertEquals(accountPPK.getAddress(), result.a.getAddress());
                    assertTrue(Account.isValidAddress(accountPPK.getAddress()));

                }
            }
        }

        String address = "4SywCCA1rkL-ySgfehgD7DLP3Qje5";
        result = Account.tryMakeAccount(address);
        assertNull(result.a);
        //assertEquals(address, result.a.getAddress());

        String pubKey = "5Tm7uhFLkCbQ1maNvWkDXCkPsbTreoY6wA4xbPCcy1z";
        result = Account.tryMakeAccount(pubKey);
        assertEquals("788TNN4ujpCys3zeE261q767BHaj2UsDu6", result.a.getAddress());


    }

    @Test
    public void parse() {

        System.out.println(Integer.toBinaryString(Byte.toUnsignedInt(Account.DAPP_ADDRESS_VERSION_MASK)));

        // 23, 83, 118
        account = Account.makeFromOld("788TNN4ujpCys3zeE261q767BHaj2UsDu6");
        String address = account.getAddress();
        byte[] bytes = account.getBytes();
        assertTrue((bytes[0] & Account.DAPP_ADDRESS_VERSION_MASK) != 0);
        assertFalse(account.isDAppOwned());

        assertEquals("4SywCCA1rkLySgfehgD7DLP3Qje5", address);

        Account accountB = new Account(address);
        assertEquals(account, accountB);
        assertArrayEquals(account.getBytes(), accountB.bytes);
        assertFalse(accountB.isDAppOwned());

        String addressFull = account.getAddressFull();
        assertEquals("acc:4SywCCA1rkLySgfehgD7DLP3Qje5", addressFull);

        try {
            accountB = Account.parse(addressFull);
        } catch (TxException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        assertArrayEquals(account.getBytes(), accountB.bytes);

        try {
            accountB = Account.parse("1" + addressFull);
        } catch (TxException e) {
            assertEquals(e.getMessage(), "not Erchi address, Use `acc:ADDRESS`");
        }

    }

    @Test
    public void parseDapp() throws TxException {
        byte[] pubKey = crypto.digest("TEST0123".getBytes());
        account = new Account(Crypto.DAPP_SYSTEM, pubKey);
        assertTrue(account.isDAppOwned());

        String address = account.getAddress();
        assertEquals("APPx5NEd4s1yqkpb1DpNdfumRo", address);

        Account accountB = Account.parse(account.getAddress());
        assertEquals(accountB.getAddress(), account.getAddress());
        assertArrayEquals(account.getBytes(), accountB.bytes);
        assertTrue(accountB.isDAppOwned());


        accountB = new Account(address);
        assertArrayEquals(account.getBytes(), accountB.bytes);


        try {
            accountB = Account.parse(account.getAddress());
            assertTrue(accountB.isDAppOwned());
            assertArrayEquals(account.getBytes(), accountB.bytes);
        } catch (TxException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        account = PublicKeyAccount.makeForDApp(Crypto.getInstance().digest("MEMO DOGE".getBytes(StandardCharsets.UTF_8)));
        assertEquals(account.getAddress(), "APPxDqYMDsyLtj7Tej6gAUCdCP");
        assertTrue(account.isDAppOwned());

    }

    @Test
    public void compareTo() {
        account = PublicKeyAccount.makeForDApp(crypto.digest("TEST 22".getBytes(StandardCharsets.UTF_8)));

        Account accountB = account;
        // SAME FULLY
        assertEquals(0, accountB.compareTo(account));

        byte[] bytes = account.getBytes();
        // SAME FULLY BY BYTES
        accountB = new Account(bytes);
        assertEquals(0, accountB.compareTo(account));

        // make COPY!
        bytes = Arrays.copyOf(bytes, bytes.length);

        // SAME as COPY
        accountB = new Account(bytes);
        assertEquals(0, accountB.compareTo(account));

        bytes[18] += 1;
        accountB = new Account(bytes);
        assertEquals(1, accountB.compareTo(account));


        bytes = Arrays.copyOf(bytes, bytes.length);
        bytes[18] -= 2;
        accountB = new Account(bytes);

        assertEquals(-1, accountB.compareTo(account));

    }

    @Test
    public void isValid() {
        assertTrue(Account.isValidAddress("erc:acc:4YuDWoBZ6LpmZxyR1Nky82orpy1H"));
        assertTrue(Account.isValidAddress("acc:4YuDWoBZ6LpmZxyR1Nky82orpy1H"));
        assertTrue(Account.isValidAddress("4YuDWoBZ6LpmZxyR1Nky82orpy1H"));

        assertFalse(Account.isValidAddress("wer:acc:4YuDWoBZ6LpmZxyR1Nky82orpy1H"));
        assertFalse(Account.isValidAddress("pub:4YuDWoBZ6LpmZxyR1Nky82orpy1H"));

        assertFalse(Account.isValidAddress("1YuDWoBZ6LpmZxyR1Nky82orpy1H"));
        assertFalse(Account.isValidAddress("4YuDWoBZ6LpmZxyR1Nky82orpy1qeqeH"));
        assertFalse(Account.isValidAddress("4YuDWoBZ6LpmZxyR1Nky82"));

    }

    @Test
    public void getForgingBalance() {
        assertEquals(12300, new BigDecimal("1.23").setScale(4)
                .movePointRight(BlockChain.MINOR_ERA_BALANCE_SHIFT).intValue());
    }
}