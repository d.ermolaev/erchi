package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.item.persons.PersonHuman;
import org.erachain.core.wallet.Wallet;
import org.erachain.database.IDB;
import org.erachain.datachain.AddressPersonMap;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.KKPersonStatusMap;
import org.erachain.datachain.PersonAddressMap;
import org.erachain.ntp.NTP;
import org.erachain.settings.Settings;
import org.erachain.utils.SimpleFileVisitorForRecursiveFolderDeletion;
import org.junit.Test;
import org.mapdb.Fun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RCertifyPubKeysTest {

    static Logger LOGGER = LoggerFactory.getLogger(RCertifyPubKeysTest.class.getName());
    private final byte[] icon = new byte[]{1, 3, 4, 5, 6, 9}; // default value
    private final byte[] image = new byte[18000]; // default value
    int[] TESTED_DBS = new int[]{
            IDB.DBS_MAP_DB
            , IDB.DBS_ROCK_DB
    };
    BigDecimal BG_ZERO = BigDecimal.ZERO.setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
    long ERM_KEY = Transaction.RIGHTS_KEY;
    long FEE_KEY = Transaction.FEE_KEY;
    //long ALIVE_KEY = StatusCls.ALIVE_KEY;
    byte FEE_POWER = (byte) 1;
    byte[] personReference = new byte[64];
    long timestamp = NTP.getTime();
    long dbRef = 0L;
    byte[] itemAppData = null;
    Long last_ref;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    //GENERATE ACCOUNT SEED
    int nonce = 1;
    PrivateKeyAccount registrar = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, nonce++));
    PrivateKeyAccount certifier = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, nonce++));
    PrivateKeyAccount userAccount1 = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, nonce++));
    PrivateKeyAccount userAccount2 = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, nonce++));
    PrivateKeyAccount userAccount3 = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, nonce++));
    //List<PrivateKeyAccount> certifiedPrivateKeys = new ArrayList<PrivateKeyAccount>();
    List<PublicKeyAccount> certifiedPublicKeys = new ArrayList<PublicKeyAccount>();
    PersonCls personGeneral;
    PersonCls person;
    long genesisPersonKey = -1;
    long personKey = -1;
    IssuePersonRecord issuePersonTransaction;
    RCertifyPubKeys r_CertifyPubKeys;
    KKPersonStatusMap dbPS;
    PersonAddressMap dbPA;
    AddressPersonMap dbAP;
    //int version = 0; // without signs of person
    int version = 1; // with signs of person
    int seqNo = 1;
    private byte[] ownerSignature = new byte[Crypto.SIGNATURE_LENGTH];
    //CREATE EMPTY MEMORY DATABASE
    private DCSet dcSet;
    private BlockChain bchain;
    private Controller cntrl;
    private GenesisBlock gb;

    // INIT PERSONS
    @SneakyThrows
    private void init(int dbs) {
        LOGGER.info(" ********** open DBS: " + dbs);

        File tempDir = new File(Settings.getInstance().getDataTempDir());
        try {
            Files.walkFileTree(tempDir.toPath(), new SimpleFileVisitorForRecursiveFolderDeletion());
        } catch (Throwable e) {
            LOGGER.info(" ********** " + e.getMessage());
        }

        dcSet = DCSet.createEmptyHardDatabaseSetWithFlush(null, dbs);

        cntrl = Controller.getInstance();
        cntrl.initBlockChain(dcSet);
        bchain = cntrl.getBlockChain();
        gb = bchain.getGenesisBlock();

        dbPA = dcSet.getPersonAddressMap();
        dbAP = dcSet.getAddressPersonMap();
        dbPS = dcSet.getPersonStatusMap();

        try {
            gb.process(dcSet, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        last_ref = gb.getTimestamp();

        registrar.setLastTimestamp(new long[]{last_ref, 0}, dcSet);
        registrar.changeBalance(dcSet, false, ERM_KEY, BigDecimal.valueOf(1000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        registrar.changeBalance(dcSet, false, FEE_KEY, BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);

        certifier.setLastTimestamp(new long[]{last_ref, 0}, dcSet);
        certifier.changeBalance(dcSet, false, ERM_KEY, BigDecimal.valueOf(1000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        certifier.changeBalance(dcSet, false, FEE_KEY, BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);

        byte gender = 0;
        long birthDay = timestamp - 12345678;

        ownerSignature = new byte[64];
        ownerSignature[1] = (byte) 1;
        person = new PersonHuman(registrar, "Ermolaev Dmitrii Sergeevich as registrar", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, birthDay - 1,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, ownerSignature);
        person.setReference(ownerSignature, dbRef);
        dcSet.getItemPersonMap().incrementPut(person);
        long keyRegistrar = person.getKey();

        ownerSignature = new byte[64];
        ownerSignature[1] = (byte) 2;
        person = new PersonHuman(certifier, "Ermolaev Dmitrii Sergeevich as certifier", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, birthDay - 1,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, ownerSignature);
        person.setReference(ownerSignature, dbRef);
        dcSet.getItemPersonMap().incrementPut(person);
        long keyCertifier = person.getKey();

        // внесем его как удостовренную персону
        Fun.Tuple3<Integer, Integer, Integer> itemPRegistrar = new Fun.Tuple3<Integer, Integer, Integer>(999999, 2, 2);
        Fun.Tuple4<Long, Integer, Integer, Integer> itemARegistrar = new Fun.Tuple4<Long, Integer, Integer, Integer>(keyRegistrar, 999999, 2, 3);

        dcSet.getAddressPersonMap().addItem(registrar.getBytes(), itemARegistrar);
        dcSet.getPersonAddressMap().addItem(33L, registrar.getAddress(), itemPRegistrar);

        // внесем его как удостовренную персону
        Fun.Tuple3<Integer, Integer, Integer> itemPCertifier = new Fun.Tuple3<Integer, Integer, Integer>(999999, 2, 2);
        Fun.Tuple4<Long, Integer, Integer, Integer> itemACertifier = new Fun.Tuple4<Long, Integer, Integer, Integer>(keyCertifier, 999999, 2, 3);

        dcSet.getAddressPersonMap().addItem(certifier.getBytes(), itemACertifier);
        dcSet.getPersonAddressMap().addItem(33L, certifier.getAddress(), itemPCertifier);

        ownerSignature = new byte[64];
        ownerSignature[1] = (byte) -1;

        // GET RIGHTS TO CERTIFIER
        personGeneral = new PersonHuman(registrar, "Ermolaev Dmitrii Sergeevich as certifier", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, birthDay - 1,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, ownerSignature);
        //personGeneral.setKey(genesisPersonKey);

        GenesisIssuePersonRecord genesis_issue_person = new GenesisIssuePersonRecord(personGeneral, null, null, null);
        genesis_issue_person.setDC(dcSet, Transaction.FOR_NETWORK, 3, seqNo++, true);
        genesis_issue_person.process(gb, Transaction.FOR_NETWORK);
        //genesisPersonKey = dcSet.getIssuePersonMap().size();
        genesisPersonKey = genesis_issue_person.getAssetKey();

        GenesisCertifyPersonRecord genesis_certify = new GenesisCertifyPersonRecord(registrar, genesisPersonKey, null, null, null);
        genesis_certify.setDC(dcSet, Transaction.FOR_NETWORK, 3, seqNo++, true);
        genesis_certify.process(gb, Transaction.FOR_NETWORK);

        person = new PersonHuman(registrar, "Ermolaev Dmitrii Sergeevich", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, birthDay - 2,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188,
                null);

        //person.setKey(genesisPersonKey + 1);
        //CREATE ISSUE PERSON TRANSACTION
        issuePersonTransaction = new IssuePersonRecord(registrar, person, FEE_POWER, timestamp, null);

        if (certifiedPublicKeys.isEmpty()) {
            certifiedPublicKeys.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, userAccount1.getPublicKey()));
            certifiedPublicKeys.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, userAccount2.getPublicKey()));
            certifiedPublicKeys.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, userAccount3.getPublicKey()));
        }

    }

    @SneakyThrows
    public void initPersonalize() {

        issuePersonTransaction.sign(registrar, Transaction.FOR_NETWORK);
        issuePersonTransaction.setDC(dcSet, Transaction.FOR_NETWORK, 3, seqNo++, true);
        assertEquals(Transaction.VALIDATE_OK, issuePersonTransaction.isValid(Transaction.FOR_NETWORK, 0L));

        issuePersonTransaction.process(gb, Transaction.FOR_NETWORK);

        // нужно занести ее в базу чтобы считать по этой записи персону создавшую эту запись
        dcSet.getTransactionFinalMap().put(issuePersonTransaction);
        dcSet.getTransactionFinalMapSigns().put(issuePersonTransaction.signature, issuePersonTransaction.dbRef);

        personKey = person.getKey();

        // issue 1 genesis person in init() here
        //assertEquals( genesisPersonKey + 1, personKey);
        //assertEquals( null, dbPS.getItem(personKey, ALIVE_KEY));

        //CREATE PERSONALIZE REcORD
        timestamp += 100;
        r_CertifyPubKeys = new RCertifyPubKeys(certifier, FEE_POWER, personKey,
                certifiedPublicKeys,
                timestamp);

    }

    @Test
    public void test1() throws TxException {

        // see org.erachain.core.transaction.IssuePersonRecordTest.init
        for (int dbs : TESTED_DBS) {

            init(dbs);

            assertNotEquals(registrar.getLastTimestamp(dcSet), null);

            RCertifyPubKeys certPubKey = new RCertifyPubKeys(registrar, FEE_POWER,
                    person.getKey(), certifiedPublicKeys, timestamp);
            certPubKey.sign(registrar, Transaction.FOR_NETWORK);
            certPubKey.setDC(dcSet, Transaction.FOR_NETWORK, 3, 5, true);
            certPubKey.process(null, Transaction.FOR_NETWORK);

            registrar.getLastTimestamp(dcSet);

            assertEquals(new long[]{123L, 345L}, registrar.getLastTimestamp(dcSet));

        }

    }


    @Test
    public void parse() {

        init(IDB.DBS_MAP_DB);
        initPersonalize();

        // 123
        RCertifyPubKeys certPubKey = new RCertifyPubKeys(registrar, FEE_POWER,
                person.getKey(), certifiedPublicKeys, timestamp);
        certPubKey.sign(registrar, Transaction.FOR_NETWORK);

        //CONVERT TO BYTES
        byte[] rawTX = certPubKey.toBytes(Transaction.FOR_NETWORK);

        //CHECK DATA LENGTH
        assertEquals(rawTX.length, certPubKey.getDataLength(Transaction.FOR_NETWORK));

        Transaction parsedTX;
        try {
            //PARSE FROM BYTES
            parsedTX = TransactionFactory.getInstance().parse(rawTX, Transaction.FOR_NETWORK);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail("Exception while parsing transaction.");
            return;
        }

        //CHECK INSTANCE
        assertTrue(parsedTX instanceof RCertifyPubKeys);

        //CHECK SIGNATURE
        assertArrayEquals(certPubKey.getSignature(), parsedTX.getSignature());

        //CHECK ISSUER
        assertEquals(certPubKey.getCreator().getAddress(), parsedTX.getCreator().getAddress());

        //CHECK TIMESTAMP
        assertEquals(certPubKey.getTimestamp(), parsedTX.getTimestamp());

        assertEquals(certPubKey.getPublicKeysSize(), ((RCertifyPubKeys) parsedTX).getPublicKeysSize());

        assertEquals(certPubKey.getPublicKeys().get(0), parsedTX.getPublicKeys().get(0));

        //PARSE TRANSACTION FROM WRONG BYTES
        rawTX = new byte[certPubKey.getDataLength(Transaction.FOR_NETWORK)];

        try {
            //PARSE FROM BYTES
            TransactionFactory.getInstance().parse(rawTX, Transaction.FOR_NETWORK);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }

    }

    @Test
    public void toBytes() {
    }

    @Test
    public void isSignatureValid() {
    }

    @Test
    public void isValid() {
    }

    @Test
    public void process() {
    }

    @Test
    public void orphan() {
    }
}