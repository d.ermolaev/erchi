package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.block.Block;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetVenture;
import org.erachain.datachain.DCSet;
import org.erachain.ntp.NTP;
import org.junit.Test;
import org.mapdb.Fun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TransactionTests {

    static Logger LOGGER = LoggerFactory.getLogger(TransactionTests.class.getName());
    private final byte[] icon = new byte[]{1, 3, 4, 5, 6, 9}; // default value
    private final byte[] image = new byte[]{4, 11, 32, 23, 45, 122, 11, -45}; // default value
    //Long Transaction.FOR_NETWORK = null;
    long ERM_KEY = Transaction.RIGHTS_KEY;
    long FEE_KEY = Transaction.FEE_KEY;
    byte FEE_POWER = (byte) 1;
    byte[] assetReference = new byte[64];
    long timestamp = NTP.getTime();
    long last_ref;
    long new_ref;
    int forDeal = Transaction.FOR_NETWORK;
    byte[] itemAppData = null;
    long txFlags = 0L;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    byte[] seed_b = Crypto.getInstance().digest("buyer".getBytes());
    byte[] privateKey_b = Crypto.getInstance().createKeyPair(seed_b).getA();
    PrivateKeyAccount buyer = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey_b);
    Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");
    DCSet databaseSet;
    Block block;
    //CREATE EMPTY MEMORY DATABASE
    private DCSet db;
    private GenesisBlock gb;

    @SneakyThrows
    public static void signAndProcess(DCSet dcSet, PrivateKeyAccount maker, Block block, Transaction tx, int height, int seqNo) {
        tx.sign(maker, Transaction.FOR_NETWORK);
        tx.setDC(dcSet, Transaction.FOR_NETWORK, height, seqNo, true);
        tx.process(block, Transaction.FOR_NETWORK);
        dcSet.getTransactionFinalMap().put(tx);
        dcSet.getTransactionFinalMapSigns().put(tx.getSignature(), tx.getDBRef());
    }

    @SneakyThrows
    public static void process(DCSet dcSet, Block block, Transaction tx) {
        tx.process(block, Transaction.FOR_NETWORK);
        dcSet.getTransactionFinalMap().put(tx);
        dcSet.getTransactionFinalMapSigns().put(tx.getSignature(), tx.getDBRef());
    }

    // INIT ASSETS
    private void init() {

        File log4j = new File("log4j_test.properties");
        System.out.println(log4j.getAbsolutePath());
        if (log4j.exists()) {
            System.out.println("configured");
            //PropertyConfigurator.configure(log4j.getAbsolutePath());
        }

        databaseSet = db = DCSet.createEmptyDatabaseSet(0);
        BlockChain chain;
        try {
            chain = new BlockChain(db);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        gb = chain.getGenesisBlock();
        block = gb;

        try {
            gb.process(db, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        last_ref = gb.getTimestamp();

        // FEE FUND
        maker.setLastTimestamp(new long[]{last_ref, 0}, db);
        maker.changeBalance(db, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        new_ref = maker.getLastTimestamp(db)[0];

        buyer.setLastTimestamp(new long[]{last_ref, 0}, db);
        buyer.changeBalance(db, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        buyer.changeBalance(db, false, Account.BALANCE_POS_OWN, ERM_KEY, BigDecimal.valueOf(2000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false); // for bye


    }

    //PAYMENT

    @Test
    public void validateSignatureR_Send() {
        String s = "";
        init();

        //CREATE PAYMENT
        Transaction payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp);
        payment.sign(maker, Transaction.FOR_NETWORK);
        //CHECK IF PAYMENT SIGNATURE IS VALID
        payment.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertTrue(payment.isSignatureValid());

        //INVALID SIGNATURE
        payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp + 1, new byte[64]);

        //CHECK IF PAYMENT SIGNATURE IS INVALID
        payment.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertFalse(payment.isSignatureValid());
    }

    @Test
    public void validateR_Send() {

        init();


        //CREATE VALID PAYMENT
        Transaction payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(0.5).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp);
        try {
            assertEquals(Transaction.VALIDATE_OK, payment.isValid(Transaction.FOR_NETWORK, txFlags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //CREATE INVALID PAYMENT INVALID RECIPIENT ADDRESS
        payment = new TransactionAmount(maker, FEE_POWER, Account.makeFromOld("test"), Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp);
        try {
            assertEquals(Transaction.INVALID_ADDRESS, payment.isValid(Transaction.FOR_NETWORK, txFlags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //CREATE INVALID PAYMENT NEGATIVE AMOUNT
        payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(-100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp);
        try {
            assertEquals(Transaction.NEGATIVE_AMOUNT, payment.isValid(Transaction.FOR_NETWORK, txFlags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //CREATE INVALID PAYMENT WRONG REFERENCE
        payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp, new byte[64]);
        try {
            assertEquals(Transaction.INVALID_REFERENCE, payment.isValid(Transaction.FOR_NETWORK, txFlags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //CREATE INVALID PAYMENT WRONG TIMESTAMP
        payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getLastTimestamp(db)[0]);
        try {
            assertEquals(Transaction.INVALID_TIMESTAMP, payment.isValid(Transaction.FOR_NETWORK, txFlags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }
        payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getLastTimestamp(db)[0] - 10);
        try {
            assertEquals(Transaction.INVALID_TIMESTAMP, payment.isValid(Transaction.FOR_NETWORK, txFlags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    public void parseR_Send() {
        init();

        //CREATE VALID PAYMENT
        Transaction payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp);
        payment.sign(maker, Transaction.FOR_NETWORK);

        //CONVERT TO BYTES
        byte[] rawPayment = payment.toBytes(Transaction.FOR_NETWORK);

        TransactionAmount parsedPayment;
        try {
            //PARSE FROM BYTES
            parsedPayment = (TransactionAmount) TransactionFactory.getInstance().parse(rawPayment, Transaction.FOR_NETWORK);
        } catch (Exception e) {
            fail("Exception while parsing transaction.");
            return;
        }

            //CHECK INSTANCE
            assertTrue(parsedPayment instanceof TransactionAmount);

            //CHECK SIGNATURE
            assertTrue(Arrays.equals(payment.getSignature(), parsedPayment.getSignature()));

        //CHECK AMOUNT SENDER
        assertEquals(payment.getAmount(maker), parsedPayment.getAmount(maker));

        //CHECK AMOUNT RECIPIENT
        assertEquals(payment.getAmount(recipient), parsedPayment.getAmount(recipient));

        //CHECK FEE
        assertEquals(payment.getFee(), parsedPayment.getFee());

        assertEquals(payment.getDataMessage(), null);
        assertEquals(payment.getDataMessage(), parsedPayment.getDataMessage());

        //CHECK TIMESTAMP
        assertEquals(payment.getTimestamp(), parsedPayment.getTimestamp());

        //PARSE TRANSACTION FROM WRONG BYTES
        rawPayment = new byte[payment.getDataLength(Transaction.FOR_NETWORK)];

        try {
            //PARSE FROM BYTES
            TransactionFactory.getInstance().parse(rawPayment, Transaction.FOR_NETWORK);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }
    }

    @SneakyThrows
    @Test
    public void processR_Send() {

        init();

        //CREATE PAYMENT
        BigDecimal amount = BigDecimal.valueOf(0.5).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
        Transaction payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, amount.setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp);
        payment.sign(maker, Transaction.FOR_NETWORK);
        BigDecimal fee = payment.getFee();
        payment.process(gb, Transaction.FOR_NETWORK);

        LOGGER.info("getConfirmedBalance: " + maker.getBalanceUSE(FEE_KEY, databaseSet));
        LOGGER.info("getConfirmedBalance FEE_KEY:" + maker.getBalanceUSE(FEE_KEY, databaseSet));

        //CHECK BALANCE SENDER
        assertEquals(0, BigDecimal.valueOf(1).subtract(amount).subtract(fee).setScale(BlockChain.AMOUNT_DEFAULT_SCALE).compareTo(maker.getBalanceUSE(FEE_KEY, databaseSet)));

        //CHECK BALANCE RECIPIENT
        assertEquals(amount, recipient.getBalanceUSE(FEE_KEY, databaseSet));

        //CHECK REFERENCE SENDER
        assertEquals(maker.getLastTimestamp(databaseSet)[0], timestamp);

        //CHECK REFERENCE RECIPIENT
        assertEquals(payment.getTimestamp(), recipient.getLastTimestamp(databaseSet));
    }

    @Test
    public void orphanR_Send() throws TxException {

        init();

        //CREATE PAYMENT
        Transaction payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp);
        payment.sign(maker, Transaction.FOR_NETWORK);
        payment.process(gb, Transaction.FOR_NETWORK);

        BigDecimal amount1 = maker.getBalanceUSE(FEE_KEY, databaseSet);
        BigDecimal amount2 = recipient.getBalanceUSE(FEE_KEY, databaseSet);

        //CREATE PAYMENT2
        Transaction payment2 = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), timestamp);
        payment2.sign(maker, Transaction.FOR_NETWORK);
        payment.process(gb, Transaction.FOR_NETWORK);

        //ORPHAN PAYMENT
        payment2.orphan(gb, Transaction.FOR_NETWORK);

        //CHECK BALANCE SENDER
        assertEquals(0, amount1.compareTo(maker.getBalanceUSE(FEE_KEY, databaseSet)));

        //CHECK BALANCE RECIPIENT
        assertEquals(amount2, recipient.getBalanceUSE(FEE_KEY, databaseSet));

        //CHECK REFERENCE SENDER
        assertEquals(maker.getLastTimestamp(databaseSet)[0], maker.getLastTimestamp(databaseSet)[0]);

        //CHECK REFERENCE RECIPIENT
        assertNull(recipient.getLastTimestamp(databaseSet)[0]);

    }


    //ISSUE ASSET TRANSACTION

    @Test
    public void validateSignatureIssueAssetTransaction() {

        init();

        //CREATE ASSET
        AssetCls asset = new AssetVenture(0, maker, "test", icon, image, "strontje", 0, 50000L);
        //byte[] data = asset.toBytes(false);
        //Asset asset2 = Asset.parse(data);


        //CREATE SIGNATURE
        long timestamp = NTP.getTime();

        //CREATE ISSUE ASSET TRANSACTION
        Transaction issueAssetTransaction = new IssueAssetTransaction(maker, null, asset, FEE_POWER, timestamp);
        issueAssetTransaction.sign(maker, Transaction.FOR_NETWORK);

        //CHECK IF ISSUE ASSET TRANSACTION IS VALID
        issueAssetTransaction.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertTrue(issueAssetTransaction.isSignatureValid());

        //INVALID SIGNATURE
        issueAssetTransaction = new IssueAssetTransaction(
                maker, asset, FEE_POWER, timestamp, new byte[64]);

        //CHECK IF ISSUE ASSET IS INVALID
        issueAssetTransaction.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertFalse(issueAssetTransaction.isSignatureValid());
    }


    @SneakyThrows
    @Test
    public void parseIssueAssetTransaction() {

        init();

        //CREATE SIGNATURE
        long timestamp = NTP.getTime();
        AssetCls asset = new AssetVenture(0, maker, "test", icon, image, "strontje", 0, 50000L);

        //CREATE ISSUE ASSET TRANSACTION
        IssueAssetTransaction issueAssetTransaction = new IssueAssetTransaction(maker, null, asset, FEE_POWER, timestamp);
        issueAssetTransaction.sign(maker, Transaction.FOR_NETWORK);
        issueAssetTransaction.process(gb, Transaction.FOR_NETWORK);

        //CONVERT TO BYTES
        byte[] rawIssueAssetTransaction = issueAssetTransaction.toBytes(Transaction.FOR_NETWORK);

        //CHECK DATA LENGTH
        assertEquals(rawIssueAssetTransaction.length, issueAssetTransaction.getDataLength(Transaction.FOR_NETWORK));

        try {
            //PARSE FROM BYTES
            IssueAssetTransaction parsedIssueAssetTransaction = (IssueAssetTransaction) TransactionFactory.getInstance().parse(rawIssueAssetTransaction, Transaction.FOR_NETWORK);

            //CHECK INSTANCE
            assertTrue(parsedIssueAssetTransaction instanceof IssueAssetTransaction);

            //CHECK SIGNATURE
            assertTrue(Arrays.equals(issueAssetTransaction.getSignature(), parsedIssueAssetTransaction.getSignature()));

            //CHECK ISSUER
            assertEquals(issueAssetTransaction.getCreator().getAddress(), parsedIssueAssetTransaction.getCreator().getAddress());

            //CHECK OWNER
            assertEquals(issueAssetTransaction.getItem().getAuthor().getAddress(), parsedIssueAssetTransaction.getItem().getAuthor().getAddress());

            //CHECK NAME
            assertEquals(issueAssetTransaction.getItem().getName(), parsedIssueAssetTransaction.getItem().getName());

            //CHECK DESCRIPTION
            assertEquals(issueAssetTransaction.getItem().getDescription(), parsedIssueAssetTransaction.getItem().getDescription());

            //CHECK QUANTITY
            assertEquals(((AssetCls) issueAssetTransaction.getItem()).getQuantity(), ((AssetCls) parsedIssueAssetTransaction.getItem()).getQuantity());

            //SCALE
            assertEquals(((AssetCls) issueAssetTransaction.getItem()).getScale(), ((AssetCls) parsedIssueAssetTransaction.getItem()).getScale());

            //ASSET TYPE
            assertEquals(((AssetCls) issueAssetTransaction.getItem()).getClassModel(), ((AssetCls) parsedIssueAssetTransaction.getItem()).getClassModel());

            //CHECK FEE
            assertEquals(issueAssetTransaction.getFee(), parsedIssueAssetTransaction.getFee());

            //CHECK REFERENCE
            //assertEquals(issueAssetTransaction.getReference(), parsedIssueAssetTransaction.getReference());

            //CHECK TIMESTAMP
            assertEquals(issueAssetTransaction.getTimestamp(), parsedIssueAssetTransaction.getTimestamp());
        } catch (Exception e) {
            fail("Exception while parsing transaction.");
        }

        //PARSE TRANSACTION FROM WRONG BYTES
        rawIssueAssetTransaction = new byte[issueAssetTransaction.getDataLength(Transaction.FOR_NETWORK)];

        try {
            //PARSE FROM BYTES
            TransactionFactory.getInstance().parse(rawIssueAssetTransaction, Transaction.FOR_NETWORK);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }
    }


    @SneakyThrows
    @Test
    public void processIssueAssetTransaction() {

        init();

        //CREATE SIGNATURE
        long timestamp = NTP.getTime();
        AssetCls asset = new AssetVenture(0, maker, "test", icon, image, "strontje", 0, 50000L);


        //CREATE ISSUE ASSET TRANSACTION
        IssueAssetTransaction issueAssetTransaction = new IssueAssetTransaction(maker, null, asset, FEE_POWER, timestamp);
        issueAssetTransaction.sign(maker, Transaction.FOR_NETWORK);

        try {
            assertEquals(Transaction.VALIDATE_OK, issueAssetTransaction.isValid(Transaction.FOR_NETWORK, txFlags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        issueAssetTransaction.process(gb, Transaction.FOR_NETWORK);

        LOGGER.info("asset KEY: " + asset.getKey());

        //CHECK BALANCE ISSUER
        assertEquals(BigDecimal.valueOf(50000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(asset.getKey(), db));

        //CHECK ASSET EXISTS SENDER
        long key = issueAssetTransaction.key;
        assertTrue(db.getItemAssetMap().contains(key));

        //CHECK ASSET IS CORRECT
        assertTrue(Arrays.equals(db.getItemAssetMap().get(key).toBytes(true), asset.toBytes(true)));

        //CHECK ASSET BALANCE SENDER
        assertTrue(db.getAssetBalanceMap().get(maker.getBytes(), key).a.b.compareTo(new BigDecimal(asset.getQuantity())) == 0);

        //CHECK REFERENCE SENDER
        assertEquals(issueAssetTransaction.getTimestamp(), maker.getLastTimestamp(db));
    }


    @SneakyThrows
    @Test
    public void orphanIssueAssetTransaction() {

        init();


        long timestamp = NTP.getTime();
        AssetCls asset = new AssetVenture(0, maker, "test", icon, image, "strontje", 0, 50000L);

        //CREATE ISSUE ASSET TRANSACTION
        IssueAssetTransaction issueAssetTransaction = new IssueAssetTransaction(maker, null, asset, FEE_POWER, timestamp);
        issueAssetTransaction.sign(maker, Transaction.FOR_NETWORK);
        issueAssetTransaction.process(gb, Transaction.FOR_NETWORK);
        long key = issueAssetTransaction.key;
        assertEquals(new BigDecimal(50000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(key, db));
        assertEquals(issueAssetTransaction.getTimestamp(), maker.getLastTimestamp(db));

        issueAssetTransaction.orphan(block, Transaction.FOR_NETWORK);

        //CHECK BALANCE ISSUER
        assertEquals(BigDecimal.ZERO.setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(key, db));

        //CHECK ASSET EXISTS SENDER
        assertFalse(db.getItemAssetMap().contains(key));

        //CHECK ASSET BALANCE SENDER
        assertEquals(0, db.getAssetBalanceMap().get(maker.getBytes(), key).a.b.longValue());

        //CHECK REFERENCE SENDER
        //assertEquals(issueAssetTransaction.getReference(), maker.getLastReference(db));
    }

    /**
     * Пассивный доход за время
     */
    @Test
    public void calcRoyalty() {

        init();

        int height = 10;
        long koeff = 1 + 1000000L * (long) BlockChain.ACTION_ROYALTY_PER_DAY / (long) BlockChain.BLOCKS_PER_DAY(height);

        Transaction payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), ++timestamp);
        payment.sign(maker, Transaction.FOR_NETWORK);
        //CHECK IF PAYMENT SIGNATURE IS VALID
        payment.setDC(db, Transaction.FOR_NETWORK, height, 1);
        ///payment.process(null, Transaction.FOR_NETWORK);

        payment.calcRoyalty(null, maker, koeff, false);

        long royaltyID = maker.hashCodeLong();

        Fun.Tuple3<Long, Long, Long> value = payment.peekRoyaltyData(royaltyID);

        assertEquals((Long) 0L, value.c);

        payment = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), ++timestamp);
        payment.sign(maker, Transaction.FOR_NETWORK);
        height += BlockChain.BLOCKS_PER_DAY(height);
        payment.setDC(db, Transaction.FOR_NETWORK, height, 1);

        payment.calcRoyalty(null, maker, koeff, false);

        value = payment.peekRoyaltyData(royaltyID);

        assertEquals(BlockChain.ACTION_ROYALTY_PER_DAY, value.c / 1000L);

    }

}

