package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetVenture;
import org.erachain.core.wallet.Wallet;
import org.erachain.dapp.DAPP;
import org.erachain.datachain.DCSet;
import org.erachain.ntp.NTP;
import org.junit.Test;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple5;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TransactionAmountTestOutsideClaims {

    static Logger LOGGER = LoggerFactory.getLogger(TransactionAmountTestOutsideClaims.class.getName());

    //Long Transaction.FOR_NETWORK = null;
    private final byte[] icon = new byte[]{1, 3, 4, 5, 6, 9}; // default value
    private final byte[] image = new byte[]{4, 11, 32, 23, 45, 122, 11, -45}; // default value
    long ERM_KEY = 3;
    long FEE_KEY = AssetCls.FEE_KEY;
    byte FEE_POWER = (byte) 0;
    byte[] assetReference = new byte[64];
    long timestamp = NTP.getTime();
    ExLink exLink = null;
    DAPP DAPP = null;
    Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>> balanceA;
    Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>> balanceB;
    IssueAssetTransaction issueAssetTransaction;
    AssetCls assetA;
    long keyA;
    byte[] itemAppData = null;
    long txFlags = 0L;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    byte[] accountSeed1 = Wallet.makeAccountSeedByNonce(seed, 1);
    PrivateKeyAccount recipientPK = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, accountSeed1);
    Account recipient = new Account(recipientPK.getAddress());
    byte[] accountSeed2 = Wallet.makeAccountSeedByNonce(seed, 2);
    PrivateKeyAccount recipientPK2 = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, accountSeed2);
    Account recipient2 = new Account(recipientPK2.getAddress());
    BigDecimal amount = BigDecimal.valueOf(10); //;
    String head = null;
    byte[] data = null;
    byte[] isText = new byte[]{1};
    byte[] encrypted = new byte[]{0};
    //CREATE EMPTY MEMORY DATABASE
    private DCSet db;
    private GenesisBlock gb;

    // INIT ASSETS
    @SneakyThrows
    private void init() {

        db = DCSet.createEmptyDatabaseSet(0);
        Controller.getInstance().setDCSet(db);
        gb = new GenesisBlock();
        try {
            gb.process(db, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // FEE FUND
        maker.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, db);
        maker.changeBalance(db, false, Account.BALANCE_POS_OWN, ERM_KEY, BigDecimal.valueOf(100), false, false);
        maker.changeBalance(db, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1), false, false);
        recipient.changeBalance(db, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1), false, false);
        recipient2.changeBalance(db, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1), false, false);

        assetA = new AssetVenture(AssetCls.AS_OUTSIDE_OTHER_CLAIM, maker, "AAA", icon, image, ".", 2, 0L);

        // set SCALABLE assets ++

        issueAssetTransaction = new IssueAssetTransaction(maker, assetA, (byte) 0, timestamp++, new byte[64]);
        issueAssetTransaction.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        issueAssetTransaction.process(null, Transaction.FOR_NETWORK);

        keyA = issueAssetTransaction.getAssetKey(db);
        balanceA = maker.getBalance(db, keyA);

    }


    @Test
    public void validate_R_Send_OutSideClaim() throws TxException {

        init();

        //////////////// VALIDATE

        /// invalid CLAIM
        TransactionAmount r_SendV3 = new TransactionAmount(
                maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, keyA, amount, head, data, isText, encrypted, timestamp);
        r_SendV3.sign(maker, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.INVALID_BACKWARD_ACTION);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        /// invalid CLAIM
        r_SendV3 = new TransactionAmount(
                recipientPK, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, keyA, amount, head, data, isText, encrypted, timestamp);
        r_SendV3.sign(maker, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.NO_BALANCE);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        /// invalid CLAIM
        r_SendV3 = new TransactionAmount(
                recipientPK, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, keyA, amount, head, data, isText, encrypted, timestamp);
        r_SendV3.sign(maker, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.INVALID_CLAIM_RECIPIENT);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        /////////// PROCESS
        /// CLAIM
        r_SendV3 = new TransactionAmount(
                maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, keyA, amount, head, data, isText, encrypted, timestamp);
        r_SendV3.sign(maker, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, BlockChain.SKIP_INVALID_SIGN_BEFORE, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        balanceA = maker.getBalance(db, keyA);
        balanceB = recipient.getBalance(db, keyA);
        assertEquals(BigDecimal.valueOf(-10), balanceA.a.a);
        assertEquals(BigDecimal.valueOf(-10), balanceA.a.b);
        assertEquals(BigDecimal.valueOf(0), balanceA.b.a);
        assertEquals(BigDecimal.valueOf(0), balanceA.b.b);

        assertEquals(BigDecimal.valueOf(10), balanceB.a.a);
        assertEquals(BigDecimal.valueOf(10), balanceB.a.b);
        assertEquals(BigDecimal.valueOf(0), balanceB.b.a);
        assertEquals(BigDecimal.valueOf(0), balanceB.b.b);

        assertTrue(r_SendV3.isSignatureValid());

        r_SendV3 = new TransactionAmount(
                recipientPK, exLink, DAPP, FEE_POWER, recipient2, Account.BALANCE_POS_OWN, keyA, BigDecimal.ONE, head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        balanceA = recipientPK.getBalance(db, keyA);
        balanceB = recipient2.getBalance(db, keyA);
        assertEquals(BigDecimal.valueOf(9), balanceA.a.a);
        assertEquals(BigDecimal.valueOf(9), balanceA.a.b);
        assertEquals(BigDecimal.valueOf(0), balanceA.b.a);
        assertEquals(BigDecimal.valueOf(0), balanceA.b.b);

        assertEquals(BigDecimal.valueOf(1), balanceB.a.a);
        assertEquals(BigDecimal.valueOf(1), balanceB.a.b);
        assertEquals(BigDecimal.valueOf(0), balanceB.b.a);
        assertEquals(BigDecimal.valueOf(0), balanceB.b.b);

        //////// TRY IN CLAIM to not emitter
        long credit_keyA = -keyA;
        r_SendV3 = new TransactionAmount(
                recipientPK, exLink, DAPP, FEE_POWER, recipient2, Account.BALANCE_POS_OWN, credit_keyA, BigDecimal.ONE, head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.INVALID_CLAIM_DEBT_RECIPIENT);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //////// TRY RE CLAIM to not emitter
        r_SendV3 = new TransactionAmount(
                recipientPK, exLink, DAPP, FEE_POWER, recipient2, Account.BALANCE_POS_OWN, credit_keyA, BigDecimal.ONE, head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.INVALID_CLAIM_DEBT_RECIPIENT);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //////// TRY IN CLAIM to EMITTER - NO BALANCE
        r_SendV3 = new TransactionAmount(
                recipientPK2, exLink, DAPP, FEE_POWER, maker, Account.BALANCE_POS_OWN, credit_keyA, new BigDecimal(2), head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK2, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.NO_BALANCE);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //////// TRY IN CLAIM to EMITTER - VALID
        r_SendV3 = new TransactionAmount(
                recipientPK, exLink, DAPP, FEE_POWER, maker, Account.BALANCE_POS_OWN, credit_keyA, new BigDecimal(2), head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        balanceA = recipientPK.getBalance(db, keyA);
        balanceB = maker.getBalance(db, keyA);
        assertEquals(BigDecimal.valueOf(9), balanceA.a.a);
        assertEquals(BigDecimal.valueOf(9), balanceA.a.b);
        assertEquals(BigDecimal.valueOf(-2), balanceA.b.a);
        assertEquals(BigDecimal.valueOf(-2), balanceA.b.b);

        assertEquals(BigDecimal.valueOf(-10), balanceB.a.a);
        assertEquals(BigDecimal.valueOf(-10), balanceB.a.b);
        assertEquals(BigDecimal.valueOf(2), balanceB.b.a);
        assertEquals(BigDecimal.valueOf(2), balanceB.b.b);

        //////// TRY backward IN CLAIM to EMITTER - VALID
        r_SendV3 = new TransactionAmount(
                recipientPK, exLink, DAPP, FEE_POWER, maker, Account.BALANCE_POS_OWN, credit_keyA, new BigDecimal(1), head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        balanceA = recipientPK.getBalance(db, keyA);
        balanceB = maker.getBalance(db, keyA);

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        balanceA = recipientPK.getBalance(db, keyA);
        balanceB = maker.getBalance(db, keyA);
        assertEquals(BigDecimal.valueOf(9), balanceA.a.a);
        assertEquals(BigDecimal.valueOf(9), balanceA.a.b);
        assertEquals(BigDecimal.valueOf(-1), balanceA.b.a);
        assertEquals(BigDecimal.valueOf(-1), balanceA.b.b);

        assertEquals(BigDecimal.valueOf(-10), balanceB.a.a);
        assertEquals(BigDecimal.valueOf(-10), balanceB.a.b);
        assertEquals(BigDecimal.valueOf(1), balanceB.b.a);
        assertEquals(BigDecimal.valueOf(1), balanceB.b.b);

        //////// TRY backward IN CLAIM FROM EMITTER - INVALID !
        r_SendV3 = new TransactionAmount(
                maker, exLink, DAPP, FEE_POWER, recipientPK, Account.BALANCE_POS_OWN, credit_keyA, new BigDecimal(1), head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.INVALID_CLAIM_DEBT_RECIPIENT);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //////// TRY CLOSE OVER amount CLAIM to EMITTER - INVALID
        r_SendV3 = new TransactionAmount(
                recipientPK, exLink, DAPP, FEE_POWER, maker, Account.BALANCE_POS_OWN, keyA, new BigDecimal(10), head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.NO_INCLAIM_BALANCE);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //////// TRY CLOSE CLAIM to EMITTER - VALID
        r_SendV3 = new TransactionAmount(
                recipientPK, exLink, DAPP, FEE_POWER, maker, Account.BALANCE_POS_OWN, keyA, new BigDecimal(1), head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        balanceA = recipientPK.getBalance(db, keyA);
        balanceB = maker.getBalance(db, keyA);

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        balanceA = recipientPK.getBalance(db, keyA);
        balanceB = maker.getBalance(db, keyA);
        assertEquals(BigDecimal.valueOf(9), balanceA.a.a);
        assertEquals(BigDecimal.valueOf(8), balanceA.a.b);
        assertEquals(BigDecimal.valueOf(-1), balanceA.b.a);
        assertEquals(BigDecimal.valueOf(0), balanceA.b.b);

        assertEquals(BigDecimal.valueOf(-10), balanceB.a.a);
        assertEquals(BigDecimal.valueOf(-9), balanceB.a.b);
        assertEquals(BigDecimal.valueOf(1), balanceB.b.a);
        assertEquals(BigDecimal.valueOf(0), balanceB.b.b);


        /* **********************
         */

        //////// TRY CLOSE CLAIM to EMITTER - VALID
        r_SendV3 = new TransactionAmount(
                recipientPK2, exLink, DAPP, FEE_POWER, maker, Account.BALANCE_POS_OWN, credit_keyA, new BigDecimal(1), head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK2, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        //////// TRY CLOSE CLAIM to EMITTER - VALID
        r_SendV3 = new TransactionAmount(
                recipientPK2, exLink, DAPP, FEE_POWER, maker, Account.BALANCE_POS_OWN, keyA, new BigDecimal(1), head, data, isText, encrypted, timestamp);
        r_SendV3.sign(recipientPK2, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);

        balanceA = recipientPK2.getBalance(db, keyA);
        balanceB = maker.getBalance(db, keyA);

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        balanceA = recipientPK2.getBalance(db, keyA);
        balanceB = maker.getBalance(db, keyA);
        assertEquals(BigDecimal.valueOf(1), balanceA.a.a);
        assertEquals(BigDecimal.valueOf(0), balanceA.a.b);
        assertEquals(BigDecimal.valueOf(-1), balanceA.b.a);
        assertEquals(BigDecimal.valueOf(0), balanceA.b.b);

        assertEquals(BigDecimal.valueOf(-10), balanceB.a.a);
        assertEquals(BigDecimal.valueOf(-8), balanceB.a.b);
        assertEquals(BigDecimal.valueOf(2), balanceB.b.a);
        assertEquals(BigDecimal.valueOf(0), balanceB.b.b);

    }

}
