package org.erachain.core.transaction;

//package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Base58;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetVenture;
import org.erachain.dapp.DAPP;
import org.erachain.datachain.DCSet;
import org.erachain.ntp.NTP;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.*;


public class TransactionAmountTest01 {

    static Logger LOGGER = LoggerFactory.getLogger(TransactionAmountTest01.class.getName());
    private final byte[] icon = new byte[]{1, 3, 4, 5, 6, 9}; // default value
    private final byte[] image = new byte[]{4, 11, 32, 23, 45, 122, 11, -45}; // default value
    private final long seqNo = 0;
    //Long Transaction.FOR_NETWORK = null;
    long assetKeyTest = 1011;
    long ERA_KEY = AssetCls.ERA_KEY;
    long FEE_KEY = AssetCls.FEE_KEY;
    byte FEE_POWER = (byte) 0;
    byte[] assetReference = new byte[64];
    long timestamp = NTP.getTime();
    long dbRef = 0L;
    ExLink exLink = null;
    DAPP DAPP = null;
    byte[] itemAppData = null;
    long txFlags = 0L;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");
    BigDecimal amount = BigDecimal.valueOf(10).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
    String head = "headdd";
    byte[] data = "test123!".getBytes();
    byte[] isText = new byte[]{1};
    byte[] encrypted = new byte[]{0};
    Block block;
    //CREATE EMPTY MEMORY DATABASE
    private DCSet db;
    private GenesisBlock gb;

    // INIT ASSETS
    private void init() {

        System.setProperty("qwe", "qw");

        db = DCSet.createEmptyDatabaseSet(0);
        Controller.getInstance().setDCSet(db);
        gb = new GenesisBlock();
        block = gb;

        try {
            gb.process(db, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // FEE FUND
        maker.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, db);
        maker.changeBalance(db, false, Account.BALANCE_POS_OWN, ERA_KEY, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        maker.changeBalance(db, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);

    }

    @Test
    public void scaleTestNew() {
        BigDecimal amount = new BigDecimal("1234000078123456.0012345678");
        BigDecimal amountZ = amount.stripTrailingZeros();
        int scale = amountZ.scale();
        byte[] bytes = amountZ.unscaledValue().toByteArray();
        assertEquals(amount, new BigDecimal(new BigInteger(bytes), scale));

    }

    @Test
    public void scaleTest() {

        init();

        Integer bbb = 31;
        assertEquals("11111", Integer.toBinaryString(bbb));
        assertEquals("10000000", Integer.toBinaryString(128));
        assertEquals((byte) 128, (byte) -128);

        byte noData = (byte) 128;
        //assertEquals((byte)-1, (byte)128);
        assertEquals((byte) 128, (byte) -128);
        //assertEquals(org.erachain.core.transaction.PublicKeyAccount.NO_DATA_MASK));

        BigDecimal amountTest = new BigDecimal("123456781234567812345678");
        BigDecimal amountForParse = new BigDecimal("1234567812345678");
        BigDecimal amountBase;
        BigDecimal amount;
        BigDecimal amount_result;

        AssetCls asset23 = new AssetVenture(0, maker, "AAA", icon, image, ".", TransactionAmount.maxSCALE, 0L);
        long assetKey23 = asset23.getKey();

        //int shift = 64;
        int scale;
        int different_scale;
        int fromScale = TransactionAmount.SCALE_MASK_HALF + BlockChain.AMOUNT_DEFAULT_SCALE - 1;
        int toScale = BlockChain.AMOUNT_DEFAULT_SCALE - TransactionAmount.SCALE_MASK_HALF;
        assertEquals("11111", Integer.toBinaryString(fromScale - toScale));

        TransactionAmount r_Send;
        byte[] raw_r_Send;
        TransactionAmount r_Send_2;
        for (scale = fromScale; scale >= toScale; scale--) {

            amount = amountTest.scaleByPowerOfTen(-scale);

            // TO BASE
            different_scale = scale - BlockChain.AMOUNT_DEFAULT_SCALE;

            if (different_scale != 0) {
                // to DEFAUTL base 8 decimals
                amountBase = amount.scaleByPowerOfTen(different_scale);
                if (different_scale < 0)
                    different_scale += TransactionAmount.SCALE_MASK + 1;

            } else {
                amountBase = amount;
            }

            assertEquals(8, amountBase.scale());

            // CHECK ACCURACY of AMOUNT
            int accuracy = different_scale & TransactionAmount.SCALE_MASK;
            String sss = Integer.toBinaryString(accuracy);
            if (scale == 24)
                assertEquals("10000", sss);
            else if (scale < 9)
                assertTrue(true);

            if (accuracy > 0) {
                if (accuracy >= TransactionAmount.SCALE_MASK_HALF) {
                    accuracy -= TransactionAmount.SCALE_MASK + 1;
                }
                // RESCALE AMOUNT
                amount_result = amountBase.scaleByPowerOfTen(-accuracy);
            } else {
                amount_result = amountBase;
            }

            assertEquals(amount, amount_result);

            // TRY PARSE - PRICISION must be LESS
            amount = amountForParse.scaleByPowerOfTen(-scale);

            r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey23,
                    amount,
                    "", null, isText, encrypted, timestamp
            );
            r_Send.sign(maker, Transaction.FOR_NETWORK);
            // need for check !
            r_Send.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
            assertTrue(r_Send.isSignatureValid());
            //r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1);
            r_Send.setDC(db, Transaction.FOR_NETWORK, this.gb.heightBlock, 2, true);
            try {
                assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
            } catch (TxException e) {
                throw new RuntimeException(e);
            }

            raw_r_Send = r_Send.toBytes(Transaction.FOR_NETWORK);

            r_Send_2 = null;
            try {
                r_Send_2 = (TransactionAmount) TransactionFactory.getInstance().parse(raw_r_Send, Transaction.FOR_NETWORK);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                assertNull(true);
            }

            // FOR DEBUG POINT
            if (!r_Send.getAmount().equals(r_Send_2.getAmount())) {
                try {
                    r_Send_2 = (TransactionAmount) TransactionFactory.getInstance().parse(raw_r_Send, Transaction.FOR_NETWORK);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }


            assertEquals(r_Send.getAmount(), r_Send_2.getAmount());

            r_Send_2.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
            assertTrue(r_Send_2.isSignatureValid());
            r_Send_2.setDC(db, Transaction.FOR_NETWORK, gb.heightBlock, 1, true);
            try {
                assertEquals(r_Send_2.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
            } catch (TxException e) {
                throw new RuntimeException(e);
            }

            assertArrayEquals(r_Send.getSignature(), r_Send_2.getSignature());

            // NAGATIVE AMOUNT
            r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, ERA_KEY,
                    amount.negate(),
                    head, data, isText, encrypted, timestamp
            );
            r_Send.sign(maker, Transaction.FOR_NETWORK);

            raw_r_Send = r_Send.toBytes(Transaction.FOR_NETWORK);

            r_Send_2 = null;
            try {
                r_Send_2 = (TransactionAmount) TransactionFactory.getInstance().parse(raw_r_Send, Transaction.FOR_NETWORK);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }

            assertEquals(r_Send.getAmount(), r_Send_2.getAmount());

        }

        /////////////////////// VALIDATE
        int thisScale = 5;
        AssetCls assetA = new AssetVenture(0, maker, "AAA", icon, image, ".", thisScale, 0L);
        assetA.insertToMap(db, 0L);
        long assetKey = assetA.getKey();
        head = "";
        data = null;

        // IS VALID
        BigDecimal bal_A_keyA = amountForParse.scaleByPowerOfTen(-thisScale);
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                bal_A_keyA,
                head, data, isText, encrypted, timestamp
        );
        r_Send.sign(maker, Transaction.FOR_NETWORK);
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        // INVALID
        bal_A_keyA = amountForParse.scaleByPowerOfTen(-thisScale - 1);
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                bal_A_keyA,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.AMOUNT_SCALE_WRONG);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        ///////////////////////
        // INVALID
        BigDecimal amountInvalid = amountTest;
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey + 1,
                amountInvalid,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.ITEM_ASSET_NOT_EXIST);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        // INVALID
        assetA = new AssetVenture(0, maker, "AAA", icon, image, ".", 30, 0L);
        assetA.insertToMap(db, 0L);
        assetKey = assetA.getKey();

        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                amountInvalid,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.AMOUNT_LENGHT_SO_LONG);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                amountInvalid.negate(),
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.AMOUNT_LENGHT_SO_LONG);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        // INVALID
        amountInvalid = amountForParse.scaleByPowerOfTen(-fromScale - 1);
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                amountInvalid,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.AMOUNT_SCALE_WRONG);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        amountInvalid = amountForParse.scaleByPowerOfTen(-toScale + 1);
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                amountInvalid,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.AMOUNT_SCALE_WRONG);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    public void scaleTestBIG() {

        init();

        BigDecimal amountTest = new BigDecimal("12345678123456781234560000000000");
        BigDecimal amountForParse = new BigDecimal("1234560000000000");
        BigDecimal amountBase;
        BigDecimal amount;
        BigDecimal amount_result;


        //int shift = 64;
        int scale;
        int different_scale;
        int fromScale = -5;
        int toScale = BlockChain.AMOUNT_DEFAULT_SCALE - TransactionAmount.SCALE_MASK_HALF;

        AssetCls assetBIG = new AssetVenture(0, maker, "AAA", icon, image, ".", fromScale + 2, 0L);
        assetBIG.insertToMap(db, BlockChain.START_KEY_UP_ITEMS);
        long assetKeyBIG = assetBIG.getKey();

        TransactionAmount r_Send;
        byte[] raw_r_Send;
        TransactionAmount r_Send_2;
        for (scale = fromScale; scale >= toScale; scale--) {

            amount = amountTest.scaleByPowerOfTen(-scale);

            // TO BASE
            different_scale = scale - BlockChain.AMOUNT_DEFAULT_SCALE;

            if (different_scale != 0) {
                // to DEFAUTL base 8 decimals
                amountBase = amount.scaleByPowerOfTen(different_scale);
                if (different_scale < 0)
                    different_scale += TransactionAmount.SCALE_MASK + 1;

            } else {
                amountBase = amount;
            }

            assertEquals(8, amountBase.scale());

            // CHECK ACCURACY of AMOUNT
            int accuracy = different_scale & TransactionAmount.SCALE_MASK;
            String sss = Integer.toBinaryString(accuracy);
            if (scale == 24)
                assertEquals("10000", sss);
            else if (scale < 9)
                assertTrue(true);

            if (accuracy > 0) {
                if (accuracy >= TransactionAmount.SCALE_MASK_HALF) {
                    accuracy -= TransactionAmount.SCALE_MASK + 1;
                }
                // RESCALE AMOUNT
                amount_result = amountBase.scaleByPowerOfTen(-accuracy);
            } else {
                amount_result = amountBase;
            }

            assertEquals(amount, amount_result);

            // TRY PARSE - PRICISION must be LESS
            amount = amountForParse.scaleByPowerOfTen(-scale);

            r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKeyBIG,
                    amount,
                    "", null, isText, encrypted, timestamp
            );
            r_Send.sign(maker, Transaction.FOR_NETWORK);
            r_Send.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
            assertTrue(r_Send.isSignatureValid());
            //r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1);
            r_Send.setDC(db, Transaction.FOR_NETWORK, gb.heightBlock, 1, true);
            try {
                assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
            } catch (TxException e) {
                throw new RuntimeException(e);
            }

            raw_r_Send = r_Send.toBytes(Transaction.FOR_NETWORK);

            r_Send_2 = null;
            try {
                r_Send_2 = (TransactionAmount) TransactionFactory.getInstance().parse(raw_r_Send, Transaction.FOR_NETWORK);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                assertNull(true);
            }

            // FOR DEBUG POINT
            if (!r_Send.getAmount().equals(r_Send_2.getAmount())) {
                try {
                    r_Send_2 = (TransactionAmount) TransactionFactory.getInstance().parse(raw_r_Send, Transaction.FOR_NETWORK);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }


            assertEquals(r_Send.getAmount(), r_Send_2.getAmount());

            r_Send_2.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
            assertTrue(r_Send_2.isSignatureValid());
            r_Send_2.setDC(db, Transaction.FOR_NETWORK, gb.heightBlock, 1, true);
            try {
                assertEquals(r_Send_2.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
            } catch (TxException e) {
                throw new RuntimeException(e);
            }

            assertArrayEquals(r_Send.getSignature(), r_Send_2.getSignature());

            // NAGATIVE AMOUNT
            r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, ERA_KEY,
                    amount.negate(),
                    head, data, isText, encrypted, timestamp
            );
            r_Send.sign(maker, Transaction.FOR_NETWORK);

            raw_r_Send = r_Send.toBytes(Transaction.FOR_NETWORK);

            r_Send_2 = null;
            try {
                r_Send_2 = (TransactionAmount) TransactionFactory.getInstance().parse(raw_r_Send, Transaction.FOR_NETWORK);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }

            assertEquals(r_Send.getAmount(), r_Send_2.getAmount());

        }

        /////////////////////// VALIDATE
        int thisScale = 5;
        AssetCls assetA = new AssetVenture(0, maker, "AAA", icon, image, ".", thisScale, 0L);
        assetA.insertToMap(db, 0L);
        long assetKey = assetA.getKey();
        head = "";
        data = null;

        // IS VALID
        BigDecimal bal_A_keyA = amountForParse.scaleByPowerOfTen(-thisScale);
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                bal_A_keyA,
                head, data, isText, encrypted, timestamp
        );
        r_Send.sign(maker, Transaction.FOR_NETWORK);
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        // VALID because trailing ZERO - amount.stripTrailingZeros()
        bal_A_keyA = amountForParse.scaleByPowerOfTen(-thisScale - 1);
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                bal_A_keyA,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        ///////////////////////
        // INVALID
        BigDecimal amountInvalid = amountTest;
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey + 1,
                amountInvalid,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.ITEM_ASSET_NOT_EXIST);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        // INVALID
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                amountInvalid,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.AMOUNT_LENGHT_SO_LONG);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                amountInvalid.negate(),
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.AMOUNT_LENGHT_SO_LONG);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        // INVALID
        amountInvalid = amountForParse.scaleByPowerOfTen(-fromScale - 1);
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                amountInvalid,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        amountInvalid = amountForParse.scaleByPowerOfTen(-toScale + 1);
        r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, assetKey,
                amountInvalid,
                head, data, isText, encrypted, timestamp
        );
        r_Send.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, 0L), Transaction.AMOUNT_SCALE_WRONG);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

    }

    @SneakyThrows
    @Test
    public void validateMessageTransactionV3() {

        //Integer bbb = -128;
        //assertEquals("1111", Integer.toBinaryString(bbb));

        init();

        /// MESSAGE + AMOUNT
        TransactionAmount r_SendV3 = new TransactionAmount(
                maker, exLink, DAPP, FEE_POWER,
                recipient,
                Account.BALANCE_POS_OWN, ERA_KEY,
                amount,
                head, data,
                isText,
                encrypted,
                1547048069000L//timestamp,
        );
        r_SendV3.sign(maker, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, BlockChain.ALL_VALID_BEFORE, 1, true);
        assertTrue(r_SendV3.isSignatureValid());

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK); //);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        assertEquals(maker.getLastTimestamp(db)[0], gb.getTimestamp());
        r_SendV3.process(gb, Transaction.FOR_NETWORK);
        assertEquals(maker.getLastTimestamp(db)[0], timestamp);

        //assertEquals(BigDecimal.valueOf(1).subtract(r_SendV3.getFee()).setScale(BlockChain.AMOUNT_DEDAULT_SCALE), maker.getBalanceUSE(FEE_KEY, db));
        assertEquals(BigDecimal.valueOf(1090).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(ERA_KEY, db));
        assertEquals(BigDecimal.valueOf(1010).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), recipient.getBalanceUSE(ERA_KEY, db));

        byte[] rawMessageTransactionV3 = r_SendV3.toBytes(Transaction.FOR_NETWORK);
        int dd = r_SendV3.getDataLength(Transaction.FOR_NETWORK);
        assertEquals(rawMessageTransactionV3.length, r_SendV3.getDataLength(Transaction.FOR_NETWORK));


        TransactionAmount messageTransactionV3_2 = null;
        try {
            messageTransactionV3_2 = (TransactionAmount) TransactionFactory.getInstance().parse(rawMessageTransactionV3, Transaction.FOR_NETWORK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        assertEquals(new String(r_SendV3.getDataMessage()), new String(messageTransactionV3_2.getDataMessage()));
        assertEquals(r_SendV3.getCreator(), messageTransactionV3_2.getCreator());
        assertEquals(r_SendV3.getRecipient(), messageTransactionV3_2.getRecipient());
        assertEquals(r_SendV3.getKey(), messageTransactionV3_2.getKey());
        assertEquals(r_SendV3.getAmount(), messageTransactionV3_2.getAmount());
        assertEquals(r_SendV3.isEncrypted(), messageTransactionV3_2.isEncrypted());
        assertEquals(r_SendV3.isText(), messageTransactionV3_2.isText());

        messageTransactionV3_2.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertTrue(messageTransactionV3_2.isSignatureValid());

        //// MESSAGE ONLY
        r_SendV3.orphan(block, Transaction.FOR_NETWORK);
        assertEquals(maker.getLastTimestamp(db)[0], gb.getTimestamp());

        r_SendV3 = new TransactionAmount(
                maker, exLink, DAPP, FEE_POWER,
                recipient,
                Account.BALANCE_POS_OWN, ERA_KEY,
                null,
                head, data,
                isText,
                encrypted,
                timestamp
        );
        r_SendV3.sign(maker, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, BlockChain.ALL_VALID_BEFORE, 1, true);
        assertTrue(r_SendV3.isSignatureValid());

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK); //Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_SendV3.process(gb, Transaction.FOR_NETWORK);
        assertEquals(maker.getLastTimestamp(db)[0], timestamp);

        //assertEquals(BigDecimal.valueOf(1).subtract(r_SendV3.getFee()).setScale(BlockChain.AMOUNT_DEDAULT_SCALE), maker.getBalanceUSE(FEE_KEY, db));
        assertEquals(BigDecimal.valueOf(1100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(ERA_KEY, db));
        assertEquals(BigDecimal.valueOf(1000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), recipient.getBalanceUSE(ERA_KEY, db));

        rawMessageTransactionV3 = r_SendV3.toBytes(Transaction.FOR_NETWORK);
        dd = r_SendV3.getDataLength(Transaction.FOR_NETWORK);
        assertEquals(rawMessageTransactionV3.length, r_SendV3.getDataLength(Transaction.FOR_NETWORK));


        messageTransactionV3_2 = null;
        try {
            messageTransactionV3_2 = (TransactionAmount) TransactionFactory.getInstance().parse(rawMessageTransactionV3, Transaction.FOR_NETWORK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        assertEquals(new String(r_SendV3.getDataMessage()), new String(messageTransactionV3_2.getDataMessage()));
        assertEquals(r_SendV3.getCreator(), messageTransactionV3_2.getCreator());
        assertEquals(r_SendV3.getRecipient(), messageTransactionV3_2.getRecipient());
        assertEquals(0, messageTransactionV3_2.getKey());
        assertEquals(r_SendV3.getAmount(), messageTransactionV3_2.getAmount());
        assertEquals(r_SendV3.isEncrypted(), messageTransactionV3_2.isEncrypted());
        assertEquals(r_SendV3.isText(), messageTransactionV3_2.isText());

        messageTransactionV3_2.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertTrue(messageTransactionV3_2.isSignatureValid());


        //// AMOUNT ONLY
        r_SendV3.orphan(block, Transaction.FOR_NETWORK);
        assertEquals(maker.getLastTimestamp(db)[0], gb.getTimestamp());

        r_SendV3 = new TransactionAmount(
                maker, exLink, DAPP, FEE_POWER,
                recipient,
                Account.BALANCE_POS_OWN, ERA_KEY,
                amount,
                "", null,
                null,
                null,
                timestamp
        );
        r_SendV3.sign(maker, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, BlockChain.ALL_VALID_BEFORE, 1, true);
        assertTrue(r_SendV3.isSignatureValid());

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        //assertEquals(BigDecimal.valueOf(1).subtract(r_SendV3.getFee()).setScale(BlockChain.AMOUNT_DEDAULT_SCALE), maker.getBalanceUSE(FEE_KEY, db));
        assertEquals(BigDecimal.valueOf(1090).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(ERA_KEY, db));
        assertEquals(BigDecimal.valueOf(1010).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), recipient.getBalanceUSE(ERA_KEY, db));

        rawMessageTransactionV3 = r_SendV3.toBytes(Transaction.FOR_NETWORK);
        dd = r_SendV3.getDataLength(Transaction.FOR_NETWORK);
        assertEquals(rawMessageTransactionV3.length, r_SendV3.getDataLength(Transaction.FOR_NETWORK));


        messageTransactionV3_2 = null;
        try {
            messageTransactionV3_2 = (TransactionAmount) TransactionFactory.getInstance().parse(rawMessageTransactionV3, Transaction.FOR_NETWORK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        assertNull(r_SendV3.getDataMessage());
        assertNull(messageTransactionV3_2.getDataMessage());
        assertEquals(r_SendV3.getCreator(), messageTransactionV3_2.getCreator());
        assertEquals(r_SendV3.getRecipient(), messageTransactionV3_2.getRecipient());
        assertEquals(r_SendV3.getKey(), messageTransactionV3_2.getKey());
        assertEquals(r_SendV3.getAmount(), messageTransactionV3_2.getAmount());

        messageTransactionV3_2.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertTrue(messageTransactionV3_2.isSignatureValid());

        //// EMPTY - NOT AMOUNT and NOT TEXT
        r_SendV3.orphan(block, Transaction.FOR_NETWORK);

        r_SendV3 = new TransactionAmount(
                maker, exLink, DAPP, FEE_POWER,
                recipient,
                Account.BALANCE_POS_OWN, ERA_KEY,
                null,
                null, null,
                null,
                null,
                timestamp
        );
        r_SendV3.sign(maker, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, BlockChain.SKIP_INVALID_SIGN_BEFORE, 1, true);
        assertTrue(r_SendV3.isSignatureValid());

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        //assertEquals(BigDecimal.valueOf(1).subtract(r_SendV3.getFee()).setScale(BlockChain.AMOUNT_DEDAULT_SCALE), maker.getBalanceUSE(FEE_KEY, db));
        assertEquals(BigDecimal.valueOf(1100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(ERA_KEY, db));
        assertEquals(BigDecimal.valueOf(1000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), recipient.getBalanceUSE(ERA_KEY, db));

        rawMessageTransactionV3 = r_SendV3.toBytes(Transaction.FOR_NETWORK);
        dd = r_SendV3.getDataLength(Transaction.FOR_NETWORK);
        assertEquals(rawMessageTransactionV3.length, r_SendV3.getDataLength(Transaction.FOR_NETWORK));


        messageTransactionV3_2 = null;
        try {
            messageTransactionV3_2 = (TransactionAmount) TransactionFactory.getInstance().parse(rawMessageTransactionV3, Transaction.FOR_NETWORK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        assertNull(r_SendV3.getDataMessage());
        assertNull(messageTransactionV3_2.getDataMessage());
        assertEquals(r_SendV3.getCreator(), messageTransactionV3_2.getCreator());
        assertEquals(r_SendV3.getRecipient(), messageTransactionV3_2.getRecipient());
        assertEquals(0, messageTransactionV3_2.getKey());
        assertEquals(r_SendV3.getAmount(), messageTransactionV3_2.getAmount());

        messageTransactionV3_2.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertTrue(messageTransactionV3_2.isSignatureValid());

        // NEGATE for test HOLD ///////////////////
        amount = amount.negate();
        recipient.changeBalance(db, false, Account.BALANCE_POS_OWN, -ERA_KEY, amount.negate(), false, false);
        /// MESSAGE + AMOUNT
        r_SendV3 = new TransactionAmount(
                maker, exLink, DAPP, FEE_POWER,
                recipient,
                Account.BALANCE_POS_OWN, -ERA_KEY,
                amount,
                head, data,
                isText,
                encrypted,
                ++timestamp
        );
        r_SendV3.sign(maker, Transaction.FOR_NETWORK);
        r_SendV3.setDC(db, Transaction.FOR_NETWORK, BlockChain.ALL_VALID_BEFORE, 1, true);
        assertTrue(r_SendV3.isSignatureValid());

        try {
            assertEquals(r_SendV3.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK); //ransaction.VALIDATE_OK);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        r_SendV3.process(gb, Transaction.FOR_NETWORK);

        //assertEquals(BigDecimal.valueOf(1).subtract(r_SendV3.getFee()).setScale(BlockChain.AMOUNT_DEDAULT_SCALE), maker.getBalanceUSE(FEE_KEY, db));
        assertEquals(BigDecimal.valueOf(1100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(ERA_KEY, db));
        assertEquals(BigDecimal.valueOf(1010).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), recipient.getBalanceUSE(ERA_KEY, db));

        dd = r_SendV3.getDataLength(Transaction.FOR_NETWORK);
        assertEquals(rawMessageTransactionV3.length, r_SendV3.getDataLength(Transaction.FOR_NETWORK));

        try {
            messageTransactionV3_2 = (TransactionAmount) TransactionFactory.getInstance().parse(rawMessageTransactionV3, Transaction.FOR_NETWORK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        assertEquals(new String(r_SendV3.getDataMessage()), new String(messageTransactionV3_2.getDataMessage()));
        assertEquals(r_SendV3.getCreator(), messageTransactionV3_2.getCreator());
        assertEquals(r_SendV3.getRecipient(), messageTransactionV3_2.getRecipient());
        assertEquals(r_SendV3.getKey(), messageTransactionV3_2.getKey());
        assertEquals(r_SendV3.getAmount(), messageTransactionV3_2.getAmount());
        assertEquals(r_SendV3.isEncrypted(), messageTransactionV3_2.isEncrypted());
        assertEquals(r_SendV3.isText(), messageTransactionV3_2.isText());

        messageTransactionV3_2.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertTrue(messageTransactionV3_2.isSignatureValid());

    }


    @Test
    public void makeMessageTransactionV3_DISCREDIR_ADDRESSES() {

        // HPftF6gmSH3mn9dKSAwSEoaxW2Lb6SVoguhKyHXbyjr7 -
        PublicKeyAccount maker = new PublicKeyAccount(Crypto.ED25519_SYSTEM, BlockChain.DISCREDIR_ADDRESSES[0]);
        Account recipient = Account.makeFromOld("7R2WUFaS7DF2As6NKz13Pgn9ij4sFw6ymZ");
        BigDecimal amount = BigDecimal.valueOf(49800).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);

        long era_key = 1L;
        /// DISCREDIR_ADDRESSES
        TransactionAmount r_Send = new TransactionAmount(maker, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, era_key, amount, "", null, isText, encrypted, timestamp);

        byte[] data = r_Send.toBytes(Transaction.FOR_NETWORK);
        int port = BlockChain.NETWORK_PORT;
        data = Bytes.concat(data, Ints.toByteArray(port));
        byte[] digest = Crypto.getInstance().digest(data);
        digest = Bytes.concat(digest, digest);

        TransactionAmount r_SendSigned = new TransactionAmount(maker, FEE_POWER, recipient, Account.BALANCE_POS_OWN, era_key, amount,
                "", null, isText, encrypted, timestamp, 1L, digest);
        String raw = Base58.encode(r_SendSigned.toBytes(Transaction.FOR_NETWORK));
        System.out.print(raw);

        //DCSet dcSet = DCSet.getInstance();
        //assertEquals(r_SendSigned.isSignatureValid(dcSet), true);
        //r_SendSigned.setDC(dcSet, false);
        //assertEquals(r_SendSigned.isValid(dcSet, null), Transaction.VALIDATE_OK);
        //Controller.getInstance().broadcastTransaction(r_SendSigned);

    }

}
