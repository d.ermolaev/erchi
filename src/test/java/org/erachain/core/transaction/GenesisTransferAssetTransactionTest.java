package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.database.IDB;
import org.erachain.datachain.DCSet;
import org.erachain.settings.Settings;
import org.erachain.utils.SimpleFileVisitorForRecursiveFolderDeletion;
import org.junit.Test;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;

import static org.junit.Assert.*;

@Slf4j
public class GenesisTransferAssetTransactionTest {

    int[] TESTED_DBS = new int[]{
            IDB.DBS_MAP_DB,
            IDB.DBS_ROCK_DB};

    long ERA_KEY = AssetCls.ERA_KEY;
    long FEE_KEY = AssetCls.FEE_KEY;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");
    Account debtor = new Account("4YuDWoBZ6LpmZxyR1Nky82orpy3H");

    BigDecimal amount = BigDecimal.valueOf(10).setScale(AssetCls.ERA_SCALE);
    String title = "title probe";
    String tags = "tag";
    String message = "test123!";

    byte[] bytes;

    int[] TESTED_DEAL = new int[]{
            Transaction.FOR_NETWORK,
            Transaction.FOR_DB_RECORD
    };
    BlockChain chain;
    private Controller cntrl;
    private DCSet dcSet;
    private GenesisBlock gb;

    @SneakyThrows
    private void init(int dbs) {

        logger.info(" ********** open DBS: " + dbs);

        File tempDir = new File(Settings.getInstance().getDataTempDir());
        try {
            Files.walkFileTree(tempDir.toPath(), new SimpleFileVisitorForRecursiveFolderDeletion());
        } catch (Throwable e) {
        }

        dcSet = DCSet.createEmptyHardDatabaseSetWithFlush(tempDir.getPath(), dbs);

        chain = new BlockChain(dcSet);
        gb = chain.getGenesisBlock();
    }

    @SneakyThrows
    @Test
    public void parse() {

        for (int forDeal : TESTED_DEAL) {
            System.out.println("*** DEAL: " + forDeal + " ***");

            GenesisTransferAssetTransaction genSend1 = new GenesisTransferAssetTransaction(
                    recipient,
                    ERA_KEY,
                    amount.negate(),
                    title, tags, message);
            assertEquals(Transaction.INVALID_AMOUNT, genSend1.isValid(forDeal, 0L));

            genSend1 = new GenesisTransferAssetTransaction(
                    recipient,
                    -ERA_KEY,
                    amount,
                    title, tags, message);
            assertEquals(Transaction.INVALID_ITEM_KEY, genSend1.isValid(forDeal, 0L));

            genSend1 = new GenesisTransferAssetTransaction(
                    recipient,
                    ERA_KEY,
                    amount,
                    title, tags, message);

            bytes = genSend1.toBytes(forDeal);
            assertEquals(bytes.length, genSend1.getDataLength(forDeal));


            GenesisTransferAssetTransaction genSend2 = null;
            try {
                genSend2 = (GenesisTransferAssetTransaction) TransactionFactory.getInstance().parse(bytes, forDeal);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            assertEquals(genSend1.getCreator(), genSend2.getCreator());
            assertEquals(genSend1.getRecipient(), genSend2.getRecipient());
            assertEquals(1L, genSend2.getKey());
            assertEquals(genSend1.getAmount(), genSend2.getAmount().setScale(AssetCls.ERA_SCALE));
            assertEquals(genSend1.getTitle(), genSend2.getTitle());
            assertEquals(genSend1.getTags(), genSend2.getTags());
            assertArrayEquals(genSend1.getDataMessage(), genSend2.getDataMessage());

            assertTrue(genSend2.isSignatureValid());
            assertArrayEquals(genSend1.toBytes(forDeal), genSend2.toBytes(forDeal));

            /////////////////////////////

            genSend1 = new GenesisTransferAssetTransaction(
                    recipient,
                    ERA_KEY,
                    amount,
                    null, null, null);

            bytes = genSend1.toBytes(forDeal);
            assertEquals(bytes.length, genSend1.getDataLength(forDeal));


            genSend2 = null;
            try {
                genSend2 = (GenesisTransferAssetTransaction) TransactionFactory.getInstance().parse(bytes, forDeal);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            assertEquals(genSend1.getCreator(), genSend2.getCreator());
            assertEquals(genSend1.getRecipient(), genSend2.getRecipient());
            assertEquals(1L, genSend2.getKey());
            assertEquals(genSend1.getAmount(), genSend2.getAmount().setScale(AssetCls.ERA_SCALE));
            assertEquals(genSend1.getTitle(), genSend2.getTitle());
            assertEquals(genSend1.getTags(), genSend2.getTags());
            assertEquals(null, genSend2.tags);

            // as null
            assertEquals(genSend1.getDataMessage(), genSend2.getDataMessage());

            assertTrue(genSend2.isSignatureValid());

            assertArrayEquals(genSend1.toBytes(forDeal), genSend2.toBytes(forDeal));


            /////////////////////// DEBT

            genSend1 = new GenesisTransferAssetTransaction(
                    recipient,
                    ERA_KEY,
                    amount,
                    debtor,
                    title, tags, message);

            bytes = genSend1.toBytes(forDeal);
            assertEquals(bytes.length, genSend1.getDataLength(forDeal));

            genSend2 = null;
            try {
                genSend2 = (GenesisTransferAssetTransaction) TransactionFactory.getInstance().parse(bytes, forDeal);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            assertEquals(genSend1.getCreator(), genSend2.getCreator());
            assertEquals(genSend1.getRecipient(), genSend2.getRecipient());
            assertEquals(1L, genSend2.getKey());
            assertEquals(genSend1.getAmount(), genSend2.getAmount().setScale(AssetCls.ERA_SCALE));
            assertEquals(genSend1.getTitle(), genSend2.getTitle());
            assertEquals(genSend1.getTags(), genSend2.getTags());
            assertArrayEquals(genSend1.getDataMessage(), genSend2.getDataMessage());

            assertTrue(genSend2.isSignatureValid());
            assertArrayEquals(genSend1.toBytes(forDeal), genSend2.toBytes(forDeal));

            /////////////////////////////

            genSend1 = new GenesisTransferAssetTransaction(
                    recipient,
                    ERA_KEY,
                    amount,
                    debtor,
                    null, null, null);

            bytes = genSend1.toBytes(forDeal);
            assertEquals(bytes.length, genSend1.getDataLength(forDeal));


            genSend2 = null;
            try {
                genSend2 = (GenesisTransferAssetTransaction) TransactionFactory.getInstance().parse(bytes, forDeal);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            assertEquals(genSend1.getCreator(), genSend2.getCreator());
            assertEquals(genSend1.getRecipient(), genSend2.getRecipient());
            assertEquals(1L, genSend2.getKey());
            assertEquals(genSend1.getAmount(), genSend2.getAmount().setScale(AssetCls.ERA_SCALE));
            assertEquals(genSend1.getTitle(), genSend2.getTitle());
            assertEquals(genSend1.getTags(), genSend2.getTags());
            assertEquals(null, genSend2.tags);

            // as null
            assertEquals(genSend1.getDataMessage(), genSend2.getDataMessage());

            assertTrue(genSend2.isSignatureValid());

            assertArrayEquals(genSend1.toBytes(forDeal), genSend2.toBytes(forDeal));

        }
    }

    @Test
    public void processBody() {

        for (int dbs : TESTED_DBS) {

            init(dbs);

            assertEquals(recipient.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_OWN).b,
                    BigDecimal.ZERO);

            BigDecimal initAmount = GenesisBlock.CREATOR.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_OWN).b;
            GenesisTransferAssetTransaction genSend1 = new GenesisTransferAssetTransaction(
                    recipient,
                    ERA_KEY,
                    amount,
                    title, tags, message);

            genSend1.setDC(dcSet);
            try {
                genSend1.processBody(gb, Transaction.FOR_NETWORK);
            } catch (TxException e) {
                throw new RuntimeException(e);
            }

            /////////////////////////////

            assertEquals(recipient.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_OWN).b,
                    amount);

            assertEquals(GenesisBlock.CREATOR.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_OWN).b,
                    initAmount.subtract(amount));


            /////////////////////// DEBT

            assertEquals(recipient.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_DEBT).b,
                    BigDecimal.ZERO);
            assertEquals(debtor.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_DEBT).b,
                    BigDecimal.ZERO);

            genSend1 = new GenesisTransferAssetTransaction(
                    recipient,
                    ERA_KEY,
                    amount,
                    debtor,
                    title, tags, message);

            genSend1.setDC(dcSet);
            try {
                genSend1.processBody(gb, Transaction.FOR_NETWORK);
            } catch (TxException e) {
                throw new RuntimeException(e);
            }

            /////////////////////////////

            assertEquals(recipient.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_OWN).b,
                    amount);
            assertEquals(recipient.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_DEBT).b,
                    amount.negate());

            assertEquals(debtor.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_OWN).b,
                    BigDecimal.ZERO);
            assertEquals(debtor.getBalance(dcSet, AssetCls.ERA_KEY, Account.BALANCE_POS_DEBT).b,
                    amount);


        }
    }

}