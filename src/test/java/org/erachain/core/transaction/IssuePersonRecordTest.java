package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.bouncycastle.util.encoders.Base64;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.ItemFactory;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.persons.PersonHuman;
import org.erachain.core.wallet.Wallet;
import org.erachain.database.IDB;
import org.erachain.datachain.AddressPersonMap;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.KKPersonStatusMap;
import org.erachain.datachain.PersonAddressMap;
import org.erachain.ntp.NTP;
import org.erachain.settings.Settings;
import org.erachain.utils.SimpleFileVisitorForRecursiveFolderDeletion;
import org.junit.Test;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class IssuePersonRecordTest {

    static Logger LOGGER = LoggerFactory.getLogger(IssuePersonRecordTest.class.getName());
    private final byte[] icon = new byte[]{1, 3, 4, 5, 6, 9}; // default value
    private final byte[] image = new byte[18000]; // default value
    int forDeal = Transaction.FOR_NETWORK;
    int[] TESTED_DBS = new int[]{
            IDB.DBS_MAP_DB
            , IDB.DBS_ROCK_DB
    };
    BigDecimal BG_ZERO = BigDecimal.ZERO.setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
    long ERM_KEY = Transaction.RIGHTS_KEY;
    long FEE_KEY = Transaction.FEE_KEY;
    //long ALIVE_KEY = StatusCls.ALIVE_KEY;
    byte FEE_POWER = (byte) 1;
    byte[] personReference = new byte[64];
    long timestamp;
    long dbRef = 0L;
    int height = 101;
    byte[] itemAppData = null;
    long txFlags = 0L;
    Long last_ref;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    int nonce = 1;
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, nonce++));
    PrivateKeyAccount registrar = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, nonce++));
    PrivateKeyAccount certifier = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, nonce++));
    //byte[] accountSeed;
    //core.wallet.Wallet.generateAccountSeed(byte[], int)
    byte[] accountSeed1 = Wallet.makeAccountSeedByNonce(seed, nonce++);
    PrivateKeyAccount userAccount1 = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, accountSeed1);
    String userAddress1 = userAccount1.getAddress();
    byte[] accountSeed2 = Wallet.makeAccountSeedByNonce(seed, nonce++);
    PrivateKeyAccount userAccount2 = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, accountSeed2);
    String userAddress2 = userAccount2.getAddress();
    byte[] accountSeed3 = Wallet.makeAccountSeedByNonce(seed, nonce++);
    PrivateKeyAccount userAccount3 = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, accountSeed3);
    String userAddress3 = userAccount3.getAddress();
    List<PrivateKeyAccount> certifiedPrivateKeys = new ArrayList<PrivateKeyAccount>();
    List<PublicKeyAccount> certifiedPublicKeys = new ArrayList<PublicKeyAccount>();
    PersonHuman personGeneral;
    PersonHuman person;
    long genesisPersonKey = -1;
    long personKey = -1;
    IssuePersonRecord issuePersonTransaction;
    RCertifyPubKeys r_CertifyPubKeys;
    KKPersonStatusMap dbPS;
    PersonAddressMap dbPA;
    AddressPersonMap dbAP;
    //int version = 0; // without signs of person
    int version = 1; // with signs of person
    int seqNo = 1;
    private byte[] makerSignature = new byte[Crypto.SIGNATURE_LENGTH];
    //CREATE EMPTY MEMORY DATABASE
    private DCSet dcSet;
    private Controller cntrl;
    private BlockChain bchain;
    private GenesisBlock gb;

    // INIT PERSONS
    private void initPerson() {
        byte gender = 0;
        long birthDay = timestamp - 12345678;

        person = new PersonHuman(maker, "Ermolaev Dmitrii Sergeevich as registrar", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, null,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, makerSignature);
        person.sign(maker);

        issuePersonTransaction = new IssuePersonRecord(registrar, null, false, person, FEE_POWER, timestamp);
        issuePersonTransaction.sign(registrar, Transaction.FOR_NETWORK);

    }

    @SneakyThrows
    private void init(int dbs) {

        Settings.getInstance();
        timestamp = NTP.getTime();

        LOGGER.info(" ********** open DBS: " + dbs);

        File tempDir = new File(Settings.getInstance().getDataTempDir());
        try {
            Files.walkFileTree(tempDir.toPath(), new SimpleFileVisitorForRecursiveFolderDeletion());
        } catch (Throwable e) {
            LOGGER.info(" ********** " + e.getMessage());
        }

        dcSet = DCSet.createEmptyHardDatabaseSetWithFlush(null, dbs);

        cntrl = Controller.getInstance();
        cntrl.initBlockChain(dcSet);
        bchain = cntrl.getBlockChain();
        gb = bchain.getGenesisBlock();

        dbPA = dcSet.getPersonAddressMap();
        dbAP = dcSet.getAddressPersonMap();
        dbPS = dcSet.getPersonStatusMap();

        try {
            gb.process(dcSet, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        last_ref = gb.getTimestamp();

        registrar.setLastTimestamp(new long[]{last_ref, 0}, dcSet);
        registrar.changeBalance(dcSet, false, ERM_KEY, BigDecimal.valueOf(1000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        registrar.changeBalance(dcSet, false, FEE_KEY, BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);

        certifier.setLastTimestamp(new long[]{last_ref, 0}, dcSet);
        certifier.changeBalance(dcSet, false, ERM_KEY, BigDecimal.valueOf(1000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        certifier.changeBalance(dcSet, false, FEE_KEY, BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);

        byte gender = 0;
        long birthDay = timestamp - 12345678;

        makerSignature = new byte[64];
        makerSignature[1] = (byte) 1;
        person = new PersonHuman(registrar, "Ermolaev Dmitrii Sergeevich as registrar", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, birthDay - 1,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, makerSignature);
        person.setReference(makerSignature, dbRef);
        dcSet.getItemPersonMap().incrementPut(person);
        long keyRegistrar = person.getKey();

        makerSignature = new byte[64];
        makerSignature[1] = (byte) 2;
        person = new PersonHuman(certifier, "Ermolaev Dmitrii Sergeevich as certifier", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, birthDay - 1,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, makerSignature);
        person.setReference(makerSignature, dbRef);
        dcSet.getItemPersonMap().incrementPut(person);
        long keyCertifier = person.getKey();

        // внесем его как удостовренную персону
        Fun.Tuple3<Integer, Integer, Integer> itemPRegistrar = new Fun.Tuple3<Integer, Integer, Integer>(999999, 2, 2);
        Tuple4<Long, Integer, Integer, Integer> itemARegistrar = new Tuple4<Long, Integer, Integer, Integer>(keyRegistrar, 999999, 2, 3);

        dcSet.getAddressPersonMap().addItem(registrar.getBytes(), itemARegistrar);
        dcSet.getPersonAddressMap().addItem(33L, registrar.getAddress(), itemPRegistrar);

        // внесем его как удостовренную персону
        Fun.Tuple3<Integer, Integer, Integer> itemPCertifier = new Fun.Tuple3<Integer, Integer, Integer>(999999, 2, 2);
        Tuple4<Long, Integer, Integer, Integer> itemACertifier = new Tuple4<Long, Integer, Integer, Integer>(keyCertifier, 999999, 2, 3);

        dcSet.getAddressPersonMap().addItem(certifier.getBytes(), itemACertifier);
        dcSet.getPersonAddressMap().addItem(33L, certifier.getAddress(), itemPCertifier);

        makerSignature = new byte[64];
        makerSignature[1] = (byte) -1;

        // GET RIGHTS TO CERTIFIER
        personGeneral = new PersonHuman(registrar, "Ermolaev Dmitrii Sergeevich as certifier", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, birthDay - 1,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, makerSignature);
        //personGeneral.setKey(genesisPersonKey);

        GenesisIssuePersonRecord genesis_issue_person = new GenesisIssuePersonRecord(personGeneral, null, null, null);
        genesis_issue_person.setDC(dcSet, Transaction.FOR_NETWORK, 3, seqNo++, true);
        genesis_issue_person.process(gb, Transaction.FOR_NETWORK);
        //genesisPersonKey = dcSet.getIssuePersonMap().size();
        genesisPersonKey = genesis_issue_person.getAssetKey();

        GenesisCertifyPersonRecord genesis_certify = new GenesisCertifyPersonRecord(registrar, genesisPersonKey, null, null, null);
        genesis_certify.setDC(dcSet, Transaction.FOR_NETWORK, 3, seqNo++, true);
        genesis_certify.process(gb, Transaction.FOR_NETWORK);

        person = new PersonHuman(maker, "Ermolaev Dmitrii Sergeevich", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, birthDay - 2,
                gender, "Slav", (float) 28.12345, (float) 133.7777,
                "white", "green", "шанет", 188, makerSignature);
        person.sign(maker);

        //person.setKey(genesisPersonKey + 1);
        //CREATE ISSUE PERSON TRANSACTION
        issuePersonTransaction = new IssuePersonRecord(registrar, person, FEE_POWER, timestamp, null);

        if (certifiedPrivateKeys.isEmpty()) {
            certifiedPrivateKeys.add(userAccount1);
            certifiedPrivateKeys.add(userAccount2);
            certifiedPrivateKeys.add(userAccount3);
        }

        if (certifiedPublicKeys.isEmpty()) {
            certifiedPublicKeys.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, userAccount1.getPublicKey()));
            certifiedPublicKeys.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, userAccount2.getPublicKey()));
            certifiedPublicKeys.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, userAccount3.getPublicKey()));
        }

    }

    @SneakyThrows
    public void initPersonalize() {

        issuePersonTransaction.sign(registrar, Transaction.FOR_NETWORK);

        issuePersonTransaction.setDC(dcSet, Transaction.FOR_NETWORK, 3, seqNo++, true);
        try {
            assertEquals(Transaction.VALIDATE_OK, issuePersonTransaction.isValid(Transaction.FOR_NETWORK, txFlags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        issuePersonTransaction.process(gb, Transaction.FOR_NETWORK);

        // нужно занести ее в базу чтобы считать по этой записи персону создавшую эту запись
        dcSet.getTransactionFinalMap().put(issuePersonTransaction);
        dcSet.getTransactionFinalMapSigns().put(issuePersonTransaction.signature, issuePersonTransaction.dbRef);

        personKey = person.getKey();

        // issue 1 genesis person in init() here
        //assertEquals( genesisPersonKey + 1, personKey);
        //assertEquals( null, dbPS.getItem(personKey, ALIVE_KEY));

        //CREATE PERSONALIZE REcORD
        timestamp += 100;
        r_CertifyPubKeys = new RCertifyPubKeys(certifier, FEE_POWER, personKey,
                certifiedPublicKeys,
                timestamp);

    }

    //ISSUE PERSON TRANSACTION

    @Test
    public void validateSignatureIssuePersonRecord() {

        initPerson();

        issuePersonTransaction.sign(registrar, Transaction.FOR_NETWORK);

        //CHECK IF ISSUE PERSON TRANSACTION IS VALID
        issuePersonTransaction.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertTrue(issuePersonTransaction.isSignatureValid());

        //INVALID SIGNATURE
        issuePersonTransaction = new IssuePersonRecord(registrar, person, FEE_POWER, timestamp, new byte[64]);
        //CHECK IF ISSUE PERSON IS INVALID
        issuePersonTransaction.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertFalse(issuePersonTransaction.isSignatureValid());

    }

    @Test
    public void validateIssuePersonRecord() {

        for (int dbs : TESTED_DBS) {

            try {
                init(dbs);

                issuePersonTransaction.sign(registrar, Transaction.FOR_NETWORK);

                issuePersonTransaction.setDC(dcSet, true);


                // ADD FEE
                userAccount1.changeBalance(dcSet, false, FEE_KEY,
                        BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);

                //CHECK IF ISSUE PERSON IS VALID
                assertEquals(Transaction.VALIDATE_OK, issuePersonTransaction.isValid(Transaction.FOR_NETWORK, txFlags));

                // clear SIGN
                person.sign(registrar);

                //CREATE INVALID ISSUE PERSON - INVALID PERSONALIZE
                issuePersonTransaction = new IssuePersonRecord(registrar, person, FEE_POWER, timestamp, new byte[64]);
                issuePersonTransaction.setDC(dcSet, true);
                assertEquals(Transaction.ITEM_PERSON_AUTHOR_SIGNATURE_INVALID, issuePersonTransaction.isValid(Transaction.FOR_NETWORK,
                        txFlags | Transaction.NOT_VALIDATE_FLAG_PERSONAL));

                person.sign(maker);
                assertEquals(Transaction.VALIDATE_OK, issuePersonTransaction.isValid(Transaction.FOR_NETWORK, txFlags));

            } catch (TxException e) {
                throw new RuntimeException(e);
            }
            dcSet.close();
        }
    }

    @Test
    public void parseIssuePersonRecord() {

        // PARSE PERSON

        byte[] rawPerson = Base64.decode("AAEEAAEAAABkZAgDAAARfQAwhh4WHmnzDE6uTTiWZqgQgx9eeqv277XWNADg4hF9ADCGHhYeafMMTq5NOJZmqBCDH156q/bvtdY0AODiUGVzciBTYW10YWcAAAAAS9//2P/gABBKRklGAAECAAABAAEAAP/bAEMACAYGBwYFCAcHBwkJCAoMFA0MCwsMGRITDxQdGh8eHRocHCAkLicgIiwjHBwoNyksMDE0NDQfJzk9ODI8LjM0Mv/bAEMBCQkJDAsMGA0NGDIhHCEyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/AABEIAX0BLAMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AODKIwGOR6qeaaVYAFZMpVUSNG5wD+BwakW5BVcnaT6jis7GpIpkGCQCPrUqTKG278HrjnFMV0JBkGz0K9qc6qxOzDAe3T6ikBNG+xlKrsOeGjO0mtWz1u5skKgCVGABLZD4HociueaRkxg9enXn+lSrOhwJPkJ4DLxSKPRdL8UqZULyjCoEZHXPPr6d+xH1NdTaXySjBkG8HB+bOff1/OvHI2KLuUjAwAe/5itSw1yeBcABgDwwJyPXHXnFPmZLguh7BHKCMnoemO9Tr1ridJ8SfaZAquiv5RwhUlf94gdfw59Qa6i21CKRUIIBYZ2Zz+IYcH+dWpGcotGgDTw5yM1GGUqCGz7ilziqIJs88U4GolJ4p2e4oGPzTwePeo88U7tQBes71oHCscoeo9K2lYMMggg8giuZDDPFX7C98nKSH936+lAGzRSA5ANLQMKKKKACiiigAooooAKKKKACiiigAqCeHeMj7w6VPRSYGcOpB6jrTSoNW54d3zIPmHb1qr/PuPSkBA6fnTUbacHpU7LULx56UAP4P1puM0xH28Hp71J16dKYj5TbIXk7lFNaJeqkkHoe5q8YOWJfBB7jB/EVXeNo5GYqVbOWHY/T3rJSudDjYqrmEnkYPYjipI5io+hzg9vpTh86gg8Hjn+tRsm3JK4PUY/pVElpJ45uZMkHqR1oeAIjOmNpOeO3+H4VTBwBIc4xkMuM/wD16f8Aa2t2VnwQejxjI/H3pDRYVmWXKluepU/e/HoamimS5lVt2Gx24NM3JIFfoWHDqMhh71HJD91pUwRxuX+o7ik0Mux6hLbYMydRjco/mDXU6T4hYMRJ84k5YZ4bHTBz19iPxrjI5ZIyDI6sv95T/OplBhKzRDazHr/D9D6VJW57LpmsLPCJIizw/wASOMOp+mf0rbjkSWMMhDKRkGvFdO1iS0u0Qr5br0DrwRnOAfSu+0PXPtCEqFyMNIjH5seqnufr+vWrUuhlKFjrwc08E/8A66rQ3KzRqwxyan6f/WrRMyHg+2CakFRZx1pc9MUwJhxTlNRbu1ODUAadjeiLEUh+TsfQ1sZrlg2K09PvtuIZOnRT/SgEa1FA5ooGFFFFABRRRQAUUUUAFFFFABRRRQAVXuIN37xfvCrFFKwGaGyM00jNW7iDcd6DnuPWq2eKAK0gHYU1XZBgc1YYZqIqM0CPnSe2KkZG3HQj+h/pVJkO0rnGOSD0/D3rTFwSSDEZYscqOWH+NNltonj3xMCCOCDkj61yJ2PRcU0Yc8a53KCp9TUHOQOD3I9fpWpLGOUkAIxgGqNxbsmNvA71qpXMHCxVdQSzRnDHof8AGmYydh+UtkHPIPrjNSkg5UY3d/8A9VNyD8jKAMZ5OAceh/ziqJsQhJIW3W7FMjLK3Qn2NW4LgZCkbOf9WwwR74pnGG57554zTJIxKCWDYAwCAd6nt7GgNi8QAd0Xccox4P5Uit5BZ4i45xgKCfy6VQSSe3+WbbIuSA65xnuAfX2rQRoZV3Kytn35H1A//VUtDTJUeN0GVU56j+E/QjnPtVy1vZLXlGcA5BXOcDr35/H9RWdIhG54mVXfnevKntgjv9RT4LgHYtzhZM43qcr9cnn86ko77SPEJd2cNk/LvAIyB3z6j36jn0rs7HVIbqMkSqwXHAYZXPY142rSQyecjnfkFZI+d2Omcdq6PStfGAkpaOXbtLjDIw9Mf4/pVRlYiUbnqayDjjFODj0b8qxdP1SOW2zFIJExkqG+57HJ49vXnk1qxuHXchIOAcH3rVMwasT7hwfm/KnA59ciotxHVc/SnZA5BBHQiqEShh2NSBqiQ0oODQI2rC93DypWwf4TWnXKq2CD36itmwvRIojkb5x0J70DNGijNFAwooooAKKKKACiiigAooooAKKKKACqlxAc70HPcVbopAZnB+lNK5NWJ4dpLoDjuBUQwRncB9aQj5/1HQ5rOaR4gxUDCBR39Qe/+7WbEzFty7RJ3UfxfSvT2t0mUq6q69cFa5PXPDRtY/tFuWIzwAfu9xz257/nXDGV9D1JxtqjnZFSVDlQMHkg5H/1qzLiUROFKBlPcN0q3cSSIoV1YFRywGN31Hr796omGSVl2cr0wwz165FbLQxZBNArjehwxOdw5I+o7/WqwOXIl4IJIboD7g1sx2TRAsc4POQOh+vp7VVubYZJG3jPy49fT0/z1FUmQ0Z/KMNwzgcdv8/SnDDYVCAcd+f/ANVPCiIFWwU4O0jIz6f/AFvxpjxhPusWRgfmIx/ke/Q1aIsG9ZgylcMzAsjEnI6Z6/qOf5VGVMS+ZEzK3Qp3X/Gplw5KSA56humKSXKMySjcOob7uT7eh9u9Aye3vVZyso25OC2MAn6VYkhEsYKDI/ujkH6Gs14htDlQQer4/Qj+tJHJJasPnbBIKkH/ADn6VLQF+KWW2Uhc7CeY26irkVwsxyCAT6tjpzz6j3qulzFc/fXa3v0P+FI0GJFdNy9eQcc/0+o/GkUjptG1qTSruNpM+R/HxgYxx9fr/Ou90zUlv4VeBwWAwVJyAfr6Z9segHSvIobtoSoZlKgcq3H4/j61tWGpPEVe3k2bjuZc8fTHcUJtEyimewW8+8bWwrjqDwT74qU85I/MVyelayl6VSScq5B+cNgpx0z1AyOvvzxXQW96X2+dtUMBtkX7j9fw7H2raLTMHGxeUnnFPzkentUEMgaJec5qUEFeuaokeDzUiyFSGHUc1Bk09fwoEdDY3ouF2tw49/vVdrl45XjYMpwR6VvWd2tzF6OAMjNA7lqiiigYUUUUAFFFFABRRRQAUUUUAFFFFACYqq9oGYlX2j0xmrdJilYDzZMBTmua8W62LK0+zxHM0w2/Qdz+Vbt/MtvAWL4715ZrF7Lc3jySM2Au5QTnb2/wNefBNs9ao7Ij+0xzysk+1WYjDY4P+FKsawXBjdPlkwVkQ4aP/aA/iH+z7UaLp5vbkF1LLGQTn17Z9uK6q90a3mjGEOQOwGc44I44rRuzMlG6uc/ghkXcWEi5Qj7soA6gdvUjtVKdASWDDjuO1Szxz2DMhBKZ+5k/gw9DVeaYuN4XcOTnHJHqaEJlSWEH5m4Geff/AOtVMhkwu0gAE9ecH/PWrhuFh+aY/uQcJKBnHsw65ps0JH3Fxzyo/h+hrVMxZXdQ5LQgFS3CgHjjBx6fT/8AVTY5N6kMokXGdp5/T0qXbsO5cMCTxggE8joenT6/pmNlVhuRsEnHJxmncVgKBAWVsxuTyCcge/rUZQsVZNpV2AEZHyn6HtTlUqG6r1yMcU4JuUmMYY4LJngj27f57U7iIVDLIy/MCDja33h+PerVvcujYYlh7nkfSoTslGZB8ynG4fKy/if8+lMdfLcCQkIOkg689Mgf5+tAzScRXCrtZQwBGD64zx6Hjt1pqJPaEN5oZBjK4PUj6cVR3MgD5G0HIbPB/wA+9XIb1HUrOuRxz7VPKFzWstUjMgeKUK4b5G+YEe2SBmu30TXFlVYwkSSY2mPBCOcj5geNp5Pv7Ht5s1tlWe3baccgZC/lVi1vJllVJg6kfMOeT/Q0thuN0ew6dfI8cQYeRIwOUPK8env0/PvWskgIAOEcnAXIIP4jivMtM19jHHFcKksQbPmEZf25PX6H867HS7+KeGFBPv3cKMMCefVsc+xwenXrWkZ3MZQsdEGwcHg05eOtUYbjIZS+5VO0MBjJ9CD90/zqwJNuAxwCcA4rRMyaLO7ipoJ2hkDqeRVbv1pc84pgdPa3K3MW9eo4IHY1Yrmba6aCUMh56EdjXRQzxzxh4zkUDJKKKKACiiigAooooAKKKKACiiigAooooA8V8V3PlL5aNiQDPII3cdK8+ltJZbokDdv4Az07Y/z7V6b4g0ldQjDFmVhyCDjmuTtNKnivQHw3lck+vNedGVj15RuT6TZfYrRVK4ZuWz1rW80ADJHtnmoWTHGAKr3MpghaReSFJH17frS3FaxS1KaB5DG6KUB5PHXPOM8DB65rnbu2a1fKMWj/AIWDAgc4/D6Hp3qfXNSWxtBGJZVuJQEQqccDGSfcdR7+orB0jUJtOkIupS1pIwTLnBDHJH4Zz64zz6VtCOhzzkrk0tq5lLw/xn54uzgHtz1qJZXgdmYkwk/I5yFHAGCOo/Dj+VbdzaqF86FlMTjOD1BzyMe3cVEq/MwJG44yDwGHrx3qrisVZItozk+WxwSO31HTsPb29YJYMqSPvE9Ox9MZyQfrV2K0FuTsB8s/w5xtFO8iRQdihkbkr7+3+FFw5TMAZuGIb6Zz/n3qGRGUkqzDHIOeD9f8/Wr7RB1DqW8vbu6Dgg89+eO3XHrUTcttO1seh65ppkNWK6usqvFIAkgAP/6j/kUhcpIUlVTFnG4duc9PqB04/lTpYBsbC/L29U9/b/PWoop1+dHLFVPPPzL/AExVCEKbEEkDZB52HlT9Pz7ikBRzkNslB+52PH6fy96nZVZ2mhYcZDLjnPfI71GVSQDLfN2I6UwHrcyQkCQur5GPm/rV6Oa3uHKyfuyM4ZQQB+A56+hrOVp7b5QcoTnA5FPjETECHauTnaTgY9AaVhmsolgCNC+SD8wc8Efhxn8K29OvShwcxsBkY6E9s88j6frXJ21zNaNgjODyp61fS5inx5Z2OequOD9DUtDXmelWGtoLeT7X8qHDG5QEZAPfqSBjv0963I9RCRgzMWgk+5Mqkqc9M4zg9ue/5Dyuy1d7OUCUHqMMB068+31rqtOvRLJLPYusTybd8Dr+6kXuSB075I6ZqlNmcoI7uOTAwT0AK5PUVMrluRyK5Oy1oQSeRcIIih2nnKegI9v6Y5rpopVljDoVLc7lxyD6VqpXMWrFgMAauWd41u+Qcr3X1rPU8HA70/I96pEnXRSpKgaNgy+op9c7Y3xtpDkZQ8kV0KMHUMpyp6GgBaKKKBhRRRQAUUUUAFFFFABRRRQB51MOuaxbyNVbcBznrW3cKecVkXcTMOteUe2Y7v8AvOlUdUfbCB0yeyk9x6VoNAwck07yDPC6hSWCnAFWiGeZaqL7UdYt7F2QOq/u92VChvXknqBWrHHcCB9LeERNGFVnjwVOOM+p6eg96jOl3N/4vuAEdYYiFZtpGABxg47mustrMW+FSOTaOgCcfifWtnJWOdUm3cy9P0p7ZGPmFlxhlzkNjv2/z61Dd2BRC8RG3JPC8j/P4/4dSthLI+6VsDsoOas/YRtKlBt7YNZ8xtyK1jhonwcuD6nHP4/Sp5bYou7gjoOv8q0dT0Z7ctPaozqM7toznp29f89RiqNtdIY2+8wjG5ogvzKc9R6j/PFF7k2sZ89uMt8zKenXgj3qlPBtIDDIzjPcf/XroLi3WNSVfdER8rAcc81nSQmNsYJXocc8VSZMo3MdomUgqMc4GRyD/jUTRLPIqYIlz1Y4568Ht+P6VoyWykAxfLgYAAzx3BzziqjKXcKFCsvGwnhsdCv/ANfpWiZlKNihloZB0DA4PHAPuO2fy9KsgxznB/dyMMHj5WPv3/lTmRJf3cpA28DccFSe3tVSaGS2BODJH1yo5X61adyCco8MiIwLkg4KnO725x+RANRCMSossTbhzgoefpzTre7MeEDCRDxs+9kfzH0HFWVtop5VktX3EEoFJIYD1BBJwOeOR7CmBWW5kSHDYdB9weh/p+dSiOOYboX6jlWpiscb5WVnJ+9wrkepXpj3BP4UjREKkqkoxPuP0pbgWor2W2KRSj5ifm3k9PrWpY3rxzbrWR45Mg7HIwTngg+vvWKtw8aMknzKccgcEc/l+FKFQn9zIEJ/hJ70mgPSLDXbe/2Wt/EUnVxlumBzkA5POB3B9vUasaXGlTCSzVgkg4EpyjAng8Hv+h9c15XDdSQFopUZ48ckj5f/AK35Ct/S/ESQoYcsYyADGeVP+H86E7EtXPTrTV47ggOjRSE8gY2/5B49fatRHBJC4bvgHNcbpd/a6gqCRkJQ/dLcAY6E+3TPUjrxWvFC1vCPszyhSfuklgR688g/Tj6VpGRlKJvbsnnOK0tP1EwERvkx+3UVz0GoeYdsw2E8kkcD/wCtV5OOc44q7kWOzUhlDAgg9waWsDTtRaJxDK2Y+gx2P+FbysrAFSCD0INMBaKKKACiiigAooooAKKKKAPPiPMHAzVea23dRVi1uY5BjIxirbxrIPlxXkXPbObmtwuRtqtGvlSDjrW7dwYBGKyJFKEgimpD5bjGtkyzxKFZjk49aVLduOMkVJG+PepVc8d81TlcXLYRInA5GPwqUQkHJpySDHap1Occ0hEItVd8kDkYPv8AhXMa14XYyNdWGY5oyGBBO5hjpzwa7NMMKY69cDr1FCdgtc8st7oW0jRTRMvy4kiI4HHUZ5//AF1PNbrIm+Bg6d1B+6fYdfzrrda8PQ6grSrhLgch845ANcVIt1Y3TQ3KssmeJMYD56ex9KtO5DVipJEyksv3u4qlOkcwVJV+UYPBwc+oP1rZbZIQThXI7Z5rPuogBjBKt1x69verTM5RuZThw2Z/nUniXrwP73+OTQrlGCkgqOV9cevuKklDBSFO08nPY+xz0qEMgIRl2H7wwBg8Y69j7H8zWiZk4kU9jFKhMRCkL0zkHHpmq7tPE7eZngDPrnsef681caRgemN3JIHB9yO31FPZlmUFx0OFbP8AIjrV3IsNS7hukO9SJCd2BkdsdOPzFDW5Ab7JJtL8AAlwfbHpVae3CDdjfGeh5BB9xTVuZImLZLqME846e4/+tTETBmRR9pjG4fekjyVH+felWOGYEoTuH3SMfrz/ACqdbyO5LsHHIHDH+vr9fzNMMETJmLdFJ/fQ8H88j8qAFSeeJvm2yADHv/L+dOR7Wd25MbdMk4/+sfxxUIFykYEg8zB4kAIz7cZA/ShZEnkKyffUYAPT3yR/WiwF+Ge8snSaGRmVehjOa6bRvGstr8kzkxnJBdQp+mDgfqK42MuhcQSZw2CoOcfzz+NStOJBiaFX7FlOD+XSkFrnsFnr+m6iiNvVHPyhSwyR9D1/DOK1IZZYciMrJEONucsMcYx/k14fCZIUZbW7GDwEcDGfXBzj64/Gt2y8YajZFC7uqrwSVDZ/ME/qfpVKTRnKB7HHcxyYwSvOMN61t6XqKoBDK/yfwk44/wDrV5rpni+yvvknKRTcbyTxjOOR+XPWult7sOgaGRHH9wNkj6etWpXM3Fo9ABzyOlLWDpGrhiIJ+OgVif0rdyKpMkWiiimAUUUUAFFFFAHjNrdup4z9M10FlebgAzViqlvIu6JuR1yMGpEDAcHkV5Tie0mdIVEynkGs26tCM8fjimW18yYU1pCRZU564qTS9jnHRoz7UgcitK8gxnHes54pF5pXKtcesoBHQetWkmGztWUUfI4I/rSDzkOMHHpmncho3UmA4zipt2RwawoLnDEE/nWlHNkDrQFiZx71kanYw3URWSNXyMDI6f8A1q1SeMgZzUEi7hk0XEef6lpNxYsTEplhDA5I5A79euOeOtZxkWb5VI69uSD9Ov8AhXe3ca7W44NcvqOlLIzSwYhmGCHUDnHqO9aRl3IlDsc9Pbht6kZVuuz5sf59Kz3VlDRuoZB8obHUf57VqTie04uBlSflcEZ9+ev6n60wmKUE4zz94Hgj3Hf6/rWqZhJGY6MvVndB0kPYds+n9agjkIckFd2zJiY7QWz+OB79D7VoPbPkeWSQTgnArPv02o84AWReQgbHGOoPp7HrxzWsWZSVieCZJELA4fvFIeR6HB7UjwrMxZf3TdcdvofSqS/MQ7HzFcEccnj0HX8vy71JHdOiltxkG3JxgHHqD+HSqaZncZKjwNh49hY888/meop0c7xhQCJB6EdfqKuxyxXEMIG2Xd0DEDH07H8MVDNpp3mSI9f4Gz/OgY+K6AQjDRtnIB4OPr3HueanBibG+IMf74HP1zWXh48rICQOCD2NKGKn9zO6jvjPHtg9KANFLaMS+ZHM7FugKb1+meoocMH3bd4P/POUOSfoeRVaC9kjwHhWQd3U4OPcD+dTQ6nbSTFJW4J481cn8+MUtQHNERJteMofpu/T/wCtTllZW27iQeCN2cn2FTFw6bkm6dA2SB+lRvDNNwYWKkdY5Ace+DTAnSSORsLmKTuYzwPwFadjrV9YbdkxlRD8uxuB6+35YrFkibG/zFYK2PmjKkcdMdRQJXjZ3Ee0feJVsgn/AD65oA9M0nxrHLsiuIzuJADhhuH1PevSvD+vJexiJpklPRWU5/Cvm+O/RpI3lUIrHG8IGXP8x+Fa+l6/PYyM0Ej8EEFHIPH0x+tNSaIlBM+nBS15t4Y+I1rMqQancCMrGC0pU8H/AGv8QCPpXVXfiixgjJgkFy+AcJ0wffmtOZWMuR3sjcLY9PxNUrjWbG1+/cIx/uodx/IVx1/rl5ety5ROmxCQPx9aooScZXA68VzyrpfCbwwzesmdW/iyLP7q1dl9WbH9KB4pcjP2Nf8Av6f/AImueRQTkgGptqf3B+VYvETNlh4dTidCslsbEL5jyOxyzsc5+ldBGmUBxUVhaH7MCVx7VoJDiOs27nRHQzpgUbI4qeC9IwCxp80WR07VmSExSVDNou5vKjT4wN1XY7OMAZUE1Q0qcMPm7d61JJ0Vcq2TSSKbKGoaeNm6IfXFc3c6rp1hdRW15dRxTSjKIx5I5/wra1DU3Q7R69a868beHbvWdftZ7Yovl24BY9PvsauKTYnex1spidQ8R/Ed6t20u6Mc1kWkM8NiI5cmX1zWxAm0BRxSBlkMSMZ4pkhKj2p4AHFNkwQaQijMQeDWbcQbhkZH41pyhc9frVWRcdOlMDn7m2DZ3ICD1B6VhXWmGINJbllweRn3/wA9a7OaBWB9ax7mAruwOapSsTKBzDyNG5imjx6MAcfjUEkJc5CCRCMkH/DvW3cQqe34Y4rKniKLhDtG7dyc1vGWpzzhoc8loCcRES26g4kVSWUgnllPJA9hnj2qKOWQIFyykjeGDDD/AIjgn8/etprMTsZhugkJykqncrEcH7vI/wD11Ve1kkt438sSRltwdZOe+c9ifwzn6YrdSucji0VE2SzmJg8cpILbMIwHuP4v1/lU8V7fQMcH7SmCcsDuwOoIPP5fjUFxbMIoWUKS2QFfgjGPut0/A/8A1hD5k9s+Jl3fOCd/3lzzkH9ev0Ip6CV0a63lreRAcqx42vyM+xxx9DT5bSLqMrvxneM89c5HB/Ssnzh5brt3EMNz4w4HuR1Puc/WrtnN5sjRW0xkx1XnI68Y4Pbtn+QMtFJhLYzwScBg3YhsZ/x+vSqwdmyGjEgY9TWml4vlhZsKFJJGDj/635U94kmyrjYzcgrgjH8qVxmdG8anaJCp6ckjNW43u9oMUp9Tkg/niklsWYHaEkweg4P59KhYGP5ZN8BJzhucn69aLgWlv5vOPmRb+g4P+J/lUqXcIIO0L1+7lSKorJIQGV1cdw4z/OlZyzgyQFAOvlNjI/HP8qANJYvNQvEocZ+Ynkkf1qBLdCzbMq452cfyqEiE4aCYowOCJF5z9V/wp6/ag28Nvx03t5gH4dR+FMCRLme1kD+c644yASR26/5zW/pniWS3dEaTqOB/Cfw7Vza3r5Zbi3jYdPmUkY/nSedEU2xpsG7JQOSpPrj1pNJgtGeuafqUN6qmNju27ipPI5xWtCDkggV41p2rXenSLJG++JTjDE+mPwr1bR9Xt7+286OZXjJ4ODkfWuWcLanTCd9DbjXDY9qnBz6VBGTnFPz6VkWRQxhIwvTip1T5ee9IvA6VOBhKosqvFwRWPfwYIPP5V0SgEnH61SvoPMTb0IzSGjnHmkiUbPTrUsGrCMhbmaKPjgu4XP0zRMTAdrDiuc8UeHo9fFltlMbQ7+/Xdt9j/doi1fUvW2h01zNBfwloJUcdMowI/SkgjMzKGwDVLw/pCaXYx2yMSR1Y/SugFrkbg1BQ0WKg8ntRN5duhZnGFqfz1iQtIcY659Kx5JjKxuGHzN9xT2H+RTSuOMbiy38jLmCIKO5k449eP8RWfcag6jaZQD3CjFLLIZQXZgVySoz04rIubmKFFQbmYjgADjPr7VaijRxSQs+oOSxDsRjn0qbR7truKUsDhWKgkg5x6VzVrenXLi6t0ieLyGAYMQCQffPtW/pai2jAHTdgj0zRKNtCNGro2mwVIqhdRfKTV8DPABqCVeCDWRNjn5kOTWXPHkkD071vXEWAc1mXMYB49K1i9TGSPO9Tuns9XukQsVyDwSCDgH+v09qvR6iHaO5cuqg4EqcEHPAb0z+NZ2ugLrNyuOMr/wCgiqttO1pLvUrtKlXXsykcqfb/ACOa7kk0eY5NSZ05u0nmVmbg48mQgqzn0zyCc/T6VNJuWFcvwxxllGD6qcf5PvXPblijjubN3VWyseSC2M8q/qOfTofyhjvNjyH95bzYKgwnC+4I9PpScOw1PudEujRSbjDOkZY7kjP3V9QP4v5VnTae4dgSqytzlOjH15+vXNNh1C7jQLxOhwW2dc/0/GrMWsmJ90gcpyZFBHr3B9KVmh6PYiiu7u0EcMhRlI285GQBjg+v4VcivIZC0QARzyquu059v/105NWs5dsgSOM52kMhRsY65XtnjNSrZ6dODLbsw24JaB1/VQP5rSYyOOUBOQ/OeApIq1HdOighgQRj5fmBFQXGlRblMU4I7ttH8wTTBZTLGD50LYJ53E8flmkFyaS1tJ2L7NmDuZoWx09BUgsfNVZLaaSQqfnP8X44x+uah+z6grg5hdP7uSDj9RTLgyBdsloSFPUjO36Ec0hklxazjAhRJP8AbU4P8/6VXm22+I3SSMnsycH/AD9KlgvNiKj3AbHASQ5A/wC+uf1qxHqLCXBt41Q5BG3g+/p+dFwK6XDqgMTJIhGGAyefTFNG0MTcQyADgeTJj9GB/pVkvayud8JiyMfIThvwyB+VPS280ERS4YHgF8Ej2B7fjSuBWH2CYBrW4lQ52skyKMfr/Q1ueGdVk0K/IkYNBLw4DEMPQjI6j8e9ZdzaH7Q8RjynX92wB3egPTPtkfSoIGCEy2sgKKfnVshl+qkY/Ghq6BOzPc7adXQAYCEZXHTPpVoSEjtXn/hLUozp5jJKy2453c5XJIOQfw+tdvFL5kYcEYPrXG1ZnXF3RcjKsoIBHsasKAf/AK9ZdtdB1HPXiryzBhwelJm1iyoGeOtNdM8YpqyEsOnvUpYEmkKxl3Nks4IZeBWPcaTIjgxNhT261078qTxzULqG6gZosUmYtpamJl3HOK05Jo4Yi7ZwOw6mmsiqapXUm51zyi84Hc1S1LirsrTEzzb7rAiAyIgen1NVbiVJgzyNiMckYAOKJZCxznIH0GfbtUEVpPqcnkQZjhibMsrKwUjuo46kGrS6G7koq7G2mn3GpSiQMwtgRmcLxLx/D6/Ws/Wxe6W7aZZxRM10P3U2GLR8AsWGDkDII7c4Paugutai8P6eYbyTcyjEcoAAc8kLjs3H0OKzrMzanctdSBwhxiJyAUU8447k5J/ADpWqagjkjzVp+RFpOiR2NorATebksXPXcepP+fSo7hSFdiPmz9O9dJ5YC7W5PTr0rFuQNwjbkluPXAPP9Kyv1O2UUo6FmEExjPpTJFyCMVbii/djj6VHNHiszAxbiMgGsy4jGMt+Fb08eQTWRdr0Hf3qovUxmjzDX4g9/cMOPnIz+lZHbPtXSajF5jO+3JZyawGiKjOOR1FehB6Hl1I6jreYgPEzgROQXBA6dyPcdfwolQztywMqA5ZuPNA9PfH51WKgfj0oVmVk6gq2R7VZnYXcQvB/D2q1BeMiCJnJTI4z0+np+FV2xIM4Hmd+2aaQetIC2w3MTkvuOSR3/CkeIq2AR8vTBz+VQpIRxuHsf7tThyGPmoS3cjqKBoRGeKUOjsG6qS5B+mfxq9/alzvEjMrAH7h7Ae/Xr0qAKrAKpVsc4pu0q3AIBGDkdqNxp2NKPVZHkUxMqE8bZBhfpkf596kOtTRMfMhR14wyPkfhjg1kOrKFZQ2GOBkUqylAQcFW9O4+n+FTyormZvLr2PvQzAY7Of8AEU19VtLnAeFyO5IX+fWsVVQACJjE2enOD/UfhS7pVbLMQCcZPQ/jRyoOZm2lzbdQsgA98f1pTdWoGXD5PV24GPwNYglI6dM8+9SxyEA7GK56jtU8o+Y6BZDKytFPKSPulWDke3NNaUuyPKqlY/uk7kwfZl5/pWLBO9q+6LAPY9j+HQj9a347qPUyWV5IZlTGcZIx6joV564yKVmNMvaLdRWOpCVZWeNxiUOpBZe/bsfavTdPvonso2W5GDz1BrxMXE0TmNxgj+f0/wDrV0uneKmsLQQRyywqCSI02Moz6FufesZwvqjaE7aHW2upEgfNx9a3Le/yOoP0rzS1vmXBDZz/ALXSty11RgoG4/n0rGUGdUZpneR3nAANWVuQcc1yNvqIIG5s/U1pQ3e4DDfrWdizoBKD/EDTGYMVO45BzxWaLvgf409bgZHOaALMjA9Kw7yYx7xkg54FajSBuhrC1dishGOCKqO5pTdmZ9zchFY8jHHzEdf61NB4jGksNNlVp9sZaOWEZ35PI29Rz69cVxHi69u0a3t7MyBgwcqgJOcHt3//AFeldP4d8P8AkMt1dsWunxvOS23jA/8A110RtFXImnVly9jRGm/2xKk14TIFcSJExBWM9M8dT9a2ktltG+Ubo2PJA+7juf8APFTxBUQdOBg1BNcCNThgFI79qybudsIRirIZc3RjVskdKz7WNrmfzZF46DioyGvJ8D5Ygeg5B/8ArVt2lsBtXHT2qWzKpPoiRIPkGM0yWD5a1Y4Ao9aHgBB4/SpMLnOTQHBOBXPaoBEpOeQCa7a6t1CnHWuL8Qrh0TOCe3r/AJwKcdyJ7HF3MBKcjpXN3cOxySpBPtXcTwbkJHU81zupW5IbIrrhKxx1I6HLuoDFfy9qaGJ+Vs56VPPHtlIyMnoT1zUB+bJI5FdC2OTqALKcg9OBSkBgCD+HpSI3OG6GnDAyAcKeCO1AWGAfzqXeNgDAE4GGzyv+IpoBUHOSKVdjMDuK9hxQBMpKyKeQf8/mKmExUjjf6VXALYO4OFXAyQdo9MVKFymNg3D+6TQBYTYqko4GeGB4yPbNJJEFOWQurDIA4Yfh/WolyAR5bfkacs8igKpYD0xkflSGBSNkGJdpzna4wacI5EB2Dgn+HkGpfNilZi6bX9gQD+HakAVOnyZ9OP5UDG7FJGVKtjng8/hTdjBiecDoQafuf7rOMH+/z+tKJAoztHHQqc0AM3dR0z27Gp7e5eMgo5Vh0I6ikAiboCPoDQLb5g6qxx2x/hQxos3UwnO/Chj1UcCohM2B0J/2jz/KpEh2rukPIPT/AOuO1MMLEk8/gakZrEtbNgtwD97+hxV2G7IPB6dayI5jGAso3xE889B7ZqXDRqrxsXhY/K1ZuJpGdjo4LwgZLHFa9pqGAMtmuNguGGCx7/5zWjBdEjg4P14rGUDphM7SO+DfxfhVtLnPfgVyNvetgBic9q0YLzI61i42Nk7nSJcr3J/OoNRia4UYJx7HFVIZwRjIPFXQ3GM8ULQtPU5yy0gxyme7dZLgnglQNnsO341tqzIflkAGO5xU5RG+8oP4VGbW3P8AyyQ/VRV85amRtqJX5FkDseAo5pkUU92483ckYPTdkn2NWo7eFANqAD2FXYUXHAqXIrndgtrRVXAxgdOK1reEBR69zVZAAuMVdjcAAelK5kWEAxSNg8DpSbyR6VFJLgZFArFe+dUQnbzXD6wPNu8A7tq46dDXT6jd7YySa5dkZyzOfmPfNCFJGUY88Vl6rajbyOn8X4VviMiQnHNVb63EqFccngeoNaJ6mEkebX0BG7KgFe/8qz8Z+bow+8K6jVLIozHHB6j6VzUqNFMQc/412Qd0cNRWZG6gruU5H8qbnjH9afkq3y/ivrQVBGQMfyqyADEHA4B6U9ZFDAMF6dMUzYXXI6j06UxevJ/CgCyCh7/T/JqVFw/BIP4f41WBHOWOR65xUmRj+FiPUUAXFibtuI9qeICT8qvz2JFVFwwKjjHPDVIu32Oe5IJpDSJ5I2T7ynj1ZaRSB3P0J4/LvUW1cDk5HfOP5VJyQMYJ/P8ArQMm3RkD5AG9dxx+VCouOWUY77KrOGHUAfoaFUkE7U+rY4pMC1uQD/Wj/vk0AhRnzWJz6H+VRqVUEM4z7DipS6Y4Vxj0x/8AXpDJA4H3pMqepIOak2f3XUj1ziohIN338Y7H/OKk3gcfL+JFJlWI0lMRKE5A56f54q1E6hC8IBVuGib7q+4HY1Dcx5YuuADzioRIyOrx5V1PPoRTaEjRypZmj5Qc4HapY2IYlG4AycHpVOOVJQpOVkIOWHT8e9PdnjmyCgc4IxyOnUemfX9KhotSaNq3mLKDkZFaEEjbh+Wa5qOctIwYFCvO4cHn09ea1INR2qolAI2/eA549fWs5RN4TOghu3UjJ4rRhvcgZbpWDG6sgbcCCMgjvUyvgjk4rFo2UjpEnDDqKXzR2NYkV0RnPT2qwt1kUuUtM1lnHrVmGbJ6/lWEs4zirUM/Gc1DKudCk49atxy575rnBeFetWE1AAYBBNAzfebPANVLm6EUZ5rJn1ZY1wAC2Og61nS3RmbL9M9B/WgZLczNcSDkle3vURXjikWXJ6c+lPLYXjk5oJZA8BC/dGfaqsiZOCK0tpZcEVXeP5h7cU0xNHK63Ybo5XUY28/XjmuFvYf4gOVJzXq9/AXhdT/dPWvOr+ILKy84zg+3OK6aUjjrw6nOnP50oYjqM+9Plj8uUrj7px+NRc5zwc11HG9B6Hjg5pxII/r3qJen+NP569u4oAUL+FOUZOOKARsyBnnt1pIyrds/VaAJlABBzz7CnYCnK7dvbcecVHtCj0+gqRMtH94ZXuGIoGmSLKQAMr/OpUfknC59QlRbcjgkY9zS4xjKpgf3gaQywrM2TsU/hSHBGHjTnvnBqMMQ2FZfoEpwYkEb2B9elIZMn2eNcM0qnHswoXypP9XMW/DH9MUyNlCnDl/fdT8dOVY46ZzSGicI2/HzN05DZ/lSbY2JL+YT67Qf5ioTIQpBJC9uCR+FSBiwBLE/8CoGTBxJagkYPIaolG59hK715H+0PamBjDcfMQFbg+/rViaA4wqlWX7ue1AED7VO5MjHOKsW9yZIhE+10Prn5c9xjH49elRq+9NsnLjOCOgqJlKsT+lIZNLGUj2sVdGYnKk/KfXPY/nTk2yxyLhjwCDj5s+vHX8KbC4ddjMFY/dZjwPrUciFHIPDKc7T3+hoAuxXd5ZSBois0ZG7Dcs30Pf8cH1qWHxNOIPNe2jkQHDeSSCv1U8iqa3WX+cEsB0xhsfTuae0UVyN5Ku2eHRvmH5c0rJ7lc7WxfHjKzckC3uQf+Anj8xVqHxFbyjKLIPqo/xrmZrQO7m4BkJX5XjAEg/4DwCPfrVAC6tW2hRKo5KqDkfVeo/Gj2cXsCrS6noKamXb5S341cjurhu4yPauLsNSjkcBXIwejcV0VpfKQO/9awlTsbwqXN+I3Dj5nIz0GKspAzEbnJ9smqNtcjA9K04mEgHPBrFqxsmItshOAWA7jNSi2UDJz7VLEi54Iqfac9sVLLuVvJGO/tTgNlWgqkcimMmBwCKkYzBccVEww/8AKnuSv3QVFLncPSqQircRkpuyPQmvO9dtyuoTL35ZfcEfy4NelygFMDj1Ncd4otJWEV1EBsiYrNx820n734c/nWtN6mVVXRwFwhKhjjI4561TdcgjA4rYvY/LmYDsckfWs7HzexrsTOCSKoHrjNKCRyPyxTmXBPt2ppGT1GaszH5HUjNOYqW5Jb3xzUSgd+tByOmDn3oAm3MAD8rDPHSnLM24ZDgVEGHPrTg528HHsQKBonUhW2ncSD17U9lBOVyD35xUQdsDlSehO0VIk8w42D/vkCpYx4Cjowz3yxpwJDDaqn60q3M5O3Yyf8B60NLdH+FiO/BoGSeXK7BlR29wpwOKfHFKSVOxRj+MgCqxaYnJXn1Ip8e+TB2px/eJH9aQFw2zZwWUN35P88VIFKjDSIT+dVdrl+Dbr7k81GY5Dz+7OfcUii7eIM7wAR/L61IGaRUZnzxg+5//AFUsqh4yhxhjjBPt0qvbZDNG2eBQCHsNspI4DcNx0oBG4h1J2/dPcE/zp5U4HqOoppGAOcg8EelAyCVCfmBwRxnsCf8AGpYmF2BFKSkycRyMAAPUE/hx70KOsZ79M9vbFVmR0bep5XqCO3pSAnkVljKy/JIvAb0Oaq7J0HysN399eVP+Bq95q3CgMoL9iP5H39+pqu48lsrwp46Zx7UwHx6gpj/fp8v9frT1EE7KyPGGX7scjFXXPvjP8xVV4Vk3GFlR+pXdlW9+arurhFV48oo4R8/L+PUU0TctSWX72WQbdyZIkztY4/8AHT+FWbSeWHAZg36n9KpJezJyd0y9x0Zfx7irMV3bu5HyhmIGA2P06fkBSaGpWOksbuNkHzYJ7E4rds7sqOW6fhXDx/K+UfnIyMZras71kOH5X1B4P49qwnA6YTO1gmDsefzq6kikD6VztleKwG444yM9K0YJ8jKtuFc0lY3Tuaq/NwBT2QYOc1VhuPk7Z9O9TCXeAcgD0PalYu5FIueOahLYwOc1dcDbnI475qockkhaaAjJ3g9Bisy+VHjdGAIdSrqTgMp4I/Kr8h2/X3NVJQJT/e9xVXsSzzjVrR7KcLKMRjIQn+Ne34jjNY0ibWI9TxXpmoaSL2AqUbrwwGTn1H+FcPqmkT6bJslyY+iyAYH0Pp9K6oTT0OSrB7mO6kj3qIpnoKsMMEg880xhlfb0rdHM0Q7R/Efy5owpGCTg+1KVKnvS9AR+tMkbtwvJ/GnAA8GjbujK56EGkUYXAxzxQMkAXowPt7fWlEjIwwDj0zSDjZjkZ/KnAAc4zjoPrSAespYYxg++alVjxwuKgJbJwOM9KkU7s5BUDq2M/pRYaJF3FsgLj16GpdzMuUfB9Q3P86YuRn92G9weakwO4J7cVIx3nSuF+U4PcKD/ADp+9++Px4/pTUQHJUqfZiF/U07yjgEBiPZSaCrFy4ztLY6DdUKDMiseD6n+VW3XcjIep+X9KoZyQck7hSEWpDiRsZpqHcCBjJoXmNWY8kHGfamDMcoJ4GcfnQUEqHAZM5Of+AEUxm3APg5GQ/oD/wDXqweMnPAP51WJYTEbch+g65pAIFXGVbGfwwfSrBK3NszEAMMBk6Z9x/jVZhyVIAVzxj1pyTmB1kZQcHnJyD9aYFbHlnlsg914/wA/SmSSGFyMI8fQr0wD6HtV+4iRovPBzER8+F+6ehOO9V5YG2DoSpxjHDf4U0yWiiFUn5HGW7Dt+HrSSxZ+bHOc7l7EUySKXcdodkyCpHXOKWJ5CApBDZ4YkfqD1/GrIuSRXMsUjFmZxxnLcj+tbNrOJArRv82OfUVj4SbzJCY0OQCUyQW/p9Dx7ilXfazKVY8EjAGBUSiXGVjrrTUHhkjWQER9Cy8jHuP610dnexSqpjIyf7rZ/L1rg7Wcy4JlYKMDI55rTghMcqtHM6k5IKkjP4VzzgdMJ6Hc+YvG2RSR0bPFK94qjJcL7bq5ywvkC4klljmHRXYsCOxHH/16s20F5dvvdxgj5dw6n+lYONjoUrm0l6XXGTx3Y9atgtIoHKD3HJqlbWzIyq8Yyf7r/wBcc1qJC2QdhOe+P6UiyDyCRxgn25/Wm/ZWJGevoK0hFlRnIHoBinhOyqAKVwsZ0Np94OScHg4/lTrnSbW+t3guLdHSQYfKjPtWikSg5I4qdYxjii4mkzyLxF4Jn0x2uLMtNaZJK7SWjH9frXIMoBHUAj86+ipYg2VIzn2rgfFHgcTO95piCP5cNbqoAY56jkAH+ddNOt0Zy1aN9UeYFcHJHtUZQgdzVyRSj7WXGP8AP4UzZkAceo4rpTOVorxkAE/hT9m9Syge4A6Ujx7ByMfNTVdoySP4SDnsaYrC7DtJ64pFYZ5x71YVVuBujIV+6g/e/wAKR7V921fvjHykYP096Q7Au0g4PIqR43ztVWYAdQMgn1zUULtFMGVQWXqp71IGERWRWypzt/qD70wHoCMH5sEZx/8AWqUHPQk/7J/wqPcWAeOUgY55/mKtpLE+VnhVzj76tsOfyqGMaI/lyoLD0xz+VWFCqMccfWq6qqxqyzYOcFXO0j8en54qczmNirxYI9wf60iixIcTSdeBux/n61QUgjjoDxWhcfKcjqB/n+lUNuWZV/u5/HNU9zOOxIxxGhz0OadIAVyeeMio4/mgKk8A8U9T8qfiDQWKp3EHJ+hpJAEkzngnIpMhbiMKxGQcfh1/SnuodMg7h1H40mMqycyAEBkPB9M9vzp6jzE2k5I6+9NmAkgwoUEfL9cdKjt7guu4cEHBB7H0oAmikaE4wGU8FTxkHv7Hil8sRSNCrLJ8pKEZG4Z4Iz/kdKeURpNpTKsuOajFwirCLgsig53L/Cef0/xoAoTO0bkr88XBBPOPfHYg1VcgTBmwpyOQMgH+YrQ1aJ7WdSPlU/LgHjpVCQRsMYC56YHFWtjJisu795Cyl+hA4z+lTRXIb93OgCkcc9+2PT/OPSqm10f7xxnBIqTzCRk7Wx13DP502ho0UQwEshzHnAI6+279a07a74XOQR27EetZFnclGw5yD2OSMVrR2ih1nUOYSegxhSe1YzN4GpaR/bpBjdtHUr1FdXoshmX7NcBUvEbOxTw68DI/PpWfoltgPgFkJ2nJAwa0NQtPJVLmLzFeLaWkBBKpnnHuP5E1zTdzqijo0gVcDbz1HHSrSxheciq9pI81sjuFWUcOAwOCOv8A9ap2dNvykMR1wQaxNUOK84puNvSmo7MR6elTKNx4HAoGIgOCSKmVeOKkjTI5HFSbOw6etAiDhsZ4pkse5dhHXmrYQHmmkAtgCmSzzHxl4TklnF1YW6ZcBSqkKXfnp7kevXbXnBJ3ZByvtX0Xd2izxbWQOewPQ/5/rXlPi7ww2nytcxRYhdhyiKADj0H5/jXTTq9Gc1Wn1RySeXKvlzD5T91xyQe1Ry2TJ93Ljkg+1R5aMkNwfr3rTsp0J2PneM8H+lb3MEl1Mfa0ODgqD19D6VcgmLLsZixOMDcQcex6D+R9q05LCGSMvEw3EZCMMg/0zWe9oFw68A9Bnt/nt/Ki4ctiVreC5YqDyi7t6jDr/vr/ABfVen0qHyCo8i4AWOXG2RQCCezex7HPUVLHG8TiTfwB8jc5X6dx+FTCZo4z9ojDpnLfN8xHtzg/iDSuKxmKksErxurKyn5wOCtTA9SV2kdVXp+HtWktvFeu32a5WZ0G1d6kkDsCPvfiOvvUAi3rG4T7wJQbgc+2Rxnj9abYWI4g6PvjwTjGOlSC+lhAjO8EdtpohETt8r59c8H36irQtLh1DRspXHcikMfeD5SR3AzWemfOBPdCOPw/wrUuuYAOgJIrOXKyW7N0yc/rWjMYvQWMnbKmBwfT1HejB8r0AOak2+UxA9cflUbAkMOwwf51JogJx5cg/gYYpVJCk46dOaF6MmeSMj8KjhcK3OTkf/WpMYsiqpY4JGeKpyL5NyWDABxlgP0/Wr7E7ecfJ8nXqP8AJqlKpMAZgBsbacdcdqAZcQ5QHJB64qvdREwygEcHdyOpH/6zUlrIJITnqOCfWnyLvV8AlipApIY64UXWihQuWj3MzHkkDPH1xj9awLhTG7J8uTjGB8pGOx/GtawldYL2PA3IxYHv0/8ArCqc5SaLcNoLNuCqO+MnH5/zrRaGckVG5Ct3A6nrSr8xyAN3bFK2Ex1Kk4BxyaUphQGGQc9O/wDhTEi/bRDAO4e+fU9q6y2iBtphIGH7v5lXvj/9XFcvp4V5YkOducECu0EwgguWyQ0cRcADPOcZrnqbnTS2NrR4lg08EoHy5bcT1/8ArYxW7dgtallIyCChHc+9ULVRaTRQBtyiNQQ3JHHStNVjBEQJGTtGelcrOxGfp/k2d40KxMEPyg5yCCdwP1B4+laUNpBAzGJMZ7msu8l8gRTou5UfcVI7ZII+nINbjBs5AAGPWpZSDAB6flVqEDAJFVFLFxzxV2MLtFSBaVRin46YFMVSVyOnvSksFwMcetAg549BSkccUgYjkgY9qNwZcg8U7hIYR3qneWsc1u6OAVIII654q+wOMdahKDdjn15oTJPG/EnhSS1eaaD5owclc5ZeK5Fg0RIOeOc+lfQV9Zq6HPTdmuC8QeEYr1nltyI5hyRjKnj9K6YVNLM550+qOEiuH27nO5O7DqPrU24SuzplXxgHjDCoL7S7jTLkxTjbtOQUJx+BqFD1w8mD1ycVto9jGzW5dXdDvkiT5WA3onRiO+D/APWp0TKJVmZxGjA5AOPzByKoruDHEwU9OpzT/MbglycdietOwif7RHKNlwC3Qds+3r/OtGJ2uMGaVSy8eZJgMw9M9CPrzWQJpicq6AD1FSKZ3JJZCfQpRYC9Mj+Qgu4zgk5ZgCB6Z5yP1+tRfZ+TlXb0I5H6g/zpIBco2WkQJ/un/GrYUsM74Qe4J/8Ar0mgHz8xqOuDn9KyYiC0Y9D/AErSunYQbuODxWXCCZwM85P8q2Zgti/P1z7E1W5Abj05/X+tTyklccen1qIng57nmoZpEEZS6EjjBDfjUG3bKUGcA4qUEKGzjjmkl+Viw5IJ4pFEhfcPXcgJ+o61WcYYcgq4wfep7cgspPHJH51XYkoo7qMUCYlsPLkKnoen6VaQHlSPaqvKyIR34q5G2Zg3ODjnHakxop2KiO/uU42lc4/A5qmvyyCPpjknuvofwNXrb/j4vX2ncIiVP/AQP61SlXYzMpycBOO/HNaEsbksXB2+bzuyeG/xpI0LDK8sD09Mf5NN+Ygvg57mpGYeWMZDKM5z3NAJF/S28uXecryO3vXS7mkh8tmJSRgpJHbjiuUWZlKqW5I65/Ouo0WU3F1Ao+ZUG588g8YUfy/KsJrqdFNo7iMl08xjlz82D+X8qvQsP4jxtPT9DWRZ/KhVnJ6lSDzj0q/C4UDdyAAM57c1ys60Pum/eNkblLnjHTI5H6mrVrP51tBJvBLIM49cDP65rPvHIglJI/vc+wH+A/P3qbSyBZoAQVGQD+NSykaAbEgA71dg4xmqSLzmr8Q+QE0mBZ3lhgECjdgDIyRTBhRnI/OgHdn3pCZIDnj8aawCkEcClGB2waRlzyelAMB0ximng85xTw3cc0x24xTEMlUSKV71l3NtuU44IFaLH3qGVgVx1ppktHOXVmshZSoweoNYM/hrTZpSzWqszZ53lcf98muylRcHA696oNF83zDHPNaKTWxLSe5yT+D9Lb5lWeMg9FfcP1qleeGYoo1NursQeR3Yf0rtSNvGeKrSR7v6VfOyHFI4TXNOtbbT473TF+UN+9UOz7VPAySTg54rnVllZecnvw56V3t9p8aQXThWMMykXEQ/iHUMPQhsE4wMLXBmN4JGTkMOlb05XRz1I2eg1JVVidiEn+JgTVoT3K8AwgfQVE0YAIwQM5qMrIvC4x7irMzdlfdaMp69c1nIQlwp9eP0qx5m6PA9OlVZP9ZAQP41q2ZxL8gOEA7kk1GcZ44B6CpMghmJPQY9qiONx/TioZohrDOc9waG/wBY2R1wTilfhSRxSSL8zY4+UUigXEYXB67Tn86i4Lv1zu6+2ae2dq9RwBn8ajYHzmA/vfnQJjXLJt574q2CFhyBzjFVpwAR2+bIqeYgQ9vv5oYIgUshmlQcSbUyf1/Rap7NyjJ5OX/z+dWpZcQbBz049yD/AEqNsI5OOFGPzpiIlQZ2+nUUBMzBSMruOfenpglzxz7Ur7c4yMhefxpjRA0heVyAcAcACtfS7yaG4Rockjho9wXf+efrWRGAJDjGBVq0JVzJGcSIR8vTIBqZLQuOjPTdOuDIkciygqQSCVA7HitSyQBlQshXaDn1Hr+v6VyumXKsGC4Jkw64b1roreYhFPC7QEIxkdT/AI1xNWOyLJZsmHYSSZGGMfVc/otWtJhMOmW6PkFRkj681k6nPIj+TH88m0MpXjBJ249u5ro4yGHynK9MCpZoieP5jnPJ7VdX7o9agiALdhjGKsjripGOXGOcDHtQTg8UFgFI6VGXPHNIRKXAAOKaz5U5/WoSW45yPr0pjNlDzQBIZtowOKYZT1PP41DyQKblgetAiZnXbmoWbgnOfemnkGo2OBjHNNCZHKy1UbG4570+Y7ScmqElwUOCCc9/SrRDJJORxz7VWkyvAGeemetH2jedi53HOADzWxZ2At4455lLSONyIEySCO3065renDnMZ1FBGSmni7hM03mRpyGhkQqWx054OPf8K8+8V6WlnciS3kjYY+fywAF9+vTtn1r1O/nmmuSLYIGlPyxhflAHUZI4/wDr1gXen21vbCzZBJI2FLzH5QuDz+fSurlUVZHG5OTuzy9ZcbSQChHGeo+tTfLgfIGHY5NTz2htLzyHDSZ4CsevuKgw2SUkZkOCCkJx09O30pFI0ry3+zyF1GUJ6dhVA/dUHHB610csQkQoy5B4I9a56VGiuHjPYDHFNkosQENDyT0pQCxyPX8qitiVUjPUVYto3mkSNQSSeg/WpZaRXnB2sByfrTn/AIienHFWZrV9jShTt3Y578Z/lVWcFTz1ziouXsNXlVyRndURbM6EEj94cn8KmRSY055qshDOo9ycGqJZM6hpE3dBg9e+T/jSsjSoqA45HPpxUhRTLkDIDZGfxob5Ixz1HrRcZSlfzZlGNoZi5H5AfpTp1xKwI4zk/hTIhuk3t1xgGnNl8sO9MkSNMSnPf29KaFH2mQ8ckYGOv+c1LCeGbrgHrUfSVXAwDn+X/wBegaIRwW4qS0mC3ABXgE54z7U0KfMcEd8Uy2wJmLZyDmgbdjrtElRHaNuSPmTP8h/hXUWEi78f3QTzzz/nP5V5/aXBi2SK53ocq3Q59j9K7C2uDGilpFJI3NsXGfp/9f3rKdO+prCqlua9uRdX/mRk7VG4lvm56AY+nNbELSBgSBtxzj69aw9ME0druMWGf52wRkAjj9BWtBPy4yMjsQciuaUGtzpjNPZmvFKMAg7vWrIk7g1lQykqeMfSrCuxXh2/E1nYu5cLnr19ab5gx1AOagyTxnJpygHgsw/GkK5PioyhXjNEZ3dT+PrVgICORmgLlYBvSnCNu69qs7B02ilKgAe1AXKDIQeOKjZOtaDRqRkk/nUE+I03NwKpK5DkjKnjOD3rNe1muCY4kLMDg84AJ9T2rcjjE86JgyDzArKmN3Ucc9KmVkCyQygwAy7RDHgH5c8n+9698c1106DesjmqYhLRGdaafDYSxQu6veS43QPtJ9xwSAOueueMgVfu38q48mEeZL5asVC4ESH7oB6Y7cdqPsymZJoIYvtJYlZmQEKp689c4/yaLiO5hVY7NROWG5nd/vn1weT/ACFdaSSsjkcm9WUXlCZmgbem399LkBQD79z7CqbQzXFs010iGBcEDbtGTnkg9Tg1ptZRLazxSRwfZyAWMke/kHI+Ud81kTl3t2ilnugxcrCqkEyHsSMHj3xQwOP1uxhntln4jl3hUATBXjJO7+nHWuUkikRyCHDd9rlf5V6Jd2iIyJeIGjYYGSGAb296xZNPuI2xJabWPOGAOB/n9c1FikxWHHp3rN1a2RrdrgACQHB915rRB4xUF6ubSQeoOfypsVzAjyG69u9dF4c8vzLjzF3S/ZpBH0xk9iD1rIhhBso3zyQQfcZpiu0Tq6khuMY+uKzkawZ1FnHGNFmQ4aUStuBHB4OP0rm9UjMLIQTsIyvPYEf0rehnkeOy5Ch96HbwcVR1hQ8AXn5d2CevWs1oaOzRmRDKYHPp9KpRcsHAPft7Vdj4jU9SNoqqigZP+yxxVpkIuAE9sHrjHtVO8k2mQL0x8uPy/rV8nETv/ESFz6c//WrImP7o/X1oQMkjXbAhP8RJHtzSDHlEexFTMoL4wAAT0pm3D7B3zz+FUSBXFq/JOQQfypRHkNu42jjNPIC24UZxu5yfWnxqDFM3cE4qWUipcKzABRyx5b2H/wBfFRrhBgkdeauSLiNsdRxn61nzAAOBkYwBVITJo2eKRC+4xnjArodOla7RLYS4DHYcDkqPb1I/lWHb/vWkDeuOKsQySWmp7IJGQ+WeR6ZHFNCPT7GVJFWKR2EiglsKSG6cD0A6AdOM1clVpZSkQUkYLMeo6dT1PHP6Vg6HdyXAhWQL88ZZiByeproraJookmSaVY3kKmFSAu3J9s546+9VZPcz1WwqCQudqZU9CvCrx3zz6dOaTzZY2IJHy9cE8VAl4/myEquN/br6Dmr9vYQNYQzMuTdo4KE/KgHGAP61EsPBmirzWg+KUsBg9f73+ferKkEEN0rKt7LMjXayYYyLEkZGUUKrDp349+tWfKeexFy0pVBu/doMZI4yT+FZPC9mbLFd0aSDAG3Bx2qwHUt7ngD1rFAGxVkG4NErgZxj/OKSeWS6XyotkImwCdpbn8/ap+qsPrSNvzkxuBBHtSp++MhQHEY3MSCBiqtvZLesqPI67l5YdcZxiqnh+RtWEc8jNGgMkKRKeEGcZHv8vWtI4WPUzliX0NGZdqyRNJtdeSFODjGc5qjGYJLmWMEM0fLeYSWPHA557j060mu3H2C2uLhY0cxxebtIwCFP3fYVHpjfa997lo8wi4MangccKPbjn1z2reNOMdjGU29y2LiGxkiwqPcTcpGq8sc8Ddjj3zUN48jXPmzFVl6iGM4IOOnXFQSReTarqHyGe73GRguD8p4xWZYDztQyThp2YMw6jap6fnVsgv3OpC0tFZ1R5XDCOFASWYYyB2GM8k1JeFrjSVju53geMKzm1kKc55Xd1I/LNOSRoEumjwGWJOWGSe1MhuhPJHI0KZih3EHJDfJnn86BkjXAu9pEYhTIyuOGHvioLiOCxmmlxnKgNNIR8v8AsjHQdDSG5ea/jtowsCTxtLIYhgnrx+lVdSCQ3e2FFjVB/D1J/wA5pAjMmt7ySJ52wjncw4AIGf8ADp14pr2M0+HmSMvjtkY/Iiti6s4ZWgVwxJjaQsDg/KRx+tRSaf5sh2ShVUlQGjDHr60mM//ZAAAAA2RzZgAAAJlGnmFNAQAAAAAAQSAAAAAAAKoYqgN9OtxLB0fbNAFeCOqJf+sjSbUUWV0ov9APGIG8NTaqbwQBfL+1zJnx1neYuk9NxwnKHxrP/FpXFHRwrtsB");
        try {
            //PARSE FROM BYTES
            person = (PersonHuman) ItemFactory.getInstance()
                    .parse(ItemCls.PERSON_TYPE, 0, rawPerson, false);
        } catch (Exception e) {
            fail("Exception while parsing transaction.  : " + e);
        }

        issuePersonTransaction = new IssuePersonRecord(registrar, person, FEE_POWER, timestamp, null);

        // PARSE ISSEU PERSON RECORD
        issuePersonTransaction.sign(registrar, Transaction.FOR_NETWORK);
        issuePersonTransaction.copy();

        //CONVERT TO BYTES
        byte[] rawIssuePersonRecord = issuePersonTransaction.toBytes(Transaction.FOR_NETWORK);

        //CHECK DATA LENGTH
        assertEquals(rawIssuePersonRecord.length, issuePersonTransaction.getDataLength(Transaction.FOR_NETWORK));

        IssuePersonRecord parsedIssuePersonRecord = null;
        try {
            //PARSE FROM BYTES
            parsedIssuePersonRecord = (IssuePersonRecord) TransactionFactory.getInstance().parse(rawIssuePersonRecord, Transaction.FOR_NETWORK);

        } catch (Exception e) {
            fail("Exception while parsing transaction.  : " + e);
        }

        //CHECK INSTANCE
        assertTrue(parsedIssuePersonRecord instanceof IssuePersonRecord);

        //CHECK SIGNATURE
        assertArrayEquals(issuePersonTransaction.getSignature(), parsedIssuePersonRecord.getSignature());

        //CHECK ISSUER
        assertEquals(issuePersonTransaction.getCreator().getAddress(), parsedIssuePersonRecord.getCreator().getAddress());

        PersonHuman parsedPerson = (PersonHuman) parsedIssuePersonRecord.getItem();

        //CHECK OWNER
        assertEquals(person.getAuthor().getAddress(), parsedPerson.getAuthor().getAddress());

        //CHECK NAME
        assertEquals(person.getName(), parsedPerson.getName());

        //CHECK REFERENCE
        //assertEquals(issuePersonTransaction.getReference(), parsedIssuePersonRecord.getReference());

        //CHECK TIMESTAMP
        assertEquals(issuePersonTransaction.getTimestamp(), parsedIssuePersonRecord.getTimestamp());

        //CHECK DESCRIPTION
        assertEquals(person.getDescription(), parsedPerson.getDescription());

        assertEquals(person.getItemTypeName(), parsedPerson.getItemTypeName());
        assertEquals(person.getStartDate(), parsedPerson.getStartDate());
        assertEquals(person.getGender(), parsedPerson.getGender());
        assertEquals(person.getRace(), parsedPerson.getRace());
        assertEquals(person.getBirthLatitude(), parsedPerson.getBirthLatitude(), 0.0);
        assertEquals(person.getBirthLongitude(), parsedPerson.getBirthLongitude(), 0.0);
        assertEquals(person.getSkinColor(), parsedPerson.getSkinColor());
        assertEquals(person.getEyeColor(), parsedPerson.getEyeColor());
        assertEquals(person.getHairColor(), parsedPerson.getHairColor());
        assertEquals(person.getHumanHeight(), parsedPerson.getHumanHeight());

        Transaction txCopy = issuePersonTransaction.copy();

        //PARSE TRANSACTION FROM WRONG BYTES
        rawIssuePersonRecord = new byte[issuePersonTransaction.getDataLength(Transaction.FOR_NETWORK)];

        try {
            //PARSE FROM BYTES
            TransactionFactory.getInstance().parse(rawIssuePersonRecord, Transaction.FOR_NETWORK);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }
    }

    @Test
    public void parseIssuePersonRecord_2() {

        initPerson();

        // PARSE PERSON

        byte[] rawPerson = person.toBytes(false);
        person.sign(maker);
        assertEquals(rawPerson.length, person.getDataLength(false));
        rawPerson = person.toBytes(true);
        assertEquals(rawPerson.length, person.getDataLength(true));

        rawPerson = person.toBytes(false);
        PersonHuman parsedPerson = null;
        try {
            //PARSE FROM BYTES
            parsedPerson = (PersonHuman) ItemFactory.getInstance()
                    .parse(ItemCls.PERSON_TYPE, 0, rawPerson, false);
        } catch (Exception e) {
            fail("Exception while parsing transaction.  : " + e);
        }
        assertEquals(rawPerson.length, person.getDataLength(false));
        assertEquals(parsedPerson.getHumanHeight(), person.getHumanHeight());
        assertEquals(person.getAuthor().getAddress(), parsedPerson.getAuthor().getAddress());
        assertEquals(person.getName(), parsedPerson.getName());
        assertEquals(person.getDescription(), parsedPerson.getDescription());
        assertEquals(person.getItemTypeName(), parsedPerson.getItemTypeName());
        assertEquals(person.getStartDate(), parsedPerson.getStartDate());
        assertEquals(person.getStopDate(), parsedPerson.getStopDate());
        assertEquals(person.getGender(), parsedPerson.getGender());
        assertEquals(person.getRace(), parsedPerson.getRace());
        assertEquals(person.getBirthLatitude(), parsedPerson.getBirthLatitude(), 0.0);
        assertEquals(person.getBirthLongitude(), parsedPerson.getBirthLongitude(), 0.0);
        assertEquals(person.getSkinColor(), parsedPerson.getSkinColor());
        assertEquals(person.getEyeColor(), parsedPerson.getEyeColor());
        assertEquals(person.getHairColor(), parsedPerson.getHairColor());
        assertEquals(person.getHumanHeight(), parsedPerson.getHumanHeight());


        // PARSE ISSEU PERSON RECORD
        issuePersonTransaction.sign(registrar, Transaction.FOR_NETWORK);
        //issuePersonTransaction.process(dcSet, false);

        //CONVERT TO BYTES
        byte[] rawIssuePersonRecord = issuePersonTransaction.toBytes(Transaction.FOR_NETWORK);

        //CHECK DATA LENGTH
        assertEquals(rawIssuePersonRecord.length, issuePersonTransaction.getDataLength(Transaction.FOR_NETWORK));

        IssuePersonRecord parsedIssuePersonRecord = null;
        try {
            //PARSE FROM BYTES
            parsedIssuePersonRecord = (IssuePersonRecord) TransactionFactory.getInstance().parse(rawIssuePersonRecord, Transaction.FOR_NETWORK);

        } catch (Exception e) {
            fail("Exception while parsing transaction.  : " + e);
        }

        //CHECK INSTANCE
        assertTrue(parsedIssuePersonRecord instanceof IssuePersonRecord);

        //CHECK SIGNATURE
        assertArrayEquals(issuePersonTransaction.getSignature(), parsedIssuePersonRecord.getSignature());

        //CHECK ISSUER
        assertEquals(issuePersonTransaction.getCreator().getAddress(), parsedIssuePersonRecord.getCreator().getAddress());

        parsedPerson = (PersonHuman) parsedIssuePersonRecord.getItem();

        //CHECK OWNER
        assertEquals(person.getAuthor().getAddress(), parsedPerson.getAuthor().getAddress());

        //CHECK NAME
        assertEquals(person.getName(), parsedPerson.getName());

        //CHECK REFERENCE
        //assertEquals(issuePersonTransaction.getReference(), parsedIssuePersonRecord.getReference());

        //CHECK TIMESTAMP
        assertEquals(issuePersonTransaction.getTimestamp(), parsedIssuePersonRecord.getTimestamp());

        //CHECK DESCRIPTION
        assertEquals(person.getDescription(), parsedPerson.getDescription());

        assertEquals(person.getItemTypeName(), parsedPerson.getItemTypeName());
        assertEquals(person.getStartDate(), parsedPerson.getStartDate());
        assertEquals(person.getGender(), parsedPerson.getGender());
        assertEquals(person.getRace(), parsedPerson.getRace());
        assertEquals(person.getBirthLatitude(), parsedPerson.getBirthLatitude(), 0.0);
        assertEquals(person.getBirthLongitude(), parsedPerson.getBirthLongitude(), 0.0);
        assertEquals(person.getSkinColor(), parsedPerson.getSkinColor());
        assertEquals(person.getEyeColor(), parsedPerson.getEyeColor());
        assertEquals(person.getHairColor(), parsedPerson.getHairColor());
        assertEquals(person.getHumanHeight(), parsedPerson.getHumanHeight());

        Transaction txCopy = issuePersonTransaction.copy();

        //PARSE TRANSACTION FROM WRONG BYTES
        rawIssuePersonRecord = new byte[issuePersonTransaction.getDataLength(Transaction.FOR_NETWORK)];

        try {
            //PARSE FROM BYTES
            TransactionFactory.getInstance().parse(rawIssuePersonRecord, Transaction.FOR_NETWORK);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }
    }

    @Test
    public void processIssuePersonRecord() {

        for (int dbs : TESTED_DBS) {

            try {
                init(dbs);

                person.sign(maker);
                issuePersonTransaction.sign(registrar, Transaction.FOR_NETWORK);

                issuePersonTransaction.setDC(dcSet, true);

                assertEquals(Transaction.VALIDATE_OK, issuePersonTransaction.isValid(Transaction.FOR_NETWORK, txFlags));
                issuePersonTransaction.setDC(dcSet, Transaction.FOR_NETWORK, 3, seqNo++, true);

                issuePersonTransaction.process(gb, Transaction.FOR_NETWORK);

                LOGGER.info("person KEY: " + person.getKey());

                BigDecimal eraUSE = new BigDecimal("1000");

                //CHECK BALANCE ISSUER
                assertEquals(eraUSE.setScale(registrar.getBalanceUSE(ERM_KEY, dcSet).scale()), registrar.getBalanceUSE(ERM_KEY, dcSet));

                BigDecimal compuUSE = new BigDecimal("1");
                assertEquals(compuUSE.subtract(issuePersonTransaction.getFee()).setScale(BlockChain.AMOUNT_DEFAULT_SCALE),
                        registrar.getBalanceUSE(FEE_KEY, dcSet));

                //CHECK PERSON EXISTS DB AS CONFIRMED:  key > -1
                long key = issuePersonTransaction.getKey();
                assertTrue(key >= 0);
                assertTrue(dcSet.getItemPersonMap().contains(key));

                //CHECK PERSON IS CORRECT
                assertArrayEquals(dcSet.getItemPersonMap().get(key).toBytes(true), person.toBytes(true));

                //CHECK REFERENCE SENDER
                assertEquals(issuePersonTransaction.getTimestamp(), (Long) registrar.getLastTimestamp(dcSet)[0]);

                //////// ORPHAN /////////
                issuePersonTransaction.orphan(gb, Transaction.FOR_NETWORK);

                //CHECK BALANCE ISSUER
                assertEquals(eraUSE.setScale(registrar.getBalanceUSE(ERM_KEY, dcSet).scale()), registrar.getBalanceUSE(ERM_KEY, dcSet));
                assertEquals(compuUSE.setScale(BlockChain.AMOUNT_DEFAULT_SCALE), registrar.getBalanceUSE(FEE_KEY, dcSet));

                //CHECK PERSON EXISTS ISSUER
                assertFalse(dcSet.getItemPersonMap().contains(personKey));

                //CHECK REFERENCE ISSUER
                //assertEquals(issuePersonTransaction.getReference(), registrar.getLastReference(dcSet));
            } catch (TxException e) {
                throw new RuntimeException(e);
            }

            dcSet.close();

        }
    }

    @Test
    public void processAndCertify() {

        for (int dbs : TESTED_DBS) {

            try {
                init(dbs);

                person.sign(maker);
                assertFalse(maker.isPersonAlive(dcSet, Integer.MAX_VALUE));

                issuePersonTransaction = new IssuePersonRecord(registrar, null, true, person, FEE_POWER, ++timestamp);
                issuePersonTransaction.sign(registrar, Transaction.FOR_NETWORK);

                issuePersonTransaction.setDC(dcSet, true);

                assertEquals(Transaction.VALIDATE_OK, issuePersonTransaction.isValid(Transaction.FOR_NETWORK, txFlags));

                issuePersonTransaction.setDC(dcSet, Transaction.FOR_NETWORK, 3, seqNo++, true);
                issuePersonTransaction.process(gb, Transaction.FOR_NETWORK);

                LOGGER.info("person KEY: " + person.getKey());

                BigDecimal eraUSE = new BigDecimal("1000");

                //CHECK BALANCE ISSUER
                assertEquals(eraUSE.setScale(registrar.getBalanceUSE(ERM_KEY, dcSet).scale()), registrar.getBalanceUSE(ERM_KEY, dcSet));

                BigDecimal compuUSE = new BigDecimal("1");
                assertEquals(compuUSE.setScale(BlockChain.AMOUNT_DEFAULT_SCALE),
                        registrar.getBalanceUSE(FEE_KEY, dcSet));

                //CHECK PERSON EXISTS DB AS CONFIRMED:  key > -1
                long key = issuePersonTransaction.getKey();
                assertTrue(key >= 0);
                assertTrue(dcSet.getItemPersonMap().contains(key));

                //CHECK PERSON IS CORRECT
                assertArrayEquals(dcSet.getItemPersonMap().get(key).toBytes(true), person.toBytes(true));

                //CHECK REFERENCE SENDER
                assertEquals(issuePersonTransaction.getTimestamp(), (Long) registrar.getLastTimestamp(dcSet)[0]);

                //
                assertTrue(maker.isPerson(dcSet));
                assertTrue(maker.isPersonAlive(dcSet, 1));
                assertFalse(maker.isPersonAlive(dcSet, Integer.MAX_VALUE));

                //////// ORPHAN /////////
                issuePersonTransaction.orphan(gb, Transaction.FOR_NETWORK);

                //CHECK BALANCE ISSUER
                assertEquals(eraUSE.setScale(registrar.getBalanceUSE(ERM_KEY, dcSet).scale()), registrar.getBalanceUSE(ERM_KEY, dcSet));
                assertEquals(compuUSE.setScale(BlockChain.AMOUNT_DEFAULT_SCALE), registrar.getBalanceUSE(FEE_KEY, dcSet));

                //CHECK PERSON EXISTS ISSUER
                assertFalse(dcSet.getItemPersonMap().contains(personKey));
                assertFalse(maker.isPerson(dcSet));

                //CHECK REFERENCE ISSUER
                //assertEquals(issuePersonTransaction.getReference(), registrar.getLastReference(dcSet));
            } catch (TxException e) {
                throw new RuntimeException(e);
            }

            dcSet.close();

        }
    }


    ///////////////////////////////////////
    // PERSONONALIZE RECORD
    ///////////////////////////////////////
    @Test
    public void validatePersonalizeRecord() {

        for (int dbs : TESTED_DBS) {

            try {
                init(dbs);

                initPersonalize();

                r_CertifyPubKeys.setDC(dcSet, Transaction.FOR_NETWORK, height, 3, true);

                assertEquals(Transaction.VALIDATE_OK, r_CertifyPubKeys.isValid(Transaction.FOR_NETWORK, txFlags));

                certifier.changeBalance(this.dcSet, true, AssetCls.ERA_KEY, new BigDecimal("1000"), false, false);
                assertEquals(Transaction.NOT_ENOUGH_ERA_USE_100, r_CertifyPubKeys.isValid(Transaction.FOR_NETWORK,
                        txFlags | Transaction.NOT_VALIDATE_FLAG_PERSONAL));

                //CREATE INVALID PERSONALIZE RECORD NOT ENOUGH ERM BALANCE
                RCertifyPubKeys personalizeRecord_0 = new RCertifyPubKeys(userAccount1, FEE_POWER, personKey,
                        certifiedPublicKeys,
                        356, timestamp);
                personalizeRecord_0.setDC(dcSet, Transaction.FOR_NETWORK, height, 4, true);
                if (!Settings.getInstance().isTestNet()) {
                    assertEquals(Transaction.CREATOR_NOT_PERSONALIZED, personalizeRecord_0.isValid(Transaction.FOR_NETWORK, txFlags));
                }

                //CREATE INVALID PERSONALIZE RECORD KEY NOT EXIST
                personalizeRecord_0 = new RCertifyPubKeys(registrar, FEE_POWER, personKey + 10,
                        certifiedPublicKeys, registrar.getLastTimestamp(dcSet)[0] + 1);
                personalizeRecord_0.setDC(dcSet, Transaction.FOR_NETWORK, height, 5, true);
                assertEquals(Transaction.ITEM_PERSON_NOT_EXIST, personalizeRecord_0.isValid(Transaction.FOR_NETWORK, txFlags));

                //CREATE INVALID ISSUE PERSON FOR INVALID PERSONALIZE
                personalizeRecord_0 = new RCertifyPubKeys(userAccount2, FEE_POWER, personKey,
                        certifiedPublicKeys,
                        356, timestamp);
                personalizeRecord_0.setDC(dcSet, Transaction.FOR_NETWORK, BlockChain.ALL_BALANCES_OK_TO + 1, 5, true);

                //CREATE INVALID ISSUE PERSON - NOT FEE
                userAccount2.changeBalance(dcSet, false, ERM_KEY,
                        BigDecimal.valueOf(1000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
                assertEquals(Transaction.NOT_ENOUGH_FEE, personalizeRecord_0.isValid(Transaction.FOR_NETWORK,
                        txFlags | Transaction.NOT_VALIDATE_FLAG_PERSONAL));
                // ADD FEE
                userAccount2.changeBalance(dcSet, false, FEE_KEY,
                        BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
                assertEquals(Transaction.CREATOR_NOT_PERSONALIZED, personalizeRecord_0.isValid(Transaction.FOR_NETWORK, txFlags));
                // ADD RIGHTS
                userAccount1.changeBalance(dcSet, false, ERM_KEY,
                        BigDecimal.valueOf(10000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
                assertEquals(Transaction.CREATOR_NOT_PERSONALIZED, personalizeRecord_0.isValid(Transaction.FOR_NETWORK, txFlags));

                List<PublicKeyAccount> certifiedPublicKeys011 = new ArrayList<PublicKeyAccount>();
                certifiedPublicKeys011.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, new byte[60]));
                certifiedPublicKeys011.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, userAccount2.getPublicKey()));
                certifiedPublicKeys011.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, userAccount3.getPublicKey()));
                personalizeRecord_0 = new RCertifyPubKeys(registrar, FEE_POWER, personKey,
                        certifiedPublicKeys011,
                        356, timestamp);
                personalizeRecord_0.setDC(dcSet, Transaction.FOR_NETWORK, BlockChain.ALL_BALANCES_OK_TO + 1, 5, true);
                assertEquals(Transaction.INVALID_PUBLIC_KEY, personalizeRecord_0.isValid(Transaction.FOR_NETWORK, txFlags));

            } catch (TxException e) {
                throw new RuntimeException(e);
            }
            dcSet.close();
        }
    }

    @Test
    public void validateSignaturePersonalizeRecord() {

        for (int dbs : TESTED_DBS) {

            init(dbs);

            // SIGN only by registrar
            version = 0;
            initPersonalize();

            r_CertifyPubKeys.sign(certifier, Transaction.FOR_NETWORK);
            // TRUE
            r_CertifyPubKeys.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
            assertTrue(r_CertifyPubKeys.isSignatureValid());

            version = 1;
            r_CertifyPubKeys = new RCertifyPubKeys(registrar, FEE_POWER, personKey,
                    certifiedPublicKeys,
                    timestamp);

            r_CertifyPubKeys.sign(registrar, Transaction.FOR_NETWORK);

            // true !
            //CHECK IF PERSONALIZE RECORD SIGNATURE IS VALID
            r_CertifyPubKeys.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
            assertTrue(r_CertifyPubKeys.isSignatureValid());

            //INVALID SIGNATURE
            r_CertifyPubKeys.setTimestamp(r_CertifyPubKeys.getTimestamp() + 1);

            //CHECK IF PERSONALIZE RECORD SIGNATURE IS INVALID
            assertFalse(r_CertifyPubKeys.isSignatureValid());

            // BACK TO VALID
            r_CertifyPubKeys.setTimestamp(r_CertifyPubKeys.getTimestamp() - 1);
            assertTrue(r_CertifyPubKeys.isSignatureValid());

            r_CertifyPubKeys.signature = new byte[64];
            //CHECK IF PERSONALIZE RECORD SIGNATURE IS INVALID
            assertFalse(r_CertifyPubKeys.isSignatureValid());

            // BACK TO VALID
            r_CertifyPubKeys.sign(registrar, Transaction.FOR_NETWORK);
            assertTrue(r_CertifyPubKeys.isSignatureValid());

            dcSet.close();
        }
    }

    @Test
    public void parsePersonalizeRecord() {

        for (int dbs : TESTED_DBS) {

            init(dbs);

            version = 1;
            initPersonalize();

            // SIGN
            r_CertifyPubKeys.sign(registrar, Transaction.FOR_NETWORK);

            //CONVERT TO BYTES
            byte[] rawPersonTransfer = r_CertifyPubKeys.toBytes(Transaction.FOR_NETWORK);

            //CHECK DATALENGTH
            assertEquals(rawPersonTransfer.length, r_CertifyPubKeys.getDataLength(Transaction.FOR_NETWORK));

            try {
                //PARSE FROM BYTES
                RCertifyPubKeys parsedPersonTransfer = (RCertifyPubKeys) TransactionFactory.getInstance().parse(rawPersonTransfer, Transaction.FOR_NETWORK);

                //CHECK INSTANCE
                assertTrue(parsedPersonTransfer instanceof RCertifyPubKeys);

                //CHECK TYPEBYTES
                assertArrayEquals(r_CertifyPubKeys.getTypeBytes(), parsedPersonTransfer.getTypeBytes());

                //CHECK TIMESTAMP
                assertEquals(r_CertifyPubKeys.getTimestamp(), parsedPersonTransfer.getTimestamp());

                //CHECK REFERENCE
                //assertEquals(r_CertifyPubKeys.getReference(), parsedPersonTransfer.getReference());

                //CHECK CREATOR
                assertEquals(r_CertifyPubKeys.getCreator().getAddress(), parsedPersonTransfer.getCreator().getAddress());

                //CHECK FEE POWER
                assertEquals(r_CertifyPubKeys.getFeePow(), parsedPersonTransfer.getFeePow());

                //CHECK SIGNATURE
                assertArrayEquals(r_CertifyPubKeys.getSignature(), parsedPersonTransfer.getSignature());

                //CHECK KEY
                assertEquals(r_CertifyPubKeys.getKey(), parsedPersonTransfer.getKey());

                //CHECK AMOUNT
                assertEquals(r_CertifyPubKeys.getAmount(registrar), parsedPersonTransfer.getAmount(registrar));

                //CHECK USER SIGNATURES
                assertArrayEquals(r_CertifyPubKeys.getCertifiedPublicKeys().get(2).getPublicKey(), parsedPersonTransfer.getCertifiedPublicKeys().get(2).getPublicKey());

            } catch (Exception e) {
                fail("Exception while parsing transaction." + e);
            }

            //PARSE TRANSACTION FROM WRONG BYTES
            rawPersonTransfer = new byte[r_CertifyPubKeys.getDataLength(Transaction.FOR_NETWORK)];

            try {
                //PARSE FROM BYTES
                TransactionFactory.getInstance().parse(rawPersonTransfer, Transaction.FOR_NETWORK);

                //FAIL
                fail("this should throw an exception");
            } catch (Exception e) {
                //EXCEPTION IS THROWN OK
            }
            dcSet.close();
        }
    }

    @SneakyThrows
    @Test
    public void process_orphan_PersonalizeRecord() {

        for (int dbs : TESTED_DBS) {

            init(dbs);

            assertFalse(userAccount1.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount2.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount3.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            BigDecimal erm_amount_registrar = registrar.getBalanceUSE(ERM_KEY, dcSet);
            BigDecimal oil_amount_registrar = registrar.getBalanceUSE(FEE_KEY, dcSet);

            BigDecimal erm_amount_certifier = certifier.getBalanceUSE(ERM_KEY, dcSet);
            BigDecimal oil_amount_certifier = certifier.getBalanceUSE(FEE_KEY, dcSet);

            BigDecimal erm_amount_user = userAccount1.getBalanceUSE(ERM_KEY, dcSet);
            BigDecimal oil_amount_user = userAccount1.getBalanceUSE(FEE_KEY, dcSet);

            initPersonalize();

            //CHECK BALANCE REGISTRAR
            assertEquals(erm_amount_registrar, registrar.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(oil_amount_registrar.subtract(issuePersonTransaction.getFee()), registrar.getBalanceUSE(FEE_KEY, dcSet));

            //CHECK BALANCE CERTIFIER
            assertEquals(erm_amount_certifier, certifier.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(oil_amount_certifier, certifier.getBalanceUSE(FEE_KEY, dcSet));

            // .a - personKey, .b - end_date, .c - block height, .d - reference
            // PERSON STATUS ALIVE
            // exist assertEquals( null, dbPS.getItem(personKey));
            // exist assertEquals( new TreeMap<String, Stack<Tuple3<Integer, Integer, byte[]>>>(), dbPA.getItems(personKey));

            // .a - personKey, .b - end_date, .c - block height, .d - reference
            // PERSON STATUS ALIVE
            //assertEquals( genesisPersonKey + 1, personKey);
            //assertEquals( null, dbPS.getItem(personKey, ALIVE_KEY));

            // ADDRESSES
            assertNull(dbAP.getItem(userAddress1));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress1));

            assertNull(dbAP.getItem(userAddress2));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress2));

            assertNull(dbAP.getItem(userAddress3));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress3));

            //// PROCESS /////
            r_CertifyPubKeys.setDC(dcSet, Transaction.FOR_NETWORK, height, 3, true);
            r_CertifyPubKeys.sign(registrar, Transaction.FOR_NETWORK);
            r_CertifyPubKeys.process(gb, Transaction.FOR_NETWORK);
            int transactionIndex = gb.getTransactionSeq(r_CertifyPubKeys.getSignature());

            //CHECK BALANCE REGISTRAR
            assertEquals(erm_amount_registrar, registrar.getBalanceUSE(ERM_KEY, dcSet));
            // CHECK FEE BALANCE - FEE - GIFT
            assertEquals("0.00062550", r_CertifyPubKeys.getFee().toPlainString());
            assertEquals(oil_amount_registrar, registrar.getBalanceUSE(FEE_KEY, dcSet));

            //CHECK BALANCE CERTIFIER
            assertEquals(erm_amount_certifier, certifier.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(oil_amount_certifier.subtract(r_CertifyPubKeys.getFee()), certifier.getBalanceUSE(FEE_KEY, dcSet));

            //CHECK BALANCE PERSON CREATOR
            assertEquals(erm_amount_user, userAccount1.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(oil_amount_user.add(BlockChain.BONUS_FOR_NEW_PERSON(1, person)), userAccount1.getBalanceUSE(FEE_KEY, dcSet));

            assertEquals(userAccount2, userAccount2.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(userAccount2, userAccount2.getBalanceUSE(FEE_KEY, dcSet));
            assertEquals(userAccount3, userAccount3.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(userAccount3, userAccount3.getBalanceUSE(FEE_KEY, dcSet));

            //CHECK REFERENCE SENDER
            assertEquals((long) r_CertifyPubKeys.getTimestamp(), certifier.getLastTimestamp(dcSet)[0]);

            //CHECK REFERENCE RECIPIENT
            // TRUE - new reference for first send FEE
            assertEquals((long) r_CertifyPubKeys.getTimestamp(), userAccount1.getLastTimestamp(dcSet)[0]);
            assertEquals((long) r_CertifyPubKeys.getTimestamp(), userAccount2.getLastTimestamp(dcSet)[0]);
            assertEquals((long) r_CertifyPubKeys.getTimestamp(), userAccount3.getLastTimestamp(dcSet)[0]);

            ////////// TO DATE ////////
            // .a - personKey, .b - end_date, .c - block height, .d - reference
            int to_date = BlockChain.DEFAULT_DURATION + (int) (r_CertifyPubKeys.getTimestamp() / 86400000.0);

            // PERSON STATUS ALIVE - beg_date = person birthDay
            //assertEquals( (long)person.getBirthday(), (long)dbPS.getItem(personKey, ALIVE_KEY).a);
            // PERSON STATUS ALIVE - to_date = 0 - permanent alive
            //assertEquals( (long)Long.MAX_VALUE, (long)dbPS.getItem(personKey, ALIVE_KEY).b);
            //assertEquals( (int)dbPS.getItem(personKey, ALIVE_KEY).d, transactionIndex);

            // ADDRESSES
            assertEquals(personKey, (long) dbAP.getItem(userAddress1).a);
            assertEquals(to_date, (int) dbAP.getItem(userAddress1).b);
            assertEquals(1, (int) dbAP.getItem(userAddress1).c);
            // PERSON -> ADDRESS
            assertEquals(to_date, (int) dbPA.getItem(personKey, userAddress1).a);
            assertEquals(1, (int) dbPA.getItem(personKey, userAddress1).b);

            assertEquals(personKey, (long) dbAP.getItem(userAddress2).a);
            assertEquals(to_date, (int) dbAP.getItem(userAddress2).b);
            assertEquals(1, (int) dbAP.getItem(userAddress2).c);
            // PERSON -> ADDRESS
            assertEquals(to_date, (int) dbPA.getItem(personKey, userAddress2).a);
            assertEquals(1, (int) dbPA.getItem(personKey, userAddress2).b);

            assertEquals(personKey, (long) dbAP.getItem(userAddress3).a);
            assertEquals(to_date, (int) dbAP.getItem(userAddress3).b);
            assertEquals(1, (int) dbAP.getItem(userAddress3).c);
            // PERSON -> ADDRESS
            assertEquals(to_date, (int) dbPA.getItem(personKey, userAddress3).a);
            assertEquals(1, (int) dbPA.getItem(personKey, userAddress3).b);

            assertTrue(userAccount1.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertTrue(userAccount2.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertTrue(userAccount3.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            ////////// ORPHAN //////////////////
            r_CertifyPubKeys.orphan(gb, Transaction.FOR_NETWORK);

            //CHECK BALANCE REGISTRAR
            assertEquals(erm_amount_registrar, registrar.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(oil_amount_registrar.subtract(issuePersonTransaction.getFee()), registrar.getBalanceUSE(FEE_KEY, dcSet));

            //CHECK BALANCE CERTIFIER
            assertEquals(erm_amount_certifier, certifier.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(oil_amount_certifier.setScale(certifier.getBalanceUSE(FEE_KEY, dcSet).scale()), certifier.getBalanceUSE(FEE_KEY, dcSet));

            //CHECK BALANCE RECIPIENT
            assertEquals(erm_amount_user, userAccount1.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(oil_amount_user.setScale(userAccount1.getBalanceUSE(FEE_KEY, dcSet).scale()), userAccount1.getBalanceUSE(FEE_KEY, dcSet));
            assertEquals(userAccount2, userAccount2.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(userAccount2, userAccount2.getBalanceUSE(FEE_KEY, dcSet));
            assertEquals(userAccount3, userAccount3.getBalanceUSE(ERM_KEY, dcSet));
            assertEquals(userAccount3, userAccount3.getBalanceUSE(FEE_KEY, dcSet));

            //CHECK REFERENCE SENDER

            //CHECK REFERENCE RECIPIENT
            assertNull(userAccount1.getLastTimestamp(dcSet));
            assertNull(userAccount2.getLastTimestamp(dcSet));
            assertNull(userAccount3.getLastTimestamp(dcSet));

            // .a - personKey, .b - end_date, .c - block height, .d - reference
            // PERSON STATUS ALIVE - must not be modified!
            //assertEquals( (long)person.getBirthday(), (long)dbPS.getItem(personKey, ALIVE_KEY).a);

            // ADDRESSES
            assertNull(dbAP.getItem(userAddress1));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress1));

            assertNull(dbAP.getItem(userAddress2));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress2));

            assertNull(dbAP.getItem(userAddress3));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress3));

            assertFalse(userAccount1.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount2.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount3.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            /////////////////////////////////////////////// TEST DURATIONS
            // TRY DURATIONS
            int end_date = 222;
            r_CertifyPubKeys = new RCertifyPubKeys(registrar, FEE_POWER, personKey,
                    certifiedPublicKeys,
                    end_date, timestamp);
            r_CertifyPubKeys.sign(registrar, Transaction.FOR_NETWORK);
            r_CertifyPubKeys.setDC(dcSet, Transaction.FOR_NETWORK, height, 3, true);
            r_CertifyPubKeys.process(gb, Transaction.FOR_NETWORK);

            // у нас перезапись времени - по максимум сразу берем если не минус
            end_date = BlockChain.DEFAULT_DURATION;
            int abs_end_date = end_date + (int) (r_CertifyPubKeys.getTimestamp() / 86400000.0);

            // PERSON STATUS ALIVE - date_begin
            //assertEquals( (long)person.getBirthday(), (long)dbPS.getItem(personKey, ALIVE_KEY).a);

            assertEquals(abs_end_date, (int) userAccount1.getPersonDuration(dcSet).b);
            assertTrue(userAccount2.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            Block lastBlock = dcSet.getBlockMap().last();
            long currentTimestamp = lastBlock.getTimestamp();

            // TEST LIST and STACK
            int end_date2 = -12;
            r_CertifyPubKeys = new RCertifyPubKeys(registrar, FEE_POWER, personKey,
                    certifiedPublicKeys,
                    end_date2, currentTimestamp);
            r_CertifyPubKeys.sign(registrar, Transaction.FOR_NETWORK);
            r_CertifyPubKeys.setDC(dcSet, Transaction.FOR_NETWORK, height, 3, true);
            r_CertifyPubKeys.process(gb, Transaction.FOR_NETWORK);

            int abs_end_date2 = end_date2 + (int) (r_CertifyPubKeys.getTimestamp() / 86400000L);

            assertEquals(abs_end_date2, (int) userAccount2.getPersonDuration(dcSet).b);
            assertFalse(userAccount2.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            r_CertifyPubKeys.orphan(gb, Transaction.FOR_NETWORK);

            assertEquals(abs_end_date, (int) userAccount2.getPersonDuration(dcSet).b);
            assertTrue(userAccount2.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            dcSet.close();
        }
    }

    @SneakyThrows
    @Test
    public void fork_process_orphan_PersonalizeRecord() {

        // TODO !!!
        // need .process in DB then .process in FORK - then test DB values!!!
        for (int dbs : TESTED_DBS) {

            init(dbs);

            //assertEquals( null, dbPS.getItem(personKey, ALIVE_KEY));

            assertFalse(userAccount1.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount2.isPersonAlive(dcSet, dcSet.getBlockMap().size()));
            assertFalse(userAccount3.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            BigDecimal oil_amount_start = registrar.getBalanceUSE(FEE_KEY, dcSet);

            initPersonalize();

            // .a - personKey, .b - end_date, .c - block height, .d - reference
            // PERSON STATUS ALIVE
            // exist assertEquals( null, dbPS.getItem(personKey));
            // exist assertEquals( new TreeMap<String, Stack<Tuple3<Integer, Integer, byte[]>>>(), dbPA.getItems(personKey));

            // .a - personKey, .b - end_date, .c - block height, .d - reference
            // PERSON STATUS ALIVE
            assertEquals(4, personKey);
            //assertEquals( null, dbPS.getItem(personKey, ALIVE_KEY));

            // ADDRESSES
            assertNull(dbAP.getItem(userAddress1));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress1));

            assertNull(dbAP.getItem(userAddress2));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress2));

            assertNull(dbAP.getItem(userAddress3));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress3));

            BigDecimal oil_amount_diff;
            oil_amount_diff = BlockChain.BONUS_FOR_NEW_PERSON(1, person);

            BigDecimal erm_amount = registrar.getBalanceUSE(ERM_KEY, dcSet);
            BigDecimal oil_amount = registrar.getBalanceUSE(FEE_KEY, dcSet);

            BigDecimal erm_amount_user = userAccount1.getBalanceUSE(ERM_KEY, dcSet);
            BigDecimal oil_amount_user = userAccount1.getBalanceUSE(FEE_KEY, dcSet);

            long last_refRegistrar = registrar.getLastTimestamp(dcSet)[0];
            long last_refCertifier = certifier.getLastTimestamp(dcSet)[0];

            //// PROCESS /////
            r_CertifyPubKeys.sign(registrar, Transaction.FOR_NETWORK);

            DCSet fork = dcSet.fork(this.toString());

            BigDecimal userAccount2ERM = userAccount2.getBalanceUSE(ERM_KEY, fork);
            BigDecimal userAccount2FEE = userAccount2.getBalanceUSE(FEE_KEY, fork);
            BigDecimal userAccount3ERM = userAccount3.getBalanceUSE(ERM_KEY, fork);
            BigDecimal userAccount3FEE = userAccount3.getBalanceUSE(FEE_KEY, fork);

            //CHECK REFERENCE RECIPIENT
            long[] userAccount1ref = userAccount1.getLastTimestamp(fork);
            long[] userAccount2ref = userAccount2.getLastTimestamp(fork);
            long[] userAccount3ref = userAccount3.getLastTimestamp(fork);

            PersonAddressMap dbPA_fork = fork.getPersonAddressMap();
            AddressPersonMap dbAP_fork = fork.getAddressPersonMap();
            KKPersonStatusMap dbPS_fork = fork.getPersonStatusMap();

            /////////////// PROCESS ///////////////
            r_CertifyPubKeys.setDC(fork, true);
            r_CertifyPubKeys.process(gb, Transaction.FOR_NETWORK);
            int transactionIndex = gb.getTransactionSeq(r_CertifyPubKeys.getSignature());

            //CHECK BALANCE SENDER
            assertEquals(erm_amount, registrar.getBalanceUSE(ERM_KEY, dcSet));
            // CHECK FEE BALANCE - FEE - GIFT
            assertEquals(oil_amount, registrar.getBalanceUSE(FEE_KEY, dcSet));

            // IN FORK
            //CHECK BALANCE SENDER
            assertEquals(erm_amount, registrar.getBalanceUSE(ERM_KEY, fork));
            // CHECK FEE BALANCE - FEE - GIFT
            // тут мы заверителю возвращаем его затраты - у него должно сать столько же как до внесения персоны
            assertEquals(oil_amount_start, registrar.getBalanceUSE(FEE_KEY, fork));

            //CHECK BALANCE RECIPIENT
            assertEquals(erm_amount_user, userAccount1.getBalanceUSE(ERM_KEY, dcSet));
            // in FORK
            //CHECK BALANCE RECIPIENT
            assertEquals(oil_amount_diff, userAccount1.getBalanceUSE(FEE_KEY, fork));

            //CHECK REFERENCE REGISTRAR
            assertEquals(last_refRegistrar, registrar.getLastTimestamp(dcSet)[0]);
            assertEquals(last_refRegistrar, registrar.getLastTimestamp(fork)[0]);

            //CHECK REFERENCE CERTIFIER
            assertEquals(last_refCertifier, certifier.getLastTimestamp(dcSet)[0]);
            assertEquals(r_CertifyPubKeys.getTimestamp(), (Long) certifier.getLastTimestamp(fork)[0]);

            //CHECK REFERENCE PERSON MAKER
            // TRUE - new reference for first send FEE
            assertNull(userAccount1.getLastTimestamp(dcSet));
            assertEquals((long) r_CertifyPubKeys.getTimestamp(), userAccount1.getLastTimestamp(fork)[0]);

            ////////// TO DATE ////////
            // .a - personKey, .b - end_date, .c - block height, .d - reference
            int to_date = BlockChain.DEFAULT_DURATION + (int) (r_CertifyPubKeys.getTimestamp() / 86400000.0);

            // PERSON STATUS ALIVE - beg_date = person birthDay
            //assertEquals( null, dbPS.getItem(personKey, ALIVE_KEY));
            //assertEquals( (long)person.getBirthday(), (long)dbPS_fork.getItem(personKey, ALIVE_KEY).a);
            // PERSON STATUS ALIVE - to_date = 0 - permanent alive
            //assertEquals( (long)Long.MAX_VALUE, (long)dbPS_fork.getItem(personKey, ALIVE_KEY).b);
            //assertEquals( (int)dbPS_fork.getItem(personKey, ALIVE_KEY).d, transactionIndex);

            // ADDRESSES
            assertNull(dbAP.getItem(userAddress1));
            Tuple4<Long, Integer, Integer, Integer> item_AP = dbAP_fork.getItem(userAddress1);
            assertEquals(personKey, (long) item_AP.a);
            assertEquals(to_date, (int) item_AP.b);
            assertEquals(1, (int) item_AP.c);
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress1));
            assertEquals(to_date, (int) dbPA_fork.getItem(personKey, userAddress1).a);
            assertEquals(1, (int) dbPA_fork.getItem(personKey, userAddress1).b);

            assertNull(dbAP.getItem(userAddress2));
            assertEquals(personKey, (long) dbAP_fork.getItem(userAddress2).a);
            assertEquals(to_date, (int) dbAP_fork.getItem(userAddress2).b);
            //assertEquals( 1, (int)dbAP_fork.getItem(userAddress2).c);

            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress2));
            assertEquals(to_date, (int) dbPA_fork.getItem(personKey, userAddress2).a);
            //assertEquals( 1, (int)dbPA_fork.getItem(personKey, userAddress2).b);

            assertNull(dbAP.getItem(userAddress3));
            assertEquals(personKey, (long) dbAP_fork.getItem(userAddress3).a);
            assertEquals(to_date, (int) dbAP_fork.getItem(userAddress3).b);
            //assertEquals( 1, (int)dbAP_fork.getItem(userAddress3).c);

            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress3));
            assertEquals(to_date, (int) dbPA_fork.getItem(personKey, userAddress3).a);
            //assertEquals( 1, (int)dbPA_fork.getItem(personKey, userAddress3).b);

            assertFalse(userAccount1.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertNull(userAccount1.getPerson(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount2.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount3.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            assertTrue(userAccount1.isPersonAlive(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertNotEquals(null, userAccount1.getPerson(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertTrue(userAccount2.isPersonAlive(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertTrue(userAccount3.isPersonAlive(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            ////////// ORPHAN //////////////////
            r_CertifyPubKeys.orphan(gb, Transaction.FOR_NETWORK);

            //CHECK BALANCE SENDER
            assertEquals(erm_amount, registrar.getBalanceUSE(ERM_KEY, fork));
            assertEquals(oil_amount, registrar.getBalanceUSE(FEE_KEY, fork));

            //CHECK BALANCE RECIPIENT
            assertEquals(erm_amount_user, userAccount1.getBalanceUSE(ERM_KEY, fork));
            assertEquals(oil_amount_user.setScale(userAccount1.getBalanceUSE(FEE_KEY, fork).scale()), userAccount1.getBalanceUSE(FEE_KEY, fork));
            assertEquals(userAccount2ERM, userAccount2.getBalanceUSE(ERM_KEY, fork));
            assertEquals(userAccount2FEE, userAccount2.getBalanceUSE(FEE_KEY, fork));
            assertEquals(userAccount3ERM, userAccount3.getBalanceUSE(ERM_KEY, fork));
            assertEquals(userAccount3FEE, userAccount3.getBalanceUSE(FEE_KEY, fork));

            //CHECK REFERENCE REGISTRAR
            assertEquals(last_refRegistrar, registrar.getLastTimestamp(dcSet)[0]);
            assertEquals(last_refRegistrar, registrar.getLastTimestamp(fork)[0]);

            //CHECK REFERENCE CERTIFIER
            assertEquals(last_refCertifier, certifier.getLastTimestamp(dcSet)[0]);
            assertEquals(last_refCertifier, certifier.getLastTimestamp(fork)[0]);

            //CHECK REFERENCE PERSON MAKER
            // TRUE - new reference for first send FEE
            assertNull(userAccount1.getLastTimestamp(dcSet));

            //CHECK REFERENCE RECIPIENT
            assertNull(userAccount1.getLastTimestamp(fork));
            assertNull(userAccount2.getLastTimestamp(fork));
            assertNull(userAccount3.getLastTimestamp(fork));

            // .a - personKey, .b - end_date, .c - block height, .d - reference
            // PERSON STATUS ALIVE - must not be modified!
            //assertEquals( (long)person.getBirthday(), (long)dbPS_fork.getItem(personKey, ALIVE_KEY).a);

            // ADDRESSES
            assertNull(dbAP_fork.getItem(userAddress1));
            // PERSON -> ADDRESS
            assertNull(dbPA_fork.getItem(personKey, userAddress1));

            assertNull(dbAP_fork.getItem(userAddress2));
            // PERSON -> ADDRESS
            assertNull(dbPA_fork.getItem(personKey, userAddress2));

            assertNull(dbAP_fork.getItem(userAddress3));
            // PERSON -> ADDRESS
            assertNull(dbPA_fork.getItem(personKey, userAddress3));

            assertFalse(userAccount1.isPersonAlive(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount2.isPersonAlive(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount3.isPersonAlive(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            dcSet.close();
        }
    }

    @SneakyThrows
    @Test
    public void validatePersonHumanRecord() {

        for (int dbs : TESTED_DBS) {

            init(dbs);

            long birthDay = timestamp - 12345678;

            person = new PersonHuman(registrar, "Ermolaev Dmitrii Sergeevich", icon, image, "изобретатель, мыслитель, создатель идей", birthDay, birthDay - 2,
                    (byte) 0, "Slav", (float) 28.12345, (float) 133.7777,
                    "white", "green", "шанет", 188, makerSignature);

            GenesisIssuePersonRecord genesis_issue_person = new GenesisIssuePersonRecord(personGeneral, null, null, null);
            genesis_issue_person.setDC(dcSet, Transaction.FOR_NETWORK, 3, seqNo++, true);
            genesis_issue_person.process(gb, Transaction.FOR_NETWORK);
            //genesisPersonKey = dcSet.getIssuePersonMap().size();
            genesisPersonKey = genesis_issue_person.getAssetKey();

            dcSet.close();
        }
    }

    /**
     * see https://lab.erachain.org/erachain/Erachain/issues/1212
     * <p>
     * Тут мы с одного счета создаем и персону и удостовреяем счет, а потом в форке откатываем удостоврение и точка времени должна откатиться тоже
     * Поидее еще и создание персоны нужно откатить и заново в форке создать - и ситуация в задаче 1212 повторится
     */
    @SneakyThrows
    @Test
    public void fork_process_orphan_forIssue_1212() {

        for (int dbs : TESTED_DBS) {

            init(dbs);

            //assertEquals( null, dbPS.getItem(personKey, ALIVE_KEY));

            assertFalse(userAccount1.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount2.isPersonAlive(dcSet, dcSet.getBlockMap().size()));
            assertFalse(userAccount3.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            BigDecimal oil_amount_start = registrar.getBalanceUSE(FEE_KEY, dcSet);

            initPersonalize();

            //// только заверитель счета тот же что и регистратор персоны
            r_CertifyPubKeys = new RCertifyPubKeys(registrar, FEE_POWER, personKey,
                    certifiedPublicKeys,
                    timestamp);

            // .a - personKey, .b - end_date, .c - block height, .d - reference
            // PERSON STATUS ALIVE
            // exist assertEquals( null, dbPS.getItem(personKey));
            // exist assertEquals( new TreeMap<String, Stack<Tuple3<Integer, Integer, byte[]>>>(), dbPA.getItems(personKey));

            // .a - personKey, .b - end_date, .c - block height, .d - reference
            // PERSON STATUS ALIVE
            assertEquals(4, personKey);
            //assertEquals( null, dbPS.getItem(personKey, ALIVE_KEY));

            // ADDRESSES
            assertNull(dbAP.getItem(userAddress1));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress1));

            assertNull(dbAP.getItem(userAddress2));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress2));

            assertNull(dbAP.getItem(userAddress3));
            // PERSON -> ADDRESS
            assertNull(dbPA.getItem(personKey, userAddress3));

            BigDecimal oil_amount_diff;
            oil_amount_diff = BlockChain.BONUS_FOR_NEW_PERSON(1, person);

            BigDecimal erm_amount = registrar.getBalanceUSE(ERM_KEY, dcSet);
            BigDecimal oil_amount = registrar.getBalanceUSE(FEE_KEY, dcSet);

            BigDecimal erm_amount_user = userAccount1.getBalanceUSE(ERM_KEY, dcSet);
            BigDecimal oil_amount_user = userAccount1.getBalanceUSE(FEE_KEY, dcSet);

            long last_refRegistrar = registrar.getLastTimestamp(dcSet)[0];
            long last_refCertifier = certifier.getLastTimestamp(dcSet)[0];

            //// PROCESS /////
            r_CertifyPubKeys.sign(registrar, Transaction.FOR_NETWORK);


            BigDecimal userAccount2ERM = userAccount2.getBalanceUSE(ERM_KEY, dcSet);
            BigDecimal userAccount2FEE = userAccount2.getBalanceUSE(FEE_KEY, dcSet);
            BigDecimal userAccount3ERM = userAccount3.getBalanceUSE(ERM_KEY, dcSet);
            BigDecimal userAccount3FEE = userAccount3.getBalanceUSE(FEE_KEY, dcSet);

            //CHECK REFERENCE RECIPIENT
            long[] userAccount1ref = userAccount1.getLastTimestamp(dcSet);
            long[] userAccount2ref = userAccount2.getLastTimestamp(dcSet);
            long[] userAccount3ref = userAccount3.getLastTimestamp(dcSet);

            PersonAddressMap dbPA_dcSet = dcSet.getPersonAddressMap();
            AddressPersonMap dbAP_dcSet = dcSet.getAddressPersonMap();
            KKPersonStatusMap dbPS_dcSet = dcSet.getPersonStatusMap();

            r_CertifyPubKeys.setDC(dcSet, true);
            r_CertifyPubKeys.process(gb, Transaction.FOR_NETWORK);
            int transactionIndex = gb.getTransactionSeq(r_CertifyPubKeys.getSignature());

            //CHECK BALANCE SENDER
            assertEquals(erm_amount, registrar.getBalanceUSE(ERM_KEY, dcSet));
            // CHECK FEE BALANCE - FEE + GIFT
            assertEquals(oil_amount.add(issuePersonTransaction.getFee()), registrar.getBalanceUSE(FEE_KEY, dcSet));

            //CHECK BALANCE RECIPIENT
            assertEquals(erm_amount_user, userAccount1.getBalanceUSE(ERM_KEY, dcSet));
            // in FORK
            //CHECK BALANCE RECIPIENT
            assertEquals(oil_amount_diff, userAccount1.getBalanceUSE(FEE_KEY, dcSet));

            //CHECK REFERENCE REGISTRAR
            assertEquals(r_CertifyPubKeys.getTimestamp(), (Long) registrar.getLastTimestamp(dcSet)[0]);

            //CHECK REFERENCE CERTIFIER
            assertEquals(last_refCertifier, certifier.getLastTimestamp(dcSet)[0]);

            //CHECK REFERENCE PERSON MAKER
            // TRUE - new reference for first send FEE
            assertEquals((long) r_CertifyPubKeys.getTimestamp(), userAccount1.getLastTimestamp(dcSet)[0]);

            ////////// TO DATE ////////
            // .a - personKey, .b - end_date, .c - block height, .d - reference
            int to_date = BlockChain.DEFAULT_DURATION + (int) (r_CertifyPubKeys.getTimestamp() / 86400000.0);

            // PERSON STATUS ALIVE - beg_date = person birthDay
            //assertEquals( null, dbPS.getItem(personKey, ALIVE_KEY));
            //assertEquals( (long)person.getBirthday(), (long)dbPS_fork.getItem(personKey, ALIVE_KEY).a);
            // PERSON STATUS ALIVE - to_date = 0 - permanent alive
            //assertEquals( (long)Long.MAX_VALUE, (long)dbPS_fork.getItem(personKey, ALIVE_KEY).b);
            //assertEquals( (int)dbPS_fork.getItem(personKey, ALIVE_KEY).d, transactionIndex);

            // ADDRESSES
            Tuple4<Long, Integer, Integer, Integer> item_AP = dbAP.getItem(userAddress1);
            assertEquals(personKey, (long) item_AP.a);
            assertEquals(to_date, (int) item_AP.b);
            assertEquals(1, (int) item_AP.c);
            // PERSON -> ADDRESS
            assertEquals(to_date, (int) dbPA_dcSet.getItem(personKey, userAddress1).a);
            assertEquals(1, (int) dbPA_dcSet.getItem(personKey, userAddress1).b);

            assertEquals(personKey, (long) dbAP.getItem(userAddress2).a);
            assertEquals(to_date, (int) dbAP.getItem(userAddress2).b);
            //assertEquals( 1, (int)dbAP_fork.getItem(userAddress2).c);

            // PERSON -> ADDRESS
            assertEquals(to_date, (int) dbPA.getItem(personKey, userAddress2).a);
            //assertEquals( 1, (int)dbPA_fork.getItem(personKey, userAddress2).b);

            assertEquals(personKey, (long) dbAP_dcSet.getItem(userAddress3).a);
            assertEquals(to_date, (int) dbAP.getItem(userAddress3).b);
            //assertEquals( 1, (int)dbAP_fork.getItem(userAddress3).c);

            // PERSON -> ADDRESS
            assertEquals(to_date, (int) dbPA_dcSet.getItem(personKey, userAddress3).a);
            //assertEquals( 1, (int)dbPA_fork.getItem(personKey, userAddress3).b);

            assertTrue(userAccount1.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertNotEquals(null, userAccount1.getPerson(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertTrue(userAccount2.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertTrue(userAccount3.isPersonAlive(dcSet, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));

            ////////// ORPHAN //////////////////
            DCSet fork = dcSet.fork(this.toString());
            r_CertifyPubKeys.setDC(fork, true);

            r_CertifyPubKeys.orphan(gb, Transaction.FOR_NETWORK);

            //CHECK BALANCE SENDER
            assertEquals(erm_amount, registrar.getBalanceUSE(ERM_KEY, fork));
            assertEquals(oil_amount, registrar.getBalanceUSE(FEE_KEY, fork));

            assertEquals(last_refRegistrar, registrar.getLastTimestamp(fork)[0]);
            assertTrue(r_CertifyPubKeys.getTimestamp() > registrar.getLastTimestamp(fork)[0]);
            assertEquals((long) issuePersonTransaction.getTimestamp(), registrar.getLastTimestamp(fork)[0]);

            //CHECK BALANCE RECIPIENT
            assertEquals(erm_amount_user, userAccount1.getBalanceUSE(ERM_KEY, fork));
            assertEquals(oil_amount_user.setScale(userAccount1.getBalanceUSE(FEE_KEY, fork).scale()), userAccount1.getBalanceUSE(FEE_KEY, fork));
            assertEquals(userAccount2ERM, userAccount2.getBalanceUSE(ERM_KEY, fork));
            assertEquals(userAccount2FEE, userAccount2.getBalanceUSE(FEE_KEY, fork));
            assertEquals(userAccount3ERM, userAccount3.getBalanceUSE(ERM_KEY, fork));
            assertEquals(userAccount3FEE, userAccount3.getBalanceUSE(FEE_KEY, fork));

            //CHECK REFERENCE SENDER
            //assertEquals(r_CertifyPubKeys.getReference(), registrar.getLastReference(fork));

            //CHECK REFERENCE RECIPIENT
            assertNull(userAccount1.getLastTimestamp(fork));
            assertNull(userAccount2.getLastTimestamp(fork));
            assertNull(userAccount3.getLastTimestamp(fork));

            // .a - personKey, .b - end_date, .c - block height, .d - reference
            // PERSON STATUS ALIVE - must not be modified!
            //assertEquals( (long)person.getBirthday(), (long)dbPS_fork.getItem(personKey, ALIVE_KEY).a);

            // ADDRESSES
            assertNull(fork.getAddressPersonMap().getItem(userAddress1));
            // PERSON -> ADDRESS
            assertNull(fork.getPersonAddressMap().getItem(personKey, userAddress1));

            assertNull(fork.getAddressPersonMap().getItem(userAddress2));
            // PERSON -> ADDRESS
            assertNull(fork.getPersonAddressMap().getItem(personKey, userAddress2));

            assertNull(fork.getAddressPersonMap().getItem(userAddress3));
            // PERSON -> ADDRESS
            assertNull(fork.getPersonAddressMap().getItem(personKey, userAddress3));

            assertFalse(userAccount1.isPersonAlive(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount2.isPersonAlive(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            assertFalse(userAccount3.isPersonAlive(fork, dcSet.getBlockSignsMap().get(dcSet.getBlockMap().getLastBlockSignature())));
            dcSet.close();
        }
    }

}
