package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetVenture;
import org.erachain.dapp.DAPP;
import org.erachain.database.IDB;
import org.erachain.datachain.DCSet;
import org.erachain.ntp.NTP;
import org.junit.Test;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple5;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TransactionAmountTestMovable {

    static Logger LOGGER = LoggerFactory.getLogger(TransactionAmountTestMovable.class.getName());
    private final byte[] icon = new byte[]{1, 3, 4, 5, 6, 9}; // default value
    private final byte[] image = new byte[]{4, 11, 32, 23, 45, 122, 11, -45}; // default value

    int[] TESTED_DEAL = new int[]{
            Transaction.FOR_NETWORK,
            Transaction.FOR_DB_RECORD
    };
    int[] TESTED_DBS = new int[]{
            IDB.DBS_MAP_DB
            , IDB.DBS_ROCK_DB, IDB.DBS_MAP_DB_IN_MEM
    };

    long FEE_KEY = AssetCls.FEE_KEY;
    byte FEE_POWER = (byte) 0;
    long timestamp = NTP.getTime();
    ExLink exLink = null;
    DAPP DAPP = null;
    byte[] itemAppData = null;
    long txFlags = 0L;
    Controller cntrl;
    //CREATE KNOWN ACCOUNT
    byte[] privateKey_1 = Crypto.getInstance().createKeyPair(Crypto.getInstance().digest("tes213sdffsdft".getBytes())).getA();
    PrivateKeyAccount deliver = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey_1);
    byte[] privateKey_2 = Crypto.getInstance().createKeyPair(Crypto.getInstance().digest("tes213sdffsdft2".getBytes())).getA();
    PrivateKeyAccount producer = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey_2);
    byte[] privateKey_3 = Crypto.getInstance().createKeyPair(Crypto.getInstance().digest("tes213sdffsdft3".getBytes())).getA();
    PrivateKeyAccount spender = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey_3);
    AssetCls asset;
    AssetCls assetMovable;
    long assetKey;
    int scale = 3;
    TransactionAmount r_Send;
    Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>> deliverBalance;
    Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>> producerBalance;
    Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>> spenderBalance;
    //CREATE EMPTY MEMORY DATABASE
    private DCSet dcSet;
    private GenesisBlock gb;
    private BlockChain bchain;

    // INIT ASSETS
    private void init(int dbs, boolean withIssue) throws TxException {

        dcSet = DCSet.createEmptyDatabaseSet(dbs);
        cntrl = Controller.getInstance();
        cntrl.initBlockChain(dcSet);
        bchain = cntrl.getBlockChain();
        gb = bchain.getGenesisBlock();
        //gb.process(db);

        // FEE FUND
        deliver.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, dcSet);
        deliver.changeBalance(dcSet, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1), false, false);

        producer.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, dcSet);
        producer.changeBalance(dcSet, false, Account.BALANCE_POS_OWN, FEE_KEY, BigDecimal.valueOf(1), false, false);

        asset = new AssetVenture(AssetCls.AS_INSIDE_ASSETS, deliver, "aasdasd", icon, image, "asdasda", 8, 50000L);
        // set SCALABLE assets ++
        asset.insertToMap(dcSet, 0L);

        assetMovable = new AssetVenture(AssetCls.AS_OUTSIDE_GOODS, producer, "movable", icon, image, "...", scale, 500L);

        if (withIssue) {

            //CREATE ISSUE ASSET TRANSACTION
            IssueAssetTransaction issueAssetTransaction = new IssueAssetTransaction(producer, null, assetMovable, FEE_POWER, ++timestamp);
            issueAssetTransaction.sign(producer, Transaction.FOR_NETWORK);
            issueAssetTransaction.setDC(dcSet, Transaction.FOR_NETWORK, 1, 1, true);
            issueAssetTransaction.process(gb, Transaction.FOR_NETWORK);

            assetKey = assetMovable.getKey();
        }

    }

    /////////////////////////////////////////////
    ////////////


    @Test
    public void process_Movable_Asset() throws TxException {

        for (int dbs : TESTED_DBS) {
            System.out.println("*** DB: " + dbs + " ***");

            try {
                init(dbs, true);

                long keyMovable = assetMovable.getKey();

                producerBalance = producer.getBalance(dcSet, keyMovable);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.a);
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.b);
                assertEquals(BigDecimal.valueOf(500), producerBalance.c.a);
                assertEquals(BigDecimal.valueOf(500), producerBalance.c.b);


                //CREATE SIGNATURE
                Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");
                long timestamp = NTP.getTime();

                //CREATE ASSET TRANSFER
                r_Send = new TransactionAmount(producer, "", null, false, false, FEE_POWER,
                        recipient, Account.BALANCE_POS_OWN, null, keyMovable, timestamp++);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 1, true);
                try {
                    assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.NO_BALANCE);
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }

                r_Send = new TransactionAmount(producer, exLink, DAPP, FEE_POWER, recipient, Account.BALANCE_POS_OWN, keyMovable, BigDecimal.valueOf(5),
                        "", null, new byte[]{1}, new byte[]{1},
                        timestamp++);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 2, true);
                try {
                    assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }


                r_Send.sign(producer, Transaction.FOR_NETWORK);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 3, true);
                r_Send.process(gb, Transaction.FOR_NETWORK);

                producerBalance = producer.getBalance(dcSet, keyMovable);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.a);
                assertEquals(BigDecimal.valueOf(495), producerBalance.a.b);

                assertEquals(BigDecimal.valueOf(500), producerBalance.c.b);
                assertEquals(BigDecimal.valueOf(500), producerBalance.c.b);

                //CHECK BALANCE RECIPIENT
                assertEquals(BigDecimal.valueOf(5), recipient.getBalanceUSE(keyMovable, dcSet));

                //CHECK REFERENCE SENDER
                assertEquals(r_Send.getTimestamp(), producer.getLastTimestamp(dcSet));

                //CHECK REFERENCE RECIPIENT
                assertNotEquals(r_Send.getTimestamp(), recipient.getLastTimestamp(dcSet));

                //////////////////////////////////////////////////
                /// ORPHAN
                /////////////////////////////////////////////////
                r_Send.orphan(gb, Transaction.FOR_NETWORK);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(500), producer.getBalanceUSE(keyMovable, dcSet));

                //CHECK BALANCE RECIPIENT
                assertEquals(BigDecimal.ZERO, recipient.getBalanceUSE(keyMovable, dcSet));

                //CHECK REFERENCE SENDER
                //assertEquals(r_Send.getReference(), producer.getLastReference(db));

                //CHECK REFERENCE RECIPIENT
                assertNotEquals(r_Send.getTimestamp(), recipient.getLastTimestamp(dcSet));
            } finally {
                dcSet.close();
            }
        }
    }

    @Test
    public void validate_R_Send_Movable_Asset() throws TxException {

        for (int dbs : TESTED_DBS) {
            System.out.println("*** DB: " + dbs + " ***");

            try {
                init(dbs, true);

                producerBalance = producer.getBalance(dcSet, assetKey);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.a);
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.b);
                assertEquals(BigDecimal.valueOf(500), producerBalance.c.a);
                assertEquals(BigDecimal.valueOf(500), producerBalance.c.b);

                //CREATE ASSET TRANSFER
                r_Send = new TransactionAmount(producer, exLink, DAPP, FEE_POWER, spender, Account.BALANCE_POS_OWN, assetKey, BigDecimal.valueOf(1000),
                        "", null, new byte[]{1}, new byte[]{1},
                        ++timestamp);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 1, true);
                try {
                    assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.NO_BALANCE);
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }

                r_Send = new TransactionAmount(producer, exLink, DAPP, FEE_POWER, spender, Account.BALANCE_POS_OWN, assetKey, BigDecimal.valueOf(50),
                        "", null, new byte[]{1}, new byte[]{1},
                        ++timestamp);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 2, true);
                try {
                    assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }

                r_Send.sign(producer, Transaction.FOR_NETWORK);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 3, true);
                r_Send.process(gb, Transaction.FOR_NETWORK);

                producerBalance = producer.getBalance(dcSet, assetKey);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.a);
                assertEquals(BigDecimal.valueOf(450), producerBalance.a.b);

                assertEquals(BigDecimal.valueOf(500), producerBalance.c.a);
                assertEquals(BigDecimal.valueOf(500), producerBalance.c.b);

                //CHECK BALANCE RECIPIENT
                deliverBalance = spender.getBalance(dcSet, assetKey);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(50), deliverBalance.a.a);
                assertEquals(BigDecimal.valueOf(50), deliverBalance.a.b);

                assertEquals(BigDecimal.valueOf(0), deliverBalance.c.a);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.c.b);

                //////////////////////////////////////////////////
                /// ORPHAN
                /////////////////////////////////////////////////
                r_Send.orphan(gb, Transaction.FOR_NETWORK);

                //CHECK BALANCE SENDER
                producerBalance = producer.getBalance(dcSet, assetKey);
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.a);
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.b);

                //CHECK BALANCE RECIPIENT
                deliverBalance = spender.getBalance(dcSet, assetKey);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.a);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.b);

                // BACK PROCESS
                r_Send.process(gb, Transaction.FOR_NETWORK);

                // INVALID
                r_Send = new TransactionAmount(
                        deliver, exLink, DAPP, FEE_POWER, spender, Account.BALANCE_POS_OWN, assetKey, BigDecimal.valueOf(-100),
                        "", null, new byte[]{1}, new byte[]{1},
                        ++timestamp);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 4, true);

                try {
                    assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.NO_HOLD_BALANCE);
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }

                // GET ON HOLD - доставщик берет к себе на руки товар
                r_Send = new TransactionAmount(
                        deliver, exLink, DAPP, FEE_POWER, producer, Account.BALANCE_POS_OWN, assetKey, BigDecimal.valueOf(-10),
                        "", null, new byte[]{1}, new byte[]{1},
                        ++timestamp);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 5, true);
                try {
                    assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }

                r_Send.sign(producer, Transaction.FOR_NETWORK);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 6, true);
                r_Send.process(gb, Transaction.FOR_NETWORK);

                producerBalance = producer.getBalance(dcSet, assetKey);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.a);
                assertEquals(BigDecimal.valueOf(450), producerBalance.a.b);

                assertEquals(BigDecimal.valueOf(500), producerBalance.c.a);
                assertEquals(BigDecimal.valueOf(490), producerBalance.c.b);

                //CHECK BALANCE RECIPIENT
                deliverBalance = deliver.getBalance(dcSet, assetKey);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.a);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.b);

                assertEquals(BigDecimal.valueOf(10), deliverBalance.c.a);
                assertEquals(BigDecimal.valueOf(10), deliverBalance.c.b);

                //////////////////////////////////////////////////
                /// ORPHAN
                /////////////////////////////////////////////////
                r_Send.orphan(gb, Transaction.FOR_NETWORK);

                //CHECK BALANCE SENDER
                producerBalance = producer.getBalance(dcSet, assetKey);
                assertEquals(BigDecimal.valueOf(500), producerBalance.a.a);
                assertEquals(BigDecimal.valueOf(450), producerBalance.a.b);
                assertEquals(BigDecimal.valueOf(500), producerBalance.c.a);
                assertEquals(BigDecimal.valueOf(500), producerBalance.c.b);

                //CHECK BALANCE RECIPIENT
                deliverBalance = deliver.getBalance(dcSet, assetKey);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.a);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.b);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.c.a);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.c.b);

                r_Send.process(gb, Transaction.FOR_NETWORK);

                //////////////////////
                // GET ON HOLD - доставщик передает новому собственнику на руки товар - это подтверждает собственник
                r_Send = new TransactionAmount(
                        spender, exLink, DAPP, FEE_POWER, deliver, Account.BALANCE_POS_OWN, assetKey, BigDecimal.valueOf(-10),
                        "", null, new byte[]{1}, new byte[]{1},
                        ++timestamp);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 2, true);
                try {
                    assertEquals(r_Send.isValid(Transaction.FOR_NETWORK, txFlags), Transaction.VALIDATE_OK);
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }

                r_Send.sign(producer, Transaction.FOR_NETWORK);
                r_Send.setDC(dcSet, Transaction.FOR_NETWORK, 1, 5, true);
                r_Send.process(gb, Transaction.FOR_NETWORK);

                spenderBalance = spender.getBalance(dcSet, assetKey);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(50), spenderBalance.a.a);
                assertEquals(BigDecimal.valueOf(50), spenderBalance.a.b);

                assertEquals(BigDecimal.valueOf(10), spenderBalance.c.a);
                assertEquals(BigDecimal.valueOf(10), spenderBalance.c.b);

                //CHECK BALANCE RECIPIENT
                deliverBalance = deliver.getBalance(dcSet, assetKey);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.a);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.b);

                assertEquals(BigDecimal.valueOf(10), deliverBalance.c.a);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.c.b);

                //////////////////////////////////////////////////
                /// ORPHAN
                /////////////////////////////////////////////////
                r_Send.orphan(gb, Transaction.FOR_NETWORK);

                spenderBalance = spender.getBalance(dcSet, assetKey);

                //CHECK BALANCE SPENDER
                assertEquals(BigDecimal.valueOf(50), spenderBalance.a.a);
                assertEquals(BigDecimal.valueOf(50), spenderBalance.a.b);

                assertEquals(BigDecimal.valueOf(0), spenderBalance.c.a);
                assertEquals(BigDecimal.valueOf(0), spenderBalance.c.b);

                //CHECK BALANCE DELIVER
                deliverBalance = deliver.getBalance(dcSet, assetKey);

                //CHECK BALANCE SENDER
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.a);
                assertEquals(BigDecimal.valueOf(0), deliverBalance.a.b);

                assertEquals(BigDecimal.valueOf(10), deliverBalance.c.a);
                assertEquals(BigDecimal.valueOf(10), deliverBalance.c.b);

                r_Send.process(gb, Transaction.FOR_NETWORK);
            } finally {
                dcSet.close();
            }

        }
    }

}
