package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import lombok.extern.slf4j.Slf4j;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.templates.Template;
import org.erachain.core.item.templates.TemplateCls;
import org.erachain.database.IDB;
import org.junit.Test;

import static org.junit.Assert.*;

@Slf4j
public class GenesisIssueTemplateTransactionTest {

    private final byte[] icon = new byte[]{1, 3, 4, 5, 6, 9}; // default value
    private final byte[] image = new byte[]{4, 11, 32, 23, 45, 122, 11, -45}; // default value
    byte[] itemAppData = null;
    int[] TESTED_DBS = new int[]{IDB.DBS_MAP_DB, IDB.DBS_ROCK_DB, IDB.DBS_MAP_DB_IN_MEM};
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("tes213sdffsdft".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    byte[] seed_1 = Crypto.getInstance().digest("tes213sdffsdft_1".getBytes());
    byte[] privateKey_1 = Crypto.getInstance().createKeyPair(seed_1).getA();
    TemplateCls item;
    String title = "title probe";
    String tags = "tag";
    String message = "test123!";

    int[] TESTED_DEAL = new int[]{
            Transaction.FOR_NETWORK,
            Transaction.FOR_DB_RECORD
    };

    @Test
    public void parse() {

        for (int forDeal : TESTED_DEAL) {
            System.out.println("*** DEAL: " + forDeal + " ***");

            item = new Template(maker, "test", icon, image, "strontje");

            GenesisIssueTemplateRecord tx = new GenesisIssueTemplateRecord(item, title, tags, message);

            //CONVERT TO BYTES
            byte[] rawTX = tx.toBytes(forDeal);

            //CHECK DATA LENGTH
            assertEquals(rawTX.length, tx.getDataLength(forDeal));

            Transaction parsedTX;
            try {
                //PARSE FROM BYTES
                parsedTX = TransactionFactory.getInstance().parse(rawTX, forDeal);
            } catch (Exception e) {
                fail("Exception while parsing transaction.");
                logger.error(e.getMessage(), e);
                return;
            }

            //CHECK INSTANCE
            assertTrue(parsedTX instanceof GenesisIssueTemplateRecord);

            //CHECK SIGNATURE
            assertArrayEquals(tx.getSignature(), parsedTX.getSignature());

            //CHECK ISSUER
            assertEquals(tx.getCreator().getAddress(), parsedTX.getCreator().getAddress());

            assertEquals(tx.getItem(), ((GenesisIssueTemplateRecord) parsedTX).getItem());

            //assertArrayEquals(tx.getItem().getAppData(), ((GenesisIssueTemplateRecord) parsedTX).getItem().getAppData());

            //assertEquals((tx.getItem()).getProps(), ((GenesisIssueTemplateRecord) parsedTX).getItem().getProps());
            assertEquals(((TemplateCls) (tx.getItem())).getVarNames(), ((TemplateCls) ((GenesisIssueTemplateRecord) parsedTX).getItem()).getVarNames());


            //PARSE TRANSACTION FROM WRONG BYTES
            rawTX = new byte[tx.getDataLength(forDeal)];

            try {
                //PARSE FROM BYTES
                TransactionFactory.getInstance().parse(rawTX, forDeal);

                //FAIL
                fail("this should throw an exception");
            } catch (Exception e) {
                //EXCEPTION IS THROWN OK
            }
        }
    }

    @Test
    public void processBody() {
    }
}