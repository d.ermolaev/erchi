package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.datachain.DCSet;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TestRecGenesisAsset {

    static Logger LOGGER = LoggerFactory.getLogger(TestRecGenesisAsset.class.getName());

    int forDeal = Transaction.FOR_NETWORK;

    //int asPack = Transaction.FOR_NETWORK;

    long FEE_KEY = 1L;
    byte FEE_POWER = (byte) 1;
    byte[] assetReference = new byte[64];

    long flags = 0L;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    AssetCls asset;
    long key = 0L;
    GenesisIssueAssetTransaction genesisIssueAssetTransaction;
    //CREATE EMPTY MEMORY DATABASE
    private DCSet dcSet;
    private GenesisBlock gb;

    private void initIssue(boolean toProcess) {

        //CREATE EMPTY MEMORY DATABASE
        dcSet = DCSet.createEmptyDatabaseSet(0);

        //CREATE ASSET
        asset = GenesisBlock.makeAsset(1);
        //byte[] rawAsset = asset.toBytes(true); // reference is new byte[64]
        //assertEquals(rawAsset.length, asset.getDataLength());

        //CREATE ISSUE ASSET TRANSACTION
        genesisIssueAssetTransaction = new GenesisIssueAssetTransaction(asset, null, null, null);
        genesisIssueAssetTransaction.setDC(dcSet);
        if (toProcess) {
            try {
                genesisIssueAssetTransaction.process(gb, Transaction.FOR_NETWORK);
            } catch (TxException e) {
                e.printStackTrace();
            }
            key = asset.getKey();
        }

    }

    //GENESIS

    // GENESIS ISSUE
    @Test
    public void validateGenesisIssueAssetTransaction() {

        initIssue(false);

        //genesisIssueAssetTransaction.sign(creator);
        //CHECK IF ISSUE ASSET TRANSACTION IS VALID
        assertTrue(genesisIssueAssetTransaction.isSignatureValid());
        try {
            assertEquals(Transaction.VALIDATE_OK, genesisIssueAssetTransaction.isValid(Transaction.FOR_NETWORK, flags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //CONVERT TO BYTES
        //logger.info("CREATOR: " + genesisIssueAssetTransaction.getCreator().getPublicKey());
        byte[] rawGenesisIssueAssetTransaction = genesisIssueAssetTransaction.toBytes(Transaction.FOR_NETWORK);

        //CHECK DATA LENGTH
        assertEquals(rawGenesisIssueAssetTransaction.length, genesisIssueAssetTransaction.getDataLength(Transaction.FOR_NETWORK));
        //logger.info("rawGenesisIssueAssetTransaction.length") + ": + rawGenesisIssueAssetTransaction.length);

        try {
            //PARSE FROM BYTES
            GenesisIssueAssetTransaction parsedGenesisIssueAssetTransaction = (GenesisIssueAssetTransaction) TransactionFactory.getInstance().parse(rawGenesisIssueAssetTransaction, Transaction.FOR_NETWORK);

            //CHECK INSTANCE
            assertTrue(parsedGenesisIssueAssetTransaction instanceof GenesisIssueAssetTransaction);

            //CHECK SIGNATURE
            assertArrayEquals(genesisIssueAssetTransaction.getSignature(), parsedGenesisIssueAssetTransaction.getSignature());

            //CHECK NAME
            assertEquals(genesisIssueAssetTransaction.getItem().getName(), parsedGenesisIssueAssetTransaction.getItem().getName());

            //CHECK DESCRIPTION
            assertEquals(genesisIssueAssetTransaction.getItem().getDescription(), parsedGenesisIssueAssetTransaction.getItem().getDescription());

            AssetCls asset = (AssetCls) genesisIssueAssetTransaction.getItem();
            AssetCls asset1 = (AssetCls) parsedGenesisIssueAssetTransaction.getItem();

            //CHECK QUANTITY
            assertEquals(asset.getQuantity(), asset1.getQuantity());

            //SCALE
            assertEquals(asset.getScale(), asset1.getScale());

            //ASSET TYPE
            assertEquals(asset.getClassModel(), asset1.getClassModel());


        } catch (Exception e) {
            fail("Exception while parsing transaction." + e);
        }

        //PARSE TRANSACTION FROM WRONG BYTES
        rawGenesisIssueAssetTransaction = new byte[genesisIssueAssetTransaction.getDataLength(Transaction.FOR_NETWORK)];

        try {
            //PARSE FROM BYTES
            TransactionFactory.getInstance().parse(rawGenesisIssueAssetTransaction, Transaction.FOR_NETWORK);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }
    }


    @Test
    public void parseGenesisIssueAssetTransaction() {

        initIssue(false);

        //CONVERT TO BYTES
        byte[] rawGenesisIssueAssetTransaction = genesisIssueAssetTransaction.toBytes(Transaction.FOR_NETWORK);

        //CHECK DATA LENGTH
        assertEquals(rawGenesisIssueAssetTransaction.length, genesisIssueAssetTransaction.getDataLength(Transaction.FOR_NETWORK));

        try {
            //PARSE FROM BYTES
            GenesisIssueAssetTransaction parsedGenesisIssueAssetTransaction = (GenesisIssueAssetTransaction) TransactionFactory.getInstance().parse(rawGenesisIssueAssetTransaction, Transaction.FOR_NETWORK);

            //CHECK INSTANCE
            assertTrue(parsedGenesisIssueAssetTransaction instanceof GenesisIssueAssetTransaction);

            //CHECK SIGNATURE
            assertArrayEquals(genesisIssueAssetTransaction.getSignature(), parsedGenesisIssueAssetTransaction.getSignature());

            //CHECK NAME
            assertEquals(genesisIssueAssetTransaction.getItem().getName(), parsedGenesisIssueAssetTransaction.getItem().getName());

            //CHECK DESCRIPTION
            assertEquals(genesisIssueAssetTransaction.getItem().getDescription(), parsedGenesisIssueAssetTransaction.getItem().getDescription());

            //CHECK QUANTITY
            AssetCls asset = (AssetCls) genesisIssueAssetTransaction.getItem();
            AssetCls asset1 = (AssetCls) parsedGenesisIssueAssetTransaction.getItem();
            assertEquals(asset.getQuantity(), asset1.getQuantity());

            //SCALE
            assertEquals(asset.getScale(), asset1.getScale());

            //ASSET TYPE
            assertEquals(asset.getClassModel(), asset1.getClassModel());

        } catch (Exception e) {
            fail("Exception while parsing transaction." + e);
        }

        //PARSE TRANSACTION FROM WRONG BYTES
        rawGenesisIssueAssetTransaction = new byte[genesisIssueAssetTransaction.getDataLength(Transaction.FOR_NETWORK)];

        try {
            //PARSE FROM BYTES
            TransactionFactory.getInstance().parse(rawGenesisIssueAssetTransaction, Transaction.FOR_NETWORK);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }
    }


    @Test
    public void processGenesisIssueAssetTransaction() {

        initIssue(true);
        LOGGER.info("asset KEY: " + key);

        //CHECK BALANCE ISSUER - null
        //assertEquals(BigDecimal.valueOf(asset.getQuantity()).setScale(BlockChain.AMOUNT_DEDAULT_SCALE), maker.getConfirmedBalance(key, db));

        //CHECK ASSET EXISTS SENDER
        long key = asset.getKey();
        assertTrue(dcSet.getItemAssetMap().contains(key));

        //CHECK ASSET IS CORRECT
        assertArrayEquals(dcSet.getItemAssetMap().get(key).toBytes(true), asset.toBytes(true));

        //CHECK ASSET BALANCE SENDER - null
        //assertEquals(true, db.getAssetBalanceMap().get(maker.getAddress(), key).compareTo(new BigDecimal(asset.getQuantity())) == 0);

        //CHECK REFERENCE SENDER - null
        //assertEquals(true, Arrays.equals(genesisIssueAssetTransaction.getSignature(), maker.getLastReference(db)));
    }


    @Test
    public void orphanIssueAssetTransaction() throws TxException {

        initIssue(true);

        //assertEquals(new BigDecimal(asset.getQuantity()).setScale(BlockChain.AMOUNT_DEDAULT_SCALE), maker.getConfirmedBalance(key, db));
        //assertEquals(true, Arrays.equals(genesisIssueAssetTransaction.getSignature(), maker.getLastReference(db)));

        genesisIssueAssetTransaction.orphan(gb, Transaction.FOR_NETWORK);

        //CHECK BALANCE ISSUER
        assertEquals(BigDecimal.ZERO.setScale(BlockChain.AMOUNT_DEFAULT_SCALE), maker.getBalanceUSE(key, dcSet));

        //CHECK ASSET EXISTS SENDER
        assertFalse(dcSet.getItemAssetMap().contains(key));

        //CHECK ASSET BALANCE SENDER
        assertEquals(0, dcSet.getAssetBalanceMap().get(maker.getBytes(), key).a.b.longValue());

        //CHECK REFERENCE SENDER
        // it for not genesis - assertEquals(true, Arrays.equals(genesisIssueAssetTransaction.getReference(), maker.getLastReference(db)));
        assertNull(maker.getLastTimestamp(dcSet));

    }

    //GENESIS TRANSFER ASSET

    @Test
    public void validateSignatureGenesisTransferAssetTransaction() {

        initIssue(true);

        //CREATE SIGNATURE
        Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");
        System.out.println("key" + key);

        //CREATE ASSET TRANSFER
        Transaction assetTransfer = new GenesisTransferAssetTransaction(recipient, key, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), null, null, null);
        //assetTransfer.sign(sender);

        //CHECK IF ASSET TRANSFER SIGNATURE IS VALID
        assertTrue(assetTransfer.isSignatureValid());
    }

    @Test
    public void validateGenesisTransferAssetTransaction() throws TxException {

        initIssue(true);

        //CREATE SIGNATURE
        Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");

        //CREATE VALID ASSET TRANSFER
        Transaction assetTransfer = new GenesisTransferAssetTransaction(recipient, key, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), null, null, null);

        //CHECK IF ASSET TRANSFER IS VALID
        try {
            assertEquals(Transaction.VALIDATE_OK, assetTransfer.isValid(Transaction.FOR_NETWORK, flags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        assetTransfer.process(gb, Transaction.FOR_NETWORK);

        //CREATE VALID ASSET TRANSFER
        maker.changeBalance(dcSet, false, 1, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        assetTransfer = new GenesisTransferAssetTransaction(recipient, key, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), null, null, null);

        //CHECK IF ASSET TRANSFER IS VALID
        try {
            assertEquals(Transaction.VALIDATE_OK, assetTransfer.isValid(Transaction.FOR_NETWORK, flags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //CREATE INVALID ASSET TRANSFER INVALID RECIPIENT ADDRESS
        assetTransfer = new GenesisTransferAssetTransaction(Account.makeFromOld("test"), key, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), null, null, null);

        //CHECK IF ASSET TRANSFER IS INVALID
        try {
            assertNotEquals(Transaction.VALIDATE_OK, assetTransfer.isValid(Transaction.FOR_NETWORK, flags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //CREATE INVALID ASSET TRANSFER NEGATIVE AMOUNT
        assetTransfer = new GenesisTransferAssetTransaction(recipient, key, BigDecimal.valueOf(-100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), null, null, null);

        //CHECK IF ASSET TRANSFER IS INVALID
        try {
            assertNotEquals(Transaction.VALIDATE_OK, assetTransfer.isValid(Transaction.FOR_NETWORK, flags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        //CREATE INVALID ASSET TRANSFER NOT ENOUGH ASSET BALANCE
        assetTransfer = new GenesisTransferAssetTransaction(recipient, key, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), null, null, null);

        //CHECK IF ASSET TRANSFER IS INVALID
        // nor need for genesis - assertNotEquals(Transaction.VALIDATE_OK, assetTransfer.isValid(db));
    }

    @Test
    public void parseGenesisTransferAssetTransaction() {

        initIssue(true);

        //CREATE SIGNATURE
        Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");

        //CREATE VALID ASSET TRANSFER
        GenesisTransferAssetTransaction genesisTransferAsset = new GenesisTransferAssetTransaction(recipient, key, BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), null, null, null);
        //genesisTransferAsset.sign(maker);
        //genesisTransferAsset.process(db);

        //CONVERT TO BYTES
        byte[] rawGenesisTransferAsset = genesisTransferAsset.toBytes(Transaction.FOR_NETWORK);

        //CHECK DATALENGTH
        assertEquals(rawGenesisTransferAsset.length, genesisTransferAsset.getDataLength(Transaction.FOR_NETWORK));

        try {
            //PARSE FROM BYTES
            GenesisTransferAssetTransaction parsedAssetTransfer = (GenesisTransferAssetTransaction) TransactionFactory.getInstance().parse(rawGenesisTransferAsset, Transaction.FOR_NETWORK);
            LOGGER.info(" 1: " + parsedAssetTransfer.getKey());

            //CHECK INSTANCE
            assertTrue(parsedAssetTransfer instanceof GenesisTransferAssetTransaction);

            //CHECK SIGNATURE
            assertArrayEquals(genesisTransferAsset.getSignature(), parsedAssetTransfer.getSignature());

            //CHECK KEY
            assertEquals(genesisTransferAsset.getKey(), parsedAssetTransfer.getKey());

            //CHECK AMOUNT SENDER
            assertEquals(genesisTransferAsset.getAmount(maker), parsedAssetTransfer.getAmount(maker));

            //CHECK AMOUNT RECIPIENT
            assertEquals(genesisTransferAsset.getAmount(recipient), parsedAssetTransfer.getAmount(recipient));

        } catch (Exception e) {
            fail("Exception while parsing transaction. " + e);
        }

        //PARSE TRANSACTION FROM WRONG BYTES
        rawGenesisTransferAsset = new byte[genesisTransferAsset.getDataLength(Transaction.FOR_NETWORK)];

        try {
            //PARSE FROM BYTES
            TransactionFactory.getInstance().parse(rawGenesisTransferAsset, Transaction.FOR_NETWORK);

            //FAIL
            fail("this should throw an exception");
        } catch (Exception e) {
            //EXCEPTION IS THROWN OK
        }
    }

    @Test
    public void processGenesisTransferAssetTransaction() throws TxException {

        initIssue(true);

        BigDecimal total = BigDecimal.valueOf(asset.getQuantity()).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
        BigDecimal amoSend = BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
        //assertEquals(total, amoSend);

        //CHECK BALANCE SENDER - null
        //assertEquals(total, maker.getConfirmedBalance(key, db));

        //CREATE SIGNATURE
        Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");

        //CREATE ASSET TRANSFER
        Transaction assetTransfer = new GenesisTransferAssetTransaction(recipient, key, amoSend, null, null, null);
        try {
            assertEquals(Transaction.VALIDATE_OK, assetTransfer.isValid(Transaction.FOR_NETWORK, flags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        // assetTransfer.sign(sender); // not  NEED
        assetTransfer.process(gb, Transaction.FOR_NETWORK);

        //CHECK BALANCE SENDER - null
        //assertEquals(total.subtract(amoSend), maker.getConfirmedBalance(key, db));

        //CHECK BALANCE RECIPIENT
        assertEquals(amoSend, recipient.getBalanceUSE(key, dcSet));

		/* not NEED
		//CHECK REFERENCE SENDER
		assertEquals(true, Arrays.equals(assetTransfer.getSignature(), sender.getLastReference(databaseSet)));
		 */

        //CHECK REFERENCE RECIPIENT
        assertEquals(assetTransfer.getTimestamp(), recipient.getLastTimestamp(dcSet));
    }

    @Test
    public void orphanGenesisTransferAssetTransaction() throws TxException {

        initIssue(true);

        BigDecimal total = BigDecimal.valueOf(asset.getQuantity()).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
        BigDecimal amoSend = BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);

        //CREATE SIGNATURE
        Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");

        //CREATE ASSET TRANSFER
        Transaction assetTransfer = new GenesisTransferAssetTransaction(recipient, key, amoSend, null, null, null);
        // assetTransfer.sign(sender); not NEED
        assetTransfer.process(gb, Transaction.FOR_NETWORK);
        assetTransfer.orphan(gb, Transaction.FOR_NETWORK);

        //CHECK BALANCE SENDER - null
        //assertEquals(total, maker.getConfirmedBalance(key, db));

        //CHECK BALANCE RECIPIENT
        assertEquals(BigDecimal.ZERO.setScale(BlockChain.AMOUNT_DEFAULT_SCALE), recipient.getBalanceUSE(key, dcSet));

		/* not NEED
		//CHECK REFERENCE SENDER
		assertEquals(true, Arrays.equals(transaction.getSignature(), sender.getLastReference(databaseSet)));
		 */

        //CHECK REFERENCE RECIPIENT
        assertNotEquals(assetTransfer.getSignature(), recipient.getLastTimestamp(dcSet));
    }

    @Test
    public void processGenesisTransferAssetTransaction3() throws TxException {

        initIssue(true);

        BigDecimal total = BigDecimal.valueOf(asset.getQuantity()).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
        BigDecimal amoSend = BigDecimal.valueOf(100).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);
        //assertEquals(total, amoSend);

        //CHECK BALANCE SENDER - null
        //assertEquals(total, maker.getConfirmedBalance(key, db));

        Account owner = Account.makeFromOld("7JS4ywtcqrcVpRyBxfqyToS2XBDeVrdqZL");
        Account recipient = Account.makeFromOld("7MFPdpbaxKtLMWq7qvXU6vqTWbjJYmxsLW");

        //CREATE ASSET TRANSFER
        Transaction assetTransfer = new GenesisTransferAssetTransaction(recipient, -key, amoSend, owner, null, null, null);
        try {
            assertEquals(Transaction.VALIDATE_OK, assetTransfer.isValid(Transaction.FOR_NETWORK, flags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        /// PARSE
        byte[] rawGenesisTransferAsset = assetTransfer.toBytes(Transaction.FOR_NETWORK);

        //CHECK DATALENGTH
        assertEquals(rawGenesisTransferAsset.length, assetTransfer.getDataLength(Transaction.FOR_NETWORK));

        try {
            //PARSE FROM BYTES
            GenesisTransferAssetTransaction parsedAssetTransfer = (GenesisTransferAssetTransaction) TransactionFactory.getInstance().parse(rawGenesisTransferAsset, Transaction.FOR_NETWORK);
            System.out.println(" 1: " + parsedAssetTransfer.getKey());

            //CHECK INSTANCE
            assertTrue(parsedAssetTransfer instanceof GenesisTransferAssetTransaction);

            //CHECK SIGNATURE
            assertArrayEquals(assetTransfer.getSignature(), parsedAssetTransfer.getSignature());

            //CHECK KEY
            assertEquals(assetTransfer.getKey(), parsedAssetTransfer.getKey());

            //CHECK AMOUNT SENDER
            assertEquals(assetTransfer.getAmount(maker), parsedAssetTransfer.getAmount(maker));

            //CHECK AMOUNT RECIPIENT
            assertEquals(assetTransfer.getAmount(recipient), parsedAssetTransfer.getAmount(recipient));

            //CHECK A
            GenesisTransferAssetTransaction aaa = (GenesisTransferAssetTransaction) assetTransfer;
            assertEquals(aaa.getCreator(), parsedAssetTransfer.getCreator());

        } catch (Exception e) {
            fail("Exception while parsing transaction. " + e);
        }

        // assetTransfer.sign(sender); // not  NEED
        assetTransfer.process(gb, Transaction.FOR_NETWORK);

        //CHECK BALANCE SENDER - null
        //assertEquals(total.subtract(amoSend), maker.getConfirmedBalance(key, db));

        //CHECK BALANCE RECIPIENT
        assertEquals(amoSend, recipient.getBalance(dcSet, -key));
        System.out.println(" 1: " + recipient.getBalance(dcSet, key));

        assertEquals(BigDecimal.ZERO.subtract(amoSend), owner.getBalance(dcSet, -key));

		/* not NEED
		//CHECK REFERENCE SENDER
		assertEquals(true, Arrays.equals(assetTransfer.getSignature(), sender.getLastReference(databaseSet)));
		 */

        //CHECK REFERENCE RECIPIENT
        assertEquals(assetTransfer.getTimestamp(), recipient.getLastTimestamp(dcSet));

        ///////////////////////////
        ////////////////////////////
        assetTransfer.orphan(gb, Transaction.FOR_NETWORK);

        //CHECK BALANCE SENDER - null
        //assertEquals(total, maker.getConfirmedBalance(key, db));

        //CHECK BALANCE RECIPIENT
        assertEquals(BigDecimal.ZERO.setScale(BlockChain.AMOUNT_DEFAULT_SCALE), recipient.getBalance(dcSet, -key));
        System.out.println(" 1: " + recipient.getBalance(dcSet, key));

        assertEquals(BigDecimal.ZERO.setScale(BlockChain.AMOUNT_DEFAULT_SCALE), owner.getBalance(dcSet, key));

        //CHECK REFERENCE RECIPIENT
        assertNotEquals(assetTransfer.getSignature(), recipient.getLastTimestamp(dcSet));
    }


}
