package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import lombok.SneakyThrows;
import org.erachain.core.BlockChain;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.statuses.Status;
import org.erachain.core.item.statuses.StatusCls;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.ItemStatusMap;
import org.erachain.ntp.NTP;
import org.erachain.utils.Corekeys;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.*;

//import java.math.BigInteger;
//import java.util.ArrayList;
//import java.util.List;

public class TestRecStatus {

    static Logger LOGGER = LoggerFactory.getLogger(TestRecStatus.class.getName());
    private final byte[] icon = new byte[]{1, 3, 4, 5, 6, 9}; // default value

    //Long releaserReference = null;
    private final byte[] image = new byte[]{4, 11, 32, 23, 45, 122, 11, -45}; // default value
    int forDeal = Transaction.FOR_NETWORK;
    boolean asPack = false;
    long ERM_KEY = AssetCls.ERA_KEY;
    long FEE_KEY = AssetCls.FEE_KEY;
    byte FEE_POWER = (byte) 0;
    byte[] statusReference = new byte[64];
    long timestamp = NTP.getTime();
    long flags = 0L;
    byte[] itemAppData = null;
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("test".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    int mapSize;
    ItemStatusMap statusMap;
    //CREATE EMPTY MEMORY DATABASE
    private DCSet db;
    private GenesisBlock gb;

    // INIT STATUSS
    private void init() {

        db = DCSet.createEmptyDatabaseSet(0);
        gb = new GenesisBlock();
        try {
            gb.process(db, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // FEE FUND
        maker.setLastTimestamp(new long[]{gb.getTimestamp(), 0}, db);
        maker.changeBalance(db, false, ERM_KEY, BigDecimal.valueOf(10000).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        maker.changeBalance(db, false, FEE_KEY, BigDecimal.valueOf(1).setScale(BlockChain.AMOUNT_DEFAULT_SCALE), false, false);
        statusMap = db.getItemStatusMap();
        mapSize = statusMap.size();

    }

    @Test
    public void testAddreessVersion() {
        int vers = Corekeys.findAddressVersion("E");
        assertEquals(-1111, vers);
    }

    //ISSUE STATUS TRANSACTION

    @Test
    public void validateSignatureIssueStatusTransaction() {

        init();

        //CREATE STATUS
        Status status = new Status(maker, "test", icon, image, "strontje", true);

        //CREATE ISSUE STATUS TRANSACTION
        Transaction issueStatusTransaction = new IssueStatusRecord(maker, null, status, FEE_POWER, timestamp);
        issueStatusTransaction.sign(maker, Transaction.FOR_NETWORK);

        //CHECK IF ISSUE STATUS TRANSACTION IS VALID
        issueStatusTransaction.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertTrue(issueStatusTransaction.isSignatureValid());

        //INVALID SIGNATURE
        issueStatusTransaction = new IssueStatusRecord(maker, status, FEE_POWER, timestamp, new byte[64]);

        //CHECK IF ISSUE STATUS IS INVALID
        issueStatusTransaction.setHeightSeq(BlockChain.SKIP_INVALID_SIGN_BEFORE, 1);
        assertFalse(issueStatusTransaction.isSignatureValid());
    }

    @SneakyThrows
    @Ignore
//TODO actualize the test
    @Test
    public void parseIssueStatusTransaction() {

        init();

        StatusCls status = new Status(maker, "test132", icon, image, "12345678910strontje", true);
        byte[] raw = status.toBytes(false);
        assertEquals(raw.length, status.getDataLength(false));

        //CREATE ISSUE STATUS TRANSACTION
        IssueStatusRecord issueStatusRecord = new IssueStatusRecord(maker, null, status, FEE_POWER, timestamp);
        issueStatusRecord.sign(maker, Transaction.FOR_NETWORK);
        issueStatusRecord.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        issueStatusRecord.process(gb, Transaction.FOR_NETWORK);

        //CONVERT TO BYTES
        byte[] rawIssueStatusTransaction = issueStatusRecord.toBytes(Transaction.FOR_NETWORK);

        //CHECK DATA LENGTH
        assertEquals(rawIssueStatusTransaction.length, issueStatusRecord.getDataLength(Transaction.FOR_NETWORK));

        try {
            //PARSE FROM BYTES
            IssueStatusRecord parsedIssueStatusTransaction = (IssueStatusRecord) TransactionFactory.getInstance().parse(rawIssueStatusTransaction, Transaction.FOR_NETWORK);
            LOGGER.info("parsedIssueStatusTransaction: " + parsedIssueStatusTransaction);

            //CHECK INSTANCE
            assertTrue(parsedIssueStatusTransaction instanceof IssueStatusRecord);

            //CHECK SIGNATURE
            assertTrue(Arrays.equals(issueStatusRecord.getSignature(), parsedIssueStatusTransaction.getSignature()));

            //CHECK ISSUER
            assertEquals(issueStatusRecord.getCreator().getAddress(), parsedIssueStatusTransaction.getCreator().getAddress());

            //CHECK OWNER
            assertEquals(issueStatusRecord.getItem().getAuthor().getAddress(), parsedIssueStatusTransaction.getItem().getAuthor().getAddress());

            //CHECK NAME
            assertEquals(issueStatusRecord.getItem().getName(), parsedIssueStatusTransaction.getItem().getName());

            //CHECK DESCRIPTION
            assertEquals(issueStatusRecord.getItem().getDescription(), parsedIssueStatusTransaction.getItem().getDescription());

            //CHECK FEE
            assertEquals(issueStatusRecord.getFee(), parsedIssueStatusTransaction.getFee());

            //CHECK REFERENCE
            //assertEquals(issueStatusRecord.getReference(), parsedIssueStatusTransaction.getReference());

            //CHECK TIMESTAMP
            assertEquals(issueStatusRecord.getTimestamp(), parsedIssueStatusTransaction.getTimestamp());
        } catch (Exception e) {
            fail("Exception while parsing transaction. " + e);
        }

    }


    @SneakyThrows
    @Test
    public void process_orphanIssueStatusTransaction() {

        init();

        Status status = new Status(maker, "test", icon, image, "strontje", true);

        //CREATE ISSUE STATUS TRANSACTION
        IssueStatusRecord issueStatusRecord = new IssueStatusRecord(maker, null, status, FEE_POWER, timestamp);
        issueStatusRecord.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        try {
            assertEquals(Transaction.CREATOR_NOT_PERSONALIZED, issueStatusRecord.isValid(Transaction.FOR_NETWORK, flags));
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        issueStatusRecord.sign(maker, Transaction.FOR_NETWORK);
        issueStatusRecord.process(gb, Transaction.FOR_NETWORK);

        LOGGER.info("status KEY: " + status.getKey());

        //CHECK STATUS EXISTS SENDER
        long key = issueStatusRecord.key;
        assertTrue(db.getItemStatusMap().contains(key));

        StatusCls status_2 = new Status(maker, "test132_2", icon, image, "2_12345678910strontje", true);
        IssueStatusRecord issueStatusTransaction_2 = new IssueStatusRecord(maker, null, status_2, FEE_POWER, timestamp + 10);
        issueStatusTransaction_2.sign(maker, Transaction.FOR_NETWORK);
        issueStatusTransaction_2.setDC(db, Transaction.FOR_NETWORK, 1, 1, true);
        issueStatusTransaction_2.process(gb, Transaction.FOR_NETWORK);
        LOGGER.info("status_2 KEY: " + status_2.getKey());
        issueStatusTransaction_2.orphan(gb, Transaction.FOR_NETWORK);
        assertEquals(mapSize + 1, statusMap.size());

        //CHECK STATUS IS CORRECT
        assertTrue(Arrays.equals(db.getItemStatusMap().get(key).toBytes(true), status.toBytes(true)));

        //CHECK REFERENCE SENDER
        assertEquals(issueStatusRecord.getTimestamp(), maker.getLastTimestamp(db));

        ////// ORPHAN ///////

        issueStatusRecord.orphan(gb, Transaction.FOR_NETWORK);

        assertEquals(mapSize, statusMap.size());

        //CHECK STATUS EXISTS SENDER
        assertFalse(db.getItemStatusMap().contains(key));

        //CHECK REFERENCE SENDER
        //assertEquals(issueStatusRecord.getReference(), maker.getLastReference(db));
    }

    // TODO - in statement - valid on key = 999
}
