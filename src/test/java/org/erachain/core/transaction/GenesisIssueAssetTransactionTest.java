package org.erachain.core.transaction;

import erchi.core.account.PrivateKeyAccount;
import lombok.extern.slf4j.Slf4j;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetVenture;
import org.erachain.core.wallet.Wallet;
import org.erachain.database.IDB;
import org.erachain.datachain.DCSet;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@Slf4j
public class GenesisIssueAssetTransactionTest {

    private final byte[] icon = new byte[]{1, 3, 4, 5, 6, 9}; // default value
    private final byte[] image = new byte[]{4, 11, 32, 23, 45, 122, 11, -45}; // default value
    long ERA_KEY = AssetCls.ERA_KEY;
    long FEE_KEY = AssetCls.FEE_KEY;
    //CREATE KNOWN ACCOUNT
    byte[] bytes;
    DCSet dcSet;
    byte[] itemAppData = null;
    int[] TESTED_DBS = new int[]{IDB.DBS_MAP_DB
            //, IDB.DBS_ROCK_DB, IDB.DBS_MAP_DB_IN_MEM
    };
    //CREATE KNOWN ACCOUNT
    byte[] seed = Crypto.getInstance().digest("tes213sdffsdft".getBytes());
    byte[] privateKey = Crypto.getInstance().createKeyPair(seed).getA();
    PrivateKeyAccount maker = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, privateKey);
    Account recipient = new PrivateKeyAccount(Crypto.ED25519_SYSTEM, Wallet.makeAccountSeedByNonce(seed, 1));
    AssetCls assetMovable;
    BigDecimal amount = BigDecimal.valueOf(10).setScale(BlockChain.AMOUNT_DEFAULT_SCALE);

    String title = "title probe";
    String tags = "tag";
    String message = "test123!";

    int[] TESTED_DEAL = new int[]{
            Transaction.FOR_NETWORK,
            Transaction.FOR_DB_RECORD
    };

    @Test
    public void parse() {

        for (int forDeal : TESTED_DEAL) {
            System.out.println("*** DEAL: " + forDeal + " ***");

            assetMovable = new AssetVenture(0, maker, "movable-111", icon, image, "...", 8, 500L);

            GenesisIssueAssetTransaction tx = new GenesisIssueAssetTransaction(assetMovable, title, tags, message);

            //CONVERT TO BYTES
            byte[] rawTX = tx.toBytes(forDeal);

            //CHECK DATA LENGTH
            assertEquals(rawTX.length, tx.getDataLength(forDeal));

            Transaction parsedTX;
            try {
                //PARSE FROM BYTES
                parsedTX = TransactionFactory.getInstance().parse(rawTX, forDeal);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                fail("Exception while parsing transaction.");
                return;
            }

            //CHECK INSTANCE
            assertTrue(parsedTX instanceof GenesisIssueAssetTransaction);

            //CHECK SIGNATURE
            assertArrayEquals(tx.getSignature(), parsedTX.getSignature());

            //CHECK ISSUER
            assertEquals(tx.getCreator().getAddress(), parsedTX.getCreator().getAddress());

            assertEquals(tx.getItem(), ((GenesisIssueAssetTransaction) parsedTX).getItem());

            assertEquals(((AssetVenture) tx.getItem()).getQuantity(), ((AssetVenture) ((GenesisIssueAssetTransaction) parsedTX).getItem()).getQuantity());
            assertArrayEquals(tx.toBytes(forDeal), parsedTX.toBytes(forDeal));


            //PARSE TRANSACTION FROM WRONG BYTES
            rawTX = new byte[tx.getDataLength(forDeal)];

            try {
                //PARSE FROM BYTES
                TransactionFactory.getInstance().parse(rawTX, forDeal);

                //FAIL
                fail("this should throw an exception");
            } catch (Exception e) {
                //EXCEPTION IS THROWN OK
            }

            tx = new GenesisIssueAssetTransaction(assetMovable, null, null, null);

            //CONVERT TO BYTES
            rawTX = tx.toBytes(forDeal);

            //CHECK DATA LENGTH
            assertEquals(rawTX.length, tx.getDataLength(forDeal));

            try {
                //PARSE FROM BYTES
                parsedTX = TransactionFactory.getInstance().parse(rawTX, forDeal);
            } catch (Exception e) {
                fail("Exception while parsing transaction.");
                logger.error(e.getMessage(), e);
                return;
            }

            //CHECK INSTANCE
            assertTrue(parsedTX instanceof GenesisIssueAssetTransaction);

            //CHECK SIGNATURE
            assertArrayEquals(tx.getSignature(), parsedTX.getSignature());

            //CHECK ISSUER
            assertEquals(tx.getCreator().getAddress(), parsedTX.getCreator().getAddress());

            assertEquals(tx.getItem(), ((GenesisIssueAssetTransaction) parsedTX).getItem());

            assertEquals(((AssetVenture) tx.getItem()).getQuantity(), ((AssetVenture) ((GenesisIssueAssetTransaction) parsedTX).getItem()).getQuantity());

            assertNull(parsedTX.tags);
            assertArrayEquals(tx.toBytes(forDeal), parsedTX.toBytes(forDeal));


            //PARSE TRANSACTION FROM WRONG BYTES
            rawTX = new byte[tx.getDataLength(forDeal)];

            try {
                //PARSE FROM BYTES
                TransactionFactory.getInstance().parse(rawTX, forDeal);

                //FAIL
                fail("this should throw an exception");
            } catch (Exception e) {
                //EXCEPTION IS THROWN OK
            }

        }
    }

    void init(int dbs) {
        dcSet = DCSet.createEmptyDatabaseSet(dbs);

    }

    @Test
    public void dbParse() {

        Transaction tx;

        for (int dbs : TESTED_DBS) {

            int seq = 0;

            init(dbs);

            assetMovable = new AssetVenture(0, maker, "movable-" + seq, icon, image, "...", 8, 500L);
            tx = new GenesisIssueAssetTransaction(assetMovable, title, tags, message);
            tx.setHeightSeq(1, ++seq);
            dcSet.getTransactionFinalMap().put(tx);

            assetMovable = new AssetVenture(0, maker, "movable-" + seq, icon, image, "...", 8, 500L);
            tx = new GenesisIssueAssetTransaction(assetMovable, title, tags, message);
            tx.setHeightSeq(1, ++seq);
            dcSet.getTransactionFinalMap().put(tx);

            tx = new GenesisTransferAssetTransaction(recipient, ERA_KEY, amount, title, tags, message);
            tx.setHeightSeq(1, ++seq);
            dcSet.getTransactionFinalMap().put(tx);


            Transaction txDB = dcSet.getTransactionFinalMap().get(1, seq);

            assertArrayEquals(tx.getSignature(), txDB.getSignature());

            dcSet.close();
        }

    }

    @Test
    public void processBody() {
    }
}