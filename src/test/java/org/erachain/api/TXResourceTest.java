package org.erachain.api;

import org.junit.Test;

import static org.junit.Assert.fail;

public class TXResourceTest {

    TXResource txRes = new TXResource();
    // post tx/make {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4"}
    // post tx/make {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","feePow":1,"recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4","dataMessage":"probe","tags":"TAG"}

    // post tx/issue {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","feePow":1,"recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4","dataMessage":"probe","tags":"TAG"}
    // post tx/issue {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","feePow":1,"recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4","dataMessage":"probe","tags":"TAG","encrypt":true}
    // post tx/issue {"type":24,"version":0,"creator": "acc:3tNqdzAtqwBZBgUGREzZpcMHycVj","feePow":1,"title":"Hello World!","dataMessage":"probe","tags":"issue","item":{"itemType":4,"typeClass":2}}

    // pub:WzZ7bmQtpxG8oZiPb71qTvQkmcFtiyv89kwN6vpc9587q
    @Test
    public void make() {
        //String data = "{\"type\":24,\"version\":0,\"cre-ator\": \"pub:WzZ7bmQtpxG8oZiPb71qTvQkmcFtiyv89kwN6vpc9587q\",\"feePow\":1,\"title\":\"Hello World!\",\"dataMessage\":\"probe\",\"tags\":\"issue\",\"item\":{\"itemType\":1,\"typeClass\":1,\"na-me\":\"AVATAR NAME\",\"startDate\":123}}";
        String data = "{\"type\":31,\"version\":0,\"creator\": \"acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu\",\"feePow\":1,\"recipient\":\"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4\",\"dataMessage\":\"probe\",\"tags\":\"TAG\",\"encrypt\":true}";
        String result = txRes.make(null, data);
        if (result.startsWith("{\"error\""))
            fail(result);

        System.out.println(result);
    }

    // {"type":24,"creator": "acc:...","title":"Hello World!","dataMessage":"probe","tags":"issue","item":{"itemType":2,"typeClass":2,"name":"AVATAR NAME","startDate":123}}
    @Test
    public void sign() {
        // TODO need to open wallet
        //String data = "{\"type\":24,\"version\":0,\"creator\": \"acc:3tNqdzAtqwBZBgUGREzZpcMHycVj\",\"feePow\":1,\"title\":\"Hello World!\",\"dataMessage\":\"probe\",\"tags\":\"issue\",\"item\":{\"itemType\":4,\"typeClass\":2}}";
        String data = "{\"type\":24,\"version\":0,\"creator\": \"pub:WzZ7bmQtpxG8oZiPb71qTvQkmcFtiyv89kwN6vpc9587q\",\"feePow\":1,\"title\":\"Hello World!\",\"dataMessage\":\"probe\",\"tags\":\"issue\",\"item\":{\"itemType\":4,\"typeClass\":2,\"name\":\"AVATAR NAME\",\"startDate\":123}}";
        String result = txRes.sign(null, data);
        if (result.startsWith("{\"error\""))
            fail(result);
    }
}