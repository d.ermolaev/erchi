package org.erachain.webAPI;

import org.erachain.SettingTests;
import org.json.simple.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class APITXTest extends SettingTests {

    @Test
    public void Default() {

    }

    @Test
    public void testBroadcastPost() {
        String raw68 = "HwCAAAAAAYTIFnblAAAAAAAAAADAfSkxpsuBDxJH2nufuplAefjWiYtVlnmiCPNWMSqMCwAAUb9/jGrGwsKjEE6Fr4MtRKPSyqZn334SWb+2jnwLTU4zwBgF6xaMTfGvBAej0JAXLDmwX78ClmZyrEKWjHoJF1N2PXMbALQ4029Ryz9C+tn1NuY/jK3DDAA4WyJtaW50IiwiVVNEIiwxMzMxLCI3NHhGcDN6dUFuem1uTDg4NWZhTkd5TVZwb2cydGdVWm1rIl0AAQ==";
        JSONObject res = APITXResource.broadcastFromRawString(raw68, true, "en");
        System.out.println(res.toJSONString());
        assertEquals(13, (int) (Integer) ((JSONObject) res.get("error")).get("code"));

    }

}

