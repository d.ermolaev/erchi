package org.erachain.database.wallet;

import org.erachain.core.crypto.Crypto;
import org.erachain.core.wallet.Wallet;
import org.erachain.datachain.DCSet;
import org.erachain.settings.Settings;
import org.erachain.utils.SimpleFileVisitorForRecursiveFolderDeletion;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;

public class DWSetTest {

    String password = "test";
    Wallet wallet;
    boolean create;
    //CREATE EMPTY MEMORY DATABASE

    // INIT NOTES
    // тест полный всех - не успевает удалить файлы и валится createWallet тест - в ручную один он проходит
    private synchronized void init() {

        // delete \dataWallet\wallet.dat
        File walletFile = new File(Settings.getInstance().getDataWalletPath(), "wallet.dat");
        if (walletFile.getParentFile().exists())
            try {
                Files.walkFileTree(walletFile.getParentFile().toPath(),
                        new SimpleFileVisitorForRecursiveFolderDeletion());
            } catch (Throwable e1) {
                System.out.println(e1.getMessage());
            }


        // delete \walletKeys\wallet.s.dat.p
        if (Settings.SECURE_WALLET_FILE.getParentFile().exists())
            try {
                Files.walkFileTree(Settings.SECURE_WALLET_FILE.getParentFile().toPath(),
                        new SimpleFileVisitorForRecursiveFolderDeletion());
            } catch (Throwable e1) {
                System.out.println(e1.getMessage());
            }

        DCSet.reCreateDBinMEmory(false, false);
        DCSet dcSet = DCSet.getInstance();


        // make anew \walletKeys\wallet.s.dat.p
        SecureWalletDatabase secureDatabase = new SecureWalletDatabase(password);

        wallet = new Wallet(dcSet, false, false);
        create = wallet.create(secureDatabase, Crypto.getInstance()
                .digest(password.getBytes()), 10, false, false);
    }

    @Test
    public void getLastBlockSignature() {

        init();


    }

    @Test
    public void setLastBlockSignature() {
    }
}