package org.erachain.utils;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class BigDecimalUtilTest {

    byte[] data = new byte[0];
    BigDecimal amount = new BigDecimal("-123.9");
    BigDecimal result;

    @Test
    public void isValid() {
    }

    @Test
    public void toBytes() {

        data = BigDecimalUtil.toBytes(data, amount);
        result = BigDecimalUtil.fromBytes(data, 0);
        assertEquals(amount, result);

        data = new byte[100];
        BigDecimalUtil.toBytes(data, 5, amount);
        result = BigDecimalUtil.fromBytes(data, 5);
        assertEquals(amount, result);

        data = BigDecimalUtil.toBytes(amount);
        result = BigDecimalUtil.fromBytes(data, 0);
        assertEquals(amount, result);

    }

    @Test
    public void testToBytes() {
    }

    @Test
    public void fromBytes() {
    }

    @Test
    public void bytesLen() {
    }
}