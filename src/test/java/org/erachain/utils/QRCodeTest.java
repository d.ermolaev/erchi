package org.erachain.utils;

import com.google.zxing.WriterException;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class QRCodeTest {

    @Test
    public void makeSVG() throws WriterException, IOException {
        String url = "https://explorer.erachain.org/index/blockexplorer.html?address=75rEoNUknMU3qYGjS3wriY53n1aRUznFus";
        FileUtils.writeStringToFile(new File("test.svg"), QRCode.makeSVG(url, 128), StandardCharsets.US_ASCII, false);
        //assertEquals(, "ww");
    }
}