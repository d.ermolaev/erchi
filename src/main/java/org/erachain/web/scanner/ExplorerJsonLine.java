package org.erachain.web.scanner;

import org.json.simple.JSONObject;

public interface ExplorerJsonLine {

    JSONObject jsonForExplorerPage(JSONObject langObj, Object[] args);

}
