package org.erachain.webAPI;

import com.mitchellbosecke.pebble.error.PebbleException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.erachain.controller.Controller;
import org.erachain.core.account.Account;
import org.erachain.core.transaction.Transaction;
import org.erachain.web.ServletUtils;
import org.erachain.web.scanner.Scanner;
import org.erachain.web.scanner.WrongSearchException;
import org.erachain.datachain.DCSet;
import org.erachain.lang.Lang;
import org.erachain.settings.Settings;
import org.erachain.utils.Pair;
import org.erachain.utils.PebbleHelper;
import org.erachain.utils.StrJSonFine;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Path("/")
public class WebResource {

    public static MediaType TYPE_IMAGE = new MediaType("image", "*");
    public static MediaType TYPE_GIF = new MediaType("image", "gif");
    public static MediaType TYPE_PNH = new MediaType("image", "png");
    public static MediaType TYPE_JPEG = new MediaType("image", "jpeg");
    public static MediaType TYPE_VIDEO = new MediaType("video", "mp4");
    public static MediaType TYPE_AUDIO = new MediaType("audio", "mp3");
    public static MediaType TYPE_HTML = new MediaType("html", "*");
    public static MediaType TYPE_ARRAY = new MediaType("array", "*");

    private static final Logger logger = LoggerFactory.getLogger(WebResource.class);
    @Context
    HttpServletRequest request;

    public static String selectTitleOpt(Document htmlDoc) {
        String title = selectFirstElementOpt(htmlDoc, "title");

        return title;
    }

    public static String selectFirstElementOpt(Document htmlDoc, String tag) {
        Elements titleElements = htmlDoc.select(tag);
        String title = null;
        if (!titleElements.isEmpty()) {
            title = titleElements.get(0).text();
        }
        return title;
    }

    public static String selectDescriptionOpt(Document htmlDoc) {
        String result = "";
        Elements descriptions = htmlDoc.select("meta[name=\"description\"]");
        if (!descriptions.isEmpty()) {
            Element descr = descriptions.get(0);
            if (descr.hasAttr("content")) {
                result = descr.attr("content");
            }
        }

        return result;
    }

    public static String getFileExtention(String filename) {
        int dotPos = filename.lastIndexOf(".") + 1;
        return filename.substring(dotPos);
    }


    public static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }


    @GET
    public Response Default() {

        // REDIRECT
        return Response.status(302).header("Location", "index/main.html")
                .build();
    }



    @SuppressWarnings("rawtypes")
    @Path("index/blockexplorer.json")
    @GET
    public Response jsonQueryMain(@Context UriInfo info) {
        Map output;
        try {
            output = Scanner.getInstance().
                    jsonQueryMain(info);
        } catch (WrongSearchException e) {
            logger.info(e.getMessage(), e);
            output = Scanner.getInstance().getOutput();
            output.put("noSearchedElements", "true");
            output.put("error", e.getMessage());
            return Response.status(200)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Access-Control-Allow-Origin", "*")
                    .entity(StrJSonFine.convert(output))
                    .build();
        } catch (Exception ee) {
            //ee.printStackTrace();
            logger.error(ee.getMessage(), ee);
            StringBuilder ss = new StringBuilder();
            for (StackTraceElement item : ee.getStackTrace()) {
                ss.append(item.toString()).append("<br>");
            }
            return Response.status(501)
                    .header("Content-Type", "text/html; charset=utf-8")
                    .entity(ss + "<br>" + ee.getMessage())
                    .build();
        }

        return Response.status(200)
                .header("Content-Type", "application/json; charset=utf-8")
                .header("Access-Control-Allow-Origin", "*")
                .entity(StrJSonFine.convert(output))
                .build();
    }

    @Path("index/blockexplorer")
    @GET
    public Response blockexplorer() {
        return blockexplorerhtml();
    }

    @GET
    @Path("blockexplorer")
    public Response newBlockExplorerIndex() {
        File file = new File("web/blockexplorer/index.html");
        try {
            BufferedInputStream is = new BufferedInputStream(new FileInputStream(file));
            String type = URLConnection.guessContentTypeFromStream(is);
            return Response.ok(file, type).build();
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error(e.getMessage(), e);
            return Response.status(500).build();
        }
    }

    @GET
    @Path("blockexplorer/{address: .+}")
    public Response newBlockExplorer(@PathParam("address") String address) {
        String addr = "web/blockexplorer/" + address;
        File file = new File(addr);

        if (!file.exists()) {
            file = new File("web/blockexplorer/index.html");
        }

        try {
            BufferedInputStream is = new BufferedInputStream(new FileInputStream(file));
            String type = URLConnection.guessContentTypeFromStream(is);
            return Response.ok(file, type).build();
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error(e.getMessage(), e);
            return Response.status(500).build();
        }
    }

    @Path("index/blockexplorer.html")
    @GET
    public Response blockexplorerhtml() {
        String content;
        JSONObject langObj;
        String lang = request.getParameter("lang");
        try {
            content = readFile("web/blockexplorer.html",
                    StandardCharsets.UTF_8);


        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return error404(request, null);
        }

        Document doc = null;

        doc = Jsoup.parse(content);

        //	Element element = doc.getElementById("menu_top_100_");
        //	if (element != null)	element.text("werwfyrtyryrtyrtyrtyrtyrtyrtyrtytyrerwer");

        //

        if (lang != null) {

            //logger.info("try lang file: " + lang + ".json for " + request.getRemoteUser() + " " + request.getRequestURL());
            langObj = Lang.openLangFile(lang + ".json");

     /*   // translate select
            Elements el = doc.getElementsByTag("option");//.select("translate");
            for (Element e : el) {
                e.text(Lang.TFromLangObj(e.text(), langObj));
          }
      */        // translate links
            Elements el = doc.getElementsByAttributeValueContaining("translate", "true");//.select("translate");
            for (Element e : el) {
                e.text(Lang.T(e.text(), langObj));
            }
        }


        return Response.ok(doc.toString(), "text/html; charset=utf-8").build();

    }

    @Path("index/test.html")
    @GET
    public Response test() {
        String content;
        JSONObject langObj;
        String lang = request.getParameter("lang");
        try {
            content = readFile("web/test.html",
                    StandardCharsets.UTF_8);


        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return error404(request, null);
        }

        Document doc = null;

        doc = Jsoup.parse(content);

        //	Element element = doc.getElementById("menu_top_100_");
        //	if (element != null)	element.text("werwfyrtyryrtyrtyrtyrtyrtyrtyrtytyrerwer");

        //

        if (lang != null) {

            logger.error("try lang file: " + lang + ".json");
            langObj = Lang.openLangFile(lang + ".json");


            Elements el = doc.select("translate");
            for (Element e : el) {
                e.text(Lang.T(e.text(), langObj));
            }
        }

        return Response.ok(doc.toString(), "text/html; charset=utf-8").build();

    }

    @Path("index/deleteunconfirmed.html")
    @GET
    public Response doDeleteUnconfirmedTxs() {

        DCSet dcSet = DCSet.getInstance();
        Collection<Transaction> values = dcSet.getTransactionTab().values();

        List<Account> myAccounts = Controller.getInstance().getWalletAccounts();

        for (Transaction transaction : values) {
            if (myAccounts.contains(transaction.getCreator())) {
                dcSet.getTransactionTab().delete(transaction);
            }
        }

        return error404(request, "Unconfirmed transactions removed.");

    }

    @SuppressWarnings("unchecked")
    @POST
    @Path("index/api.html")
    @Consumes("application/x-www-form-urlencoded")
    public Response createApiCall(@Context HttpServletRequest request,
                                  MultivaluedMap<String, String> form) throws IOException {

        String type = form.getFirst("type");
        String apiurl = form.getFirst("apiurl");

        String jsonContent = form.getFirst("json");
        JSONObject jsonanswer = new JSONObject();

        if (StringUtils.isBlank(type)
                || (!type.equalsIgnoreCase("get")
                && !type.equalsIgnoreCase("post") && !type
                .equalsIgnoreCase("delete"))) {

            jsonanswer.put("type", "apicallerror");
            jsonanswer.put("errordetail",
                    "type parameter must be post, get or delete");

            return Response.status(200)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Access-Control-Allow-Origin", "*")
                    .entity(jsonanswer.toJSONString()).build();
        }

        if (StringUtils.isBlank(apiurl)) {
            jsonanswer.put("type", "apicallerror");
            jsonanswer.put("errordetail",
                    "apiurl parameter must be correct set");

            return Response.status(200)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Access-Control-Allow-Origin", "*")
                    .entity(jsonanswer.toJSONString()).build();
        }

        // CREATE CONNECTION

        apiurl = apiurl.startsWith("/") ? apiurl.substring(1) : apiurl;

        URL urlToCall = new URL("http://127.0.0.1:"
                + Settings.getInstance().getRpcPort() + "/" + apiurl);
        HttpURLConnection connection = (HttpURLConnection) urlToCall
                .openConnection();
        connection.setRequestProperty("X-FORWARDED-FOR", ServletUtils.getRemoteAddress(request));

        // EXECUTE
        connection.setRequestMethod(type.toUpperCase());

        if (type.equalsIgnoreCase("POST")) {
            connection.setDoOutput(true);
            connection.getOutputStream().write(
                    jsonContent.getBytes(StandardCharsets.UTF_8));
            connection.getOutputStream().flush();
            connection.getOutputStream().close();
        }

        // READ RESULT
        InputStream stream;
        if (connection.getResponseCode() == 400) {
            stream = connection.getErrorStream();
        } else {
            stream = connection.getInputStream();
        }

        InputStreamReader isReader = new InputStreamReader(stream, "UTF-8");
        BufferedReader br = new BufferedReader(isReader);
        String result = br.readLine();

        if (result.contains("message") && result.contains("error")) {
            jsonanswer.put("type", "apicallerror");
            jsonanswer.put("errordetail", result);
            return Response.status(200)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Access-Control-Allow-Origin", "*")
                    .entity(jsonanswer.toJSONString()).build();
        } else {
            jsonanswer.put("type", "success");
            jsonanswer.put("result", result);
            return Response.status(200)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Access-Control-Allow-Origin", "*")
                    .entity(jsonanswer.toJSONString()).build();
        }

    }

    @SuppressWarnings("unchecked")
    @POST
    @Path("index/encodefile.html")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadMultipart(@FormDataParam("file") FormDataBodyPart is)
            throws IOException {
        try {
            InputStream valueAs = is.getValueAs(InputStream.class);
            byte[] byteArray = IOUtils.toByteArray(valueAs);
            String encode = Base64.getEncoder().encodeToString(byteArray);
            MediaType mediaType = is.getMediaType();
            String result = "data:" + mediaType.getType() + "/"
                    + mediaType.getSubtype() + ";base64, ";
            result += encode;

            JSONObject json = new JSONObject();
            if (StringUtils.isEmpty(encode)) {
                json.put("type", "error");
                json.put("result", "You did not choose a file or the file was empty!");
            } else if (checkPlainTypes(mediaType)) {
                json.put("type", "success");
                json.put("result", new String(byteArray));
            } else {
                json.put("type", "success");
                json.put("result", result);
            }

            return Response.status(200)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .entity(json.toJSONString()).build();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return null;
        // prepare the response
    }

    public boolean checkPlainTypes(MediaType mediaType) {

        List<Pair<String, String>> pairsToCheck = new ArrayList<Pair<String, String>>();
        pairsToCheck.add(new Pair<String, String>("text", "html"));
        pairsToCheck.add(new Pair<String, String>("text", "plain"));

        for (Pair<String, String> pair : pairsToCheck) {
            if (pair.getA().equalsIgnoreCase(mediaType.getType()) && pair.getB().equalsIgnoreCase(mediaType.getSubtype())) {
                return true;
            }
        }

        return false;
    }

    public String decodeIfNotNull(String parameter)
            throws UnsupportedEncodingException {
        return parameter != null ? URLDecoder.decode(parameter, "UTF-8") : null;
    }

    @Path("index/main.html")
    @GET
    public Response handleIndex() {
        //return handleDefault();
        return blockexplorer();
    }

    @Path("favicon.ico")
    @GET
    public Response favicon() {
        File file = new File("web/favicon.ico");

        if (file.exists()) {
            return Response.ok(file, "image/png").build();
            //return Response.ok(file, "image/vnd.microsoft.icon").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/favicon.ico")
    @GET
    public Response indexfavicon() {
        File file = new File("web/favicon.ico");

        if (file.exists()) {
            return Response.ok(file, "image/png").build();
            //return Response.ok(file, "image/vnd.microsoft.icon").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/img/{filename}")
    @GET
    public Response image(@PathParam("filename") String filename) {

        File file = new File("web/img/" + filename);
        if (!file.exists())
            return error404(request, null);

        String type = "";

        switch (getFileExtention(filename)) {
            case "png":
                type = "image/png";
                break;
            case "gif":
                type = "image/gif";
                break;
            case "jpg":
                type = "image/jpeg";
                break;
            case "svg":
                type = "image/svg+xml";
        }

        return Response.ok(file, type).build();

    }

    @Path("ic/{filename}")
    @GET
    public Response imageIC(@PathParam("filename") String filename) {
        return image("ic/" + filename);
    }

    // http://127.0.0.1:9067/smartcontract/epoch/000001/01/001.png
    @Path("smartcontract/epoch/{id}/{slot}/{filename}")
    @GET
    public Response smartcontractFiles(@PathParam("id") String id, @PathParam("slot") String slot, @PathParam("filename") String filename) {

        File file;
        if (slot.isEmpty()) {
            file = new File("dapp/epoch/" + id + "/" + filename);
        } else {
            file = new File("dapp/epoch/" + id + "/" + slot + "/" + filename);
        }

        if (!file.exists())
            return error404(request, null);

        String type = "";

        switch (getFileExtention(filename)) {
            case "png":
                type = "image/png";
                break;
            case "gif":
                type = "image/gif";
                break;
            case "jpg":
                type = "image/jpeg";
                break;
            case "svg":
                type = "image/svg+xml";
        }

        return Response.ok(file, type).build();
    }

    @Path("index/libs/css/style.css")
    @GET
    public Response style() {
        File file = new File("web/libs/css/style.css");

        if (file.exists()) {
            return Response.ok(file, "text/css").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/css/sidebar.css")
    @GET
    public Response sidebarcss() {
        File file = new File("web/libs/css/sidebar.css");

        if (file.exists()) {
            return Response.ok(file, "text/css").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/css/timeline.css")
    @GET
    public Response timelinecss() {
        File file = new File("web/libs/css/timeline.css");

        if (file.exists()) {
            return Response.ok(file, "text/css").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/sidebar.js")
    @GET
    public Response sidebarjs() {
        File file = new File("web/libs/js/sidebar.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/utils.js")
    @GET
    public Response utilsjs() {
        File file = new File("web/libs/js/utils.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/marked.js")
    @GET
    public Response markedjs() {
        File file = new File("web/libs/js/marked.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/third-party/highlight.pack.js")
    @GET
    public Response highlightpackjs() {
        File file = new File("web/libs/js/third-party/highlight.pack.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/third-party/github.css")
    @GET
    public Response highgitcss() {
        File file = new File("web/libs/js/third-party/github.css");

        if (file.exists()) {
            return Response.ok(file, "text/css").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/clipboard.js")
    @GET
    public Response clipboard() {
        File file = new File("web/libs/js/clipboard.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorer.js")
    @GET
    public Response explorer() {
        File file = new File("web/libs/js/explorer.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorer").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerItems.js")
    @GET
    public Response explorerItems() {
        File file = new File("web/libs/js/explorerItems.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerItems").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerAssets.js")
    @GET
    public Response explorerAssets() {
        File file = new File("web/libs/js/explorerAssets.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerAssets").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerPersons.js")
    @GET
    public Response explorerPersons() {
        File file = new File("web/libs/js/explorerPersons.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerPersons").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerStatements.js")
    @GET
    public Response explorerStatements() {
        File file = new File("web/libs/js/explorerStatements.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerStatements").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerTemplates.js")
    @GET
    public Response explorerTemplates() {
        File file = new File("web/libs/js/explorerTemplates.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerTemplates").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerStatuses.js")
    @GET
    public Response explorerStatuses() {
        File file = new File("web/libs/js/explorerStatuses.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerStatuses").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerTransactions.js")
    @GET
    public Response explorerTransactions() {
        File file = new File("web/libs/js/explorerTransactions.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerTransactions").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerExchange.js")
    @GET
    public Response explorerExchange() {
        File file = new File("web/libs/js/explorerExchange.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerExchange").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerPolls.js")
    @GET
    public Response explorerPolls() {
        File file = new File("web/libs/js/explorerPolls.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerPolls").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/explorerTransactionsTable.js")
    @GET
    public Response explorerTransactionsTable() {
        File file = new File("web/libs/js/explorerTransactionsTable.js");

        if (file.exists()) {
            return Response.ok(file, "text/explorerTransactionsTable").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/third-party/ZeroClipboard.min.js")
    @GET
    public Response ZeroClipboardmin() {
        File file = new File("web/libs/js/third-party/ZeroClipboard.min.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/third-party/ZeroClipboard.swf")
    @GET
    public Response ZeroClipboard() {
        File file = new File("web/libs/js/third-party/ZeroClipboard.swf");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/biginteger.js")
    @GET
    public Response biginteger() {
        File file = new File("web/libs/js/biginteger.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/converters.js")
    @GET
    public Response converters() {
        File file = new File("web/libs/js/converters.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/crypto/curve25519.js")
    @GET
    public Response curve25519() {
        File file = new File("web/libs/js/crypto/curve25519.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/crypto/curve25519_.js")
    @GET
    public Response curve25519_() {
        File file = new File("web/libs/js/crypto/curve25519_.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/crypto/3rdparty/cryptojs/sha256.js")
    @GET
    public Response sha256() {
        File file = new File("web/libs/js/crypto/3rdparty/cryptojs/sha256.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }


    @Path("index/libs/js/Base58.js")
    @GET
    public Response Base58js() {
        File file = new File("web/libs/js/Base58.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/common.js")
    @GET
    public Response commonjs() {
        File file = new File("web/libs/js/common.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("/index/libs/third-party/jquery.form.min.js")
    @GET
    public Response getFormMin() {
        File file = new File("web/libs/js/third-party/jquery.form.min.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/jquery/jquery.{version}.js")
    @GET
    public Response jquery(@PathParam("version") String version) {
        File file;
        if (version.equals("1")) {
            file = new File("web/libs/jquery/jquery-1.11.3.min.js");
        } else if (version.equals("2")) {
            file = new File("web/libs/jquery/jquery-2.1.4.min.js");
        } else {
            file = new File("web/libs/jquery/jquery-2.1.4.min.js");
        }

        if (file.exists()) {
            return Response.ok(file, "text/javascript; charset=utf-8").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/angular/angular.{version}.js")
    @GET
    public Response angular(@PathParam("version") String version) {
        File file;
        if (version.equals("1.3")) {
            file = new File("web/libs/angular/angular.min.1.3.15.js");
        } else if (version.equals("1.4")) {
            file = new File("web/libs/angular/angular.min.1.4.0.js");
        } else {
            file = new File("web/libs/angular/angular.min.1.3.15.js");
        }

        if (file.exists()) {
            return Response.ok(file, "text/javascript; charset=utf-8").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/bootstrap/{version}/{folder}/{filename}")
    @GET
    public Response bootstrap(@PathParam("version") String version,
                              @PathParam("folder") String folder,
                              @PathParam("filename") String filename) {
        String fullname = "web/libs/bootstrap-3.3.4-dist/";
        String type = "text/html; charset=utf-8";

        switch (folder) {
            case "css": {
                fullname += "css/";
                type = "text/css";
                switch (filename) {
                    case "bootstrap.css":

                        fullname += "bootstrap.css";
                        break;

                    case "theme.css":

                        fullname += "theme.css";
                        break;

                    case "bootstrap.css.map":

                        fullname += "bootstrap.css.map";
                        break;

                    case "bootstrap.min.css":

                        fullname += "bootstrap.min.css";
                        break;

                    case "bootstrap-theme.css":

                        fullname += "bootstrap-theme.css";
                        break;

                    case "bootstrap-theme.css.map":

                        fullname += "bootstrap-theme.css.mapp";
                        break;

                    case "bootstrap-theme.min.css":

                        fullname += "bootstrap-theme.min.css";
                        break;
                }
                break;
            }
            case "fonts": {
                fullname += "fonts/";
                switch (filename) {
                    case "glyphicons-halflings-regular.eot":

                        fullname += "glyphicons-halflings-regular.eot";
                        type = "application/vnd.ms-fontobject";
                        break;

                    case "glyphicons-halflings-regular.svg":

                        fullname += "glyphicons-halflings-regular.svg";
                        type = "image/svg+xml";
                        break;

                    case "glyphicons-halflings-regular.ttf":

                        fullname += "glyphicons-halflings-regular.ttf";
                        type = "application/x-font-ttf";
                        break;

                    case "glyphicons-halflings-regular.woff":

                        fullname += "glyphicons-halflings-regular.woff";
                        type = "application/font-woff";
                        break;

                    case "glyphicons-halflings-regular.woff2":

                        fullname += "glyphicons-halflings-regular.woff2";
                        type = "application/font-woff";
                        break;
                }
                break;
            }
            case "js": {
                fullname += "js/";
                type = "text/javascript";
                switch (filename) {
                    case "bootstrap.js":

                        fullname += "bootstrap.js";
                        break;

                    case "bootstrap.min.js":

                        fullname += "bootstrap.js";
                        break;

                    case "npm.js":

                        fullname += "npm.js";
                        break;

                }
                break;
            }
        }

        File file = new File(fullname);

        if (file.exists()) {
            return Response.ok(file, type).build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/ckeditor/{folder : .+}")
    @GET
    public Response ckeditor(@PathParam("folder") String folder) {

        String[] files =
                {
                        "adapters/jquery.js",
                        "README.md",
                        "CHANGES.md",
                        "styles.js",
                        "lang/sk.js",
                        "lang/fi.js",
                        "lang/it.js",
                        "lang/he.js",
                        "lang/uk.js",
                        "lang/sv.js",
                        "lang/en-ca.js",
                        "lang/sr-latn.js",
                        "lang/ru.js",
                        "lang/zh-cn.js",
                        "lang/no.js",
                        "lang/fr.js",
                        "lang/fa.js",
                        "lang/da.js",
                        "lang/mk.js",
                        "lang/ko.js",
                        "lang/ro.js",
                        "lang/mn.js",
                        "lang/tr.js",
                        "lang/bg.js",
                        "lang/ka.js",
                        "lang/de.js",
                        "lang/el.js",
                        "lang/pt.js",
                        "lang/af.js",
                        "lang/eu.js",
                        "lang/cy.js",
                        "lang/en-au.js",
                        "lang/hi.js",
                        "lang/en.js",
                        "lang/fr-ca.js",
                        "lang/nb.js",
                        "lang/sr.js",
                        "lang/en-gb.js",
                        "lang/ms.js",
                        "lang/pl.js",
                        "lang/is.js",
                        "lang/lv.js",
                        "lang/km.js",
                        "lang/tt.js",
                        "lang/th.js",
                        "lang/hu.js",
                        "lang/bn.js",
                        "lang/zh.js",
                        "lang/ja.js",
                        "lang/et.js",
                        "lang/nl.js",
                        "lang/ar.js",
                        "lang/eo.js",
                        "lang/lt.js",
                        "lang/gl.js",
                        "lang/ku.js",
                        "lang/cs.js",
                        "lang/vi.js",
                        "lang/ca.js",
                        "lang/ug.js",
                        "lang/fo.js",
                        "lang/id.js",
                        "lang/si.js",
                        "lang/sl.js",
                        "lang/pt-br.js",
                        "lang/es.js",
                        "lang/hr.js",
                        "lang/sq.js",
                        "lang/bs.js",
                        "lang/gu.js",
                        "skins/moono/dialog_ie7.css",
                        "skins/moono/dialog_ie.css",
                        "skins/moono/editor_iequirks.css",
                        "skins/moono/icons_hidpi.png",
                        "skins/moono/editor.css",
                        "skins/moono/readme.md",
                        "skins/moono/dialog_ie8.css",
                        "skins/moono/editor_ie.css",
                        "skins/moono/dialog.css",
                        "skins/moono/icons.png",
                        "skins/moono/dialog_iequirks.css",
                        "skins/moono/editor_ie7.css",
                        "skins/moono/editor_gecko.css",
                        "skins/moono/editor_ie8.css",
                        "skins/moono/images/spinner.gif",
                        "skins/moono/images/arrow.png",
                        "skins/moono/images/lock-open.png",
                        "skins/moono/images/lock.png",
                        "skins/moono/images/close.png",
                        "skins/moono/images/refresh.png",
                        "skins/moono/images/hidpi/lock-open.png",
                        "skins/moono/images/hidpi/lock.png",
                        "skins/moono/images/hidpi/close.png",
                        "skins/moono/images/hidpi/refresh.png",
                        "build-config.js",
                        "config.js",
                        "ckeditor.js",
                        "LICENSE.md",
                        "plugins/preview/preview.html",
                        "plugins/templates/templates/default.js",
                        "plugins/templates/templates/images/template3.gif",
                        "plugins/templates/templates/images/template1.gif",
                        "plugins/templates/templates/images/template2.gif",
                        "plugins/templates/dialogs/templates.css",
                        "plugins/templates/dialogs/templates.js",
                        "plugins/tabletools/dialogs/tableCell.js",
                        "plugins/icons_hidpi.png",
                        "plugins/dialog/dialogDefinition.js",
                        "plugins/iframe/dialogs/iframe.js",
                        "plugins/iframe/images/placeholder.png",
                        "plugins/liststyle/dialogs/liststyle.js",
                        "plugins/magicline/images/icon-rtl.png",
                        "plugins/magicline/images/icon.png",
                        "plugins/magicline/images/hidpi/icon-rtl.png",
                        "plugins/magicline/images/hidpi/icon.png",
                        "plugins/image/dialogs/image.js",
                        "plugins/image/images/noimage.png",
                        "plugins/link/dialogs/link.js",
                        "plugins/link/dialogs/anchor.js",
                        "plugins/link/images/anchor.png",
                        "plugins/link/images/hidpi/anchor.png",
                        "plugins/flash/dialogs/flash.js",
                        "plugins/flash/images/placeholder.png",
                        "plugins/about/dialogs/logo_ckeditor.png",
                        "plugins/about/dialogs/about.js",
                        "plugins/about/dialogs/hidpi/logo_ckeditor.png",
                        "plugins/icons.png",
                        "plugins/div/dialogs/div.js",
                        "plugins/specialchar/dialogs/lang/sk.js",
                        "plugins/specialchar/dialogs/lang/fi.js",
                        "plugins/specialchar/dialogs/lang/it.js",
                        "plugins/specialchar/dialogs/lang/he.js",
                        "plugins/specialchar/dialogs/lang/uk.js",
                        "plugins/specialchar/dialogs/lang/_translationstatus.txt",
                        "plugins/specialchar/dialogs/lang/sv.js",
                        "plugins/specialchar/dialogs/lang/ru.js",
                        "plugins/specialchar/dialogs/lang/zh-cn.js",
                        "plugins/specialchar/dialogs/lang/no.js",
                        "plugins/specialchar/dialogs/lang/fr.js",
                        "plugins/specialchar/dialogs/lang/fa.js",
                        "plugins/specialchar/dialogs/lang/da.js",
                        "plugins/specialchar/dialogs/lang/ko.js",
                        "plugins/specialchar/dialogs/lang/tr.js",
                        "plugins/specialchar/dialogs/lang/bg.js",
                        "plugins/specialchar/dialogs/lang/de.js",
                        "plugins/specialchar/dialogs/lang/el.js",
                        "plugins/specialchar/dialogs/lang/pt.js",
                        "plugins/specialchar/dialogs/lang/af.js",
                        "plugins/specialchar/dialogs/lang/eu.js",
                        "plugins/specialchar/dialogs/lang/cy.js",
                        "plugins/specialchar/dialogs/lang/en.js",
                        "plugins/specialchar/dialogs/lang/fr-ca.js",
                        "plugins/specialchar/dialogs/lang/nb.js",
                        "plugins/specialchar/dialogs/lang/en-gb.js",
                        "plugins/specialchar/dialogs/lang/pl.js",
                        "plugins/specialchar/dialogs/lang/lv.js",
                        "plugins/specialchar/dialogs/lang/km.js",
                        "plugins/specialchar/dialogs/lang/tt.js",
                        "plugins/specialchar/dialogs/lang/th.js",
                        "plugins/specialchar/dialogs/lang/hu.js",
                        "plugins/specialchar/dialogs/lang/zh.js",
                        "plugins/specialchar/dialogs/lang/ja.js",
                        "plugins/specialchar/dialogs/lang/et.js",
                        "plugins/specialchar/dialogs/lang/nl.js",
                        "plugins/specialchar/dialogs/lang/ar.js",
                        "plugins/specialchar/dialogs/lang/eo.js",
                        "plugins/specialchar/dialogs/lang/lt.js",
                        "plugins/specialchar/dialogs/lang/gl.js",
                        "plugins/specialchar/dialogs/lang/ku.js",
                        "plugins/specialchar/dialogs/lang/cs.js",
                        "plugins/specialchar/dialogs/lang/vi.js",
                        "plugins/specialchar/dialogs/lang/ca.js",
                        "plugins/specialchar/dialogs/lang/ug.js",
                        "plugins/specialchar/dialogs/lang/id.js",
                        "plugins/specialchar/dialogs/lang/si.js",
                        "plugins/specialchar/dialogs/lang/sl.js",
                        "plugins/specialchar/dialogs/lang/pt-br.js",
                        "plugins/specialchar/dialogs/lang/es.js",
                        "plugins/specialchar/dialogs/lang/hr.js",
                        "plugins/specialchar/dialogs/lang/sq.js",
                        "plugins/specialchar/dialogs/specialchar.js",
                        "plugins/table/dialogs/table.js",
                        "plugins/showblocks/images/block_address.png",
                        "plugins/showblocks/images/block_blockquote.png",
                        "plugins/showblocks/images/block_pre.png",
                        "plugins/showblocks/images/block_h2.png",
                        "plugins/showblocks/images/block_h3.png",
                        "plugins/showblocks/images/block_h1.png",
                        "plugins/showblocks/images/block_h4.png",
                        "plugins/showblocks/images/block_h6.png",
                        "plugins/showblocks/images/block_div.png",
                        "plugins/showblocks/images/block_p.png",
                        "plugins/showblocks/images/block_h5.png",
                        "plugins/find/dialogs/find.js",
                        "plugins/smiley/dialogs/smiley.js",
                        "plugins/smiley/images/lightbulb.gif",
                        "plugins/smiley/images/cry_smile.png",
                        "plugins/smiley/images/heart.gif",
                        "plugins/smiley/images/thumbs_up.png",
                        "plugins/smiley/images/wink_smile.png",
                        "plugins/smiley/images/teeth_smile.gif",
                        "plugins/smiley/images/teeth_smile.png",
                        "plugins/smiley/images/heart.png",
                        "plugins/smiley/images/regular_smile.gif",
                        "plugins/smiley/images/cry_smile.gif",
                        "plugins/smiley/images/shades_smile.gif",
                        "plugins/smiley/images/embarrassed_smile.png",
                        "plugins/smiley/images/broken_heart.gif",
                        "plugins/smiley/images/shades_smile.png",
                        "plugins/smiley/images/sad_smile.gif",
                        "plugins/smiley/images/omg_smile.gif",
                        "plugins/smiley/images/regular_smile.png",
                        "plugins/smiley/images/angel_smile.png",
                        "plugins/smiley/images/devil_smile.png",
                        "plugins/smiley/images/kiss.gif",
                        "plugins/smiley/images/whatchutalkingabout_smile.gif",
                        "plugins/smiley/images/omg_smile.png",
                        "plugins/smiley/images/envelope.gif",
                        "plugins/smiley/images/confused_smile.png",
                        "plugins/smiley/images/envelope.png",
                        "plugins/smiley/images/tongue_smile.gif",
                        "plugins/smiley/images/embarrassed_smile.gif",
                        "plugins/smiley/images/confused_smile.gif",
                        "plugins/smiley/images/angel_smile.gif",
                        "plugins/smiley/images/tounge_smile.gif",
                        "plugins/smiley/images/thumbs_down.png",
                        "plugins/smiley/images/thumbs_up.gif",
                        "plugins/smiley/images/lightbulb.png",
                        "plugins/smiley/images/tongue_smile.png",
                        "plugins/smiley/images/sad_smile.png",
                        "plugins/smiley/images/angry_smile.gif",
                        "plugins/smiley/images/angry_smile.png",
                        "plugins/smiley/images/devil_smile.gif",
                        "plugins/smiley/images/thumbs_down.gif",
                        "plugins/smiley/images/kiss.png",
                        "plugins/smiley/images/whatchutalkingabout_smile.png",
                        "plugins/smiley/images/wink_smile.gif",
                        "plugins/smiley/images/broken_heart.png",
                        "plugins/smiley/images/embaressed_smile.gif",
                        "plugins/forms/dialogs/textfield.js",
                        "plugins/forms/dialogs/select.js",
                        "plugins/forms/dialogs/hiddenfield.js",
                        "plugins/forms/dialogs/button.js",
                        "plugins/forms/dialogs/checkbox.js",
                        "plugins/forms/dialogs/textarea.js",
                        "plugins/forms/dialogs/form.js",
                        "plugins/forms/dialogs/radio.js",
                        "plugins/forms/images/hiddenfield.gif",
                        "plugins/pastefromword/filter/default.js",
                        "plugins/pagebreak/images/pagebreak.gif",
                        "plugins/wsc/README.md",
                        "plugins/wsc/dialogs/ciframe.html",
                        "plugins/wsc/dialogs/wsc.css",
                        "plugins/wsc/dialogs/wsc_ie.js",
                        "plugins/wsc/dialogs/wsc.js",
                        "plugins/wsc/dialogs/tmpFrameset.html",
                        "plugins/wsc/LICENSE.md",
                        "plugins/scayt/README.md",
                        "plugins/scayt/dialogs/toolbar.css",
                        "plugins/scayt/dialogs/options.js",
                        "plugins/scayt/CHANGELOG.md",
                        "plugins/scayt/LICENSE.md",
                        "plugins/colordialog/dialogs/colordialog.js",
                        "plugins/clipboard/dialogs/paste.js",
                        "plugins/a11yhelp/dialogs/a11yhelp.js",
                        "plugins/a11yhelp/dialogs/lang/sk.js",
                        "plugins/a11yhelp/dialogs/lang/fi.js",
                        "plugins/a11yhelp/dialogs/lang/it.js",
                        "plugins/a11yhelp/dialogs/lang/he.js",
                        "plugins/a11yhelp/dialogs/lang/uk.js",
                        "plugins/a11yhelp/dialogs/lang/_translationstatus.txt",
                        "plugins/a11yhelp/dialogs/lang/sv.js",
                        "plugins/a11yhelp/dialogs/lang/sr-latn.js",
                        "plugins/a11yhelp/dialogs/lang/ru.js",
                        "plugins/a11yhelp/dialogs/lang/zh-cn.js",
                        "plugins/a11yhelp/dialogs/lang/no.js",
                        "plugins/a11yhelp/dialogs/lang/fr.js",
                        "plugins/a11yhelp/dialogs/lang/fa.js",
                        "plugins/a11yhelp/dialogs/lang/da.js",
                        "plugins/a11yhelp/dialogs/lang/mk.js",
                        "plugins/a11yhelp/dialogs/lang/ko.js",
                        "plugins/a11yhelp/dialogs/lang/ro.js",
                        "plugins/a11yhelp/dialogs/lang/mn.js",
                        "plugins/a11yhelp/dialogs/lang/tr.js",
                        "plugins/a11yhelp/dialogs/lang/bg.js",
                        "plugins/a11yhelp/dialogs/lang/de.js",
                        "plugins/a11yhelp/dialogs/lang/el.js",
                        "plugins/a11yhelp/dialogs/lang/pt.js",
                        "plugins/a11yhelp/dialogs/lang/af.js",
                        "plugins/a11yhelp/dialogs/lang/eu.js",
                        "plugins/a11yhelp/dialogs/lang/cy.js",
                        "plugins/a11yhelp/dialogs/lang/hi.js",
                        "plugins/a11yhelp/dialogs/lang/en.js",
                        "plugins/a11yhelp/dialogs/lang/fr-ca.js",
                        "plugins/a11yhelp/dialogs/lang/nb.js",
                        "plugins/a11yhelp/dialogs/lang/sr.js",
                        "plugins/a11yhelp/dialogs/lang/en-gb.js",
                        "plugins/a11yhelp/dialogs/lang/pl.js",
                        "plugins/a11yhelp/dialogs/lang/lv.js",
                        "plugins/a11yhelp/dialogs/lang/km.js",
                        "plugins/a11yhelp/dialogs/lang/tt.js",
                        "plugins/a11yhelp/dialogs/lang/th.js",
                        "plugins/a11yhelp/dialogs/lang/hu.js",
                        "plugins/a11yhelp/dialogs/lang/zh.js",
                        "plugins/a11yhelp/dialogs/lang/ja.js",
                        "plugins/a11yhelp/dialogs/lang/et.js",
                        "plugins/a11yhelp/dialogs/lang/nl.js",
                        "plugins/a11yhelp/dialogs/lang/ar.js",
                        "plugins/a11yhelp/dialogs/lang/eo.js",
                        "plugins/a11yhelp/dialogs/lang/lt.js",
                        "plugins/a11yhelp/dialogs/lang/gl.js",
                        "plugins/a11yhelp/dialogs/lang/ku.js",
                        "plugins/a11yhelp/dialogs/lang/cs.js",
                        "plugins/a11yhelp/dialogs/lang/vi.js",
                        "plugins/a11yhelp/dialogs/lang/ca.js",
                        "plugins/a11yhelp/dialogs/lang/ug.js",
                        "plugins/a11yhelp/dialogs/lang/fo.js",
                        "plugins/a11yhelp/dialogs/lang/id.js",
                        "plugins/a11yhelp/dialogs/lang/si.js",
                        "plugins/a11yhelp/dialogs/lang/sl.js",
                        "plugins/a11yhelp/dialogs/lang/pt-br.js",
                        "plugins/a11yhelp/dialogs/lang/es.js",
                        "plugins/a11yhelp/dialogs/lang/hr.js",
                        "plugins/a11yhelp/dialogs/lang/sq.js",
                        "plugins/a11yhelp/dialogs/lang/gu.js",
                        "contents.css"
                };


        String fullname = "";
        String type = "text/plain";
        File file;

        for (String filename : files) {
            if (filename.equals(folder)) {
                fullname = "web/libs/ckeditor/" + filename;

                switch (filename.substring(filename.lastIndexOf(".") + 1)) {
                    case "js":
                        type = "text/javascript";
                        break;
                    case "css":
                        type = "text/css";
                        break;
                    case "html":
                        type = "text/html";
                        break;
                    case "txt":
                    case "md":
                        type = "text/plain";
                        break;
                    case "png":
                        type = "image/png";
                        break;
                    case "gif":
                        type = "image/gif";
                        break;
                    case "svg":
                        type = "image/svg+xml";
                }
            }
        }

        file = new File(fullname);

        if (file.exists()) {
            return Response.ok(file, type).build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/translation.json")
    @GET
    public Response translationjson() {

        File file = new File("languages/" + Settings.getInstance().getLangFileName());

        if (file.exists()) {
            return Response.ok(file, "application/json").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/translation.js")
    @GET
    public Response translationjs() {
        File file = new File("web/libs/js/translation.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    @Path("index/libs/js/third-party/qrcode.min.js")
    @GET
    public Response qrcodejs() {
        File file = new File("web/libs/js/third-party/qrcode.min.js");

        if (file.exists()) {
            return Response.ok(file, "text/javascript").build();
        } else {
            return error404(request, null);
        }
    }

    public Response error404(HttpServletRequest request, String titleOpt) {

        try {
            PebbleHelper pebbleHelper = PebbleHelper.getPebbleHelper("web/404.html", request);

            pebbleHelper.getContextMap().put(
                    "title",
                    titleOpt == null ? "Sorry, that page does not exist!"
                            : titleOpt);

            return Response.status(404)
                    .header("Content-Type", "text/html; charset=utf-8")
                    .entity(pebbleHelper.evaluate()).build();
        } catch (PebbleException e) {
            logger.error(e.getMessage(), e);
            return Response.status(404).build();
        }
    }

    public String miniIndex() {
        try {
            return readFile("web/main.mini.html", StandardCharsets.UTF_8);

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return "ERROR";
        }
    }

    @Path("/index/{html}")
    @GET
    public Response getHtml(@PathParam("html") String html) {
        return error404(request, null);
    }

    @GET
    @Path("robots.txt")
    public Response robotsTxt() {
        File file = new File("web/blockexplorer/robots.txt");
        try {
            BufferedInputStream is = new BufferedInputStream(new FileInputStream(file));
            String type = URLConnection.guessContentTypeFromStream(is);
            return Response.ok(file, type).build();
        } catch (Exception e) {
            return Response.status(200).entity("index.html").build();
        }
    }

}
