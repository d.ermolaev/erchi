package org.erachain.utils;

import com.google.common.primitives.Bytes;
import lombok.SneakyThrows;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

public class BigDecimalUtil {

    static public boolean isValid(BigDecimal amount) {

        if (amount == null)
            return false;

        BigDecimal amountBase = amount.stripTrailingZeros();
        byte[] amountBytes = amountBase.unscaledValue().toByteArray();
        if (amountBytes.length >= Transaction.AMOUNT_LENGTH) {
            return false;
        }

        return true;

    }

    @SneakyThrows
    static public byte[] toBytes(BigDecimal amount) {

        // CALCULATE ACCURACY of AMOUNT
        BigDecimal amountBase = amount.stripTrailingZeros();

        // WRITE AMOUNT
        byte[] amountBytes = amountBase.unscaledValue().toByteArray();
        if (amountBytes.length >= Transaction.AMOUNT_LENGTH) {
            throw new TxException("AMOUNT_LENGTH_MAX: " + amountBytes.length);
        }

        byte[] buff = new byte[Transaction.AMOUNT_LENGTH];
        buff[0] = (byte) amountBase.scale();
        if (amount.signum() < 0) {
            Arrays.fill(buff, 1, Transaction.AMOUNT_LENGTH - amountBytes.length, (byte)-1);
        }
        System.arraycopy(amountBytes, 0, buff, Transaction.AMOUNT_LENGTH - amountBytes.length, amountBytes.length);
        return buff;

    }

    @SneakyThrows
    static public byte[] toBytes(byte[] data, BigDecimal amount) {

        // CALCULATE ACCURACY of AMOUNT
        BigDecimal amountBase = amount.stripTrailingZeros();

        // WRITE AMOUNT
        byte[] amountBytes = amountBase.unscaledValue().toByteArray();
        if (amountBytes.length >= Transaction.AMOUNT_LENGTH) {
            throw new TxException("AMOUNT_LENGTH_MAX: " + amountBytes.length);
        }

        byte[] buff = new byte[Transaction.AMOUNT_LENGTH];
        buff[0] = (byte) amountBase.scale();
        if (amount.signum() < 0) {
            Arrays.fill(buff, 1, Transaction.AMOUNT_LENGTH - amountBytes.length, (byte)-1);
        }
        System.arraycopy(amountBytes, 0, buff, Transaction.AMOUNT_LENGTH - amountBytes.length, amountBytes.length);
        return Bytes.concat(data, buff);

    }

    /**
     * Transaction.AMOUNT_LENGTH_MAX
     *
     * @param data
     * @param pos
     * @param amount
     */
    @SneakyThrows
    static public void toBytes(byte[] data, int pos, BigDecimal amount) {

        if (amount == null)
            return;

        // CALCULATE ACCURACY of AMOUNT
        BigDecimal amountBase = amount.stripTrailingZeros();
        data[pos++] = (byte) amountBase.scale();

        // WRITE AMOUNT
        byte[] amountBytes = amountBase.unscaledValue().toByteArray();
        if (amountBytes.length >= Transaction.AMOUNT_LENGTH) {
            throw new TxException("AMOUNT_LENGTH_MAX: " + amountBytes.length);
        }

        if (amount.signum() < 0) {
            Arrays.fill(data, pos, pos + Transaction.AMOUNT_LENGTH - 1 - amountBytes.length, (byte)-1);
        }

        System.arraycopy(amountBytes, 0, data,
                pos + Transaction.AMOUNT_LENGTH - 1 - amountBytes.length, amountBytes.length);

    }

    static public BigDecimal fromBytes(byte[] data, int pos) {

        //READ SCALE
        byte scale = data[pos++];

        // READ AMOUNT
        byte[] amountBytes = Arrays.copyOfRange(data, pos, pos + Transaction.AMOUNT_LENGTH - 1);
        return new BigDecimal(new BigInteger(amountBytes), scale);

    }

    static public int BytesLen() {
        return Transaction.AMOUNT_LENGTH;
    }

}
