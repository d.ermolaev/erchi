package org.erachain.utils;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.error.PebbleException;
import com.mitchellbosecke.pebble.extension.escaper.EscaperExtension;
import com.mitchellbosecke.pebble.template.PebbleTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class PebbleHelper {


    private PebbleTemplate template;
    private Map<String, Object> contextMap;

    private PebbleHelper(PebbleTemplate template, Map<String, Object> contextMap) {
        this.contextMap = contextMap;
        this.template = template;

    }

    private static PebbleTemplate getRawPebbleTemplate(String htmlTemplate) throws PebbleException {
        PebbleEngine engine = new PebbleEngine();
        EscaperExtension escaper = engine.getExtension(EscaperExtension.class);
        escaper.setAutoEscaping(false);
        PebbleTemplate compiledTemplate = engine.getTemplate(htmlTemplate);
        return compiledTemplate;
    }

    private static PebbleHelper getRawPebbleHelper(String htmlTemplate) throws PebbleException {
        PebbleTemplate compiledTemplate = getRawPebbleTemplate(htmlTemplate);
        PebbleHelper pebbleHelper = new PebbleHelper(compiledTemplate, new HashMap<String, Object>());
        return pebbleHelper;
    }

    public static PebbleHelper getPebbleHelper(String htmlTemplate, HttpServletRequest requestOpt) throws PebbleException {
        PebbleHelper pebbleHelper = getRawPebbleHelper(htmlTemplate);
        return pebbleHelper;
    }

    public String evaluate() throws PebbleException {
        try (Writer writer = new StringWriter();) {
            template.evaluate(writer, contextMap);
            return writer.toString();
        } catch (IOException e) {
            throw new PebbleException(e, e.getMessage());
        }
    }

    public Map<String, Object> getContextMap() {
        return contextMap;
    }

}
