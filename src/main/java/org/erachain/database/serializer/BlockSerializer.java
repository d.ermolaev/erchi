package org.erachain.database.serializer;

import org.erachain.core.BlockChain;
import org.erachain.core.block.Block;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TransactionFactory;
import org.mapdb.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;

public class BlockSerializer implements Serializer<Block>, Serializable {
    private static final long serialVersionUID = -6538913048331349777L;
    static Logger LOGGER = LoggerFactory.getLogger(BlockSerializer.class.getName());

    @Override
    public void serialize(DataOutput out, Block value) throws IOException {

        if (BlockChain.CHECK_BUGS > 5 &&
                value.getDataLength() != value.toBytes().length) {
            throw new RuntimeException("WWWWWWWWWWWWW\n" + value
                    + "\n getDataLength: " + value.getDataLength() + " <> " + value.toBytes().length);
        }

        out.writeInt(value.getDataLength());
        out.write(value.toBytes());
    }

    @Override
    public Block deserialize(DataInput in, int available) throws IOException {
        int length = in.readInt();

        byte[] bytes = new byte[length];
        in.readFully(bytes);
        try {
            Block value = Block.parseFromDB(bytes);
            if (value.getDataLength() != bytes.length) {
                throw new RuntimeException("RRRRRRRRRRRRR\n" + value
                        + "\n getDataLength: " + value.getDataLength() + " <> " + value.toBytes().length);
            }
            return value;

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public int fixedSize() {
        return -1;
    }
}
