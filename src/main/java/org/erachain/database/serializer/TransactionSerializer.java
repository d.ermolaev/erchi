package org.erachain.database.serializer;
// upd 09/03

import org.erachain.core.BlockChain;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TransactionFactory;
import org.mapdb.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;

public class TransactionSerializer implements Serializer<Transaction>, Serializable {
    static Logger LOGGER = LoggerFactory.getLogger(TransactionSerializer.class.getName());

    @Override
    public void serialize(DataOutput out, Transaction value) throws IOException {

        if (BlockChain.CHECK_BUGS > 5 &&
                value.getDataLength(Transaction.FOR_DB_RECORD) != value.toBytes(Transaction.FOR_DB_RECORD).length) {
            throw new RuntimeException("WWWWWWWWWWWWW\n" + value
                    + "\n getDataLength: " + value.getDataLength(Transaction.FOR_DB_RECORD) + " <> " + value.toBytes(Transaction.FOR_DB_RECORD).length);
        }
        out.writeInt(value.getDataLength(Transaction.FOR_DB_RECORD));
        out.write(value.toBytes(Transaction.FOR_DB_RECORD));
    }

    @Override
    public Transaction deserialize(DataInput in, int available) throws IOException {
        int length = in.readInt();
        byte[] bytes = new byte[length];
        in.readFully(bytes);
        try {
            Transaction value = TransactionFactory.getInstance().parse(bytes, Transaction.FOR_DB_RECORD);
            if (value.getDataLength(Transaction.FOR_DB_RECORD) != bytes.length) {
                throw new RuntimeException("RRRRRRRRRRRRR\n" + value
                        + "\n getDataLength: " + value.getDataLength(Transaction.FOR_DB_RECORD) + " <> " + value.toBytes(Transaction.FOR_DB_RECORD).length);
            }
            return value;

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public int fixedSize() {
        return -1;
    }
}
