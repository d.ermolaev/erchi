package org.erachain

import com.google.common.base.Charsets
import com.google.common.io.Files
import org.erachain.controller.Controller
import org.erachain.core.account.Account
import org.erachain.settings.Settings
import org.erachain.utils.FileUtils
import org.json.simple.JSONArray
import org.slf4j.LoggerFactory
import java.io.File
import java.math.BigDecimal
import java.util.*

object Start {
    private val LOGGER = LoggerFactory.getLogger(Start::class.java)

    @Throws(Exception::class)
    @JvmStatic
    fun main(args1: Array<String>) {
        var args = args1
        println("Erchi " + Controller.getBuildDateTimeString())
        println("use option -?, -h, -help    - for see help")

        //SpringApplicationBuilder builder = new SpringApplicationBuilder(Start.class);
        if (args.size == 1 && (args[0] == "-?" || args[0] == "-h" || args[0] == "-help")) {
            val file = File("z_START_EXAMPLES", "readme-commands.txt")
            val lines = Files.readLines(file, Charsets.UTF_8)
            for (line in lines) {
                println(line)
            }
            return
        }

        //builder.headless(false).run(args);
        var file = File("startARGS.txt")
        if (file.exists()) {
            LOGGER.info("startARGS.txt USED")
            try {
                val lines = Files.readLines(file, Charsets.UTF_8)
                var parsString = ""
                for (line1 in lines) {
                    var line = line1
                    if (line.trim { it <= ' ' }.also {line = it}.startsWith("//") || line.isEmpty()) {
                        // пропускаем //
                        continue
                    }
                    parsString += " -" + line.trim { it <= ' ' } // add -
                }

                val pars =
                    parsString.trim { it <= ' ' }.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                var argsNew = arrayOf<String>()
                // PARS has LOW priority
                for (par in pars) {
                    argsNew += par
                }
                // ARGS has HIGH priority и затрут потом в версинге значения те? что были в файле заданы
                for (arg in args) {
                    argsNew += arg
                }
                //argsNew = pars + pars
                args = argsNew
            } catch (e: Exception) {
                LOGGER.info("Error while reading " + file.absolutePath)
                LOGGER.error(e.message, e)
                System.exit(3)
            }
        }
        var genesisStamp: Long
        for (arg in args) {
            if (arg == "-testnet") {
                genesisStamp = -1
                Settings.simpleTestNet = true
                Settings.genesisStamp = genesisStamp
                Settings.NET_MODE = Settings.NET_MODE_TEST
            } else if (arg.startsWith("-testnet=") && arg.length > 9) {
                try {
                    genesisStamp = arg.substring(9).toLong()
                    Settings.NET_MODE = Settings.NET_MODE_TEST
                } catch (e: Exception) {
                    genesisStamp = Settings.DEFAULT_DEMO_NET_STAMP
                    Settings.NET_MODE = Settings.NET_MODE_DEMO
                }
                Settings.genesisStamp = genesisStamp
            } else if (arg.startsWith("-testdb=") && arg.length > 8) {
                try {
                    Settings.TEST_DB_MODE = arg.substring(8).toInt()
                } catch (e: Exception) {
                }
            } else if (arg.startsWith("-bugs=") && arg.length > 6) {
                try {
                    val value = arg.substring(6)
                    Settings.CHECK_BUGS = value.toInt()
                    LOGGER.info("-bugs = " + Settings.CHECK_BUGS)
                } catch (e: Exception) {
                }
            }
        }

        ///////////////////  CLONECHAINS ///////////
        file = File(Settings.CLONE_OR_SIDE.lowercase(Locale.getDefault()) + "GENESIS.json")
        if (Settings.NET_MODE == Settings.NET_MODE_MAIN && Settings.TEST_DB_MODE == 0 && file.exists()) {

            // START SIDE CHAIN
            LOGGER.info(Settings.CLONE_OR_SIDE.lowercase(Locale.getDefault()) + "GENESIS.json USED")
            Settings.genesisJSON = FileUtils.readCommentedJSONArray(file.path)
            if (Settings.genesisJSON == null) {
                LOGGER.error("Wrong JSON or not UTF-8 encode in " + file.name)
                throw Exception("Wrong JSON or not UTF-8 encode in " + file.name)
            }
            val appArray = Settings.genesisJSON[0] as JSONArray
            Settings.APP_NAME = appArray[0].toString()
            Settings.APP_FULL_NAME = appArray[1].toString()
            val timeArray = Settings.genesisJSON[1] as JSONArray
            Settings.genesisStamp = timeArray[0] as Long
            try {
                // если там пустой список то включаем "у всех все есть"
                val holders = Settings.genesisJSON[2] as JSONArray
                if (holders.isEmpty()) {
                    Settings.ERA_COMPU_ALL_UP = true
                } else {
                    // CHECK VALID
                    for (i in holders.indices) {
                        val holder = holders[i] as JSONArray
                        // SEND FONDs
                        var accountItem = Account.tryMakeAccount(holder[0].toString())
                        if (accountItem.a == null) {
                            val error = accountItem.b + " - " + holder[0].toString()
                            LOGGER.error(error)
                            System.exit(4)
                        }
                        if (holder.size > 3) {
                            // DEBTORS
                            val debtors = holder[3] as JSONArray
                            for (j in debtors.indices) {
                                val debtor = debtors[j] as JSONArray
                                accountItem = Account.tryMakeAccount(debtor[1].toString())
                                if (accountItem.a == null) {
                                    val error = accountItem.b + " - " + debtor[1].toString()
                                    LOGGER.error(error)
                                    System.exit(4)
                                }
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                LOGGER.info("Error while parse JSON " + file.absolutePath + " - " + e.message)
                LOGGER.error(e.message, e)
                System.exit(3)
            }
            Settings.NET_MODE = Settings.NET_MODE_CLONE
        }
        Settings.getInstance()
        Controller.getInstance().startApplication(args)
    }
}