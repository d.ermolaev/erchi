package org.erachain.datachain;

import org.erachain.dbs.DBTab;
import org.erachain.dbs.nativeMemMap.NativeMapTreeMapFork;
import org.erachain.utils.ObserverMessage;
import org.mapdb.*;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import java.util.*;

/**
 * Храним выбор голосующего по Сущности Голосования
 * POLL KEY + OPTION KEY + ACCOUNT SHORT = result Transaction reference (BlockNo + SeqNo)
 * byte[] - un CORAMPABLE
 *
 * Ключ: Номер Голосования + Номер выбора + Счет Короткий
 * Значение: СТЭК ссылок на транзакцию голосования
 *
 */
public class VoteOnItemPollMap extends DCUMap<Tuple3<Long, Integer, byte[]>, Stack<Tuple2<Integer, Integer>>> {

    private static Comparator COMP_1 = new Fun.Tuple3Comparator(Fun.COMPARATOR, Fun.COMPARATOR, Fun.BYTE_ARRAY_COMPARATOR);

    public VoteOnItemPollMap(DCSet databaseSet, DB database) {
        super(databaseSet, database);

        if (databaseSet.isWithObserver()) {
            this.observableData.put(DBTab.NOTIFY_RESET, ObserverMessage.RESET_VOTEPOLL_TYPE);
            this.observableData.put(DBTab.NOTIFY_LIST, ObserverMessage.LIST_VOTEPOLL_TYPE);
            this.observableData.put(DBTab.NOTIFY_ADD, ObserverMessage.ADD_VOTEPOLL_TYPE);
            this.observableData.put(DBTab.NOTIFY_REMOVE, ObserverMessage.REMOVE_VOTEPOLL_TYPE);
        }

    }

    public VoteOnItemPollMap(VoteOnItemPollMap parent, DCSet dcSet) {
        super(parent, dcSet);
    }

    @Override
    public void openMap() {
        //OPEN MAP
        map = database.createTreeMap("vote_item_poll")
                .comparator(COMP_1)
                .makeOrGet();
    }

    @Override
    protected void getMemoryMap() {
        map = new TreeMap<Tuple3<Long, Integer, byte[]>, Stack<Tuple2<Integer, Integer>>>(COMP_1);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public NavigableSet<Tuple3> getVotes(Long pollKey) {
        BTreeMap map = (BTreeMap) this.map;
        //FILTER ALL KEYS
        NavigableSet<Tuple3> keys = ((BTreeMap<Tuple3, Tuple2>) map).subMap(
                Fun.t3(pollKey, null, null),
                Fun.t3(pollKey, Integer.MAX_VALUE, Fun.HI()))
                .keySet();


        //RETURN
        return keys;
    }

    public NavigableSet<Tuple3<Long, Integer, byte[]>> getVotes_1(Long pollKey) {
        @SuppressWarnings("rawtypes")
        BTreeMap map = (BTreeMap) this.map;
        Set ss = new TreeSet();
        @SuppressWarnings("unchecked")
        NavigableSet<Tuple3<Long, Integer, byte[]>> ks = map.keySet();
        Iterator<Tuple3<Long, Integer, byte[]>> it = ks.iterator();
        while (it.hasNext()) {
            Tuple3<Long, Integer, byte[]> a = it.next();
            if (a.a != pollKey) continue;
            ss.add(a);
        }
        return (NavigableSet<Tuple3<Long, Integer, byte[]>>) ss;

    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public NavigableSet<Tuple3> getVotes(Long pollKey, Integer option) {
        BTreeMap map = (BTreeMap) this.map;

        //FILTER ALL KEYS
        NavigableSet<Tuple3> keys = ((BTreeMap<Tuple3, Tuple2>) map).subMap(
                Fun.t3(pollKey, option, null),
                Fun.t3(pollKey, option, Fun.HI())).keySet();

        //RETURN
        return keys;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public boolean hasVotes(Long pollKey) {
        BTreeMap map = (BTreeMap) this.map;

        //FILTER ALL KEYS
        Tuple3<Long, Integer, byte[]> key = ((BTreeMap<Tuple3, Tuple2>) map).subMap(
                Fun.t3(pollKey, null, null),
                Fun.t3(pollKey, Integer.MAX_VALUE, Fun.HI())).firstKey();

        //RETURN
        return key != null;
    }

    public long countVotes(Long pollKey) {
        BTreeMap map = (BTreeMap) this.map;

        //FILTER ALL KEYS
        return ((BTreeMap<Tuple3, Tuple2>) map).subMap(
                Fun.t3(pollKey, null, null),
                Fun.t3(pollKey, Integer.MAX_VALUE, Fun.HI())).size();

    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public boolean hasVotes(Long pollKey, Integer option) {
        BTreeMap map = (BTreeMap) this.map;

        //FILTER ALL KEYS
        Tuple3 key = ((BTreeMap<Tuple3, Tuple2>) map).subMap(
                Fun.t3(pollKey, option, null),
                Fun.t3(pollKey, option, Fun.HI())).firstKey();

        //RETURN
        return key != null;
    }

    public Stack<Tuple2<Integer, Integer>> get(long pollKey, int optionKey, byte[] accountShort) {
        return this.get(new Tuple3<Long, Integer, byte[]>(pollKey, optionKey, accountShort));
    }

    @SuppressWarnings("unchecked")
    public void addItem(long pollKey, int optionKey, byte[] accountShort, Tuple2<Integer, Integer> value) {
        Tuple3<Long, Integer, byte[]> key = new Tuple3<Long, Integer, byte[]>(pollKey, optionKey, accountShort);
        Stack<Tuple2<Integer, Integer>> stack = this.get(key);
        if (stack == null)
            stack = new Stack<Tuple2<Integer, Integer>>();

        Stack<Tuple2<Integer, Integer>> new_stack;

        if (false // походу если КЭШ используется там будет такая же ошибка и поэтому надо всегда делать новый объект
                // иначе новое ззначение может передать свои значения в другую обработку после форка базы
                && this.parent == null)
            new_stack = stack;
        else {
            // !!!! NEEED .clone() !!!
            // need for updates only in fork - not in parent DB
            new_stack = (Stack<Tuple2<Integer, Integer>>) stack.clone();
        }

        new_stack.push(value);
        this.put(key, stack);
    }

    public Tuple2<Integer, Integer> getItem(long pollKey, int optionKey, byte[] accountShort) {
        Tuple3<Long, Integer, byte[]> key = new Tuple3<Long, Integer, byte[]>(pollKey, optionKey, accountShort);
        Stack<Tuple2<Integer, Integer>> stack = this.get(key);
        return stack == null || stack.isEmpty() ? null : stack.peek();
    }

    @SuppressWarnings("unchecked")
    public Tuple2<Integer, Integer> removeItem(long pollKey, int optionKey, byte[] accountShort) {
        Tuple3<Long, Integer, byte[]> key = new Tuple3<Long, Integer, byte[]>(pollKey, optionKey, accountShort);
        Stack<Tuple2<Integer, Integer>> stack = this.get(key);
        if (stack == null || stack.isEmpty())
            return null;

        Stack<Tuple2<Integer, Integer>> new_stack;
        if (false // походу если КЭШ используется там будет такая же ошибка и поэтому надо всегда делать новый объект
                // иначе новое ззначение может передать свои значения в другую обработку после форка базы
                && this.parent == null)
            new_stack = stack;
        else {
            // !!!! NEEED .clone() !!!
            // need for updates only in fork - not in parent DB
            new_stack = (Stack<Tuple2<Integer, Integer>>) stack.clone();
        }

        Tuple2<Integer, Integer> itemRemoved = new_stack.pop();

        this.put(key, new_stack);

        return itemRemoved;

    }
}
