package org.erachain.datachain;

import org.apache.commons.lang3.ArrayUtils;
import org.erachain.controller.Controller;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.persons.PersonHuman;
import org.erachain.core.transaction.Transaction;
import org.erachain.database.wallet.DWSet;
import org.erachain.utils.ObserverMessage;
import org.mapdb.BTreeMap;
import org.mapdb.Bind;
import org.mapdb.DB;
import org.mapdb.Fun;

import java.util.HashSet;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Set;

/**
 * Хранение активов.<br>
 * Ключ: номер (автоинкремент)<br>
 * Значение: Персона<br>
 */

public class ItemPersonMap extends ItemMap {

    /**
     * not bind !
     */
    protected Set<String> nameUniqueKeySet;

    public ItemPersonMap(DCSet databaseSet, DB database) {
        super(databaseSet, database,
                ItemCls.PERSON_TYPE, ObserverMessage.RESET_PERSON_TYPE,
                ObserverMessage.ADD_PERSON_TYPE,
                ObserverMessage.REMOVE_PERSON_TYPE,
                ObserverMessage.LIST_PERSON_TYPE
        );
    }

    public ItemPersonMap(ItemMap parent, DCSet dcSet) {
        super(parent, dcSet);
    }

    @Override
    protected void makeOtherKeys(DB database) {

        super.makeOtherKeys(database);

        this.nameUniqueKeySet = database.createHashSet("name_unique_persons_key")
                .makeOrGet();

    }

}
