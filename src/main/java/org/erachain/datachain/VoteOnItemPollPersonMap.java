package org.erachain.datachain;

import org.erachain.dbs.DBTab;
import org.erachain.utils.ObserverMessage;
import org.mapdb.BTreeKeySerializer;
import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import java.util.*;

/**
 * Храним выбор голосующего по Сущности Голосования
 * POLL KEY + PERSON KEY = OPTION + Transaction reference (BlockNo + SeqNo)
 * <p>
 * Ключ: Номер Голосования + номер персоны
 * Значение: Номер выбора + ссылка на транзакцию голосования
 */
public class VoteOnItemPollPersonMap extends DCUMap<Tuple2<Long, Long>, Tuple2<Integer, Long>> {

    public VoteOnItemPollPersonMap(DCSet databaseSet, DB database) {
        super(databaseSet, database);

        if (databaseSet.isWithObserver()) {
            this.observableData.put(DBTab.NOTIFY_RESET, ObserverMessage.RESET_VOTEPOLL_PERS_TYPE);
            this.observableData.put(DBTab.NOTIFY_LIST, ObserverMessage.LIST_VOTEPOLL_PERS_TYPE);
            this.observableData.put(DBTab.NOTIFY_ADD, ObserverMessage.ADD_VOTEPOLL_PERS_TYPE);
            this.observableData.put(DBTab.NOTIFY_REMOVE, ObserverMessage.REMOVE_VOTEPOLL_PERS_TYPE);
        }

    }

    public VoteOnItemPollPersonMap(VoteOnItemPollPersonMap parent, DCSet dcSet) {
        super(parent, dcSet);
    }

    @Override
    public void openMap() {
        //OPEN MAP
        map = database.createTreeMap("vote_item_poll_pers")
                .keySerializer(BTreeKeySerializer.TUPLE2)
                .makeOrGet();
    }

    @Override
    protected void getMemoryMap() {
        map = new TreeMap<Tuple2<Long, Long>, Tuple2<Integer, Long>>();
    }

}
