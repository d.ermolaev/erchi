package org.erachain.datachain;

import org.erachain.dbs.DBTab;
import org.erachain.utils.ObserverMessage;
import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.Fun.Tuple2;

import java.util.TreeMap;

/**
 * Храним суммарные значения
 * POLL KEY = Список Сумм
 * <p>
 * Ключ: Номер Голосования
 * Значение: Список сумм (как объект) - может быть Long И BigInteger
 */
public class VoteOnItemPollResultsMap extends DCUMap<Long, Object[]> {

    public VoteOnItemPollResultsMap(DCSet databaseSet, DB database) {
        super(databaseSet, database);
    }

    public VoteOnItemPollResultsMap(VoteOnItemPollResultsMap parent, DCSet dcSet) {
        super(parent, dcSet);
    }

    @Override
    public void openMap() {
        //OPEN MAP
        map = database.createTreeMap("vote_item_poll_result")
                .keySerializer(BTreeKeySerializer.BASIC)
                .makeOrGet();
    }

    @Override
    protected void getMemoryMap() {
        map = new TreeMap<Long, Object[]>();
    }

}
