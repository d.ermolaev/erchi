package org.erachain.datachain;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import java.util.HashMap;
import java.util.Iterator;

//
// last forged block for ADDRESS -> by height = 0

/**
 * Хранит данные для организации правильного отката - предыдущее значение (состояние) которое надо восстановить
 * TX.dbRef (as seqNo) -> current point: previous values State (Object[])
 *
 * @return
 */
public class OrphanValuesMap extends DCUMap<Long, Object[]> {


    public OrphanValuesMap(DCSet databaseSet, DB database) {
        super(databaseSet, database);
    }

    public OrphanValuesMap(OrphanValuesMap parent, DCSet dcSet) {
        super(parent, dcSet);
    }

    @Override
    public void openMap() {
        //OPEN MAP
        map = database.getHashMap("orphan_vals_map");
    }

    @Override
    protected void getMemoryMap() {
        map = new HashMap<Long, Object[]>();
    }

}
