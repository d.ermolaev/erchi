package org.erachain.api;

import com.google.common.primitives.Ints;
import org.erachain.controller.Controller;
import org.erachain.core.wallet.Wallet;
import org.erachain.utils.APIUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;

@Path("wallet")
@Produces(MediaType.APPLICATION_JSON)
public class WalletResource {


    private static final Logger LOGGER = LoggerFactory.getLogger(WalletResource.class);

    @Context
    HttpServletRequest request;

    @SuppressWarnings("unchecked")
    @GET
    public String getWallet() {
        String password = null;
        APIUtils.askAPICallAllowed(password, "GET wallet", request, true);

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("exists", Controller.getInstance().doesWalletKeysExists());
        jsonObject.put("isunlocked", Controller.getInstance().isWalletUnlocked());

        return jsonObject.toJSONString();
    }

    @GET
    @Path("/seed")
    public String getSeed() {

        String password = null;
        APIUtils.askAPICallAllowed(password, "GET wallet/seed", request, true);

        //CHECK IF WALLET EXISTS
        if (!Controller.getInstance().doesWalletKeysExists()) {
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_NO_EXISTS);
        }

        //CHECK WALLET UNLOCKED
        if (!Controller.getInstance().isWalletUnlocked()) {
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_LOCKED);
        }

        return Controller.getInstance().exportSeed();
    }

    @GET
    @Path("/synchronize")
    public String synchronize() {
        String password = null;
        APIUtils.askAPICallAllowed(password, "GET wallet/synchronize", request, true);

        //CHECK IF WALLET EXISTSашч
        if (!Controller.getInstance().doesWalletKeysExists()) {
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_NO_EXISTS);
        }

        if (!Controller.getInstance().getWallet().synchronizeBodyUsed) {

            // TODO: was
            //Controller.getInstance().synchronizeWallet();
            Thread thread = new Thread(() -> {
                Controller.getInstance().getWallet().synchronizeFull();
            });
            thread.start();

            return String.valueOf(true);
        } else {
            return String.valueOf(false);
        }
    }

    @GET
    @Path("/lock")
    public String lock() {
        String password = null;
        //APIUtils.askAPICallAllowed(password, "GET wallet/lock", request);

        //CHECK IF WALLET EXISTS
        if (!Controller.getInstance().doesWalletKeysExists()) {
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_NO_EXISTS);
        }

        return String.valueOf(Controller.getInstance().lockWallet());
    }

    /**
     * POST wallet {"seed":"6iqp9e", "password":"1",  "amount":5, "dir":<wallet dir>}
     *
     * @param x
     * @return
     */
    @POST
    @Consumes(MediaType.WILDCARD)
    public String createWallet(String x) {
        try {
            //READ JSON
            JSONObject jsonObject = (JSONObject) JSONValue.parse(x);

            APIUtils.askAPICallAllowed(null, "POST wallet " + x, request, true);

            String password = (String) jsonObject.get("password");
            String seed = (String) jsonObject.get("seed");
            int amount = ((Long) jsonObject.getOrDefault("amount", 1L)).intValue();
            String path = (String) jsonObject.get("dir");

            //CHECK IF WALLET EXISTS
            if (Controller.getInstance().doesWalletKeysExists()) {
                throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_ALREADY_EXISTS);
            }

            //DECODE SEED
            byte[] seedBytes;
            try {
                seedBytes = Wallet.decodeSeed(seed);
            } catch (Exception e) {
                throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_INVALID_SEED);
            }

            //CHECK SEED LENGTH
            if (seedBytes.length != 32) {
                throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_INVALID_SEED);
            }

            //CHECK AMOUNT
            if (amount < 1) {
                throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_INVALID_AMOUNT);
            }

            //CREATE WALLET
            return String.valueOf(Controller.getInstance().recoverWallet(seedBytes, password, amount, path));

        } catch (NullPointerException | ClassCastException e) {
            LOGGER.error(e.getMessage());
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_JSON);
        }
    }

    /**
     * curl http://127.0.0.1:9028/wallet/makeaccountbynonce -D "name" -X post
     *
     * @param x
     * @return
     */
    @POST
    @Path("/makeaccountbynonce")
    @Consumes(MediaType.WILDCARD)
    public String makeAccountByNonce(String x) {

        APIUtils.askAPICallAllowed(null, "POST wallet/makeaccountbynonce " + x, request, false);

        //CHECK IF WALLET EXISTS
        if (!Controller.getInstance().doesWalletKeysExists()) {
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_NO_EXISTS);
        }

        //CHECK WALLET UNLOCKED
        if (!Controller.getInstance().isWalletUnlocked()) {
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_LOCKED);
        }

        byte[] nonce;
        try {
            int i = Integer.parseInt(x);
            nonce = Ints.toByteArray(i);
        } catch (Exception e) {
            nonce = x.getBytes(StandardCharsets.UTF_8);
        }

        String address = Controller.getInstance().getWallet().generateNewAccount(nonce);
        if (address == null)
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_LOCKED);

        return address;

    }

    /**
     * curl http://127.0.0.1:9028/wallet/unlock -d "1" -X post
     *
     * @param x
     * @return
     */
    @POST
    @Path("/unlock")
    @Consumes(MediaType.WILDCARD)
    public String unlock(String x) {

        APIUtils.askAPICallAllowed(x, "POST wallet/unlock " + x, request, false);

        //CHECK IF WALLET EXISTS
        if (!Controller.getInstance().doesWalletKeysExists()) {
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_NO_EXISTS);
        }

        return String.valueOf(Controller.getInstance().isWalletUnlocked());
    }

}
