package org.erachain.api;

import com.google.common.primitives.Bytes;
import org.erachain.controller.Controller;
import org.erachain.core.crypto.Base58;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TransactionFactory;
import org.erachain.gui.transaction.OnDealClick;
import org.erachain.utils.APIUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mapdb.Fun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.util.Arrays;

@Deprecated
@Path("record")
@Produces(MediaType.APPLICATION_JSON)
public class RecResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecResource.class);
    @Context
    HttpServletRequest request;
    @Context
    private UriInfo uriInfo;


    @GET
    public String getRecord() {
        String text = "";
        JSONArray help = new JSONArray();
        JSONObject item;

        text = "SEND_ASSET_TRANSACTION: ";
        text += "type: " + Transaction.SEND_ASSET_TRANSACTION;
        text += ", recipient: Account in Base58";
        text += ", key: AssetKey - long";
        text += ", amount: BigDecimal";
        text += ", head: String UTF-8";
        text += ", data: String in Base58";
        text += ", isText: =0 - not as a TEXT";
        text += ", encryptMessage: =0 - not encrypt";
        item = new JSONObject();
        item.put(Transaction.SEND_ASSET_TRANSACTION, text);


        help.add(item);

        return help.toJSONString();
    }

    // short data without Signature
    @GET
    @Path("/parsetest/")
    // for test raw without Signature
    // http://127.0.0.1:9068/record/parsetest?data=3RKre8zCEarLNq4CQ6njRmvjGURz7KFWhec3H9H3tebEeKQEGDTsvAFizKnFpJAGDAoRQCKH9pygBQsrWfbxwgfcuEAKbARh5p6Yk2ZvfJDReFzBJbUSUwUgtxsKm2ZXHR
    //
    public String parseShort() // throws JSONException
    {

        if (uriInfo == null) {
            return APIUtils.errorMess(ApiErrorFactory.ERROR_JSON, ApiErrorFactory.getInstance().createError(
                    ApiErrorFactory.ERROR_JSON).toString());
        }

        // see http://ru.tmsoftstudio.com/file/page/web-services-java/javax_ws_rs_core.html
        MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();

        //CREATE TRANSACTION FROM RAW
        Transaction transaction;
        try {
            if (!queryParameters.containsKey("data"))
                return APIUtils.errorMess(-1, "Parameter [data] not found");

            String dataStr = queryParameters.get("data").get(0);
            byte[] data = Base58.decode(dataStr);
            int cut = 53;
            byte[] dataTail = Arrays.copyOfRange(data, cut, data.length);
            data = Bytes.concat(Arrays.copyOfRange(data, 0, cut), new byte[64], dataTail);
            transaction = TransactionFactory.getInstance().parse(data, Transaction.FOR_NETWORK);
            JSONObject json = transaction.toJson();
            json.put("raw", Base58.encode(data));
            return json.toJSONString();
        } catch (Exception e) {
            return APIUtils.errorMess(-1, e.toString());
        }

    }

    @GET
    @Path("/parse/")
    // http://127.0.0.1:9068/record/parse?data=DPDnFCNvPk4kLMQcyEp8wTmzT53vcFpVPVhBA8VuHDH6ekAWJAEgZvtjtKGcXwsAKyNs5k2aCpziAmqEDjTigbnDjMeXRfbUDUJNmEJHwB2uPdboSszwsy3fckANUgPV8Ep9CN1fdTdq3QfYE7bbpeYWS2rsTNHb3a7nEV6jg2XJguavqhNSzVeyM6UrRtbiVciMvHFayUAMrE4L3CPjZjPEf
    //
    public String parse() // throws JSONException
    {

        if (uriInfo == null) {
            return APIUtils.errorMess(ApiErrorFactory.ERROR_JSON, ApiErrorFactory.getInstance().createError(
                    ApiErrorFactory.ERROR_JSON).toString());
        }

        // see http://ru.tmsoftstudio.com/file/page/web-services-java/javax_ws_rs_core.html
        MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();

        //CREATE TRANSACTION FROM RAW
        Transaction transaction;
        try {
            if (!queryParameters.containsKey("data"))
                return APIUtils.errorMess(-1, "Parameter [data] not found");

            String dataStr = queryParameters.get("data").get(0);
            transaction = TransactionFactory.getInstance().parse(Base58.decode(dataStr), Transaction.FOR_NETWORK);
        } catch (Exception e) {
            return APIUtils.errorMess(-1, e.toString());
        }

        return transaction.toJson().toJSONString();
    }

    @POST
    @Path("/broadcast")
    public String broadcastFromRaw(String rawDataBase58) {
        int step = 1;

        try {
            byte[] transactionBytes = Base58.decode(rawDataBase58);

            step++;
            Fun.Tuple3<Transaction, Integer, String> result = Controller.getInstance().lightCreateTransactionFromRaw(transactionBytes, false);
            if (result.b == Transaction.VALIDATE_OK) {
                return "+";
            } else {
                return APIUtils.errorMess(result.b, OnDealClick.resultMess(result.b), result.a);
            }

        } catch (Exception e) {
            //logger.error(e.getMessage());
            return APIUtils.errorMess(-1, e.toString() + " on step: " + step);
        }
    }

}
