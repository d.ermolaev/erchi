package org.erachain.api;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.controller.Controller;
import org.erachain.core.account.Account;
import org.erachain.core.crypto.Base58;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.polls.PollCls;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.VoteOnItemPollTransaction;
import org.erachain.datachain.DCSet;
import org.erachain.utils.APIUtils;
import org.erachain.utils.StrJSonFine;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedHashMap;
import java.util.Map;

@Path("polls")
@Produces(MediaType.APPLICATION_JSON)
public class ItemPollsResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemPollsResource.class);

    @Context
    HttpServletRequest request;

    @GET
    public String help() {
        Map help = new LinkedHashMap();

        help.put("polls/last", "Get last key");
        help.put("polls/{key}", "Returns information about poll with the given key.");
        help.put("polls/raw/{key}", "Returns RAW in Base58 of poll with the given key.");
        help.put("polls/images/{key}", "get item Images by key");
        help.put("polls/listfrom/{start}", "get list from KEY");

        help.put("polls/vote/{key}/{option}/{voter}?feePow=feePow", "Used to vote on a poll with the given KEY. Returns the transaction in JSON when successful.");
        help.put("POST polls/vote/{key} {\"voter\":\"<voterAddress>\", \"option\": \"<optionOne>\", \"feePow\":\"<feePow>\"}", "Used to vote on a poll with the given KEY. Returns the transaction in JSON when successful.");

        help.put("polls/address/{address}", "Returns an array of all the polls owned by a specific address in your wallet.");

        return StrJSonFine.convert(help);
    }

    @GET
    @Path("last")
    public String last() {
        return "" + DCSet.getInstance().getItemPollMap().getLastKey();
    }

    /**
     * Get lite information poll by key poll
     *
     * @param key is number poll
     * @return JSON object. Single poll
     */
    @GET
    @Path("/{key}")
    public String get(@PathParam("key") String key) {
        Long asLong = null;

        try {
            asLong = Long.valueOf(key);

        } catch (NumberFormatException e) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.INVALID_ITEM_KEY);

        }

        if (!DCSet.getInstance().getItemPollMap().contains(asLong)) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.ITEM_POLL_NOT_EXIST);

        }

        return Controller.getInstance().getPoll(asLong).toJson().toJSONString();
    }

    @GET
    @Path("raw/{key}")
    public String getRAW(@PathParam("key") String key) {
        Long asLong = null;

        try {
            asLong = Long.valueOf(key);
        } catch (NumberFormatException e) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.INVALID_ITEM_KEY);
        }

        if (!DCSet.getInstance().getItemPollMap().contains(asLong)) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.ITEM_POLL_NOT_EXIST);
        }

        ItemCls item = Controller.getInstance().getPoll(asLong);
        byte[] issueBytes = item.toBytes(false);
        return Base58.encode(issueBytes);
    }

    /**
     *
     */
    @GET
    @Path("/images/{key}")
    public String getImages(@PathParam("key") String key) {
        Long asLong = null;

        try {
            asLong = Long.valueOf(key);

        } catch (NumberFormatException e) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.INVALID_ITEM_KEY);

        }

        if (!DCSet.getInstance().getItemPollMap().contains(asLong)) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.ITEM_POLL_NOT_EXIST);

        }

        return Controller.getInstance().getPoll(asLong).toJsonData().toJSONString();
    }

    @SuppressWarnings("unchecked")
    @GET
    @Path("listfrom/{start}")
    public String getList(@PathParam("start") long start,
                          @DefaultValue("20") @QueryParam("page") int page,
                          @DefaultValue("true") @QueryParam("showperson") boolean showPerson,
                          @DefaultValue("true") @QueryParam("desc") boolean descending) {

        JSONObject output = new JSONObject();
        ItemCls.makeJsonLitePage(DCSet.getInstance(), ItemCls.POLL_TYPE, start, page, output, showPerson, descending);

        return output.toJSONString();
    }

    @POST
    @Path("/vote/{key}")
    @Consumes(MediaType.WILDCARD)
    public Response createPollVote(String x, @PathParam("key") Long key) {

        String password = null;
        APIUtils.askAPICallAllowed(password, "POST polls/vote/" + key + "\n" + x, request, true);

        try {
            //READ JSON
            JSONObject jsonObject = (JSONObject) JSONValue.parse(x);
            String voter = (String) jsonObject.get("voter");
            Integer option = (Integer) jsonObject.get("option");
            String feePowStr = (String) jsonObject.get("feePow");

            //PARSE FEE
            int feePow;
            try {
                feePow = Integer.parseInt(feePowStr);
            } catch (Exception e) {
                throw ApiErrorFactory.getInstance().createError(
                        Transaction.INVALID_FEE_POWER);
            }

            //CHECK VOTER
            if (!Account.isValidAddress(voter)) {
                throw ApiErrorFactory.getInstance().createError(Transaction.INVALID_ADDRESS);
            }

            //CHECK IF WALLET EXISTS
            if (!Controller.getInstance().doesWalletKeysExists()) {
                throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_NO_EXISTS);
            }

            //CHECK WALLET UNLOCKED
            if (!Controller.getInstance().isWalletUnlocked()) {
                throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_LOCKED);
            }

            //GET ACCOUNT
            PrivateKeyAccount creator = Controller.getInstance().getWalletPrivateKeyAccountByAddress(voter);
            if (creator == null) {
                throw ApiErrorFactory.getInstance().createError(Transaction.CREATOR_NOT_OWNER);
            }

            //GET POLL
            PollCls poll = Controller.getInstance().getPoll(key);
            if (poll == null) {
                throw ApiErrorFactory.getInstance().createError(Transaction.POLL_NOT_EXISTS);
            }

            //GET OPTION
            String pollOption = poll.getOptions().get(option);
            if (pollOption == null) {
                throw ApiErrorFactory.getInstance().createError(Transaction.POLL_OPTION_NOT_EXISTS);
            }

            //CREATE POLL
            Transaction transaction = new VoteOnItemPollTransaction(creator, key, option, (byte) feePow);
            Controller.getInstance().signAndUpdateTransaction(transaction, creator);
            int result = Controller.getInstance().getTransactionCreator().afterCreate(transaction, Transaction.FOR_NETWORK, false, false);

            if (result == Transaction.VALIDATE_OK) {

                return Response.status(200).header("Content-Type", "application/json; charset=utf-8")
                        .header("Access-Control-Allow-Origin", "*")
                        .entity(transaction.toJson().toJSONString()).build();
            } else
                throw ApiErrorFactory.getInstance().createError(result);

        } catch (NullPointerException | ClassCastException e) {
            //JSON EXCEPTION
            LOGGER.error(e.getMessage());
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_JSON);
        }
    }

    /**
     * @param voter
     * @param feePow
     * @param pollKey
     * @param option
     * @return
     */
    @GET
    @Path("vote/{key}/{option}/{voter}")
    public Response createPollVoteGet(@PathParam("key") long pollKey, @PathParam("option") int option,
                                      @PathParam("voter") String voter,
                                      @QueryParam("feePow") Integer feePow
    ) {

        String password = null;
        APIUtils.askAPICallAllowed(password, "GET polls/vote/" + pollKey + "\n", request, true);

        //CHECK VOTERa
        if (!Account.isValidAddress(voter))
            throw ApiErrorFactory.getInstance().createError(Transaction.INVALID_ADDRESS);

        //CHECK IF WALLET EXISTS
        if (!Controller.getInstance().doesWalletKeysExists())
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_NO_EXISTS);

        //GET ACCOUNT
        PrivateKeyAccount creator = Controller.getInstance().getWalletPrivateKeyAccountByAddress(voter);
        if (creator == null)
            throw ApiErrorFactory.getInstance().createError(Transaction.CREATOR_NOT_OWNER);

        //GET POLL
        PollCls poll = Controller.getInstance().getPoll(pollKey);
        if (poll == null)
            throw ApiErrorFactory.getInstance().createError(Transaction.POLL_NOT_EXISTS);

        //GET OPTION
        String pollOption = poll.getOptions().get(option);
        if (pollOption == null)
            throw ApiErrorFactory.getInstance().createError(Transaction.POLL_OPTION_NOT_EXISTS);

        //CREATE POLL
        Transaction transaction = new VoteOnItemPollTransaction(creator, pollKey, option, (byte)(int) feePow);
        Controller.getInstance().signAndUpdateTransaction(transaction, creator);
        int result = Controller.getInstance().getTransactionCreator().afterCreate(transaction, Transaction.FOR_NETWORK, false, false);

        if (result == Transaction.VALIDATE_OK) {

            return Response.status(200).header("Content-Type", "application/json; charset=utf-8")
                    .header("Access-Control-Allow-Origin", "*")
                    .entity(transaction.toJson().toJSONString()).build();
        } else
            throw ApiErrorFactory.getInstance().createError(result);

    }

    @SuppressWarnings("unchecked")
    @GET
    @Path("/address/{address}")
    public String getPolls(@PathParam("address") String address) {

        //CHECK IF WALLET EXISTS
        if (!Controller.getInstance().doesWalletKeysExists()) {
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_NO_EXISTS);
        }

        //CHECK ADDRESS
        if (!Account.isValidAddress(address)) {
            throw ApiErrorFactory.getInstance().createError(Transaction.INVALID_ADDRESS);
        }

        //CHECK ACCOUNT IN WALLET
        Account account = Controller.getInstance().getWalletAccountByAddress(address);
        if (account == null) {
            throw ApiErrorFactory.getInstance().createError(ApiErrorFactory.ERROR_WALLET_ADDRESS_NO_EXISTS);
        }

        JSONArray array = new JSONArray();
        for (ItemCls poll : Controller.getInstance().getAllItems(ItemCls.POLL_TYPE, account)) {
            array.add(poll.toJson());
        }

        return array.toJSONString();
    }

}
