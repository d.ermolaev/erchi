package org.erachain.api;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.controller.Controller;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.crypto.Base58;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.exdata.exLink.ExLinkAppendix;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.transaction.RCertifyPubKeys;
import org.erachain.core.transaction.Transaction;
import org.erachain.datachain.DCSet;
import org.erachain.utils.APIUtils;
import org.erachain.utils.StrJSonFine;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Path("persons")
@Produces(MediaType.APPLICATION_JSON)
public class ItemPersonsResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemPersonsResource.class);
    public static Map help = new LinkedHashMap<String, String>() {{
        put("persons/last", "Get last key");
        put("persons/{key}", "Returns information about person with the given key.");
        put("persons/raw/{key}", "Returns RAW in Base58 of person with the given key.");
        put("persons/images/{key}", "get item Images by key");
        put("persons/listfrom/{start}", "get list from KEY");

        put("persons/certify/{creator}/{personKey}/{pubkey}?feePow=<int>&linkTo=<SeqNo>&days=<int>&password=<String>", "Certify some public key for Person by it key. Default: pubKey is owner from Person, feePow=0, days=1");
    }};
    @Context
    HttpServletRequest request;

    @SuppressWarnings("unchecked")
    @GET
    public String help() {
        return StrJSonFine.convert(help);
    }

    @GET
    @Path("last")
    public String last() {
        return "" + DCSet.getInstance().getItemPersonMap().getLastKey();
    }

    @GET
    @Path("/{key}")
    public String get(@PathParam("key") String key) {
        Long asLong = null;

        try {
            asLong = Long.valueOf(key);
        } catch (NumberFormatException e) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.INVALID_ITEM_KEY);
        }

        if (!DCSet.getInstance().getItemPersonMap().contains(asLong)) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.ITEM_PERSON_NOT_EXIST);
        }

        ItemCls item = Controller.getInstance().getPerson(asLong);
        return JSONValue.toJSONString(item.toJson());
    }

    @GET
    @Path("raw/{key}")
    public String getRAW(@PathParam("key") String key) {
        Long asLong = null;

        try {
            asLong = Long.valueOf(key);
        } catch (NumberFormatException e) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.INVALID_ITEM_KEY);
        }

        if (!DCSet.getInstance().getItemPersonMap().contains(asLong)) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.ITEM_PERSON_NOT_EXIST);
        }

        ItemCls item = Controller.getInstance().getPerson(asLong);
        byte[] issueBytes = item.toBytes(false);
        return Base58.encode(issueBytes);
    }

    @GET
    @Path("/images/{key}")
    public String getImages(@PathParam("key") String key) {
        Long asLong = null;

        try {
            asLong = Long.valueOf(key);

        } catch (NumberFormatException e) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.INVALID_ITEM_KEY);
        }

        if (!DCSet.getInstance().getItemPersonMap().contains(asLong)) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.ITEM_PERSON_NOT_EXIST);
        }

        return Controller.getInstance().getPerson(asLong).toJsonData().toJSONString();
    }

    @SuppressWarnings("unchecked")
    @GET
    @Path("listfrom/{start}")
    public String getList(@PathParam("start") long start,
                          @DefaultValue("20") @QueryParam("page") int page,
                          @DefaultValue("true") @QueryParam("showperson") boolean showPerson,
                          @DefaultValue("true") @QueryParam("desc") boolean descending) {

        JSONObject output = new JSONObject();
        ItemCls.makeJsonLitePage(DCSet.getInstance(), ItemCls.PERSON_TYPE, start, page, output, showPerson, descending);

        return output.toJSONString();
    }

    // GET persons/issueraw/7EPhDbpjsaRDFwB2nY8Cvn7XukF58kGdkz/123/asdasdasdasd
    @GET
    // @Consumes(MediaType.WILDCARD)
    //@Produces("text/plain")
    @Path("certify/{creator}/{person}/{pubkey}")
    public String certifyPubkey(@PathParam("creator") String creatorStr,
                                @PathParam("person") Long personKey,
                                @PathParam("pubkey") String pubkeyStr,
                                @DefaultValue("1") @QueryParam("days") Integer addDays,
                                @QueryParam("linkTo") String linkToRefStr,
                                @DefaultValue("0") @QueryParam("feePow") int feePow,
                                @QueryParam("password") String password) {

        Controller cntr = Controller.getInstance();

        if (!DCSet.getInstance().getItemPersonMap().contains(personKey)) {
            throw ApiErrorFactory.getInstance().createError(
                    Transaction.ITEM_PERSON_NOT_EXIST);
        }

        ExLink linkTo;
        if (linkToRefStr == null)
            linkTo = null;
        else {
            Long linkToRef = Transaction.parseDBRef(linkToRefStr);
            if (linkToRef == null) {
                throw ApiErrorFactory.getInstance().createError(
                        Transaction.INVALID_BLOCK_TRANS_SEQ_ERROR);
            } else {
                linkTo = new ExLinkAppendix(linkToRef);
            }
        }

        PublicKeyAccount pubKey;
        if (pubkeyStr == null) {
            // by Default - from Person
            PersonCls person = cntr.getPerson(personKey);
            pubKey = person.getOwner();
        } else {
            if (!PublicKeyAccount.isValidPublicKey(pubkeyStr)) {
                throw ApiErrorFactory.getInstance().createError(
                        Transaction.INVALID_PUBLIC_KEY);
            }
            pubKey = new PublicKeyAccount(pubkeyStr);
        }

        APIUtils.askAPICallAllowed(password, "GET certify\n ", request, true);
        PrivateKeyAccount creator = APIUtils.getPrivateKeyCreator(creatorStr);
        List<PublicKeyAccount> pubKeys = new ArrayList<>();
        pubKeys.add(pubKey);

        Transaction transaction = new RCertifyPubKeys(creator, linkTo, (byte) feePow, personKey,
                pubKeys, addDays, 0L);
        Controller.getInstance().signAndUpdateTransaction(transaction, creator);

        Integer result = Controller.getInstance().getTransactionCreator().afterCreate(transaction, Transaction.FOR_NETWORK, false, false);

        // CHECK VALIDATE MESSAGE
        if (result == Transaction.VALIDATE_OK) {
            return transaction.toJson().toJSONString();
        }

        return transaction.makeErrorJSON2(result).toJSONString();

    }

}
