package org.erachain.api;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.controller.Controller;
import org.erachain.core.crypto.Base58;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TransactionFactory;
import org.erachain.core.transaction.TxException;
import org.erachain.core.transaction.TxParseException;
import org.erachain.datachain.DCSet;
import org.erachain.ntp.NTP;
import org.erachain.utils.APIUtils;
import org.erachain.utils.StrJSonFine;
import org.erachain.webAPI.API;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;

@Path("tx")
@Produces(MediaType.APPLICATION_JSON)
public class TXResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(TXResource.class);

    public static Map help = new LinkedHashMap<String, String>() {{
        put("GET tx/confirms/bysign/{signature}", "Check transaction confirmations by its signature. -1 - not exist, 0 - not confirmed yet, N - has N confirmations. Signature in Base58.");
        put("GET tx/confirms/byseqno/{seqNo}", "Check transaction confirmations by its SeqNo. -1 - not exist, N - has N confirmations. SeqNo format: `123-2`");
        put("GET tx/bysign/{signature}", "Get transaction by its signature. Signature in Base58.");
        put("GET tx/byseqno/{seqNo}", "Get transaction confirmations by its SeqNo. SeqNo format: `123-2`");
        put("POST tx/make?onlyRaw", "Parse and make transaction from JSON or RAW (Base64). Use param <onlyRaw> for output only RAW data. See tips in result for fields and values");
        put("POST tx/sign?only=raw|sign", "Make and sign transaction from JSON or RAW (Base64). Set param <only> for output only RAW data or only Signature. See tips in result for fields and values. May use `password` in JSON instead Unlock before.");
        put("POST tx/check?only=raw|sign", "Make, sign and check transaction from JSON or RAW (Base64). Set param <only> for output only RAW data or only Signature. See tips in result for fields and values. May use `password` in JSON instead Unlock before.");
        put("POST tx/issue?only=raw|sign", "Make, sign, check and issue transaction from JSON or RAW (Base64). Set param <only> for output only RAW data or only Signature. See tips in result for fields and values. May use `password` in JSON instead Unlock before.");
    }};
    @Context
    HttpServletRequest request;

    @SuppressWarnings("unchecked")
    @GET
    public String help() {
        return StrJSonFine.convert(help);
    }
    @POST
    public String helpPost() {
        return StrJSonFine.convert(help);
    }

    @GET
    @Path("confirms/bysign/{signature}")
    public static String getConfirmsBySignature(@PathParam("signature") String signatureStr) {
        // DECODE SIGNATURE
        byte[] signature;
        try {
            signature = Base58.decode(signatureStr);
        } catch (Exception e) {
            throw ApiErrorFactory.getInstance().createError(Transaction.INVALID_SIGNATURE);
        }
        if (signature == null)
            throw ApiErrorFactory.getInstance().createError(Transaction.INVALID_SIGNATURE);

        JSONObject out = new JSONObject();
        DCSet dcSet = Controller.getInstance().getDCSet();

        // CHECK FIRST IN FINAL TRANSACTIONS
        if (dcSet.getTransactionFinalMapSigns().contains(signature)) {
            Long seqNo = dcSet.getTransactionFinalMapSigns().get(signature);
            int currentHeight = Controller.getInstance().getMyHeight();
            int txHeight = Transaction.parseHeightDBRef(seqNo);
            out.put("confirms", 1 + currentHeight - txHeight);
            out.put("seqNo", Transaction.viewDBRef(seqNo));

        } else if (dcSet.getTransactionTab().contains(signature)) {
            // CHEK in unconfirmed
            out.put("confirms", 0);
        } else
            out.put("confirms", -1);

        return out.toJSONString();

    }

    @GET
    @Path("confirms/byseqno/{seqNo}")
    public static String getConfirmsBySeqNo(@PathParam("seqNo") String seqNoStr) {
        // DECODE SIGNATURE

        Long seqNo = Transaction.parseDBRef(seqNoStr);
        if (seqNo == null)
            throw ApiErrorFactory.getInstance().createError(Transaction.INVALID_BLOCK_TRANS_SEQ_ERROR);

        JSONObject out = new JSONObject();
        DCSet dcSet = Controller.getInstance().getDCSet();

        if (dcSet.getTransactionFinalMap().contains(seqNo)) {
            int currentHeight = Controller.getInstance().getMyHeight();
            int txHeight = Transaction.parseHeightDBRef(seqNo);
            out.put("confirms", 1 + currentHeight - txHeight);

        } else
            out.put("confirms", -1);

        return out.toJSONString();

    }

    @GET
    @Path("bysign/{signature}")
    public static String getBySignature(@PathParam("signature") String signatureStr) {
        // DECODE SIGNATURE
        byte[] signature;
        try {
            signature = Base58.decode(signatureStr);
        } catch (Exception e) {
            throw ApiErrorFactory.getInstance().createError(Transaction.INVALID_SIGNATURE);
        }
        if (signature == null)
            throw ApiErrorFactory.getInstance().createError(Transaction.INVALID_SIGNATURE);

        JSONObject out = new JSONObject();
        DCSet dcSet = Controller.getInstance().getDCSet();

        // CHECK FIRST IN FINAL TRANSACTIONS
        if (dcSet.getTransactionFinalMapSigns().contains(signature)) {
            Long seqNo = dcSet.getTransactionFinalMapSigns().get(signature);
            Transaction tx = dcSet.getTransactionFinalMap().get(seqNo);
            out = tx.toJson();

        } else if (dcSet.getTransactionTab().contains(signature)) {
            // CHEK in unconfirmed
            Transaction tx = dcSet.getTransactionTab().get(signature);
            out = tx.toJson();
        } else
            throw ApiErrorFactory.getInstance().createError(Transaction.TRANSACTION_DOES_NOT_EXIST);

        return out.toJSONString();

    }

    @GET
    @Path("byseqno/{seqNo}")
    public static String getBySeqNo(@PathParam("seqNo") String seqNoStr) {
        // DECODE SIGNATURE

        Long seqNo = Transaction.parseDBRef(seqNoStr);
        if (seqNo == null)
            throw ApiErrorFactory.getInstance().createError(Transaction.INVALID_BLOCK_TRANS_SEQ_ERROR);

        JSONObject out = new JSONObject();
        DCSet dcSet = Controller.getInstance().getDCSet();

        if (dcSet.getTransactionFinalMap().contains(seqNo)) {
            Transaction tx = dcSet.getTransactionFinalMap().get(seqNo);
            out = tx.toJson();

        } else
            out.put("confirms", -1);

        return out.toJSONString();

    }

    /**
     * Make any transaction from JSON or RAW (Base64) - parse and make RAW.
     *
     * @param data JSON or RAW (Base64)
     * Fields:<br>
     * HEAD fields:<ul>
     * <li>type</li>
     * <li>headVersion</li>
     * <li>headFlags1</li>
     * <li>version</li>
     * <li>flags1</li>
     * <li>creator:Base58</li>
     * <li>title</li>
     * <li>tags</li>
     * <li>dataMessage</li>
     * <li>dataMessageCode:text|base64|base58 - default=text</li>
     * <li>encrypted:Boolean</li>
     * <li>isText:Boolean</li>
     * <li>exLink:JSON</li>
     * <li>feePow:0..7</li>
     * <li>timestamp:Long - default get current time</li>
     * </ul><br>
     * ExLink fields:<ul>
     * <li>type</li>
     * <li>flags</li>
     * <li>value1</li>
     * <li>value2</li>
     * <li>ref</li>
     * </ul>
     * @return
     */
    @POST
    @Path("make")
    // post tx/make {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4"}
    // post tx/make {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","feePow":1,"recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4","dataMessage":"probe","tags":"TAG"}
    public String make(@Context UriInfo info, String data) {

        boolean onlyRaw = API.checkBoolean(info, "onlyRaw", false);

        JSONObject out = new JSONObject();
        Transaction transaction;
        TxException errorMessage = new TxException();

        try {
            if (data.startsWith("{")) {
                transaction = TransactionFactory.getInstance().parseJson((JSONObject) JSONValue.parse(data), errorMessage);
            } else {
                transaction = TransactionFactory.getInstance().parse(Base64.getDecoder().decode(data), Transaction.FOR_NETWORK_NOT_CHECK_SIGN);
            }

        } catch (TxParseException e) {
            e.updateJson(out);
            return out.toJSONString();
        } catch (Exception e) {
            Transaction.updateMapByError2static(out, Transaction.JSON_ERROR, errorMessage.getMsg() + " : " + e.getMessage(), null);
            return out.toJSONString();
        }

        if (transaction.getTimestamp() == 0L)
            transaction.setTimestamp(NTP.getTime());

        if (onlyRaw)
            return Base64.getEncoder().encodeToString(transaction.toBytes(Transaction.FOR_NETWORK));

        out = transaction.toJson();
        out.put("raw", Base64.getEncoder().encodeToString(transaction.toBytes(Transaction.FOR_NETWORK)));

        return out.toJSONString();

    }

    /**
     * Parse and Sign. May use `password` in JSON instead Unlock before
     *
     * @param data JSON or RAW (Base64)
     * @return
     */
    @POST
    @Path("sign")
    // post tx/sign {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","feePow":1,"recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4","dataMessage":"probe","tags":"TAG"}
    // post tx/sign ZPaNnN/8bqv6qjY1vQkPZr/mK+l0uh87fM6Wl32mm+zIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgAAQAAAAABhSlPtxUMSGVsbG8gV29ybGQhBWlzc3VlQAAABXByb2JlAAABAQACAAAAZGQOAAAA9o2c3/xuq/qqNjW9CQ9mv+Yr6XS6Hzt8zpaXfaab7Mj2jZzf/G6r+qo2Nb0JD2a/5ivpdLofO3zOlpd9ppvsyEFWQVRBUiBOQU1FIDAxAAAAAAAAAAAAAAAAAABjnr1e
    public String sign(@QueryParam("only") String only, String data) {

        JSONObject out = new JSONObject();

        Transaction transaction;
        TxException errorMessage = new TxException();

        JSONObject json;

        try {
            json = (JSONObject) JSONValue.parse(data);
        } catch (Exception e) {
            Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR, e.getMessage(), null);
            return out.toJSONString();
        }
        if (json == null) {
            Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR, "", null);
            return out.toJSONString();
        }

        try {
            if (data.startsWith("{")) {
                transaction = TransactionFactory.getInstance().parseJson(json, errorMessage);
            } else {
                transaction = TransactionFactory.getInstance().parse(Base64.getDecoder().decode(data), Transaction.FOR_NETWORK_NOT_CHECK_SIGN);
            }
        } catch (TxParseException e) {
            e.updateJson(out);
            return out.toJSONString();
        } catch (Exception e) {
            Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR, errorMessage.getMsg() + " : " + e.getMessage(), null);
            return out.toJSONString();
        }

        APIUtils.askAPICallAllowed((String) json.get("password"), "POST sign Transaction\n ", request, true);

        Controller cntr = Controller.getInstance();

        PrivateKeyAccount privateKeyAccount = cntr.getWalletPrivateKeyAccountByAddress(transaction.getCreator());
        if (privateKeyAccount == null) {
            Transaction.updateMapByError2static(out, Transaction.INVALID_WALLET_ADDRESS, null);
            return out.toJSONString();
        }

        if (transaction.getTimestamp() == 0L)
            transaction.setTimestamp(NTP.getTime());

        transaction.sign(privateKeyAccount, Transaction.FOR_NETWORK);

        if (only == null) {
            out = transaction.toJson();
            out.put("raw", Base64.getEncoder().encodeToString(transaction.toBytes(Transaction.FOR_NETWORK)));
            return out.toJSONString();
        }

        only = only.toLowerCase();
        if (only.equals("raw"))
            return Base64.getEncoder().encodeToString(transaction.toBytes(Transaction.FOR_NETWORK));

        return Base58.encode(transaction.getSignature());

    }

    /**
     * Parse, Sign and Check without issue. May use `password` in JSON instead Unlock before
     *
     * @param data JSON or RAW (Base64)
     * @return
     */
    @POST
    @Path("check")
    // post tx/issue {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","feePow":1,"recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4","dataMessage":"probe","tags":"TAG"}
    // post tx/issue {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","feePow":1,"recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4","dataMessage":"probe","tags":"TAG","encrypt":true}
    // post tx/issue {"type":24,"version":0,"creator": "acc:3tNqdzAtqwBZBgUGREzZpcMHycVj","feePow":1,"title":"Hello World!","dataMessage":"probe","tags":"issue","item":{"itemType":4,"typeClass":2}}
    // post tx/issue {"type":24,"creator": "acc:4ZYSFXgnkhbGYU14uR2pdey1X731","title":"Hello World!","dataMessage":"probe","tags":"issue","item":{"itemType":1,"typeClass":2,"name":"AVATAR NAME 01","startDate":1671347550}}
    public String check(@QueryParam("only") String only, String data) {

        JSONObject out = new JSONObject();

        // READ JSON
        Transaction transaction;
        TxException errorMessage = new TxException();

        JSONObject json;

        try {
            json = (JSONObject) JSONValue.parse(data);
        } catch (Exception e) {
            Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR, e.getMessage(), null);
            return out.toJSONString();
        }
        if (json == null) {
            Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR, "", null);
            return out.toJSONString();
        }

        try {
            if (data.startsWith("{")) {
                transaction = TransactionFactory.getInstance().parseJson(json, errorMessage);
            } else {
                transaction = TransactionFactory.getInstance().parse(Base64.getDecoder().decode(data), Transaction.FOR_NETWORK_NOT_CHECK_SIGN);
            }
        } catch (TxParseException e) {
            e.updateJson(out);
            return out.toJSONString();
        } catch (Exception e) {
            Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR, errorMessage.getMsg() + " : " + e.getMessage(), null);
            return out.toJSONString();
        }

        APIUtils.askAPICallAllowed((String) json.get("password"), "POST sign Transaction\n ", request, true);

        Controller cntr = Controller.getInstance();

        PrivateKeyAccount privateKeyAccount = cntr.getWalletPrivateKeyAccountByAddress(transaction.getCreator());
        if (privateKeyAccount == null) {
            Transaction.updateMapByError2static(out, Transaction.INVALID_WALLET_ADDRESS, null);
            return out.toJSONString();
        }

        cntr.signAndUpdateTransaction(transaction, privateKeyAccount);
        int validate = cntr.getTransactionCreator().afterCreate(transaction, Transaction.FOR_NETWORK, false, true);

        if (validate == Transaction.VALIDATE_OK) {
            if (only == null) {
                out = transaction.toJson();
                out.put("raw", Base64.getEncoder().encodeToString(transaction.toBytes(Transaction.FOR_NETWORK)));
                return out.toJSONString();
            }

            only = only.toLowerCase();
            if (only.equals("raw"))
                return Base64.getEncoder().encodeToString(transaction.toBytes(Transaction.FOR_NETWORK));

            return Base58.encode(transaction.getSignature());

        } else {
            transaction.updateMapByError2(out, validate, null);
            return out.toJSONString();
        }

    }


    /**
     * Parse, Sign and Issue. May use `password` in JSON instead Unlock before
     *
     * @param data JSON or RAW (Base64)
     * @return
     */
    @POST
    @Path("issue")
    // post tx/issue {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","feePow":1,"recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4","dataMessage":"probe","tags":"TAG"}
    // post tx/issue {"type":31,"version":0,"creator": "acc:4MUn5X9nsvNbF9MiZ9b56FzJxKcu","feePow":1,"recipient":"acc:4Zgtvx3dUbHb2ExVyshrAmHHnEY4","dataMessage":"probe","tags":"TAG","encrypt":true}
    // post tx/issue {"type":24,"version":0,"creator": "acc:3tNqdzAtqwBZBgUGREzZpcMHycVj","feePow":1,"title":"Hello World!","dataMessage":"probe","tags":"issue","item":{"itemType":4,"typeClass":2}}
    // post tx/issue {"type":24,"creator": "acc:4ZYSFXgnkhbGYU14uR2pdey1X731","title":"Hello World!","dataMessage":"probe","tags":"issue","item":{"itemType":1,"typeClass":2,"name":"AVATAR NAME 01","startDate":1671347550}}
    public String issue(@QueryParam("only") String only, String data) {

        JSONObject out = new JSONObject();

        // READ JSON
        Transaction transaction;
        TxException errorMessage = new TxException();

        JSONObject json;

        try {
            json = (JSONObject) JSONValue.parse(data);
        } catch (Exception e) {
            Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR, e.getMessage(), null);
            return out.toJSONString();
        }
        if (json == null) {
            Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR, "", null);
            return out.toJSONString();
        }

        try {
            if (data.startsWith("{")) {
                transaction = TransactionFactory.getInstance().parseJson(json, errorMessage);
            } else {
                transaction = TransactionFactory.getInstance().parse(Base64.getDecoder().decode(data), Transaction.FOR_NETWORK_NOT_CHECK_SIGN);
            }
        } catch (TxParseException e) {
            e.updateJson(out);
            return out.toJSONString();
        } catch (Exception e) {
            Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR, errorMessage.getMsg() + " : " + e.getMessage(), null);
            return out.toJSONString();
        }

        APIUtils.askAPICallAllowed((String) json.get("password"), "POST sign Transaction\n ", request, true);

        Controller cntr = Controller.getInstance();

        PrivateKeyAccount privateKeyAccount = cntr.getWalletPrivateKeyAccountByAddress(transaction.getCreator());
        if (privateKeyAccount == null) {
            Transaction.updateMapByError2static(out, Transaction.INVALID_WALLET_ADDRESS, null);
            return out.toJSONString();
        }

        cntr.signAndUpdateTransaction(transaction, privateKeyAccount);
        int validate = cntr.getTransactionCreator().afterCreate(transaction, Transaction.FOR_NETWORK, false, false);

        if (validate == Transaction.VALIDATE_OK) {
            if (only == null) {
                out = transaction.toJson();
                out.put("raw", Base64.getEncoder().encodeToString(transaction.toBytes(Transaction.FOR_NETWORK)));
                return out.toJSONString();
            }

            only = only.toLowerCase();
            if (only.equals("raw"))
                return Base64.getEncoder().encodeToString(transaction.toBytes(Transaction.FOR_NETWORK));

            return Base58.encode(transaction.getSignature());

        } else {
            transaction.updateMapByError2(out, validate, null);
            return out.toJSONString();
        }

    }
}
