package org.erachain.gui.transaction;

import org.erachain.controller.Controller;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.transaction.Transaction;
import org.erachain.gui.MainFrame;
import org.erachain.gui.PasswordPane;
import org.erachain.lang.Lang;

import javax.swing.*;

//import javax.swing.JFrame;
//import javax.swing.JOptionPane;
//import org.erachain.lang.Lang;

public class OnDealClick {

    public static boolean proccess1(JButton button) {
        //DISABLE
        button.setEnabled(false);

        //CHECK IF NETWORK OK
        if (false && Controller.getInstance().getStatus() != Controller.STATUS_OK) {
            //NETWORK NOT OK
            JOptionPane.showMessageDialog(null, Lang.T("You are unable to send a transaction while synchronizing or while having no connections!"), Lang.T("Error"), JOptionPane.ERROR_MESSAGE);

            //ENABLE
            button.setEnabled(true);

            return false;
        }

        //CHECK IF WALLET UNLOCKED
        if (!Controller.getInstance().isWalletUnlocked()) {
            //ASK FOR PASSWORD
            String password = PasswordPane.showUnlockWalletDialog(MainFrame.getInstance());
            if (!Controller.getInstance().unlockWallet(password)) {
                //WRONG PASSWORD
                JOptionPane.showMessageDialog(null, Lang.T("Invalid password"), Lang.T("Unlock Wallet"), JOptionPane.ERROR_MESSAGE);

                //ENABLE
                button.setEnabled(true);

                return false;
            }
        }
        return true;

    }

    public static String resultMess(int error) {
        switch (error) {
            case Transaction.FUTURE_ABILITY:
                return "ERROR: Future Ability";
            case Transaction.INVALID_WALLET_ADDRESS:
                return "Invalid Wallet Address";
            case Transaction.INVALID_TYPE:
                return "Invalid transaction Type";
            case Transaction.INVALID_ADDRESS:
                return "Invalid Address";
            case Transaction.INVALID_RECEIVER:
                return "Invalid Receiver";
            case Transaction.NEGATIVE_AMOUNT:
                return "Negative amount";
            case Transaction.NOT_ENOUGH_FEE:
                return "Not enough fee";
            case Transaction.INVALID_FEE_POWER:
                return "Invalid fee power";
            case Transaction.NO_BALANCE:
                return "No balance";
            case Transaction.NO_DEBT_BALANCE:
                return "No debt balance";
            case Transaction.INVALID_BALANCE_ACTION:
                return "Invalid balance action";
            case Transaction.INVALID_BACKWARD_ACTION:
                return "Invalid backward action";
            case Transaction.NO_HOLD_BALANCE:
                return "No hold balance";
            case Transaction.INVALID_HOLD_DIRECTION:
                return "Invalid hold direction";
            case Transaction.INVALID_REFERENCE:
                return "Invalid reference";
            case Transaction.INVALID_TIMESTAMP:
                return "Invalid timestamp";
            case Transaction.INVALID_CREATOR_ADDRESS:
                return "Invalid creator Account";
            case Transaction.INVALID_PUBLIC_KEY:
                return "Invalid public key";
            case Transaction.INVALID_ISSUE_PROHIBITED:
                return "Item Issue Prohibited";
            case Transaction.INVALID_NAME_LENGTH_MIN:
                return "Invalid name MIN length";
            case Transaction.INVALID_NAME_LENGTH_MAX:
                return "Invalid name MAX length";
            case Transaction.INVALID_VALUE_LENGTH_MAX:
                return "Invalid value length";
            case Transaction.NAME_ALREADY_REGISTRED:
                return "Name already registred";
            case Transaction.NAME_DOES_NOT_EXIST:
                return "Name does not exist";
            case Transaction.NAME_ALREADY_ON_SALE:
                return "Name already on sale";
            case Transaction.NAME_NOT_FOR_SALE:
                return "Name not for sale";
            case Transaction.INVALID_UPDATE_VALUE:
                return "Invalid update value";
            case Transaction.NAME_KEY_ALREADY_EXISTS:
                return "Name key already exists";
            case Transaction.NAME_KEY_NOT_EXISTS:
                return "Name key not exists";
            case Transaction.LAST_KEY_IS_DEFAULT_KEY:
                return "Name name key id default key";
            case Transaction.PRIVATE_KEY_NOT_FOUND:
                return "Private key not found in Wallet";
            case Transaction.BUYER_ALREADY_OWNER:
                return "Buyer already Owner";
            case Transaction.INVALID_AMOUNT:
                return "Invalid amount";
            case Transaction.INVALID_AMOUNT_IS_NULL:
                return "Invalid amount is ZERO";
            case Transaction.INVALID_TITLE_LENGTH_MAX:
                return "Invalid title MAX length";
            case Transaction.INVALID_TAGS_LENGTH_MAX:
                return "Invalid tags MAX length";
            case Transaction.INVALID_MESSAGE_FORMAT:
                return "Invalid message format as data";
            case Transaction.INVALID_MESSAGE_LENGTH:
                return "Invalid message length";
            case Transaction.UNKNOWN_PUBLIC_KEY_FOR_ENCRYPT:
                return "Unknown public key (need for encrypt)";
            case Transaction.ENCRYPT_DENIED_FOR_DAPP:
                return "Encryption is denied for dApps";
            case Transaction.HASH_ALREADY_EXIST:
                return "HASH_ALREADY_EXIST";
            case Transaction.NOT_TRANSFERABLE_ASSET:
                return "Not transferable asset";
            case Transaction.WRONG_SIGNER:
                return "Wrong Signer";
            case Transaction.INVALID_BALANCE_POS:
                return "Invalid balance Position";
            case Transaction.INVALID_BALANCE_SIDE:
                return "Invalid balance Side";
            case Transaction.NAME_NOT_LOWER_CASE:
                return "Name not lower case";

            case Transaction.INVALID_ICON_LENGTH_MIN:
                return "Invalid icon MIN length";

            case Transaction.INVALID_ICON_LENGTH_MAX:
                return "Invalid icon MAX length";

            case Transaction.INVALID_ICON_TYPE:
                return "Invalid icon type";


            case Transaction.INVALID_IMAGE_TYPE:
                return "Invalid image type";

            case Transaction.INVALID_IMAGE_LENGTH_MIN:
                return "Invalid image MIN length";

            case Transaction.INVALID_IMAGE_LENGTH_MAX:
                return "Invalid image MAX length";


            case Transaction.INVALID_DESCRIPTION_LENGTH_MIN:
                return "Invalid description MIN length";

            case Transaction.INVALID_DESCRIPTION_LENGTH_MAX:
                return "Invalid description MAX length";

            case Transaction.INVALID_OPTIONS_LENGTH:
                return "Invalid options length";

            case Transaction.INVALID_OPTION_LENGTH:
                return "Invalid option length";

            case Transaction.DUPLICATE_OPTION:
                return "Invalid duplicte option";

            case Transaction.POLL_ALREADY_CREATED:
                return "Poll already created";

            case Transaction.POLL_ALREADY_HAS_VOTES:
                return "Poll already has votes";

            case Transaction.POLL_NOT_EXISTS:
                return "Poll not exists";

            case Transaction.POLL_OPTION_NOT_EXISTS:
                return "Option not exists";

            case Transaction.INVALID_OUTSIDE_ACTIVITY_PERIOD:
                return "TX.INVALID_OUTSIDE_ACTIVITY_PERIOD";


            //case Transaction.ALREADY_VOTED_FOR_THAT_OPTION:
            //	return "Already voted for that option";
            //	
            case Transaction.INVALID_DATA_LENGTH:
                return "Invalid data length";

            case Transaction.INVALID_DATA:
                return "Invalid data";

            case Transaction.INVALID_DATA_FORMAT:
                return "Invalid data format";


            case Transaction.TX_NOT_FOUND:
                return "Transaction not found";


            case Transaction.INVALID_EX_LINK_TYPE:
                return "Invalid Link Type";

            case Transaction.INVALID_EX_LINK_REF:
                return "Empty or invalid 'linkTo' parameter";


            case Transaction.INVALID_URL_LENGTH:
                return "Invalid URL length";

            case Transaction.INVALID_PARAMS_LENGTH:
                return "Invalid parameters length";

            case Transaction.INVALID_SIGNATURE:
                return "Invalid signature";

            case Transaction.ITEM_PERSON_AUTHOR_SIGNATURE_INVALID:
                return "Invalid Person Author signature";
            case Transaction.ITEM_PERSON_MUST_BE_SIGNED:
                return "Persom must be signed";
            case Transaction.TRANSACTION_DOES_NOT_EXIST:
                return "Transaction does not exist";
            case Transaction.NO_INCLAIM_BALANCE:
                return "No in claims balance";

            case Transaction.INVALID_CLAIM_RECIPIENT:
                return "Invalid claim Recipient: can not be the CLAIM Issuer";

            case Transaction.INVALID_CLAIM_DEBT_RECIPIENT:
                return "Invalid claim Recipient: claim may be only the to it Issuer";

            case Transaction.INVALID_CLAIM_DEBT_CREATOR:
                return "Invalid claim Creator: claim may be only to it Issuer";

            case Transaction.INVALID_AWARD:
                return "Invalid Award Item";

            case Transaction.INVALID_MAX_AWARD_COUNT:
                return "MAX Award Count";

            case Transaction.INVALID_MAX_ITEMS_COUNT:
                return "MAX Items Count";

            case Transaction.INVALID_QUANTITY:
                return "Invalid quantity";

            case Transaction.INVALID_ACCOUNTING_PAIR:
                return "Invalid Accounting Pair";

            case Transaction.INVALID_ECXHANGE_PAIR:
                return "Invalid Ecxhange Pair";

            case Transaction.INVALID_RETURN:
                return "Invalid return";

            case Transaction.HAVE_EQUALS_WANT:
                return "Have equals want";

            case Transaction.ORDER_DOES_NOT_EXIST:
                return "Order does not exists";

            case Transaction.ORDER_ALREADY_COMPLETED:
                return "Order already completed";

            case Transaction.INVALID_ORDER_CREATOR:
                return "Invalid order creator";

            case Transaction.ORDER_AMOUNT_HAVE_SO_SMALL:
                return "Order have amount so small";

            case Transaction.ORDER_AMOUNT_WANT_SO_SMALL:
                return "Order want amount so small";

            case Transaction.INVALID_PAYMENTS_LENGTH:
                return "Invalid payment length";

            case Transaction.NEGATIVE_PRICE:
                return "Negative price";

            case Transaction.INVALID_PRICE:
                return "Invalid price";

            case Transaction.INVALID_CREATION_BYTES:
                return "Invalid creation bytes";

            case Transaction.AT_ERROR:
                return "AT error";

            case Transaction.INVALID_TYPE_LENGTH:
                return "Invalid type lenght";

            case Transaction.NOT_MOVABLE_ASSET:
                return "Not movable asset";

            case Transaction.NOT_DEBTABLE_ASSET:
                return "Not debtable asset";

            case Transaction.NOT_HOLDABLE_ASSET:
                return "Not movable asset";

            case Transaction.NOT_SPENDABLE_ASSET:
                return "Not spendable asset";

            case Transaction.NOT_DEBT_ASSET:
                return "Not debt asset";

            case Transaction.INVALID_RAW_DATA:
                return "Invalid raw data";

            case Transaction.INVALID_DATE:
                return "Invalid date";

            case Transaction.NOT_ENOUGH_RIGHTS:
                return "Not enough rights for make transaction";


            case Transaction.NOT_ENOUGH_ERA_OWN:
                return "Not enough " + AssetCls.ERA_ABBREV + " balance in OWN";

            case Transaction.NOT_ENOUGH_ERA_USE:
                return "Not enough " + AssetCls.ERA_ABBREV + " balance in USE";

            case Transaction.NOT_ENOUGH_ERA_OWN_10:
                return "Need 10 ERA in OWN or more";

            case Transaction.NOT_ENOUGH_ERA_USE_10:
                return "Need 10 ERA in USE or more";


            case Transaction.NOT_ENOUGH_ERA_OWN_100:
                return "Need 100 ERA in OWN or more";

            case Transaction.NOT_ENOUGH_ERA_USE_100:
                return "Need 100 ERA in USE or more";


            case Transaction.NOT_ENOUGH_ERA_OWN_1000:
                return "Need 1000 ERA in OWN or more";

            case Transaction.NOT_ENOUGH_ERA_USE_1000:
                return "Need 1000 ERA in USE or more";


            case Transaction.NOT_ENOUGH_ERA_OWN_10000:
                return "Need 10000 ERA in OWN or more";

            case Transaction.NOT_ENOUGH_ERA_USE_10000:
                return "Need 10000 ERA in USE or more";

            case Transaction.INVALID_ITEM_KEY:
                return "Invalid item key";

            case Transaction.INVALID_FLAGS:
                return "Invalid flags = -1";

            case Transaction.INVALID_ITEM_VALUE:
                return "Invalid item value";

            case Transaction.CREATOR_NOT_AUTHOR:
                return "Creator not Author";

            case Transaction.CREATOR_NOT_OWNER:
                return "Creator not Owner";

            case Transaction.ITEM_DOES_NOT_EXIST:
                return "Item does not exist";

            case Transaction.OWNER_NOT_PERSONALIZED:
                return "Owner Account is not personalized";

            case Transaction.CREATOR_NOT_PERSONALIZED:
                return "Creator Account is not personalized";

            case Transaction.INVALID_PERSONALIZY_ANOTHER_PERSON:
                return "Public Key already certified to another Person";

            case Transaction.RECEIVER_NOT_PERSONALIZED:
                return "Receiver Account is not personalized";

            case Transaction.ITEM_DUPLICATE_KEY:
                return "Duplicate key";

            case Transaction.INVALID_ITEM_START_DATE:
                return "Empty Start Date";

            case Transaction.INVALID_ITEM_STOP_DATE:
                return "Empty Stop Date";

            case Transaction.INVALID_ITEM_AUTHOR:
                return "Invalid item Author";

            case Transaction.INVALID_ITEM_OWNER:
                return "Invalid item Owner";

            case Transaction.INVALID_PERSON_INVITER:
                return "Invalid person Inviter";

            case Transaction.INVALID_ITEM_NAME_NOT_UNIQUE:
                return "Not unique Name";

            case Transaction.ITEM_DUPLICATE:
                return "ITEM_DUPLICATE";

            case Transaction.INVALID_TIMESTAMP_START:
                return "Invalid start timestamp item";

            case Transaction.INVALID_TIMESTAMP_END:
                return "Invalid end timestamp";

            case Transaction.INVALID_ASSET_TYPE:
                return "Invalid asset type (not unique?)";

            case Transaction.ANONIM_OWN_DENIED:
                return "ANONIM_OWN_DENIED";

            case Transaction.MUST_CERTIFY_HERE:
                return "Need certify here";

            case Transaction.INVALID_CREATOR:
                return "Invalid creator";

            case Transaction.ITEM_PERSON_IS_DEAD:
                return "Person is Dead";

            case Transaction.INVALID_TRANSFER_TYPE:
                return "Invalid transfer type";


            case Transaction.ITEM_ASSET_NOT_EXIST:
                return "Item asset does not exist";

            case Transaction.ITEM_IMPRINT_DOES_NOT_EXIST:
                return "Item imprint does not exist";

            case Transaction.ITEM_TEMPLATE_NOT_EXIST:
                return "Item template does not exist";

            case Transaction.ITEM_PERSON_NOT_EXIST:
                return "Item person does not exist";

            case Transaction.ITEM_POLL_NOT_EXIST:
                return "Item poll does not exist";

            case Transaction.ITEM_STATUS_NOT_EXIST:
                return "Item status does not exist";

            case Transaction.ITEM_UNION_NOT_EXIST:
                return "Item union does not exist";
            case Transaction.ITEM_PERSON_LATITUDE_ERROR:
                return "Invalid birth latitude";

            case Transaction.ITEM_PERSON_LONGITUDE_ERROR:
                return "Invalid birth longitude";

            case Transaction.ITEM_PERSON_RACE_ERROR:
                return "Invalid person race";

            case Transaction.ITEM_PERSON_GENDER_ERROR:
                return "Invalid person gender";

            case Transaction.ITEM_PERSON_SKIN_COLOR_ERROR:
                return "Invalid skin color";

            case Transaction.ITEM_PERSON_EYE_COLOR_ERROR:
                return "Invalid eye color";

            case Transaction.ITEM_PERSON_HAIR_COLOR_ERROR:
                return "Invalid hair color";

            case Transaction.ITEM_PERSON_HEIGHT_ERROR:
                return "Invalid height";

            case Transaction.ACCOUNT_ALREADY_PERSONALIZED:
                return "Account already personalizes";


            case Transaction.AMOUNT_LENGHT_SO_LONG:
                return "Amount accuracy so big";

            case Transaction.AMOUNT_SCALE_SO_BIG:
                return "Amount scale so big";

            case Transaction.AMOUNT_SCALE_WRONG:
                return "Amount point scale wrong";


            case Transaction.INVALID_PACKET_SIZE:
                return "Invalid packet size";


            case Transaction.BLOCK_NOT_EXIST:
                return "Block not exist";

            case Transaction.INVALID_BLOCK_HEIGHT:
                return "Invalid block height";

            case Transaction.INVALID_BLOCK_TRANS_SEQ_ERROR:
                return "Invalid block record sequence";

            case Transaction.ACCOUNT_ACCSES_DENIED:
                return "Access denied for account";

            case Transaction.ACTION_DENIED:
                return "Access denied";

            case Transaction.KEY_COLLISION:
                return "Key collision, try again";

            case Transaction.TELEGRAM_DOES_NOT_EXIST:
                return "Telegram does not exist";


            case Transaction.INVALID_TRANSACTION_TYPE:
                return "Invalid transaction type";


            case Transaction.JSON_ERROR:
                return "Invalid JSON";

            case Transaction.PARSE_ERROR:
                return "Parse error";
            default:
                return "Unknown error: " + error;
        }
    }

}
