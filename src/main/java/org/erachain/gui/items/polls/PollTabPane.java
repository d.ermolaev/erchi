package org.erachain.gui.items.polls;

import org.erachain.core.item.ItemCls;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.polls.PollCls;
import org.erachain.gui.Gui;
import org.erachain.gui.models.ItemPollsTableModel;
import org.erachain.lang.Lang;

import javax.swing.*;

public class PollTabPane extends JTabbedPane {

    private static final long serialVersionUID = 2717571093561259483L;

    PollHeadPanel pollHeadPanel;
    JTable allVotesTable;
    private ItemPollsTableModel myVotesTableModel;
    private ItemPollsTableModel allVotesTableModel;

    @SuppressWarnings("unchecked")
    public PollTabPane(PollCls poll, ItemCls asset) {
        super();

        //POLL DETAILS
        this.pollHeadPanel = new PollHeadPanel(poll, (AssetCls) asset);
        this.addTab(Lang.T("Poll Details"), this.pollHeadPanel);

        //ALL VOTES
        allVotesTableModel = new ItemPollsTableModel();
        allVotesTable = Gui.createSortableTable(allVotesTableModel, 0);

        this.addTab(Lang.T("All Votes"), new JScrollPane(allVotesTable));

        //MY VOTES
        myVotesTableModel = new ItemPollsTableModel();
        final JTable myVotesTable = Gui.createSortableTable(myVotesTableModel, 0);

        this.addTab(Lang.T("My Votes"), new JScrollPane(myVotesTable));


    }

    public void setAsset(AssetCls asset) {
        pollHeadPanel.setAsset(asset);
        allVotesTableModel.setAsset(asset);
        myVotesTableModel.setAsset(asset);
    }

    public void close() {
        //REMOVE OBSERVERS/HANLDERS
    }

}
