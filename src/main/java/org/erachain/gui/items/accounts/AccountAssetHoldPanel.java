package org.erachain.gui.items.accounts;

import org.erachain.core.account.Account;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.transaction.TransactionAmount;

public class AccountAssetHoldPanel extends AccountAssetActionPanelCls {

    public AccountAssetHoldPanel(AssetCls assetIn, Account accountFrom, Account accountTo, PersonCls person, boolean backward) {
        super(null, null, assetIn,
                backward ? TransactionAmount.ACTION_HOLD : -TransactionAmount.ACTION_HOLD,
                accountFrom, accountTo, null);

        iconName = "AccountAssetHoldPanel";

    }

}