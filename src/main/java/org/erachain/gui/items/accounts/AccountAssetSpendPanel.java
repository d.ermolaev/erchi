package org.erachain.gui.items.accounts;

import org.erachain.core.account.Account;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.transaction.TransactionAmount;


@SuppressWarnings("serial")

public class AccountAssetSpendPanel extends AccountAssetActionPanelCls {

    public AccountAssetSpendPanel(AssetCls assetIn, Account accountFrom, Account accountTo, PersonCls person, String message, boolean backward) {
        super(null, null, assetIn,
                backward? -TransactionAmount.ACTION_SPEND : TransactionAmount.ACTION_SPEND,
                accountFrom, accountTo, message);

        iconName = "AccountAssetSpendPanel";
    }
}