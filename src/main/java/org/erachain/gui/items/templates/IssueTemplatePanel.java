package org.erachain.gui.items.templates;

import org.erachain.core.item.templates.Template;
import org.erachain.core.item.templates.TemplateCls;
import org.erachain.core.transaction.IssueTemplateRecord;
import org.erachain.gui.items.IssueItemPanel;
import org.erachain.gui.items.utils.GUIConstants;

@SuppressWarnings("serial")
public class IssueTemplatePanel extends IssueItemPanel {

    public static String NAME = "IssueTemplatePanel";
    public static String TITLE = "Issue Template";

    public IssueTemplatePanel() {
        super(NAME, TITLE, null, null, true, GUIConstants.WIDTH_IMAGE, GUIConstants.WIDTH_IMAGE, true, true);

        initComponents();

        initBottom(initTopArea(true));

        setVisible(true);
    }

    protected boolean checkValues() {
        return true;
    }

    @Override
    protected void makeTransaction() {

        item = new Template(creator, nameField.getText(), addIconLabel.getMediaBytes(), addImageLabel.getMediaBytes(),
                textAreaDescription.getText());

        transaction = new IssueTemplateRecord(creator, exLink, (TemplateCls) item, (byte) feePow, 0L);
        cnt.signAndUpdateTransaction(transaction, creator);

    }

}