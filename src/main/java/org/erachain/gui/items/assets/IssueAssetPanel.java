package org.erachain.gui.items.assets;

import org.erachain.core.exdata.exLink.ExLinkAddress;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetType;
import org.erachain.core.item.assets.AssetUnique;
import org.erachain.core.item.assets.AssetVenture;
import org.erachain.core.transaction.IssueAssetTransaction;
import org.erachain.gui.MainFrame;
import org.erachain.gui.items.utils.GUIConstants;
import org.erachain.lang.Lang;
import org.json.simple.JSONObject;

import javax.swing.*;

/**
 * @author Саша
 */
public class IssueAssetPanel extends IssueAssetPanelCls {

    public static String NAME = "IssueAssetPanel";
    public static String TITLE = "Issue Asset";

    protected final JLabel scaleJLabel = new JLabel(Lang.T("Scale") + ":");
    protected final JComboBox<String> textScale = new JComboBox<>();

    int scale;

    public IssueAssetPanel() {
        super(NAME, TITLE, null, null, true, GUIConstants.WIDTH_IMAGE, GUIConstants.WIDTH_IMAGE,
                true, true);

        textScale.setModel(new DefaultComboBoxModel<>(fillAndReceiveStringArray(24)));
        textScale.setSelectedIndex(8);

        //assetTypeJComboBox.setRenderer(new RenderComboBoxAssetActions());

        initComponents();

        // select combobox Asset type
        assetTypeJComboBox.addActionListener(e -> {
            JComboBox source = (JComboBox) e.getSource();
            refreshLabels((AssetType) source.getSelectedItem());
        });

        // set start text area asset type
        refreshLabels((AssetType) assetTypesComboBoxModel.getSelectedItem());

    }

    protected void initComponents() {

        super.initComponents();

        boolean useStartStop = true;
        // вывод верхней панели
        int gridy = super.initTopArea(useStartStop);
        if (useStartStop) {
            startCheckBox.setToolTipText(Lang.T("IssueAssetPanel.startField"));
        }

        labelGBC.gridy = gridy;
        jPanelAdd.add(typeJLabel, labelGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(assetTypeJComboBox, fieldGBC);

        textAreasAssetTypeDescription = new JTextPane();
        textAreasAssetTypeDescription.setEditable(false);
        textAreasAssetTypeDescription.setBackground(this.getBackground());
        textAreasAssetTypeDescription.setContentType("text/html");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = fieldGBC.gridx;
        gridBagConstraints.gridy = gridy;
        gridBagConstraints.gridwidth = fieldGBC.gridwidth;
        gridBagConstraints.anchor = fieldGBC.anchor;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 0.3;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = fieldGBC.insets;
        jPanelAdd.add(textAreasAssetTypeDescription, gridBagConstraints);

        ////
        labelGBC.gridy = ++gridy;
        jPanelAdd.add(quantityJLabel, labelGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(textQuantity, fieldGBC);

        labelGBC.gridy = gridy;
        jPanelAdd.add(scaleJLabel, labelGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(textScale, fieldGBC);

        // вывод подвала
        super.initBottom(gridy);
    }

    private void refreshLabels(AssetType assetType) {
        int fontSize = textScale.getFontMetrics(textScale.getFont()).getHeight();
        String fontStyle = textScale.getFont().getFontName();
        fontStyle = "<body style='font: " + (fontSize - 2) + "pt " + fontStyle + "'>";

        textAreasAssetTypeDescription.setText(fontStyle + assetType.getDescription());

        if (AssetCls.isTypeUnique(assetType.getId(), 0)) {
            textQuantity.setVisible(false);
            quantityJLabel.setVisible(false);
            textScale.setVisible(false);
            scaleJLabel.setVisible(false);
        } else {
            textQuantity.setVisible(!AssetCls.isAccounting(assetType.getId()));
            quantityJLabel.setVisible(!AssetCls.isAccounting(assetType.getId()));
            textScale.setVisible(true);
            scaleJLabel.setVisible(true);
        }

    }

    @Override
    protected boolean checkValues() {

        assetModel = ((AssetType) assetTypesComboBoxModel.getSelectedItem()).getId();

        int parseStep = 0;
        try {

            if (!AssetCls.isTypeUnique(assetModel, quantity)) {
                // READ SCALE
                scale = Byte.parseByte((String) textScale.getSelectedItem());

                // READ QUANTITY
                parseStep++;
                quantity = Long.parseLong(textQuantity.getText());
            }

        } catch (Exception e) {
            switch (parseStep) {
                case 0:
                    JOptionPane.showMessageDialog(MainFrame.getInstance(),
                            Lang.T("Invalid Scale!"), Lang.T("Error"),
                            JOptionPane.ERROR_MESSAGE);
                    break;
                case 1:
                    JOptionPane.showMessageDialog(MainFrame.getInstance(),
                            Lang.T("Invalid quantity!"), Lang.T("Error"),
                            JOptionPane.ERROR_MESSAGE);
                    break;
            }
            return false;
        }

        return true;
    }

    @Override
    protected void makeTransaction() {

        if (AssetCls.isTypeUnique(assetModel, quantity)) {
            item = new AssetUnique(assetModel, creator, creator, nameField.getText(), tagsField.getText(),
                    addIconLabel.getMediaType(), !addIconLabel.isInternalMedia(), addIconLabel.getMediaBytes(),
                    addImageLabel.getMediaType(), !addImageLabel.isInternalMedia(), addImageLabel.getMediaBytes(),
                    textAreaDescription.getText(),
                    !startCheckBox.isSelected() ? null : startField.getCalendar().getTimeInMillis(),
                    !stopCheckBox.isSelected() ? null : stopField.getCalendar().getTimeInMillis(),
                    multipleRoyaltyPanel.recipientsTableModel.getRecipients(),
                    isUnTransferable.isSelected(), isAnonimDenied.isSelected());
        } else {
            item = new AssetVenture(assetModel, creator, creator, nameField.getText(), tagsField.getText(),
                    addIconLabel.getMediaType(), !addIconLabel.isInternalMedia(), addIconLabel.getMediaBytes(),
                    addImageLabel.getMediaType(), !addImageLabel.isInternalMedia(), addImageLabel.getMediaBytes(),
                    textAreaDescription.getText(),
                    !startCheckBox.isSelected() ? null : startField.getCalendar().getTimeInMillis(),
                    !stopCheckBox.isSelected() ? null : stopField.getCalendar().getTimeInMillis(),
                    multipleRoyaltyPanel.recipientsTableModel.getRecipients(),
                    isUnTransferable.isSelected(), isAnonimDenied.isSelected(),
                    scale, quantity);
        }

        transaction = new IssueAssetTransaction(creator, exLink, (AssetCls) item, (byte) feePow, 0L);
        cnt.signAndUpdateTransaction(transaction, creator);

    }

    @Override
    protected String makeBodyView() {

        String out = super.makeBodyView();
        AssetCls asset = (AssetCls) item;
        JSONObject landObj = Lang.getInstance().getLangForNode();

        out += Lang.T("Asset Class") + ":&nbsp;"
                + Lang.T(asset.getTypeClassName() + "") + "<br>"
                + Lang.T("Asset Type") + ":&nbsp;"
                + "<b>" + asset.charAssetType() + asset.viewAssetTypeAbbrev() + "</b>:" + Lang.T(asset.viewAssetTypeFull() + "") + "<br>"
                + Lang.T("Quantity") + ":&nbsp;" + asset.getQuantity() + ", "
                + Lang.T("Scale") + ":&nbsp;" + asset.getScale() + "<br>";

        out += "<p><b>" + Lang.T("Properties") + "</b>: " + asset.viewProperties(landObj) + "</p>";

        if (asset.getDEXAwards() != null) {
            out += Lang.T("DEX royalties" + ":");
            for (ExLinkAddress award : asset.getDEXAwards()) {
                if (award == null)
                    continue;
                out += "<br>&nbsp;&nbsp;&nbsp;&nbsp;" + award.getAccount().getPersonAsString() + " <b>" + award.getValue1() * 0.001d + "%</b>"
                        + (award.getMemo() == null || award.getMemo().isEmpty() ? "" : " - " + award.getMemo());
            }
            out += "<br>";
        }

        return out;

    }

}
