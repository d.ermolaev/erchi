package org.erachain.gui.items.persons;

import com.toedter.calendar.JDateChooser;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.crypto.AEScrypto;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.persons.PersonHuman;
import org.erachain.core.transaction.IssuePersonRecord;
import org.erachain.core.transaction.Transaction;
import org.erachain.gui.items.IssueItemPanel;
import org.erachain.gui.items.utils.GUIConstants;
import org.erachain.gui.library.RecipientAddress;
import org.erachain.gui.transaction.OnDealClick;
import org.erachain.lang.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Base64;
import java.util.Calendar;
import java.util.TimeZone;

import static org.erachain.gui.items.utils.GUIUtils.checkWalletUnlock;

@SuppressWarnings("serial")
public class IssuePersonPanel extends IssueItemPanel implements RecipientAddress.RecipientAddressInterface {

    private static final Logger logger = LoggerFactory.getLogger(IssuePersonPanel.class);
    public static String NAME = "IssuePersonPanel";
    public static String TITLE = "Issue Person";
    private final JLabel jLabelBirthLatitudeLongtitude = new JLabel(Lang.T("Coordinates of Birth") + ":");
    private final JLabel jLabelBirthday = new JLabel(Lang.T("Birthday") + ":");
    private final JLabel jLabelEyeColor = new JLabel(Lang.T("Eye color") + ":");
    private final JLabel jLabelGender = new JLabel(Lang.T("Gender") + ":");
    private final JLabel jlabelhairColor = new JLabel(Lang.T("Hair color") + ":");
    private final JLabel jLabelHeight = new JLabel(Lang.T("Growth") + ":");
    protected JDateChooser txtBirthday;
    protected RecipientAddress registrarAddress;
    protected JLabel registrarAddressDesc = new JLabel();
    protected JDateChooser txtDeathDay;
    protected JComboBox<String> comboBoxGender = new JComboBox<>();
    //protected JTextField textPersonNumber = new JTextField();
    protected JTextField txtBirthLatitude = new JTextField("0.0, 0.0");
    protected JTextField txtBirthLongitudeLatitude = new JTextField("0");
    protected JTextField txtSkinColor = new JTextField();
    protected JTextField txtEyeColor = new JTextField();
    protected JTextField txtHairColor = new JTextField();
    protected JTextField txtHeight = new JTextField("170");
    protected JLabel jLabelRegistrarAddress = new JLabel(Lang.T("Registrar") + ":");
    protected JLabel jLabelDead = new JLabel(Lang.T("Deathday") + ":");
    protected JPanel jPanelHead = new JPanel();
    protected JCheckBox aliveCheckBox = new JCheckBox(Lang.T("Alive"), true);
    boolean forIssue = false;
    byte gender;
    long birthday;
    long deathday;
    float birthLatitude;
    float birthLongitude;
    int height;
    PublicKeyAccount registrar;

    public IssuePersonPanel() {
        this(NAME, TITLE);
    }

    public IssuePersonPanel(String name, String title) {
        super(name, title, null, null, false, GUIConstants.WIDTH_IMAGE, GUIConstants.HEIGHT_IMAGE, false, false);
        initComponents(true);
        initLabels();

    }

    public IssuePersonPanel(String name, String title, String issueMess) {
        super(name, title, null, issueMess, false, GUIConstants.WIDTH_IMAGE, GUIConstants.HEIGHT_IMAGE, false, false);
    }

    protected void initLabels() {
        txtDeathDay.setVisible(false);
        jLabelDead.setVisible(false);
        aliveCheckBox.addActionListener(arg0 -> {
            if (aliveCheckBox.isSelected()) {
                txtDeathDay.setVisible(false);
                jLabelDead.setVisible(false);
            } else {
                txtDeathDay.setVisible(true);
                jLabelDead.setVisible(true);
            }
        });

        String[] items = PersonHuman.GENDERS_LIST;
        items = Lang.T(items);
        comboBoxGender.setModel(new DefaultComboBoxModel<>(items));
        comboBoxGender.setSelectedIndex(2);
        setVisible(true);

    }

    protected int initComponents(boolean andBottom) {
        super.initComponents();


        registrarAddress = new RecipientAddress(this);

        exLinkTextLabel.setVisible(!andBottom);
        exLinkText.setVisible(!andBottom);
        exLinkDescriptionLabel.setVisible(!andBottom);
        exLinkDescription.setVisible(!andBottom);
        registrarAddressDesc.setVisible(andBottom);
        registrarAddress.setVisible(andBottom);
        jLabelRegistrarAddress.setVisible(andBottom);

        addImageLabel.setEditable(andBottom);
        addIconLabel.setEditable(andBottom);

        // вывод верхней панели
        int gridy = super.initTopArea(false);

        issueJButton.setText(Lang.T("Create and copy to clipboard"));
        //issueJButton.addActionListener(e -> onIssueClick());

        // SET ONE TIME ZONE for Birthday
        TimeZone tz = TimeZone.getDefault();
        //TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        txtBirthday = new JDateChooser("yyyy-MM-dd HH:mm 'UTC'", "####-##-## ##:##", '_');
        Calendar calendar = Calendar.getInstance(tz);
        calendar.set(1990, Calendar.NOVEMBER, 11, 12, 13, 1);
        txtBirthday.setCalendar(calendar);
        txtDeathDay = new JDateChooser("yyyy-MM-dd HH:mm 'UTC'", "####-##-## ##:##", '_');
        //TimeZone.setDefault(tz);

        txtBirthday.setFont(UIManager.getFont("TextField.font"));
        txtDeathDay.setFont(UIManager.getFont("TextField.font"));

        // gender
        labelGBC.gridy = gridy;
        jPanelAdd.add(jLabelGender, labelGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(comboBoxGender, fieldGBC);

        // born
        labelGBC.gridy = gridy;
        jPanelAdd.add(jLabelBirthday, labelGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(txtBirthday, fieldGBC);

        // dead
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = gridy++;
        gridBagConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new Insets(0, 10, 0, 0);
        jPanelAdd.add(aliveCheckBox, gridBagConstraints);

        labelGBC.gridy = gridy;
        jPanelAdd.add(jLabelDead, labelGBC);
        fieldGBC.gridy = gridy++;
        jPanelAdd.add(txtDeathDay, fieldGBC);

        //BirthLatitude
        labelGBC.gridy = gridy;
        jPanelAdd.add(jLabelBirthLatitudeLongtitude, labelGBC);
        fieldGBC.gridy = gridy++;
        jPanelAdd.add(txtBirthLatitude, fieldGBC);

        //HairСolor
        labelGBC.gridy = gridy;
        //jPanelAdd.add(jlabelhairColor, labelGBC);
        fieldGBC.gridy = gridy++;
        //jPanelAdd.add(txtHairColor, fieldGBC);

        // EyeColor
        labelGBC.gridy = gridy;
        //jPanelAdd.add(jLabelEyeColor, labelGBC);
        fieldGBC.gridy = gridy++;
        //jPanelAdd.add(txtEyeColor, fieldGBC);

        // Height
        labelGBC.gridy = gridy;
        //jPanelAdd.add(jLabelHeight, labelGBC);
        fieldGBC.gridy = gridy++;
        //jPanelAdd.add(txtHeight, fieldGBC);

        // registrar address
        labelGBC.gridy = gridy;
        jPanelAdd.add(jLabelRegistrarAddress, labelGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(registrarAddress, fieldGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(registrarAddressDesc, fieldGBC);

        /* Added Copy, Paste in GEO (by Samartsev. 18.03.2019) */
        JPopupMenu popup = new JPopupMenu();
        txtBirthLatitude.add(popup);
        txtBirthLatitude.setComponentPopupMenu(popup);

        JMenuItem jMenuItemCopy = new JMenuItem(Lang.T("Копировать"), KeyEvent.VK_C);
        jMenuItemCopy.setMnemonic(KeyEvent.VK_C);
        jMenuItemCopy.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_C, InputEvent.CTRL_MASK));

        JMenuItem jMenuItemPaste = new JMenuItem(Lang.T("Вставить"), KeyEvent.VK_V);
        jMenuItemPaste.setMnemonic(KeyEvent.VK_V);
        jMenuItemPaste.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_V, InputEvent.CTRL_MASK));

        popup.add(jMenuItemCopy);
        popup.add(jMenuItemPaste);
        jMenuItemCopy.addActionListener(e -> {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Clipboard clipboard = toolkit.getSystemClipboard();
            StringSelection coordString = new StringSelection(txtBirthLatitude.getText());
            clipboard.setContents(coordString, null);
        });
        jMenuItemPaste.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                Transferable transferable = clipboard.getContents(this);
                if (transferable == null) {
                    return;
                }
                try {
                    String dataBaseXX = (String) transferable.getTransferData(DataFlavor.stringFlavor);
                    dataBaseXX = dataBaseXX.trim();
                    dataBaseXX = dataBaseXX.replaceAll("\n", "");
                    txtBirthLatitude.setText(dataBaseXX);
                } catch (Exception exception) {
                    logger.error("Error menu paste", exception);
                }
            }
        });

        if (andBottom) {
            // вывод подвала
            super.initBottom(gridy);
        }

        return gridy;

    }

    protected void reset() {
        nameField.setText("");
        textAreaDescription.setText("");
        addImageLabel.reset();
    }

    protected boolean checkValues() {

        TimeZone tz = TimeZone.getDefault();
        try {

            int parse = 0;
            try {
                // READ GENDER
                parse++;
                gender = (byte) (comboBoxGender.getSelectedIndex());
                parse++;
                // SET TIMEZONE to UTC-0
                TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
                birthday = txtBirthday.getCalendar().getTimeInMillis();
                parse++;
                // END DATE
                try {
                    deathday = txtDeathDay.getCalendar().getTimeInMillis();
                } catch (Exception ed1) {
                    deathday = birthday - 1;
                }
                if (aliveCheckBox.isSelected()) {
                    deathday = birthday - 1;
                }
                parse++;
                String[] latitudeLongitude = txtBirthLatitude.getText().split(",");
                birthLatitude = Float.parseFloat(latitudeLongitude[0]);
                parse++;
                birthLongitude = Float.parseFloat(latitudeLongitude[1]);
                parse++;
                height = Integer.parseInt(txtHeight.getText());

            } catch (Exception e) {
                String mess = "Invalid pars... " + parse;
                switch (parse) {
                    case 1:
                        mess = "Invalid gender";
                        break;
                    case 2:
                        mess = "Invalid birthday [YYYY-MM-DD] or [YYYY-MM-DD hh:mm:ss]";
                        break;
                    case 3:
                        mess = "Invalid deathday [YYYY-MM-DD] or [YYYY-MM-DD hh:mm:ss]";
                        break;
                    case 4:
                        mess = "Invalid Coordinates of Birth, example: 43.123032, 131.917828";
                        break;
                    case 5:
                        mess = "Invalid Coordinates of Birth, example: 43.123032, 131.917828";
                        break;
                    case 6:
                        mess = "Invalid growth 10..255";
                        break;
                }
                JOptionPane.showMessageDialog(new JFrame(), Lang.T(mess),
                        Lang.T("Error"), JOptionPane.ERROR_MESSAGE);
                return false;
            }
            return true;
        } finally {
            TimeZone.setDefault(tz);
        }
    }

    @Override
    protected void makeTransaction() {
        item = new PersonHuman(creator, creator, creator, nameField.getText(), tagsField.getText(),
                addIconLabel.getMediaType(), !addIconLabel.isInternalMedia(), addIconLabel.getMediaBytes(),
                addImageLabel.getMediaType(), !addImageLabel.isInternalMedia(), addImageLabel.getMediaBytes(),
                textAreaDescription.getText(),
                txtBirthday.getCalendar().getTimeInMillis(),
                aliveCheckBox.isSelected() ? null : txtDeathDay.getCalendar().getTimeInMillis(),
                gender, "", birthLatitude, birthLongitude, txtSkinColor.getText(), txtEyeColor.getText(), txtHairColor.getText(), // gen / hei
                height, null);

        if (forIssue) {
            transaction = new IssuePersonRecord(creator, exLink, false, (PersonHuman) item, (byte) feePow, 0L);
            cnt.signAndUpdateTransaction(transaction, creator);
        }

    }

    @Override
    public void onIssueClick() {

        if (forIssue) {
            super.onIssueClick();
            return;
        }

        if (!checkValues())
            return;

        issueJButton.setEnabled(false);
        if (checkWalletUnlock(issueJButton)) {
            issueJButton.setEnabled(true);
            return;
        }

        // READ CREATOR
        Account creatorAccount = (Account) fromJComboBox.getSelectedItem();
        creator = cnt.getWalletPrivateKeyAccountByAddress(creatorAccount.getAddress());
        if (creator == null) {
            JOptionPane.showMessageDialog(new JFrame(),
                    Lang.T(OnDealClick.resultMess(Transaction.PRIVATE_KEY_NOT_FOUND)),
                    Lang.T("Error"), JOptionPane.ERROR_MESSAGE);
            issueJButton.setEnabled(true);
            return;
        }


        makeTransaction();

        PersonHuman personHuman = (PersonHuman) item;
        //personHuman.sm
        // SIGN
        personHuman.sign(creator);
        byte[] issueBytes = personHuman.toBytes(false);
        String base64str = Base64.getEncoder().encodeToString(issueBytes);
        if (registrar == null) {
            // copy to clipBoard

            // This method writes a string to the system clipboard.
            // otherwise it returns null.
            StringSelection stringSelection = new StringSelection(base64str);
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
            JOptionPane.showMessageDialog(new JFrame(),
                    Lang.T("Person bytecode has been copy to buffer") + "!",
                    Lang.T("Success"), JOptionPane.INFORMATION_MESSAGE);
        } else {
            // send telegram
            byte[] encryptedBytes = AEScrypto.dataEncrypt(issueBytes, creator.getPrivateKey(), registrar.getPublicKey());

            Transaction transaction = cnt.r_Send(
                    creator, "Person bytecode", null, encryptedBytes, new byte[1], new byte[]{1},
                    null, null, feePow, registrar, 0, null, 0L
            );

            cnt.broadcastTelegram(transaction, true);
            JOptionPane.showMessageDialog(new JFrame(),
                    Lang.T("Person bytecode has been send to Registrar") + "!",
                    Lang.T("Success"), JOptionPane.INFORMATION_MESSAGE);

        }

        // ENABLE
        issueJButton.setEnabled(true);
    }

    private void refreshReceiverDetails(String registrarStr) {

        //Account
        this.registrarAddressDesc.setText(Lang.T(
                Account.getDetailsForEncrypt(registrarStr, BlockChain.FEE_ASSET, true, true)));

        registrar = null;
        if (registrarStr != null && !registrarStr.isEmpty()) {
            if (Account.isValidAddress(registrarStr)) {
                byte[] pubKey = cnt.getPublicKeyByAddress(registrarStr);
                if (pubKey == null) {
                    registrar = null;
                } else {
                    registrar = new PublicKeyAccount(Crypto.ED25519_SYSTEM, pubKey);
                }
            } else {
                if (PublicKeyAccount.isValidPublicKey(registrarStr)) {
                    registrar = new PublicKeyAccount(registrarStr);
                }
            }
        }

        if (registrar == null) {
            issueJButton.setText(Lang.T("Create and copy to clipboard"));
        } else {
            issueJButton.setText(Lang.T("Create and send to Registrar"));
        }
    }

    // выполняемая процедура при изменении адреса получателя
    @Override
    public void recipientAddressWorker(String e) {
        refreshReceiverDetails(e);
    }
}