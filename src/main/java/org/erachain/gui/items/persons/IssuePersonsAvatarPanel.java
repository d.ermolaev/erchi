package org.erachain.gui.items.persons;

import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.item.persons.PersonsAvatar;
import org.erachain.core.transaction.IssuePersonRecord;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;
import org.erachain.gui.MainFrame;
import org.erachain.gui.items.IssueItemPanel;
import org.erachain.gui.items.utils.GUIConstants;
import org.erachain.gui.library.RecipientAddress;
import org.erachain.gui.transaction.OnDealClick;
import org.erachain.lang.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

@SuppressWarnings("serial")
public class IssuePersonsAvatarPanel extends IssueItemPanel implements RecipientAddress.RecipientAddressInterface {

    private static final Logger logger = LoggerFactory.getLogger(IssuePersonsAvatarPanel.class);
    public static String NAME = "IssuePersonsAvatarPanel";
    public static String TITLE = "Issue Avatar";

    protected JLabel jLabelInviterAccount = new JLabel(Lang.T("IssuePersonsAvatarPanel.inviterAccountField") + ":");
    protected RecipientAddress inviterAccountField;
    protected JLabel inviterAccountDesc = new JLabel();
    PublicKeyAccount inviterPubKey;

    protected JLabel jLabelCertifyAccount = new JLabel(Lang.T("IssuePersonsAvatarPanel.certifyOnAccountField") + ":");
    protected RecipientAddress certifyOnAccountField;
    protected JLabel certifyOnAccountDesc = new JLabel();
    PublicKeyAccount certifyOnPubKey;


    public IssuePersonsAvatarPanel() {
        super(NAME, TITLE, null, null, true,
                GUIConstants.WIDTH_IMAGE, GUIConstants.HEIGHT_IMAGE, false, false);

        initComponents();

    }

    protected void initComponents() {
        super.initComponents();

        boolean useStartStop = true;
        int gridy = super.initTopArea(useStartStop);

        validityPeriodLabel.setVisible(false);
        stopCheckBox.setVisible(false);
        stopField.setVisible(false);

        certifyOnAccountField = new RecipientAddress(this);
        inviterAccountField = new RecipientAddress(this);


        if (useStartStop) {
            startCheckBox.setText(Lang.T("IssuePersonsAvatarPanel.startField"));
            startCheckBox.setToolTipText(Lang.T("IssuePersonsAvatarPanel.startFieldTip"));
            startCheckBox.setSelected(true);
            startField.setEnabled(true);
        }

        labelGBC.gridy = ++gridy;
        jPanelAdd.add(jLabelInviterAccount, labelGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(inviterAccountField, fieldGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(inviterAccountDesc, fieldGBC);

        labelGBC.gridy = ++gridy;
        jPanelAdd.add(jLabelCertifyAccount, labelGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(certifyOnAccountField, fieldGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(certifyOnAccountDesc, fieldGBC);

        // вывод подвала
        super.initBottom(gridy);

    }

    protected void reset() {
        nameField.setText("");
        textAreaDescription.setText("");
        addIconLabel.reset();
        addImageLabel.reset();
    }

    @Override
    protected boolean checkValues() {

        certifyOnPubKey = null;
        inviterPubKey = null;

        String address = certifyOnAccountField.getSelectedAddress();
        if (address != null && !address.isEmpty()) {
            try {
                certifyOnPubKey = PublicKeyAccount.parse(address);
                this.certifyOnAccountDesc.setText(certifyOnPubKey.toString());
                if (certifyOnPubKey.isPerson()) {
                    JOptionPane.showMessageDialog(MainFrame.getInstance(),
                            Lang.T(OnDealClick.resultMess(Transaction.ACCOUNT_ALREADY_PERSONALIZED)), Lang.T("Error"),
                            JOptionPane.ERROR_MESSAGE);

                    return false;
                }
            } catch (TxException e) {
                this.certifyOnAccountDesc.setText(Lang.T(OnDealClick.resultMess(Transaction.INVALID_PUBLIC_KEY))
                        + " - " + e.getMessage());

                JOptionPane.showMessageDialog(MainFrame.getInstance(),
                        Lang.T(OnDealClick.resultMess(Transaction.INVALID_CREATOR_ADDRESS)), Lang.T("Error"),
                        JOptionPane.ERROR_MESSAGE);

                return false;
            }

        }

        address = inviterAccountField.getSelectedAddress();
        if (address != null && !address.isEmpty()) {
            try {
                inviterPubKey = PublicKeyAccount.parse(address);
                this.inviterAccountDesc.setText(inviterPubKey.toString());
            } catch (TxException e) {
                this.inviterAccountDesc.setText(Lang.T(OnDealClick.resultMess(Transaction.INVALID_PUBLIC_KEY))
                        + " - " + e.getMessage());

                JOptionPane.showMessageDialog(MainFrame.getInstance(),
                        Lang.T(OnDealClick.resultMess(Transaction.INVALID_CREATOR_ADDRESS)), Lang.T("Error"),
                        JOptionPane.ERROR_MESSAGE);

                return false;
            }

        }

        return true;
    }

    @Override
    protected String makeBodyView() {

        String out = super.makeBodyView();

        PublicKeyAccount inviter = ((PersonCls) item).getInviter();
        if (inviter != null)
            out += Lang.T("Inviter") + ":&nbsp;<b>" + inviter.getPersonAsString() + "</b><br>";

        return out;
    }


    @Override
    protected void makeTransaction() {

        if (certifyOnPubKey == null)
            certifyOnPubKey = creator;

        item = new PersonsAvatar(inviterPubKey, certifyOnPubKey, certifyOnPubKey, nameField.getText(), tagsField.getText(),
                0, false, addIconLabel.getMediaBytes(), 0, false, addImageLabel.getMediaBytes(),
                textAreaDescription.getText(), !startCheckBox.isSelected() ? null : startField.getCalendar().getTimeInMillis(), null);

        transaction = new IssuePersonRecord(creator, exLink, true, (PersonCls) item, (byte) feePow, 0L);
        cnt.signAndUpdateTransaction(transaction, creator);

    }

    @Override
    public void recipientAddressWorker(String e) {
        checkValues();
    }
}