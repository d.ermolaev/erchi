package org.erachain.gui.items.persons;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.core.account.Account;
import org.erachain.core.crypto.Base58;
import org.erachain.core.exdata.exLink.ExLinkAppendix;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.item.persons.PersonFactory;
import org.erachain.core.item.persons.PersonHuman;
import org.erachain.core.transaction.IssuePersonRecord;
import org.erachain.core.transaction.Transaction;
import org.erachain.gui.library.MButton;
import org.erachain.gui.transaction.OnDealClick;
import org.erachain.lang.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.Base64;
import java.util.Date;
import java.util.TimeZone;

import static org.erachain.gui.items.utils.GUIUtils.checkWalletUnlock;

public class InsertPersonHumanPanel extends IssuePersonPanel {

    private static final Logger logger = LoggerFactory.getLogger(InsertPersonHumanPanel.class);
    private static final long serialVersionUID = 1L;
    public static String NAME = "InsertPersonHumanPanel";
    public static String TITLE = "Insert Person";
    private final JTextField txtSign = new JTextField();
    private final JTextField txtPublicKey = new JTextField();
    private final JLabel labelSign = new JLabel();
    private final JLabel labelPublicKey = new JLabel();
    protected MButton pasteButton;

    private MButton insertButton;
    private MButton insertAndCertifyButton;

    private boolean andCertify;

    public InsertPersonHumanPanel() {
        super(NAME, TITLE, "Person issue has been sent!");
        forIssue = true;

        init();

        // нужно чтобы нельзя было случайно вставить по Ctrl-C в это поле чего угодно
        txtBirthLatitude.removeAll();
        txtBirthLatitude.setComponentPopupMenu(null);

        issueJButton.setVisible(false);
        aliveCheckBox.setVisible(false);
    }

    public String getClipboardContents() {
        String result = "";
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable contents = clipboard.getContents(null);
        boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
        if (hasTransferableText) {
            try {
                result = (String) contents.getTransferData(DataFlavor.stringFlavor);
                result = result.trim().replaceAll("\n", "");
            } catch (Exception e) {
                logger.error("Clipboard error", e);
            }
        }
        return result;
    }

    private void init() {

        int gridy = super.initComponents(false);
        initLabels();

        exLinkTextLabel.setVisible(true);
        exLinkText.setVisible(true);

        txtBirthLatitude.setText("");
        txtBirthLongitudeLatitude.setText("");
        txtHeight.setText("");
        textFeePow.setSelectedItem("0");
        nameField.setEditable(false);
        textAreaDescription.setEditable(false);
        txtBirthday.setEnabled(false);
        txtDeathDay.setEnabled(false);

        registrarAddress.setEnabled(false);
        registrarAddressDesc.setEnabled(false);

        addImageLabel.setEnabled(false);
        comboBoxGender.setEnabled(false);
        txtBirthLatitude.setEditable(false);
        txtBirthLongitudeLatitude.setEditable(false);
        txtSkinColor.setEditable(false);
        txtEyeColor.setEditable(false);
        txtHairColor.setEditable(false);
        txtHeight.setEditable(false);

        txtPublicKey.setEditable(false);

        labelSign.setText(Lang.T("Signature") + ":");
        labelGBC.gridy = ++gridy;
        jPanelAdd.add(labelSign, labelGBC);

        fieldGBC.gridy = gridy++;
        txtSign.setEditable(false);
        jPanelAdd.add(txtSign, fieldGBC);

        labelPublicKey.setText(Lang.T("Public key") + ":");
        labelGBC.gridy = gridy;
        jPanelAdd.add(labelPublicKey, labelGBC);

        fieldGBC.gridy = gridy++;
        txtPublicKey.setEditable(false);
        jPanelAdd.add(txtPublicKey, fieldGBC);

        pasteButton = new MButton(Lang.T("Paste Person from clipboard"), 2);
        pasteButton.addActionListener(arg0 -> {
            String baseXXstr = getClipboardContents();
            try {
                byte[] dataPerson = Base58.decode(baseXXstr);
                setByteCode(dataPerson);
            } catch (Exception ee) {
                try {
                    byte[] dataPerson = Base64.getDecoder().decode(baseXXstr);
                    setByteCode(dataPerson);
                } catch (Exception eee) {
                    JOptionPane.showMessageDialog(null, ee.getMessage(), Lang.T("Error"),
                            JOptionPane.ERROR_MESSAGE);
                }
            }

        });

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(pasteButton, fieldGBC);

        insertButton = new MButton(Lang.T("Check person and insert"), 2);
        insertButton.addActionListener(arg0 -> {
            insert(false);
        });

        insertAndCertifyButton = new MButton(Lang.T("Check person, insert and certify pubkey"), 2);
        insertAndCertifyButton.addActionListener(arg0 -> {
            insert(true);
        });

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(insertButton, fieldGBC);

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(insertAndCertifyButton, fieldGBC);

        super.initBottom(gridy);

    }

    @Override
    protected void makeTransaction() {

        transaction = new IssuePersonRecord(creator, exLink, andCertify, (PersonCls) item, (byte) feePow, 0L);
        cnt.signAndUpdateTransaction(transaction, creator);

    }

    private void insert(boolean andCertify) {
        if (item == null) {
            return;
        }

        if (true) {
            this.andCertify = andCertify;
            super.onIssueClick();
            return;
        }

        if (checkWalletUnlock()) {
            return;
        }

        // READ CREATOR
        Account creatorAccount = (Account) fromJComboBox.getSelectedItem();

        int feePow;
        try {
            // READ FEE POW
            feePow = Integer.parseInt((String) textFeePow.getSelectedItem());
        } catch (Exception e) {
            String mess = "Invalid fee power 0..6";
            JOptionPane.showMessageDialog(new JFrame(), Lang.T(mess),
                    Lang.T("Error"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        PrivateKeyAccount creator = cnt
                .getWalletPrivateKeyAccountByAddress(creatorAccount.getAddress());

        if (creator == null) {
            JOptionPane.showMessageDialog(new JFrame(),
                    Lang.T(OnDealClick.resultMess(Transaction.PRIVATE_KEY_NOT_FOUND)),
                    Lang.T("Error"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        Long linkRef = Transaction.parseDBRef(exLinkText.getText());
        if (linkRef != null) {
            exLink = new ExLinkAppendix(linkRef);
        }

        // TODO добавить Имя, метки, описание к самой транзакции
        tagsField.getText(); // и так далее

        transaction = new IssuePersonRecord(creator, exLink, andCertify, (PersonCls) item, (byte) feePow, 0L);
        cnt.signAndUpdateTransaction(transaction, creator);
        
    }

    public void setByteCode(byte[] dataPerson) {
        reset();

        if (dataPerson == null) {
            item = null;
            return;
        }


        try {
            item = PersonFactory.getInstance().parse(0, dataPerson, false);
        } catch (Exception ee) {
            JOptionPane.showMessageDialog(null, ee.getMessage(), Lang.T("Error"),
                    JOptionPane.ERROR_MESSAGE);
            return;
        }


        nameField.setText(item.getName());
        tagsField.setText(item.getTags());
        addImageLabel.set(item.getImage());
        //addIconLabel.set(item.getIcon());

        PersonHuman human = (PersonHuman) item;
        // SET ONE TIME ZONE for Birthday
        TimeZone tz = TimeZone.getDefault();
        try {
            TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

            if (human.hasStartDate())
                txtBirthday.setDate(new Date(human.getStartDate()));
            if (human.hasStopDate()) {
                txtDeathDay.setDate(new Date(human.getStopDate()));
                txtDeathDay.setVisible(true);
                jLabelDead.setVisible(true);
            } else {
                txtDeathDay.setVisible(false);
                jLabelDead.setVisible(false);
            }
        } finally {
            TimeZone.setDefault(tz);
        }

        textAreaDescription.setText(human.getDescription() == null ? "" : human.getDescription());

        comboBoxGender.setSelectedIndex(human.getGender());

        txtBirthLatitude.setText("" + human.getBirthLatitude() + ", " + human.getBirthLongitude());
        if (human.getSkinColor() != null) {
            txtSkinColor.setText(human.getSkinColor());
        }
        if (human.getEyeColor() != null) {
            txtEyeColor.setText(human.getEyeColor());
        }
        if (human.getHairColor() != null) {
            txtHairColor.setText(human.getHairColor());
        }
        txtHeight.setText("" + human.getHumanHeight());

        txtSign.setText(
                human.isSignatureValid() ? Base58.encode(human.getAuthorSignature())
                        : Lang.T("Wrong signature for data maker"));
        txtPublicKey.setText(Base58.encode(human.getAuthor().getPublicKey()));

    }

}
