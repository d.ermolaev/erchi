package org.erachain.gui.items.statuses;

import org.erachain.core.item.statuses.Status;
import org.erachain.core.item.statuses.StatusCls;
import org.erachain.core.transaction.IssueStatusRecord;
import org.erachain.gui.items.IssueItemPanel;
import org.erachain.gui.items.utils.GUIConstants;
import org.erachain.lang.Lang;

import javax.swing.*;
import java.awt.*;

public class IssueStatusPanel extends IssueItemPanel {

    public static String NAME = "IssueStatusPanel";
    public static String TITLE = "Issue Status";

    private final JCheckBox checkUnique;
    boolean unique;

    public IssueStatusPanel() {
        super(NAME, TITLE, null, null, true, GUIConstants.WIDTH_IMAGE, GUIConstants.WIDTH_IMAGE, true, true);

        initComponents();

        // вывод верхней панели
        int gridy = initTopArea(true);

        JLabel singleLabel = new JLabel(Lang.T("Single") + ":");
        labelGBC.gridy = gridy;
        jPanelAdd.add(singleLabel, labelGBC);

        checkUnique = new JCheckBox();
        GridBagConstraints gbcJCheckUnique = new GridBagConstraints();
        gbcJCheckUnique.gridx = 8;
        //gbcJCheckUnique.gridy = gridy++;
        gbcJCheckUnique.anchor = GridBagConstraints.NORTHEAST;

        fieldGBC.gridy = gridy++;
        jPanelAdd.add(checkUnique, fieldGBC);

        // вывод подвала
        initBottom(gridy);

        setVisible(true);
    }

    protected boolean checkValues() {

        unique = checkUnique.isSelected();
        return true;
    }

    @Override
    protected void makeTransaction() {

        item = new Status(creator, nameField.getText(), addIconLabel.getMediaBytes(), addImageLabel.getMediaBytes(),
                textAreaDescription.getText(), unique);

        transaction = new IssueStatusRecord(creator, exLink, (StatusCls) item, (byte) feePow, 0L);
        cnt.signAndUpdateTransaction(transaction, creator);

    }

    @Override
    protected String makeBodyView() {

        String out = super.makeBodyView();

        StatusCls status = (StatusCls) item;
        out += Lang.T("Unique") + ": " + status.isUnique()
                + "<br>";

        return out;

    }

}
