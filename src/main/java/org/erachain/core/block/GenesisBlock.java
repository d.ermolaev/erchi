package org.erachain.core.block;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import org.apache.commons.net.util.Base64;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.crypto.Base58;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetVenture;
import org.erachain.core.item.statuses.Status;
import org.erachain.core.item.statuses.StatusCls;
import org.erachain.core.item.templates.Template;
import org.erachain.core.item.templates.TemplateCls;
import org.erachain.core.transaction.*;
import org.erachain.datachain.DCSet;
import org.erachain.settings.Settings;
import org.json.simple.JSONArray;
import org.mapdb.Fun.Tuple2;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GenesisBlock extends Block {

    public static final PublicKeyAccount CREATOR = new PublicKeyAccount(Crypto.ED25519_SYSTEM, new byte[PublicKeyAccount.PUBLIC_KEY_LENGTH]);
    private static final byte[] itemAppData = null;
    private static final int GENESIS_VERSION = 0;
    private static final byte[] GENESIS_REFERENCE = Bytes.ensureCapacity(new byte[]{19, 66, 8, 21, 0, 0, 0, 0}, Crypto.SIGNATURE_LENGTH, 0);
    private static final byte[] icon = new byte[0];
    private static final byte[] image = new byte[0];
    private final long GENESIS_TIMESTAMP = Settings.getInstance().getGenesisStamp();
    private String testnetInfo;
    private String sideSettingString;

    public GenesisBlock() {

        super(GENESIS_VERSION, GENESIS_REFERENCE, CREATOR);

        Account recipient;
        BigDecimal bdAmount0;
        BigDecimal bdAmount1;

        // ISSUE ITEMS
        this.initItems();

        if (BlockChain.DEMO_MODE) {
        } else if (BlockChain.TEST_MODE) {
            this.testnetInfo = "";

            //ADD TESTNET GENESIS TRANSACTIONS
            this.testnetInfo += "\ngenesisStamp: " + GENESIS_TIMESTAMP;

            byte[] seed = Crypto.getInstance().digest(Longs.toByteArray(GENESIS_TIMESTAMP));

            this.testnetInfo += "\ngenesisSeed: " + Base58.encode(seed);

            this.testnetInfo += "\nStart the other nodes with command" + ":";
            this.testnetInfo += "\njava -Xms512m -Xmx1024m -jar erachain.jar -testnet=" + GENESIS_TIMESTAMP;

        } else if (BlockChain.CLONE_MODE) {

            sideSettingString = "";
            sideSettingString += Settings.genesisJSON.get(0).toString();
            sideSettingString += Settings.genesisJSON.get(1).toString();

            Account leftRecipiend = null;
            BigDecimal totalSended = BigDecimal.ZERO;
            JSONArray holders = (JSONArray) Settings.genesisJSON.get(2);
            if (!Settings.ERA_COMPU_ALL_UP) {
                for (int i = 0; i < holders.size(); i++) {
                    JSONArray holder = (JSONArray) holders.get(i);

                    sideSettingString += holder.get(0).toString();
                    sideSettingString += holder.get(1).toString();

                    // SEND FONDs
                    Account founder = null;
                    try {
                        founder = Account.parse(holder.get(0).toString());
                    } catch (TxException e) {
                        throw new RuntimeException(e);
                    }
                    if (leftRecipiend == null) {
                        leftRecipiend = founder;
                    }

                    BigDecimal fondAamount = new BigDecimal(holder.get(1).toString()).setScale(AssetCls.ERA_SCALE);
                    transactions.add(new GenesisTransferAssetTransaction(founder,
                            AssetCls.ERA_KEY, fondAamount, "genesis holder", null, null));

                    totalSended = totalSended.add(fondAamount);

                    if (holder.size() < 3)
                        continue;

                    String COMPUstr = holder.get(2).toString();
                    if (COMPUstr.length() > 0 && !COMPUstr.equals("0")) {
                        BigDecimal compu = new BigDecimal(COMPUstr).setScale(AssetCls.FEE_SCALE);
                        transactions.add(new GenesisTransferAssetTransaction(founder,
                                AssetCls.FEE_KEY, compu, "genesis holder", null, null));
                        sideSettingString += compu.toString();
                    }

                    if (holder.size() < 4)
                        continue;

                    // DEBTORS
                    // AS List - for use Arrays.asList() in init debtors in holders

                    List debtors = (List) holder.get(3);
                    BigDecimal totalCredit = BigDecimal.ZERO;
                    for (int j = 0; j < debtors.size(); j++) {
                        List debtor = (List) debtors.get(j);

                        BigDecimal creditAmount = new BigDecimal(debtor.get(0).toString()).setScale(AssetCls.ERA_SCALE);
                        if (totalCredit.add(creditAmount).compareTo(fondAamount) > 0) {
                            break;
                        }

                        sideSettingString += creditAmount.toString();
                        sideSettingString += debtor.get(1).toString();

                        try {
                            transactions.add(new GenesisTransferAssetTransaction(
                                    Account.parse(debtor.get(1).toString()),
                                    AssetCls.ERA_KEY,
                                    creditAmount, founder, "genesis forger", null, null));
                        } catch (TxException e) {
                            throw new RuntimeException(e);
                        }

                        totalCredit = totalCredit.add(creditAmount);
                    }
                }

                if (totalSended.compareTo(new BigDecimal(BlockChain.GENESIS_ERA_TOTAL)) < 0) {
                    // ADJUST end
                    transactions.add(new GenesisTransferAssetTransaction(
                            leftRecipiend, AssetCls.ERA_KEY,
                            new BigDecimal(BlockChain.GENESIS_ERA_TOTAL).subtract(totalSended).setScale(AssetCls.ERA_SCALE),
                            "genesis main holder", null, null));
                }
            }

        } else {

            /// MAIN MODE

            List<Tuple2<Account, BigDecimal>> sends_toUsers = new ArrayList<Tuple2<Account, BigDecimal>>();

            /*
             */
            ///////// GENEGAL
            List<List<Object>> generalGenesisUsers = Collections.singletonList(
                    Arrays.asList("acc:4LC59xDq6a1aPzRWyoz4hHUH5kP6", 0, "general holder", "gen")
            );

            ////////// INVESTORS ICO 10%
            List<List<Object>> genesisInvestors = Arrays.asList(
                    //Arrays.asList("4QkVCB9NY8ksA9hLGd8NpAwnVDjw", "0.1", "genesis holder", "gen"),
                    //Arrays.asList("acc:4YuDWoBZ6LpmZxyR1Nky82orpyv7", "0.1", "genesis holder", "gen")

            );

            // GENESIS FORGERS
            //ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(arr));
            // NEED for .add() ::
            String[] debtorsStr = new String[]{
                    "4Z4bPGgijfpfNT9efaEXrgNuHMcv",
                    "45pvGGK4c3vZ8fXUJ4UnAvWFe6QV",
                    "3tNqdzAtqwBZBgUGREzZpcMHycVj",
                    "4ZYSFXgnkhbGYU14uR2pdey1X731",
                    "4Aqojnzsc4nM75oGQNAKhVHmm8Vx",
                    "4UriR7UUUDEBMvizFrhDA4yvYcZb",
                    "3tfStBd7t87mXGiYu4HVKUF3eKWq",
                    "4J9G61Nf1dpfL7CtxjRSHtCoiNaq",
                    "4V77ndw7fntAQpAsD6dTQAEKuEsz",
                    "4FxK16N7j9iEWdGewWjyBKJtxtqz",
                    "3iw3ZT8Z3HEGAmGznGNTEXAvU3P1",
                    "4E6Cpuj95SKoNHJmokDfdNfty2Ld",
                    "4FMwjiJx7AQvGg3PbkTJ51EhJH18",
                    "3wdgr9VVSzV74tMFrhpsARqAQd17",
                    "3mGxr9yXSJCwupFLvtweB2ysu1mE",
                    "3ntJUEekwwNVXfgZKCA37unPseW6",
                    "3ihCtPykmdekm5BpydCJrr8rW1Hy",
                    "4X1zBcZskdduDeSpKMA5Np7nhQ7d",
                    "3ovy9a9fmMuqnoD6aHF461WrGQUx",
                    "4BtoYZimNwkXLes2aNB7cVWQaAeB"
            };

            // TRANSFERS
            //

            BigDecimal totalSended = BigDecimal.ZERO;

            boolean first = true;
            for (List<Object> item : generalGenesisUsers) {

                if (first) {
                    // пропустим админа ему внизу остатки скидываем
                    first = false;
                    continue;
                }

                try {
                    recipient = Account.parse((String) item.get(0));
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }

                bdAmount0 = new BigDecimal(item.get(1).toString()).setScale(AssetCls.ERA_SCALE);
                transactions.add(new GenesisTransferAssetTransaction(recipient, AssetCls.ERA_KEY, bdAmount0,
                        (String) item.get(2), (String) item.get(3), null));
                totalSended = totalSended.add(bdAmount0);

                // buffer for CREDIT sends
                sends_toUsers.add(new Tuple2<Account, BigDecimal>(recipient, bdAmount0));

                bdAmount1 = BigDecimal.ONE.setScale(AssetCls.FEE_SCALE);
                transactions.add(new GenesisTransferAssetTransaction(recipient, AssetCls.FEE_KEY, bdAmount1,
                        (String) item.get(2), (String) item.get(3), null));

            }

            int pickDebt = 1000;
            BigDecimal limitOwned = new BigDecimal(pickDebt * 5).setScale(AssetCls.ERA_SCALE);

            // NOT PERSONALIZE INVESTORS - ICO 10%
            for (List<Object> item : genesisInvestors) {

                if (PublicKeyAccount.isValidPublicKey((String) item.get(0))) {
                    try {
                        recipient = PublicKeyAccount.parse((String) item.get(0));
                    } catch (TxException e) {
                        throw new RuntimeException(e);
                    }
                } else if (Account.isValidAddress((String) item.get(0))) {
                    try {
                        recipient = Account.parse((String) item.get(0));
                    } catch (TxException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    continue;
                }

                bdAmount0 = new BigDecimal((String) item.get(1)).setScale(AssetCls.ERA_SCALE);
                transactions.add(new GenesisTransferAssetTransaction(recipient, AssetCls.ERA_KEY, bdAmount0,
                        (String) item.get(2), (String) item.get(3), null));
                totalSended = totalSended.add(bdAmount0);


            }

            Account founder = null;
            try {
                founder = Account.parse(generalGenesisUsers.get(0).get(0).toString());
            } catch (TxException e) {
                throw new RuntimeException(e);
            }

            // ADJUST end
            transactions.add(new GenesisTransferAssetTransaction(
                    founder, AssetCls.ERA_KEY,
                    new BigDecimal(BlockChain.GENESIS_ERA_TOTAL).subtract(totalSended).setScale(AssetCls.ERA_SCALE),
                    null, null, null));


            bdAmount0 = new BigDecimal("10").setScale(AssetCls.ERA_SCALE);
            for (String item : debtorsStr) {
                recipient = new Account(item);
                transactions.add(new GenesisTransferAssetTransaction(recipient, AssetCls.ERA_KEY,
                        bdAmount0, founder, "forger", null, null));
                transactions.add(new GenesisTransferAssetTransaction(recipient, AssetCls.FEE_KEY,
                        new BigDecimal("1000000"), null, null, null));

                transactions.add(new GenesisTransferAssetTransaction(recipient, AssetCls.DIM_KEY,
                        new BigDecimal("100"), null, null, null));

            }
        }

        //GENERATE AND VALIDATE TRANSACTIONS
        this.transactionCount = transactions.size();

        makeTransactionsRAWandHASH();

        // SIGN simple as HASH
        if (BlockChain.GENESIS_SIGNATURE == null) {
            this.signature = generateHeadHash();
        } else {
            this.signature = BlockChain.GENESIS_SIGNATURE;
        }

        // make rawBytes! Need for take transactions
        toBytes();

    }

    public GenesisBlock(byte[] data, boolean checkSign) throws Exception {
        throw new BlockException("parsing denied");
    }

    // make assets
    public static AssetVenture makeAsset(long key) {
        switch ((int) key) {
            case (int) AssetCls.ERA_KEY:
                return new AssetVenture(AssetCls.AS_INSIDE_CURRENCY, CREATOR, AssetCls.ERA_NAME, icon, image, AssetCls.ERA_DESCR,
                        AssetCls.ERA_SCALE, AssetCls.ERA_AMOUNT);
            case (int) AssetCls.FEE_KEY:
                return new AssetVenture(AssetCls.AS_INSIDE_CURRENCY, //BlockChain.FEE_ASSET_EMITTER == null ? (BlockChain.FEE_ASSET_EMITTER = CREATOR) : CREATOR,
                        CREATOR,
                        AssetCls.FEE_NAME, icon, image, AssetCls.FEE_DESCR,
                        AssetCls.FEE_SCALE, 0L);
            case (int) AssetCls.DIM_KEY:
                return new AssetVenture(AssetCls.AS_INSIDE_ACCESS,
                        CREATOR,
                        AssetCls.DIM_NAME, icon, image, "",
                        0, AssetCls.DIM_VOL);
        }
        return null;
    }

    // make templates
    public static Template makeTemplate(int key) {
        switch (key) {
            case (int) TemplateCls.LICENSE_KEY:
                return null;
            case (int) TemplateCls.MARRIAGE_KEY:
                return new Template(CREATOR, "Заявление о бракосочетании", icon, image, "Мы, %person1% и %person2%, женимся!");
            case (int) TemplateCls.UNMARRIAGE_KEY:
                return new Template(CREATOR, "Заявление о разводе", icon, image, "Я, %person1%, развожусь с %person2%");
            case (int) TemplateCls.HIRING_KEY:
                return new Template(CREATOR, "Заявление о приёме на работу", icon, image, "Прошу принять меня в объединение %union%, на должность %job%");
            case (int) TemplateCls.UNHIRING_KEY:
                return new Template(CREATOR, "Заявление об уволнении", icon, image, "Прошу уволить меня из объединения %union% по собственному желанию");
        }
        return new Template(CREATOR, "empty", icon, image, "empty");
    }

    // make statuses
    public static Status makeStatus(int key) {
        if (key == StatusCls.MEMBER_KEY) return new Status(CREATOR,
                "Членство %1 ур. в объед. %2", icon, image, "Уровень %1 членства в объединении %2", false);
        //else if (key == StatusCls.ALIVE_KEY) return new Status(CREATOR, "Alive", icon, image, "Alive or Dead");
        //else if (key == StatusCls.RANK_KEY) return new Status(CREATOR, "Rank", icon, image, "General, Major or Minor");
        //else if (key == StatusCls.USER_KEY) return new Status(CREATOR, "User", icon, image, "Admin, User, Observer");
        //else if (key == StatusCls.MAKER_KEY) return new Status(CREATOR, "Maker", icon, image, "Creator, Designer, Maker");
        //else if (key == StatusCls.DELEGATE_KEY) return new Status(CREATOR, "Delegate", icon, image, "President, Senator, Deputy");
        //else if (key == StatusCls.CERTIFIED_KEY) return new Status(CREATOR, "Certified", icon, image, "Certified, Notarized, Confirmed");
        //else if (key == StatusCls.MARRIED_KEY) return new Status(CREATOR, "Married", icon, image, "Husband, Wife, Spouse");

        return new Status(CREATOR, "Право %1 ур. в объед. %2", icon, image, "Уровень %1 прав (власти) в объединении %2", false);
    }

    private void initItems() {

        transactions = new ArrayList<Transaction>();
        for (int i = 1; i <= AssetCls.DIM_KEY; i++) {
            AssetVenture asset = makeAsset(i);
            if (asset == null)
                continue;
            transactions.add(new GenesisIssueAssetTransaction(asset, null, null, null));
        }

        if (false) {
            ///// TEMPLATES
            for (int i = 1; i <= TemplateCls.UNHIRING_KEY; i++)
                transactions.add(new GenesisIssueTemplateRecord(makeTemplate(i), null, null, null));

            ///// STATUSES
            for (int i = 1; i <= StatusCls.MEMBER_KEY; i++)
                transactions.add(new GenesisIssueStatusRecord(makeStatus(i), null, null, null));
        }

        if (BlockChain.TEST_MODE) {
            for (String name : BlockChain.NOVA_ASSETS.keySet()) {
                AssetVenture asset = new AssetVenture(AssetCls.AS_INSIDE_ASSETS, creator, name,
                        null, null, "", 8, 0L);
                transactions.add(new GenesisIssueAssetTransaction(asset, null, null, null));
            }
        }

    }

    //GETTERS

    private void addDebt(String address, int val, List<List<Object>> genesisDebtors) {

        Account recipient;

        //int i = 0;
        for (int i = 0; i < genesisDebtors.size(); i++) {

            List<Object> item = genesisDebtors.get(i);
            String address_deb = (String) item.get(0);

            if (address_deb.length() > 36) {
                try {
                    recipient = PublicKeyAccount.parse(address_deb);
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }
            } else {
                try {
                    recipient = Account.parse(address_deb);
                } catch (TxException e) {
                    throw new RuntimeException(e);
                }
            }

            if (recipient.equals(address)) {
                val += (int) item.get(1);
                genesisDebtors.set(i, Arrays.asList(address_deb, val));
                return;
            }
            i++;
        }
        genesisDebtors.add(Arrays.asList(address, val));
    }

    @Override
    public long getTimestamp() {
        return this.GENESIS_TIMESTAMP;
    }

    public String getTestNetInfo() {
        return this.testnetInfo;
    }

    @Override
    public Block getParent(DCSet db) {
        //PARENT DOES NOT EXIST
        return null;
    }

    //VALIDATE

    public byte[] generateHeadHash() {

        byte[] data = new byte[0];

        //WRITE VERSION
        byte[] versionBytes = Longs.toByteArray(GENESIS_VERSION);
        versionBytes = Bytes.ensureCapacity(versionBytes, 4, 0);
        data = Bytes.concat(data, versionBytes);

        //WRITE REFERENCE
        byte[] flagsBytes = Bytes.ensureCapacity(GENESIS_REFERENCE, Crypto.SIGNATURE_LENGTH, 0);
        data = Bytes.concat(data, flagsBytes);

        //WRITE TIMESTAMP
        byte[] genesisTimestampBytes = Longs.toByteArray(this.GENESIS_TIMESTAMP);
        genesisTimestampBytes = Bytes.ensureCapacity(genesisTimestampBytes, 8, 0);
        data = Bytes.concat(data, genesisTimestampBytes);

        if (BlockChain.CLONE_MODE) {
            //WRITE SIDE SETTINGS
            byte[] genesisjsonCloneBytes = this.sideSettingString.getBytes(StandardCharsets.UTF_8);
            data = Bytes.concat(data, genesisjsonCloneBytes);
        }

        //DIGEST [32]
        byte[] digest = Crypto.getInstance().digest(data);

        //DIGEST + transactionsHash = byte[64]
        digest = Bytes.concat(digest, transactionsHash);

        return digest;
    }

    public byte[] toBytes() {
        // toBytes denied
        return new byte[0];
    }

    public int getDataLength() {
        // Parse and toBytes denied - It not store in DB or send to NETWORK
        return 0;
    }

    @Override
    public boolean isSignatureValid() {

        //VALIDATE BLOCK SIGNATURE
        byte[] digest = generateHeadHash();
        return Arrays.equals(digest,
                // TODO - как защитить свой оригинальныЙ? Если задан наш оригинальный - то его и берем
                BlockChain.GENESIS_SIGNATURE_TRUE == null ?
                        this.signature : BlockChain.GENESIS_SIGNATURE_TRUE);
    }

    @Override
    public int isValid(DCSet db, boolean andProcess) throws Exception {
        //CHECK IF NO OTHER BLOCK IN DB
        if (db.getBlocksHeadsMap().contains(heightBlock))
            return INVALID_BLOCK_VERSION;

        //VALIDATE TRANSACTIONS
        byte[] transactionsSignatures = new byte[0];
        for (Transaction transaction : this.getTransactions()) {
            //transaction.setDC(db);
            try {
                if (transaction.isValid(Transaction.FOR_NETWORK, 0L) != Transaction.VALIDATE_OK) {
                    if (Settings.CHECK_BUGS > 7)
                        transaction.isValid(Transaction.FOR_NETWORK, 0L);
                    return INVALID_BLOCK_VERSION;
                }
            } catch (TxException e) {
                throw new RuntimeException(e);
            }
            transactionsSignatures = Bytes.concat(transactionsSignatures, transaction.getSignature());

        }
        transactionsSignatures = Crypto.getInstance().digest(transactionsSignatures);
        if (!Arrays.equals(this.transactionsHash, transactionsSignatures)) {
            LOGGER.error("*** GenesisBlock.digest(transactionsSignatures) invalid");
            return INVALID_BLOCK_VERSION;
        }

        return INVALID_NONE;
    }

    @Override
    public void assetsFeeProcess(DCSet dcSet, boolean asOrphan) {
    }

    public void process(DCSet dcSet, boolean notLog) throws Exception {

        this.target = BlockChain.BASE_TARGET / BlockChain.PERIOD_NO_WIN;

        this.blockHead = new BlockHead(this);

        super.process(dcSet, notLog);

    }

    public void orphan(DCSet dcSet) throws Exception {
        throw new BlockException("Orphan denied");
    }

    /**
     * Он болтается в памяти всегда и поэтому его нельзя чистить.
     * Вдобавок при очистке транзакции уничтожаются, а он в базу данных не катается и не может их из нее восстановить
     */

    @Override
    public synchronized void close() {
        if (validatedForkDB != null) {
            try {
                validatedForkDB.close();
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
            validatedForkDB = null;
        }
    }
}
