package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import erchi.core.account.PrivateKeyAccount;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.ItemFactory;
import org.erachain.core.item.assets.AssetCls;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public abstract class IssueItemRecord extends Transaction implements Itemable {

    protected ItemCls item;
    protected Long key;

    /**
     * NEW PARSE by make new Object from RAW
     *
     * @param itemType
     * @param data
     * @param forDeal
     * @throws Exception
     */
    public IssueItemRecord(String TYPE_NAME, int itemType, PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, pubKey, signature, startPos, data, forDeal);

        //READ ITEM
        // Item parse without reference - if is = signature
        // + 1 - skip TYPE_ITEM
        item = ItemFactory.getInstance().parse(itemType, parsePos, data, false);
        parsePos += item.getDataLength(false);
        if (signature != null)
            item.setReference(signature, dbRef);

        if (forDeal == FOR_DB_RECORD) {
            parseDB(data);
        }
    }

    public IssueItemRecord(byte TYPE_ID, String TYPE_NAME, JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);

        if (typeBytes[POS_TX_VERSION] > 0) {
            errorMessage.setMsg("invalid `version`");
            throw new TxException("wrong value: " + getVersion());
        }

        errorMessage.setTx(this);

        //READ ITEM
        errorMessage.setMsg("invalid `item` JSON");
        JSONObject itemJson = (JSONObject) json.get("item");

        errorMessage.setMsg("invalid `item.itemType`");
        int itemType = (int) (long) (Long) itemJson.get("itemType");
        item = ItemFactory.getInstance().parse(itemType, itemJson, errorMessage);

    }


    public IssueItemRecord(byte[] typeBytes, String TYPE_NAME, PublicKeyAccount creator, String title, String tags,
                           ExLink linkTo, ItemCls item, byte feePow, long timestamp) {
        super(typeBytes, TYPE_NAME, creator, title, tags, null, linkTo, null, feePow, timestamp);
        this.item = item;
        if (item.getKey() != 0)
            key = item.getKey();
    }

    public IssueItemRecord(byte[] typeBytes, String TYPE_NAME, PublicKeyAccount creator, String title, String tags,
                           ExLink linkTo, ItemCls item, byte feePow, long timestamp, byte[] signature) {
        this(typeBytes, TYPE_NAME, creator, title, tags, linkTo, item, feePow, timestamp);
        assert(signature != null);
        this.signature = signature;
        this.item.setReference(signature, dbRef);
    }

    @Override
    public void parseDB(byte[] data) throws Exception {

        super.parseDB(data);

        //READ KEY
        key = Longs.fromByteArray(Arrays.copyOfRange(data, parsePos, parsePos + KEY_LENGTH));
        parsePos += KEY_LENGTH;
        item.setKey(key);

    }

    //GETTERS/SETTERS

    @Override
    public ItemCls getItem() {
        return this.item;
    }

    /**
     * нужно для отображение в блокэксплорере
     * - не участвует в Протоколе, так как перед выпуском неизвестно его значение
     *
     * @return
     */
    @Override
    public long getKey() {
        return key == null ? (isWiped() ? 0 : null) // выдаст ошибку специально если боевая и NULL, see issues/1347
                : key;
    }

    @Override
    public String viewTitle() {
        return this.item.getName() + ":" + getTitle();
    }

    @Override
    public void makeItemsKeys() {
        if (isWiped()) {
            itemsKeys = new Object[][]{};
        }

        if (key == null)
            return;

        if (creatorPersonDuration != null) {
            // запомним что тут две сущности
            itemsKeys = new Object[][]{
                    new Object[]{ItemCls.PERSON_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()},
                    new Object[]{item.getItemType(), key, item.getTagsFull()}
            };
        } else {
            itemsKeys = new Object[][]{
                    new Object[]{item.getItemType(), key, item.getTagsFull()}
            };
        }
    }

    @Override
    public boolean isFreeFee() {
        return false;
    }

    @Override
    public long calcBaseFee(boolean withFreeProtocol) {

        int len = getFeeLength();

        if (len < BlockChain.MINIMAL_ISSUE_FEE)
            len = BlockChain.MINIMAL_ISSUE_FEE;

        return (long) len * BlockChain.FEE_PER_BYTE;
    }

    /**
     * нельзя вызывать для Форка и для isWIPED
     */
    @Override
    public void updateFromStateDB() {

        super.updateFromStateDB();

        if (this.dbRef == 0) {
            // неподтвержденная транзакция не может быть обновлена
            return;
        }

        if (key == null || key == 0) {
            // эта транзакция взята как скелет из набора блока
            // найдем сохраненную транзакцию - в ней есь Номер Сути
            if (!dcSet.getTransactionFinalMap().contains(dbRef)) {
                // это может быть при откате - транзакция уже удалена и пытается в кошельке ей мясо нарастить
                return;
            }
            IssueItemRecord issueItemRecord = (IssueItemRecord) dcSet.getTransactionFinalMap().get(this.dbRef);
            if (issueItemRecord == null) {
                // это может быть при откате - транзакция уже удалена и пытается в кошельке ей мясо нарастить
                return;
            }
            key = issueItemRecord.getKey();
            item.setKey(key);

        } else if (item.getKey() == 0) {
            item.setKey(key);
        }
    }

    @Override
    public String viewItemName() {
        return item.toString();
    }

    @Override
    public boolean hasPublicText() {
        return true;
    }

    //@Override
    @Override
    public void sign(PrivateKeyAccount creator, int forDeal) {
        super.sign(creator, forDeal);
        item.setReference(signature, dbRef);
    }

    //PARSE CONVERT


    @SuppressWarnings("unchecked")
    @Override
    public void toJsonBody(JSONObject transaction) {
        transaction.put("item", this.item.toJson());
    }

    @Override
    public byte[] toBytesBody(int forDeal) {
        byte[] data = item.toBytes(false);
        return data;
    }

    @Override
    public byte[] toBytesDB() {

        byte[] data = super.toBytesDB();
        if (key == null) {
            // для неподтвержденных когда еще номера нету
            data = Bytes.concat(data, new byte[KEY_LENGTH]);
        } else {
            byte[] keyBytes = Longs.toByteArray(key);
            keyBytes = Bytes.ensureCapacity(keyBytes, KEY_LENGTH, 0);
            data = Bytes.concat(data, keyBytes);
        }

        return data;
    }

    @Override
    public int getDataLength(int forDeal) {

        int base_len = super.getDataLength(forDeal);

        if (forDeal == FOR_DB_RECORD)
            base_len += KEY_LENGTH;

        // not include item reference
        return base_len + item.getDataLength(false);

    }

    //VALIDATE

    protected boolean noMakeRights() {
        if (super.noMakeRights())
            return true;

        if (!BlockChain.TEST_MODE
                && !BlockChain.TRUSTED_ANONYMOUS.contains(this.creator.getAddress())
                && DIM_BAL.compareTo(BlockChain.MIN_USE_DIM_FOR_ITEM_BD) < 0) {
            errorValue = "USE balance <" + BlockChain.MIN_USE_DIM_FOR_ITEM_BD.toPlainString() + " " + AssetCls.DIM_NAME;
            return true;
        }
        return false;
    }

    //@Override
    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        if (BlockChain.startKeys[this.item.getItemType()] < 0) {
            if (this.item.isNovaItem(this.dcSet) <= 0) {
                return INVALID_ISSUE_PROHIBITED;
            }
        }

        int result = item.isValid();
        if (result != Transaction.VALIDATE_OK) {
            errorValue = item.errorValue;
            return result;
        }

        //CHECK NAME LENGTH
        String name = this.item.getName();
        // TEST ONLY CHARS
        int nameLen = name.length();

        if (nameLen < item.getMinNameLen()) {
            // IF is NEW NOVA
            if (this.item.isNovaItem(this.dcSet) <= 0) {
                errorValue = nameLen + "<" + item.getMinNameLen();
                return INVALID_NAME_LENGTH_MIN;
            }
        }

        if (item.isUniqueName()
                && item.getDBMap(dcSet).containsName(item.getName())) {
            return INVALID_ITEM_NAME_NOT_UNIQUE;
        }

        return super.isValid(forDeal, checkFlags);

    }

    //PROCESS/ORPHAN

    protected void processItem() {

        this.item.setReference(this.signature, dbRef);

        //INSERT INTO DATABASE
        key = this.item.insertToMap(this.dcSet, this.item.getStartKey());

        if (creatorPerson != null) {
            dcSet.getItemsValuesMap().putIssuedItem(creatorPerson, item, dbRef);
        }
    }

    @Override
    public void processBody(Block block, int forDeal) throws TxException {

        //UPDATE CREATOR
        super.processBody(block, forDeal);

        processItem();

    }

    protected void orphanItem() {
        //DELETE FROM DATABASE
        key = this.item.deleteFromMap(this.dcSet, item.getStartKey());

        if (creatorPerson != null) {
            dcSet.getItemsValuesMap().deleteIssuedItem(creatorPerson, item);
        }
    }

    @Override
    public void orphanBody(Block block, int forDeal) throws TxException {
        orphanItem();

        //UPDATE CREATOR
        super.orphanBody(block, forDeal);


    }

    @Override
    public List<PublicKeyAccount> getPublicKeys() {
        List<PublicKeyAccount> items = new ArrayList<>();
        if (!item.getAuthor().equals(getCreator()))
            items.add(item.getAuthor());
        if (!item.getOwner().equals(item.getAuthor()) && !item.getOwner().equals(getCreator()))
            items.add(item.getOwner());
        return items;
    }

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        HashSet<Account> accounts = this.getRecipientAccounts();
        accounts.add(this.creator);
        return accounts;
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        HashSet<Account> accounts = new HashSet<>(3, 1);
        if (!this.item.getOwner().equals(this.creator)) {
            accounts.add(this.item.getOwner());
        }

        return accounts;
    }

    @Override
    public boolean isInvolved(Account account) {

        return account.equals(this.creator) || account.equals(this.item.getAuthor())
                || account.equals(this.item.getOwner());
    }

}
