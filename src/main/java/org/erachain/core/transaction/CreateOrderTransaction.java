package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.Order;
import org.erachain.core.item.assets.OrderProcess;
import org.erachain.core.item.assets.TradePair;
import org.erachain.dapp.DAPP;
import org.erachain.database.PairMapImpl;
import org.erachain.datachain.DCSet;
import org.erachain.utils.BigDecimalUtil;
import org.json.simple.JSONObject;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple3;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * #### PROPERTY 1
 * OLD: typeBytes[2].3-7 = point accuracy for HAVE amount: -16..16 = BYTE - 16
 * <br>
 * #### PROPERTY 2
 * OLD: typeBytes[3].3-7 = point accuracy for WANT amount: -16..16 = BYTE - 16
 * <br>
 * NEW: dataBody[0-1] = point accuracy for HAVE and WANT amounts: -16..16 = BYTE - 16
 */
public class CreateOrderTransaction extends Transaction implements Itemable {
    public static final byte[][] VALID_REC = new byte[][]{
            //Base58.decode("4...")
    };
    public static final byte TYPE_ID = (byte) Transaction.CREATE_ORDER_TRANSACTION;
    public static final String TYPE_NAME = "Create Order";

    // < 32 used for accuracy
    public static final byte HAS_SMART_CONTRACT_MASK = 64;

    public static final int AMOUNT_LENGTH = TransactionAmount.AMOUNT_LENGTH;
    private static final int HAVE_LENGTH = 8;
    private static final int WANT_LENGTH = 8;
    // private static final int PRICE_LENGTH = 12;

    private static final int LOAD_LENGTH = HAVE_LENGTH + WANT_LENGTH + 2 * AMOUNT_LENGTH;

    private final long haveKey;
    private final long wantKey;
    //private Order order;
    private final BigDecimal amountHave;
    private final BigDecimal amountWant;
    private AssetCls haveAsset;
    private AssetCls wantAsset;

    public CreateOrderTransaction(byte[] typeBytes, PublicKeyAccount creator, ExLink exLink, DAPP dapp, long haveKey, long wantKey,
                                  BigDecimal amountHave, BigDecimal amountWant, byte feePow, long timestamp) {
        super(typeBytes, TYPE_NAME, creator, null, null, null, exLink, dapp, feePow, timestamp);
        this.haveKey = haveKey;
        this.wantKey = wantKey;

        this.amountHave = amountHave;
        this.amountWant = amountWant;

    }

    public CreateOrderTransaction(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);

        if (typeBytes[POS_TX_VERSION] > 0) {
            errorMessage.setMsg("invalid `version`");
            throw new TxException("wrong value: " + getVersion());
        }

        errorMessage.setMsg("invalid `amountHave`");
        this.amountHave = (BigDecimal) json.get("amountHave");
        if (amountHave == null) {
            throw new TxParseException("CreateOrderTransaction. Empty value", "amountHave",
                    "Use fields: " + getTXFields());
        }

        errorMessage.setMsg("invalid `amountWant`");
        this.amountWant = (BigDecimal) json.get("amountWant");
        if (amountWant == null) {
            throw new TxParseException("CreateOrderTransaction. Empty value", "amountWant",
                    "Use fields: " + getTXFields());
        }

        errorMessage.setMsg("invalid `haveKey`");
        this.haveKey = (Long) json.get("haveKey");
        errorMessage.setMsg("invalid `wantKey`");
        this.wantKey = (Long) json.get("wantKey");

    }

    public CreateOrderTransaction(byte[] typeBytes, PublicKeyAccount creator, long haveKey, long wantKey,
                                  BigDecimal amountHave, BigDecimal amountWant, byte feePow, long timestamp,
                                  byte[] signature) {
        this(typeBytes, creator, null, null, haveKey, wantKey, amountHave, amountWant, feePow, timestamp);
        this.signature = signature;

    }

    public CreateOrderTransaction(byte[] typeBytes, PublicKeyAccount creator, ExLink exLink, DAPP dapp, long haveKey, long wantKey,
                                  BigDecimal amountHave, BigDecimal amountWant, byte feePow, long timestamp,
                                  byte[] signature, long seqNo, long feeLong) {
        this(typeBytes, creator, exLink, dapp, haveKey, wantKey, amountHave, amountWant, feePow, timestamp);
        this.signature = signature;
        this.fee = BigDecimal.valueOf(feeLong, BlockChain.FEE_SCALE);
        if (seqNo > 0)
            this.setHeightSeq(seqNo);

    }

    public CreateOrderTransaction(PublicKeyAccount creator, long haveKey, long wantKey, BigDecimal amountHave,
                                  BigDecimal amountWant, byte feePow, long timestamp, byte[] signature) {
        this(makeTypeBytes(TYPE_ID), creator, haveKey, wantKey, amountHave, amountWant, feePow, timestamp,
                signature);
    }

    public CreateOrderTransaction(PublicKeyAccount creator, long have, long want, BigDecimal amountHave,
                                  BigDecimal amountWant, byte feePow, long timestamp) {
        this(makeTypeBytes(TYPE_ID), creator, null, null, have, want, amountHave, amountWant, feePow, timestamp
        );
    }

    @Override
    public List<String> getTXFields() {

        String[] array = new String[]{
                "haveKey:long", "wantKey:long", "amountHave:BigDecimal", "amountWant:BigDecimal"
        };

        // new ArrayList<> - make modified list!
        List<String> list = new ArrayList<>(Arrays.asList(array));
        list.addAll(0, super.getTXFields());
        return list;
    }


    // GETTERS/SETTERS

    public static Transaction Parse(byte[] data, int forDeal) throws Exception {

        if (true)
            throw new TxParseException("Invalid parse", TYPE_NAME, "");

        // CHECK IF WE MATCH BLOCK LENGTH
        int test_len;
        if (forDeal == Transaction.FOR_MYPACK) {
            test_len = BASE_LENGTH_AS_MYPACK;
        } else if (forDeal == Transaction.FOR_PACK) {
            test_len = BASE_LENGTH_AS_PACK;
        } else if (forDeal == Transaction.FOR_DB_RECORD) {
            test_len = BASE_LENGTH_AS_DBRECORD;
        } else {
            test_len = BASE_LENGTH;
        }

        if (data.length < test_len) {
            throw new Exception("Data does not match RAW length " + data.length + " < " + test_len);
        }

        // READ TYPE
        byte[] typeBytes = Arrays.copyOfRange(data, 0, TYPE_LENGTH);
        int position = TYPE_LENGTH;

        long timestamp = 0;
        if (forDeal > Transaction.FOR_MYPACK) {
            //READ TIMESTAMP
            byte[] timestampBytes = Arrays.copyOfRange(data, position, position + TIMESTAMP_LENGTH);
            timestamp = Longs.fromByteArray(timestampBytes);
            position += TIMESTAMP_LENGTH;
        }

        //READ CREATOR
        byte[] creatorBytes = Arrays.copyOfRange(data, position, position + CREATOR_LENGTH);
        PublicKeyAccount creator = new PublicKeyAccount(Crypto.ED25519_SYSTEM, creatorBytes);
        position += CREATOR_LENGTH;

        ExLink exLink;
        if ((typeBytes[2] & HAS_EXLINK_MASK) > 0) {
            exLink = ExLink.parse(data, position);
            position += exLink.length();
        } else {
            exLink = null;
        }

        DAPP dapp;
        if ((typeBytes[2] & HAS_SMART_CONTRACT_MASK) > 0) {
            dapp = DAPP.Parses(data, position, forDeal);
            position += dapp.length(forDeal);
        } else {
            dapp = null;
        }

        byte feePow = 0;
        if (forDeal > Transaction.FOR_PACK) {
            // READ FEE POWER
            byte[] feePowBytes = Arrays.copyOfRange(data, position, position + 1);
            feePow = feePowBytes[0];
            position += 1;
        }

        // READ SIGNATURE
        byte[] signatureBytes = Arrays.copyOfRange(data, position, position + SIGNATURE_LENGTH);
        position += SIGNATURE_LENGTH;

        long feeLong = 0;
        long seqNo = 0;
        if (forDeal == FOR_DB_RECORD) {
            //READ SEQ_NO
            byte[] seqNoBytes = Arrays.copyOfRange(data, position, position + TIMESTAMP_LENGTH);
            seqNo = Longs.fromByteArray(seqNoBytes);
            position += TIMESTAMP_LENGTH;

            // READ FEE
            byte[] feeBytes = Arrays.copyOfRange(data, position, position + FEE_LENGTH);
            feeLong = Longs.fromByteArray(feeBytes);
            position += FEE_LENGTH;
        }

        // READ HAVE
        byte[] haveBytes = Arrays.copyOfRange(data, position, position + HAVE_LENGTH);
        long have = Longs.fromByteArray(haveBytes);
        position += HAVE_LENGTH;

        // READ WANT
        byte[] wantBytes = Arrays.copyOfRange(data, position, position + WANT_LENGTH);
        long want = Longs.fromByteArray(wantBytes);
        position += WANT_LENGTH;

        // READ AMOUNT HAVE
        byte[] amountHaveBytes = Arrays.copyOfRange(data, position, position + AMOUNT_LENGTH);
        BigDecimal amountHave = new BigDecimal(new BigInteger(amountHaveBytes), BlockChain.AMOUNT_DEFAULT_SCALE);
        position += AMOUNT_LENGTH;
        // CHECK ACCURACY of AMOUNT
        int accuracy = typeBytes[2] & TransactionAmount.SCALE_MASK;
        if (accuracy > 0) {
            if (accuracy >= TransactionAmount.SCALE_MASK_HALF) {
                accuracy -= TransactionAmount.SCALE_MASK + 1;
            }

            // RESCALE AMOUNT
            amountHave = amountHave.scaleByPowerOfTen(-accuracy);
        }

        // READ AMOUNT WANT
        byte[] amountWantBytes = Arrays.copyOfRange(data, position, position + AMOUNT_LENGTH);
        BigDecimal amountWant = new BigDecimal(new BigInteger(amountWantBytes), BlockChain.AMOUNT_DEFAULT_SCALE);
        position += AMOUNT_LENGTH;
        // CHECK ACCURACY of AMOUNT
        accuracy = typeBytes[3] & TransactionAmount.SCALE_MASK;
        if (accuracy > 0) {
            if (accuracy >= TransactionAmount.SCALE_MASK_HALF) {
                accuracy -= TransactionAmount.SCALE_MASK + 1;
            }

            // RESCALE AMOUNT
            amountWant = amountWant.scaleByPowerOfTen(-accuracy);
        }

        return new CreateOrderTransaction(typeBytes, creator, exLink, dapp, have, want, amountHave, amountWant, feePow, timestamp,
                signatureBytes, seqNo, feeLong);
    }

    public void setDC(DCSet dcSet, boolean andUpdateFromState) {

        super.setDC(dcSet, false);

        this.haveAsset = dcSet.getItemAssetMap().get(this.haveKey);
        this.wantAsset = dcSet.getItemAssetMap().get(this.wantKey);

        if (false)
            updateFromStateDB();

    }

    @Override
    public String viewTitle() {
        return //TYPE_NAME + " " +
                ItemCls.getItemTypeAndKey(ItemCls.ASSET_TYPE, haveKey)
                        + " " + ItemCls.getItemTypeAndKey(ItemCls.ASSET_TYPE, wantKey);
    }

    @Override
    public String viewAmount() {
        return this.amountHave.toPlainString();
    }

    @Override
    public long calcBaseFee(boolean withFreeProtocol) {

        long long_fee = super.calcBaseFee(withFreeProtocol);
        if (long_fee == 0)
            return 0L;

        if ((haveKey < haveAsset.START_KEY() || wantKey < haveAsset.START_KEY())) {
            return 0L;
        }

        return long_fee;
    }

    public Long getOrderId() {
        //return this.signature;
        return Transaction.makeDBRef(this.height, this.seqNo);
    }

    @Override
    public BigDecimal getAmount() {
        return this.amountHave;
    }

    @Override
    public long getKey() {
        return this.haveKey;
    }

    @Override
    public long getAssetKey() {
        return this.haveKey;
    }

    @Override
    public ItemCls getItem() {
        return this.haveAsset;
    }

    public long getHaveKey() {
        return this.haveKey;
    }

    public AssetCls getHaveAsset() {
        return this.haveAsset;
    }

    public BigDecimal getAmountHave() {
        return this.amountHave;
    }

    public long getWantKey() {
        return this.wantKey;
    }

    public AssetCls getWantAsset() {
        return this.wantAsset;
    }

    public BigDecimal getAmountWant() {
        return this.amountWant;
    }

    public BigDecimal getPriceCalc() {
        return makeOrder().calcPrice();
    }

    public BigDecimal getPriceCalcReverse() {
        return makeOrder().calcPriceReverse();
    }

    // PARSE CONVERT

    @Override
    public boolean hasPublicText() {
        return false;
    }

    public Order makeOrder() {
        return new Order(dcSet, Transaction.makeDBRef(this.height, this.seqNo), this.creator,
                this.haveKey, this.amountHave, this.haveAsset.getScale(),
                this.wantKey, this.amountWant, this.wantAsset.getScale()
        );
    }

    @SuppressWarnings("unchecked")
    @Override
    public void toJsonBody(JSONObject transaction) {
        transaction.put("haveKey", this.haveKey);
        transaction.put("wantKey", this.wantKey);
        transaction.put("amountHave", this.amountHave.toPlainString());
        transaction.put("amountWant", this.amountWant.toPlainString());
    }

    // @Override
    //@Override
    public byte[] toBytesBody(int forDeal) {

        byte[] data = new byte[2];

        // WRITE HAVE
        byte[] haveBytes = Longs.toByteArray(this.haveKey);
        haveBytes = Bytes.ensureCapacity(haveBytes, HAVE_LENGTH, 0);

        // WRITE WANT
        byte[] wantBytes = Longs.toByteArray(this.wantKey);
        wantBytes = Bytes.ensureCapacity(wantBytes, WANT_LENGTH, 0);

        // WRITE ACCURACY of AMOUNT HAVE
        int different_scale = this.amountHave.scale() - BlockChain.AMOUNT_DEFAULT_SCALE;
        BigDecimal amountBase;
        if (different_scale != 0) {
            // RESCALE AMOUNT
            amountBase = this.amountHave.scaleByPowerOfTen(different_scale);
            if (different_scale < 0)
                different_scale += TransactionAmount.SCALE_MASK + 1;

            data[0] = (byte) (data[0] | different_scale);
        } else {
            amountBase = this.amountHave;
        }
        // WRITE AMOUNT HAVE
        byte[] amountHaveBytes = amountBase.unscaledValue().toByteArray();
        byte[] fill_H = new byte[AMOUNT_LENGTH - amountHaveBytes.length];

        // WRITE ACCURACY of AMOUNT WANT
        different_scale = this.amountWant.scale() - BlockChain.AMOUNT_DEFAULT_SCALE;
        if (different_scale != 0) {
            // RESCALE AMOUNT
            amountBase = this.amountWant.scaleByPowerOfTen(different_scale);
            if (different_scale < 0)
                different_scale += TransactionAmount.SCALE_MASK + 1;

            data[1] = (byte) (data[1] | different_scale);
        } else {
            amountBase = this.amountWant;
        }
        // WRITE AMOUNT WANT
        byte[] amountWantBytes = amountBase.unscaledValue().toByteArray();
        byte[] fill_W = new byte[AMOUNT_LENGTH - amountWantBytes.length];
        data = Bytes.concat(data, haveBytes, wantBytes, fill_H, amountHaveBytes, fill_W, amountWantBytes);

        return data;
    }

    @Override
    public int getDataLength(int forDeal) {
        return super.getDataLength(forDeal) + LOAD_LENGTH;
    }


    // VALIDATE
    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        if (this.haveAsset == null || this.wantAsset == null)
            return ITEM_ASSET_NOT_EXIST;

        if (!haveAsset.isActive(timestamp, false)) {
            errorValue = haveAsset.errorValue + " " + "Have asset";
            return INVALID_OUTSIDE_ACTIVITY_PERIOD;
        }
        if (!wantAsset.isActive(timestamp, false)) {
            errorValue = wantAsset.errorValue + " " + "Want asset";
            return INVALID_OUTSIDE_ACTIVITY_PERIOD;
        }

        if (this.wantAsset.isAccounting() ^ this.haveAsset.isAccounting()
                || haveAsset.isSelfManaged() || wantAsset.isSelfManaged()) {
            return INVALID_ACCOUNTING_PAIR;
        }
        if (!wantAsset.validPair(haveKey)
                || !haveAsset.validPair(wantKey)) {
            return INVALID_ECXHANGE_PAIR;
        }

        if (this.wantAsset.isInsideBonus() ^ this.haveAsset.isInsideBonus()) {
            if (this.haveKey != AssetCls.FEE_KEY && this.wantKey != AssetCls.FEE_KEY)
                return INVALID_ECXHANGE_PAIR;
        }

        long haveKey = this.haveKey;
        long wantKey = this.wantKey;

        for (byte[] valid_item : VALID_REC) {
            if (Arrays.equals(this.signature, valid_item)) {
                return VALIDATE_OK;
            }
        }

        // CHECK IF ASSETS NOT THE SAME
        if (haveKey == wantKey) {
            return HAVE_EQUALS_WANT;
        }

        if (haveKey == RIGHTS_KEY && BlockChain.FREEZE_FROM > 0
                && height > BlockChain.FREEZE_FROM
                && BlockChain.FOUNDATION_ADDRESSES.contains(this.creator.getAddress())) {
            // LOCK ERA sell
            return INVALID_CREATOR;
        }

        // CHECK IF AMOUNT POSITIVE
        BigDecimal amountHave = this.amountHave;
        BigDecimal amountWant = this.amountWant;
        if (amountHave.signum() <= 0 || amountWant.signum() <= 0) {
            return NEGATIVE_AMOUNT;
        }

        // CHECK IF HAVE EXISTS
        if (this.haveAsset == null) {
            // HAVE DOES NOT EXIST
            return ITEM_ASSET_NOT_EXIST;
        }
        // CHECK IF WANT EXISTS
        if (this.wantAsset == null) {
            // WANT DOES NOT EXIST
            return ITEM_ASSET_NOT_EXIST;
        }

        if ((wantAsset.isAnonimDenied() || haveAsset.isAnonimDenied()) && !isCreatorPersonalized()) {
            return ANONIM_OWN_DENIED;
        }

        // CHECK IF SENDER HAS ENOUGH ASSET BALANCE
        if (height < BlockChain.ALL_BALANCES_OK_TO) {
            // NOT CHECK

        } else if (wantKey == FEE_KEY
                && (haveKey == RIGHTS_KEY || haveKey == BTC_KEY)
                // VALID if want to BY COMPU by ERA
                && amountHave.compareTo(BigDecimal.TEN) >= 0 // минимально меняем 1 ЭРА
                && this.creator.getForSale(this.dcSet, haveKey, height, true).compareTo(amountHave) >= 0 // ЭРА|BTC есть на счету
                && this.creator.getForSale(this.dcSet, FEE_KEY, height, true).signum() >= 0 // и COMPU не отрицательные
        ) { // на балансе компушки не минус
            checkFlags = checkFlags | NOT_VALIDATE_FLAG_FEE;
        } else if (haveKey == FEE_KEY) {
            if (!BlockChain.isFeeEnough(height, creator)
                    && this.creator.getForSale(this.dcSet, FEE_KEY, height, true).compareTo(amountHave.add(this.fee)) < 0) {
                return NO_BALANCE;
            }

        } else {

            switch ((int) haveKey) {
                case 111:
                case 222:
                case 333:
                case 444:
                case 555:
                case 666:
                case 777:
                case 888:
                case 999:
                    return NO_BALANCE;
            }


            // if asset is unlimited and me is creator of this asset
            boolean unLimited = haveAsset.isUnlimited(this.creator, false);

            if (!unLimited) {

                BigDecimal forSale = this.creator.getForSale(this.dcSet, haveKey, height, true);

                if (forSale.compareTo(amountHave) < 0) {
                    boolean wrong = true;
                    for (byte[] valid_item : BlockChain.VALID_BAL) {
                        if (Arrays.equals(this.signature, valid_item)) {
                            wrong = false;
                            break;
                        }
                    }

                    if (wrong)
                        return NO_BALANCE;
                }
            }

            if (height > BlockChain.FREEZE_FROM
                    && BlockChain.LOCKED__ADDRESSES.containsKey(this.creator.getAddress()))
                return INVALID_CREATOR;

            Tuple3<String, Integer, Integer> unlockItem = BlockChain.LOCKED__ADDRESSES_PERIOD.get(this.creator.getAddress());
            if (unlockItem != null && unlockItem.b > height && height < unlockItem.c)
                return INVALID_CREATOR;

            //
            Long maxWant = wantAsset.getQuantity();
            if (maxWant > 0 && new BigDecimal(maxWant).compareTo(amountWant) < 0)
                return INVALID_QUANTITY;

        }

        // for PARSE and toBYTES need only AMOUNT_LENGTH bytes
        // and SCALE
        if (true) {
            if (!BigDecimalUtil.isValid(amountHave)) {
                return AMOUNT_LENGHT_SO_LONG;
            }
            // SCALE wrong
            int scale = this.amountHave.scale();
            if (scale < TransactionAmount.minSCALE
                    || scale > TransactionAmount.maxSCALE) {
                return AMOUNT_SCALE_WRONG;
            }
            scale = this.amountHave.stripTrailingZeros().scale();
            if (scale > haveAsset.getScale()) {
                return AMOUNT_SCALE_WRONG;
            }
        }
        if (true) {
            if (!BigDecimalUtil.isValid(amountWant)) {
                return AMOUNT_LENGHT_SO_LONG;
            }
            int scale = this.amountWant.scale();
            if (scale < TransactionAmount.minSCALE
                    || scale > TransactionAmount.maxSCALE) {
                return AMOUNT_SCALE_WRONG;
            }
            scale = this.amountWant.stripTrailingZeros().scale();
            if (scale > wantAsset.getScale()) {
                return AMOUNT_SCALE_WRONG;
            }
        }

        // CHECK MIN AMOUNTS
        if (!BlockChain.EXCHANGE_MIN_AMOUNT_TAB.isEmpty()) {
            BigDecimal minAmount = BlockChain.EXCHANGE_MIN_AMOUNT_TAB.get(haveKey);
            if (minAmount != null) {
                if (minAmount.compareTo(amountHave) > 0) {
                    errorValue = minAmount.toPlainString();
                    return ORDER_AMOUNT_HAVE_SO_SMALL;
                }
            }

            minAmount = BlockChain.EXCHANGE_MIN_AMOUNT_TAB.get(wantKey);
            if (minAmount != null) {
                if (minAmount.compareTo(amountWant) > 0) {
                    errorValue = minAmount.toPlainString();
                    return ORDER_AMOUNT_WANT_SO_SMALL;
                }
            }
        }

        return super.isValid(forDeal, checkFlags);
    }

    @Override
    public void makeItemsKeys() {
        if (isWiped()) {
            itemsKeys = new Object[][]{};
        }

        if (creatorPersonDuration == null) {
            itemsKeys = new Object[][]{
                    new Object[]{ItemCls.ASSET_TYPE, haveKey, haveAsset == null ? null : haveAsset.getTagsFull()}, // транзакция ошибочная
                    new Object[]{ItemCls.ASSET_TYPE, wantKey, wantAsset == null ? null : wantAsset.getTagsFull()},
            };
        } else {
            itemsKeys = new Object[][]{
                    new Object[]{ItemCls.PERSON_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()},
                    new Object[]{ItemCls.ASSET_TYPE, haveKey, haveAsset == null ? null : haveAsset.getTagsFull()},
                    new Object[]{ItemCls.ASSET_TYPE, wantKey, wantAsset == null ? null : wantAsset.getTagsFull()},
            };
        }
    }

    // PROCESS/ORPHAN

    // @Override
    @Override
    public void processBody(Block block, int forDeal) throws TxException {
        // UPDATE CREATOR
        super.processBody(block, forDeal);

        // PROCESS ORDER
        Order order = makeOrder();

        // MOVE HAVE from OWN to PLEDGE
        creator.changeBalance(dcSet, true, Account.BALANCE_POS_OWN, haveKey, amountHave,
                null, false,
                // accounting on PLEDGE position
                true, Account.BALANCE_POS_PLEDGE);

        OrderProcess.process(order, block, this);

        if (Controller.getInstance().dlSet != null
                // так как проверка в Форке - потом быстрый слив и эта таблица вообще не будет просчитана
                && !dcSet.isFork()) {
            // статистику по парам
            PairMapImpl pairMap = Controller.getInstance().dlSet.getPairMap();
            if (!pairMap.contains(new Fun.Tuple2(haveKey, wantKey))) {
                pairMap.put(new TradePair(haveAsset, wantAsset, order.getPrice(), timestamp,
                        order.getPrice(), order.getPrice(),
                        amountHave, amountWant, BigDecimal.ZERO,
                        order.getPrice(), order.getPrice(),
                        1, timestamp, 0, 0));
            }
        }

    }

    // @Override
    @Override
    public void orphanBody(Block block, int forDeal) throws TxException {
        // UPDATE CREATOR
        super.orphanBody(block, forDeal);

        // ORPHAN ORDER

        // тут получаем Заказ не измененный - который был до отката
        Order order = OrderProcess.orphan(dcSet, dbRef, block, block == null ? timestamp : block.getTimestamp());

        // RESTORE HAVE
        // GET HAVE LEFT - if it CANCELED by Outprice or not completed
        //   - если обработка остановлена по достижению порога Инкремента
        creator.changeBalance(dcSet, false, Account.BALANCE_POS_OWN, haveKey,
                // так как внутри может сработать Unresolved by Outprice - и именно остаток недобитый тут тоже учтётся
                // этот осток от Неисполнения внутри OrderProcess.orphan делается
                order.getAmountHave(),
                null, false,
                // accounting on PLEDGE position
                true, Account.BALANCE_POS_PLEDGE);

    }

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        HashSet<Account> accounts = new HashSet<>(2, 1);
        accounts.add(this.creator);
        return accounts;
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        return new HashSet<>(1, 1);
    }

    @Override
    public boolean isInvolved(Account account) {

        return account.equals(this.creator);
    }

    @Override
    public BigDecimal getAmount(Account account) {
        if (account.getAddress().equals(this.creator.getAddress())) {
            return this.amountHave;
        }

        return BigDecimal.ZERO;
    }

    public Map<String, Map<Long, BigDecimal>> getAssetAmount() {
        Map<String, Map<Long, BigDecimal>> assetAmount = new LinkedHashMap<>();

        assetAmount = subAssetAmount(assetAmount, this.creator.getAddress(), FEE_KEY, this.fee);
        assetAmount = subAssetAmount(assetAmount, this.creator.getAddress(), this.haveKey,
                this.amountHave);

        return assetAmount;
    }

}
