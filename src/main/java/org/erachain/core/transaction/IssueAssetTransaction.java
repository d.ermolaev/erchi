package org.erachain.core.transaction;

import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetUnique;
import org.erachain.datachain.DCSet;
import org.json.simple.JSONObject;
import org.mapdb.Fun;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public class IssueAssetTransaction extends IssueItemRecord {
    public static final byte TYPE_ID = (byte) ISSUE_ASSET_TRANSACTION;
    public static final String TYPE_NAME = "Issue Asset";

    public IssueAssetTransaction(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, ItemCls.ASSET_TYPE, pubKey, signature, startPos, data, forDeal);
    }

    public IssueAssetTransaction(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);
    }

    public IssueAssetTransaction(byte[] typeBytes, PublicKeyAccount creator, ExLink linkTo,
                                 AssetCls asset, byte feePow, long timestamp) {
        super(typeBytes, TYPE_NAME, creator, null, null, linkTo, asset, feePow, timestamp);
    }

    public IssueAssetTransaction(byte[] typeBytes, PublicKeyAccount creator, ExLink linkTo, AssetCls asset,
                                 byte feePow, long timestamp, byte[] signature) {
        super(typeBytes, TYPE_NAME, creator, null, null, linkTo, asset, feePow, timestamp, signature);
    }

    public IssueAssetTransaction(byte[] typeBytes, PublicKeyAccount creator, ExLink linkTo, AssetCls asset, byte feePow,
                                 long timestamp, byte[] signature, long seqNo, long feeLong) {
        super(typeBytes, TYPE_NAME, creator, null, null, linkTo, asset, feePow, timestamp, signature);
        this.fee = BigDecimal.valueOf(feeLong, BlockChain.FEE_SCALE);
        if (seqNo > 0)
            this.setHeightSeq(seqNo);
    }

    // as pack
    public IssueAssetTransaction(byte[] typeBytes, PublicKeyAccount creator, ExLink linkTo, AssetCls asset, byte[] signature) {
        super(typeBytes, TYPE_NAME, creator, null, null, linkTo, asset, (byte) 0, 0L, signature);
    }

    public IssueAssetTransaction(PublicKeyAccount creator, AssetCls asset, byte feePow, long timestamp) {
        this(makeTypeBytes(TYPE_ID), creator, null, asset, feePow, timestamp);
    }

    public IssueAssetTransaction(PublicKeyAccount creator, AssetCls asset, byte feePow, long timestamp, byte[] signature) {
        this(makeTypeBytes(TYPE_ID), creator, null, asset, feePow, timestamp, signature);
    }

    // as pack
    public IssueAssetTransaction(PublicKeyAccount creator, AssetCls asset, byte[] signature) {
        this(makeTypeBytes(TYPE_ID), creator, null, asset, (byte) 0, 0L, signature);
    }

    public IssueAssetTransaction(PublicKeyAccount creator, ExLink linkTo, AssetCls asset, byte feePow, long timestamp) {
        this(makeTypeBytes(TYPE_ID), creator, linkTo, asset, feePow, timestamp);
    }

    //GETTERS/SETTERS

    @Override
    public long calcBaseFee(boolean withFreeProtocol) {

        int len = getFeeLength();

        if (((AssetCls) item).isAccounting()) {
            if (len < BlockChain.MINIMAL_ISSUE_FEE_ACCOUNTING_ASSET)
                len = BlockChain.MINIMAL_ISSUE_FEE_ACCOUNTING_ASSET;
        } else {
            if (len < BlockChain.MINIMAL_ISSUE_FEE)
                len = BlockChain.MINIMAL_ISSUE_FEE;
        }

        return (long) len * BlockChain.FEE_PER_BYTE;
    }

    public long getAssetKey(DCSet db) {
        return getItem().getKey();
    }

    @Override
    public AssetCls getAsset() {
        return (AssetCls) getItem();
    }

    @Override
    public BigDecimal getAmount() {
        return new BigDecimal(((AssetCls) getItem()).getQuantity());
    }

    //VALIDATE

    @Override
    public BigDecimal getAmount(Account account) {
        if (creator.equals(account)) {
            return getAmount();
        }
        AssetCls asset = (AssetCls) item;
        return BigDecimal.ZERO.setScale(asset.getScale());
    }

    @Override
    public boolean hasPublicText() {

        if (this.item.isNovaItem(this.dcSet) > 0) {
            return false;
        }

        return super.hasPublicText();

    }

    //PARSE CONVERT

    @Override
    protected boolean noMakeRights() {
        if (!BlockChain.TEST_MODE
                && !BlockChain.TRUSTED_ANONYMOUS.contains(this.creator.getAddress())
                && creator.getBalanceUSE(RIGHTS_KEY, dcSet).compareTo(BlockChain.MIN_GENERATING_BALANCE_BD) < 0) {
            errorValue = "USE balance <" + BlockChain.MIN_GENERATING_BALANCE_BD.toPlainString() + " " + AssetCls.ERA_NAME;
            return true;
        }
        return false;
    }

    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        int result = super.isValid(forDeal, checkFlags);
        if (result != Transaction.VALIDATE_OK) {
            return result;
        }

        if ((checkFlags & NOT_VALIDATE_ITEM) == 0) {
            //CHECK QUANTITY
            AssetCls asset = (AssetCls) this.getItem();

            if (asset.isUnique()) {
                if (asset instanceof AssetUnique) {
                } else {
                    // так как тип актива считываем в конце парсинга - по нему сразу не определить что было создано
                    // и может появиться ошибка сборки байт кода
                    return INVALID_ASSET_TYPE;
                }
            } else {
                //long maxQuantity = asset.isDivisible() ? 10000000000L : 1000000000000000000L;
                long maxQuantity = Long.MAX_VALUE;
                long quantity = asset.getQuantity();
                //if(quantity > maxQuantity || quantity < 0 && quantity != -1 && quantity != -2 )
                if (quantity > maxQuantity || quantity < -1) {
                    errorValue = "quantity > maxQuantity  or < -1: " + quantity + " > " + maxQuantity;
                    return INVALID_QUANTITY;
                }

                if (((AssetCls) this.item).isAccounting() && quantity != 0) {
                    errorValue = "Asset is Accounting and quantity != 0";
                    return INVALID_QUANTITY;
                }

                if (this.item.isNovaItem(this.dcSet) > 0) {
                    Fun.Tuple3<Long, Long, byte[]> item = BlockChain.NOVA_ASSETS.get(this.item.getName());
                    if (item.b < quantity) {
                        errorValue = "Nova asset quantity > set : " + quantity + " > " + item.b;
                        return INVALID_QUANTITY;
                    }
                }
            }
        }

        return Transaction.VALIDATE_OK;
    }

    //PROCESS/ORPHAN

    protected void processItem() {

        super.processItem();

        //ADD ASSETS TO OWNER
        AssetCls asset = (AssetCls) this.getItem();
        long assetKey = asset.getKey();

        long quantity = asset.getQuantity();
        if (quantity > 0) {
            creator.changeBalance(dcSet, false, assetKey,
                    new BigDecimal(quantity).setScale(0), false, false);

            // make HOLD balance
            if (!asset.isUnHoldable()) {
                creator.changeBalance(dcSet, false, Account.BALANCE_POS_HOLD, assetKey,
                        new BigDecimal(quantity).setScale(0), false, false);
            }

        } else if (quantity == 0) {
            // безразмерные - нужно баланс в таблицу нулевой записать чтобы в блокэксплорере он отображался у счета
            // см. https://lab.erachain.org/erachain/Erachain/issues/1103
            this.creator.changeBalance(this.dcSet, false, assetKey,
                    BigDecimal.ZERO.setScale(0), false, false);

        }

    }

    protected void orphanItem() {

        super.orphanItem();

        //REMOVE ASSETS FROM OWNER
        AssetCls asset = (AssetCls) this.getItem();
        long quantity = asset.getQuantity();
        long assetKey = asset.getKey();

        if (quantity > 0) {
            this.creator.changeBalance(this.dcSet, true, assetKey,
                    new BigDecimal(quantity).setScale(0), false, false);

            // на балансе На Руках - добавляем тоже
            if (!asset.isUnHoldable()) {
                creator.changeBalance(dcSet, true, Account.BALANCE_POS_HOLD, assetKey,
                        new BigDecimal(quantity).setScale(0), false, false);
            }
        }

    }

    //@Override
    public Map<String, Map<Long, BigDecimal>> getAssetAmount() {
        Map<String, Map<Long, BigDecimal>> assetAmount = new LinkedHashMap<>();
        assetAmount = subAssetAmount(assetAmount, creator.getAddress(), FEE_KEY, fee);
        AssetCls asset = (AssetCls) getItem();
        assetAmount = addAssetAmount(assetAmount, creator.getAddress(), asset.getKey(),
                new BigDecimal(asset.getQuantity()).setScale(0));
        return assetAmount;
    }

}
