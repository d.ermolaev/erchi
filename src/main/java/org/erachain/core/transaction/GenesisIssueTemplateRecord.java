package org.erachain.core.transaction;

import org.erachain.core.item.templates.TemplateCls;
import org.erachain.core.item.templates.TemplateFactory;

public class GenesisIssueTemplateRecord extends GenesisIssueItemRecord {

    private static final byte TYPE_ID = (byte) GENESIS_ISSUE_TEMPLATE_TRANSACTION;
    private static final String NAME_ID = "GENESIS Issue Template";

    public GenesisIssueTemplateRecord(TemplateCls template, String title, String tags, String message) {
        super(TYPE_ID, NAME_ID, template, title, tags, message);
    }

    public GenesisIssueTemplateRecord(byte[] data) throws Exception {
        super(NAME_ID, data);

        item = TemplateFactory.getInstance().parse(parsePos, data, false);
        this.generateSignature();

    }

}
