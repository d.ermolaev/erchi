package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Base58;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.item.persons.PersonsAvatar;
import org.erachain.datachain.DCSet;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple3;
import org.mapdb.Fun.Tuple4;

import java.math.BigDecimal;
import java.util.*;

/**
 * if person has not ALIVE status - add it
 * end_day = this.add_day + this.timestanp(days)
 * typeBytes[1] - version =0 - not need sign by person;
 * =1 - need sign by person
 * typeBytes[2] - size of personalized accounts
 */
public class RCertifyPubKeys extends Transaction implements Itemable {

    public static final byte TYPE_ID = (byte) Transaction.CERTIFY_PUB_KEYS_TRANSACTION;
    public static final String TYPE_NAME = "Certify Public Key";

    private static final int USER_ADDRESS_LENGTH = Transaction.CREATOR_LENGTH;
    private static final int DATE_DAY_LENGTH = 4; // one year + 256 days max

    private static final int LOAD_LENGTH = DATE_DAY_LENGTH + KEY_LENGTH;

    static final ReferralBonus AVATAR_REFERRAL_BONUS = new ReferralBonus(0L, new BigDecimal[]{
            new BigDecimal(500), new BigDecimal(100), new BigDecimal(20)});

    protected Long key; // PERSON KEY
    protected PersonCls person;
    protected Integer add_day; // in days
    protected List<PublicKeyAccount> certifiedPublicKeys;

    /**
     * NEW PARSE by make new Object from RAW
     *
     * @param data
     * @param forDeal
     * @throws Exception
     */
    public RCertifyPubKeys(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, pubKey, signature, startPos, data, forDeal);

        int test_len;
        if (forDeal == Transaction.FOR_MYPACK) {
            test_len = BASE_LENGTH_AS_MYPACK;
        } else if (forDeal == Transaction.FOR_PACK) {
            test_len = BASE_LENGTH_AS_PACK;
        } else if (forDeal == Transaction.FOR_DB_RECORD) {
            test_len = BASE_LENGTH_AS_DBRECORD;
        } else {
            test_len = BASE_LENGTH;
        }

        if (data.length < test_len) {
            throw new Exception("Data does not match RAW length " + data.length + " < " + test_len);
        }

        ///////////////// BODY ///////////////

        // releaserReference = null - not a pack
        // releaserReference = reference for releaser account - it is as pack

        //READ PERSON KEY
        byte[] keyBytes = Arrays.copyOfRange(data, parsePos, parsePos + KEY_LENGTH);
        key = Longs.fromByteArray(keyBytes);
        parsePos += KEY_LENGTH;

        int size = typeBytes[POS_TX_FLAGS_1];
        certifiedPublicKeys = new ArrayList<PublicKeyAccount>();
        for (int i = 0; i < size; i++) {
            //READ USER ACCOUNT
            certifiedPublicKeys.add(new PublicKeyAccount(Crypto.ED25519_SYSTEM, Arrays.copyOfRange(data, parsePos, parsePos + USER_ADDRESS_LENGTH)));
            parsePos += USER_ADDRESS_LENGTH;
        }

        // READ DURATION
        add_day = Ints.fromByteArray(Arrays.copyOfRange(data, parsePos, parsePos + DATE_DAY_LENGTH));
        parsePos += DATE_DAY_LENGTH;


        /////////////////////// TAIL /////////////////
        if (forDeal == FOR_DB_RECORD) {
            parseDB(data);
        }

    }

    public RCertifyPubKeys(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);

        if (typeBytes[POS_TX_VERSION] > 0) {
            errorMessage.setMsg("invalid `version`");
            throw new TxException("wrong value: " + getVersion());
        }

        if (!json.containsKey("pubKeys")) {
            throw new TxParseException("RCertifyPubKeys. Empty value", "pubKeys",
                    "Use fields: " + getTXFields());
        }

        errorMessage.setMsg("invalid `pubKeys`");
        JSONArray array = (JSONArray) json.get("pubKeys");
        certifiedPublicKeys = new ArrayList<>();
        int i = 0;
        for (Object item : array) {
            errorMessage.setMsg("invalid `pubKeys[" + i + "]`: " + item.toString());
            certifiedPublicKeys.add(PublicKeyAccount.parse(item.toString()));
            i++;
        }

        errorMessage.setMsg("invalid `personKey`");
        // TODO сделать возможность НУЛЬ - для продления той же самой персоны - поиск в базе тут надо делаьб будет
        // if key == null - ПРОДЛЕНИЕ?
        key = (long) (Long) json.get("personKey");

        errorMessage.setMsg("invalid `addDays`");
        add_day = (int) (long) (Long) json.getOrDefault("addDays", 0L);

    }

    public RCertifyPubKeys(PublicKeyAccount creator, ExLink exLink, byte feePow, long key,
                           List<PublicKeyAccount> certifiedPublicKeys,
                           int add_day, long timestamp) {
        super(makeTypeBytes(TYPE_ID, (byte) 0, (byte) certifiedPublicKeys.size()), TYPE_NAME, creator,
                null, null, null, exLink, null, feePow, timestamp);

        this.key = key;
        this.certifiedPublicKeys = certifiedPublicKeys;
        this.add_day = add_day;
    }


    public RCertifyPubKeys(PublicKeyAccount creator, byte feePow, long key,
                           List<PublicKeyAccount> certifiedPublicKeys,
                           int add_day, long timestamp) {
        this(creator, null, feePow, key,
                certifiedPublicKeys,
                add_day, timestamp);
    }


    // set default date
    public RCertifyPubKeys(PublicKeyAccount creator, byte feePow, long key,
                           List<PublicKeyAccount> certifiedPublicKeys,
                           long timestamp) {
        this(creator, null, feePow, key,
                certifiedPublicKeys,
                0, timestamp);
    }

    //GETTERS/SETTERS

    @Override
    public byte[] toBytesBody(int forDeal) {

        byte[] data;

        //WRITE PERSON KEY
        byte[] keyBytes = Longs.toByteArray(this.key);
        keyBytes = Bytes.ensureCapacity(keyBytes, KEY_LENGTH, 0);
        data = keyBytes;

        //WRITE USER PUBLIC KEYS
        for (PublicKeyAccount publicAccount : this.certifiedPublicKeys) {
            data = Bytes.concat(data, publicAccount.getPublicKey());
        }

        //WRITE DURATION
        data = Bytes.concat(data, Ints.toByteArray(this.add_day));

        return data;
    }

    @Override
    public List<String> getTXFields() {

        String[] array = new String[]{
                "personKey:long", "pubKeys:[Base58]", "addDays:int=0"
        };

        // new ArrayList<> - make modified list!
        List<String> list = new ArrayList<>(Arrays.asList(array));
        list.addAll(0, super.getTXFields());
        return list;
    }

    public static void processBody(DCSet dcSet, Block block, Transaction transaction, long key,
                                   List<PublicKeyAccount> certifiedPublicKeys, int add_day) {

        int height = transaction.getBlockHeight();
        int transactionIndex = transaction.getSeqNo();

        boolean personalized = false;
        TreeMap<String, Stack<Tuple3<Integer, Integer, Integer>>> personalisedData = dcSet.getPersonAddressMap().getItems(key);
        if (personalisedData != null && !personalisedData.isEmpty()) {
            for (Stack<Tuple3<Integer, Integer, Integer>> personalisedDataStack : personalisedData.values()) {
                if (!personalisedDataStack.isEmpty()) {
                    personalized = true;
                    break;
                }
            }
        }

        PublicKeyAccount pkAccount = certifiedPublicKeys.get(0);
        if (!personalized) {
            if (pkAccount.equals(transaction.getCreator()))
                personalized = true;
        }

        if (!personalized) {
            // IT IS NOT VOUCHED PERSON

            // EMITTE LIA
            transaction.creator.changeBalance(dcSet, false, -AssetCls.LIA_KEY, BigDecimal.ONE,
                    false, false);
            // SUBSTRACT from EMISSION (with minus)
            GenesisBlock.CREATOR.changeBalance(dcSet, true, -AssetCls.LIA_KEY, BigDecimal.ONE,
                    false, true);

            PersonCls person;
            if (transaction instanceof IssuePersonRecord) {
                person = (PersonCls) ((IssuePersonRecord) transaction).getItem();
            } else {
                person = (PersonCls) dcSet.getItemPersonMap().get(key);
            }

            BigDecimal issued_FEE_BD_total;
            BigDecimal personBonus;
            Account inviter = PersonCls.getInviter(dcSet, key);
            if (inviter != null) {
                inviter.changeBalance(dcSet, false, AssetCls.LIA_KEY, BigDecimal.ONE,
                        false, false);
                // SUBTRACT from EMISSION (with minus)
                GenesisBlock.CREATOR.changeBalance(dcSet, true, AssetCls.LIA_KEY, BigDecimal.ONE,
                        false, true);

                personBonus = BlockChain.BONUS_FOR_NEW_PERSON(height, person);

                // GIVE GIFT for this PUB_KEY - to PERSON
                issued_FEE_BD_total = personBonus.add(AVATAR_REFERRAL_BONUS.count(dcSet,
                        inviter, false, BlockChain.FEE_ASSET, block,
                        "referral reward", transaction.getDBRef(), transaction.getTimestamp()));
            } else {
                personBonus = new BigDecimal(500);

                issued_FEE_BD_total = personBonus;
            }

            pkAccount.changeBalance(dcSet, false, FEE_KEY, personBonus,
                    false, false);
            pkAccount.changeCOMPUStatsBalances(dcSet, false, personBonus, Account.FEE_BALANCE_SIDE_TOTAL_EARNED);
            if (block != null) {
                block.addCalculated(pkAccount, FEE_KEY, personBonus,
                        "Hello bonus", transaction.dbRef);
            }

            // TO EMITTE FEE (with minus)
            BlockChain.FEE_ASSET_EMITTER.changeBalance(dcSet, true, FEE_KEY, issued_FEE_BD_total,
                    false, true);

            if (height > BlockChain.ACTION_ROYALTY_START && !BlockChain.ACTION_ROYALTY_USE_RECIPIENTS) {
                // нужно сделать первую точку тогда для бонусов
                dcSet.getTimeRoyaltyMap().push(key, transaction.getDBRef(), 1L, 0L);
            }

        }

        add_day = add_day < 0 ? add_day : BlockChain.DEFAULT_DURATION;
        // set to time stamp of record
        int end_day = (int) (transaction.timestamp / 86400000L) + add_day;

        Tuple3<Integer, Integer, Integer> itemP = new Tuple3<Integer, Integer, Integer>(end_day,
                //Controller.getInstance().getHeight(), this.signature);
                height, transactionIndex);
        Tuple4<Long, Integer, Integer, Integer> itemA = new Tuple4<Long, Integer, Integer, Integer>(key, end_day,
                height, transactionIndex);


        // SET PERSON ADDRESS
        String address;
        for (PublicKeyAccount publicAccount : certifiedPublicKeys) {
            address = publicAccount.getAddress();
            dcSet.getAddressPersonMap().addItem(publicAccount.getBytes(), itemA);
            dcSet.getPersonAddressMap().addItem(key, address, itemP);

            // TODO удалить это если публичный ключ будет сохраняться в таблице Счетов
            if (publicAccount.getLastTimestamp(dcSet) == null && !publicAccount.equals(transaction.getCreator())) {
                // for quick search public keys by address - use PUB_KEY from Person DATA owner
                // used in - controller.Controller.getPublicKeyByAddress
                publicAccount.setLastTimestamp(new long[]{transaction.timestamp, transaction.dbRef}, dcSet);
            }

        }

    }

    public static void orphanBody(DCSet dcSet, Transaction transaction, long key,
                                  List<PublicKeyAccount> certifiedPublicKeys) {

        String address;
        for (PublicKeyAccount publicAccount : certifiedPublicKeys) {
            address = publicAccount.getAddress();
            dcSet.getAddressPersonMap().removeItem(publicAccount.getBytes());
            dcSet.getPersonAddressMap().removeItem(key, address);

            // TODO удалить это если публичный ключ будет созраняться в таблице Счетов
            // при откате нужно след в истории удалить а сам публичный ключ отсавить на всякий случай?
            long[] lastPoint = publicAccount.getLastTimestamp(dcSet);
            if (lastPoint != null && lastPoint[0] == transaction.timestamp && !publicAccount.equals(transaction.getCreator())) {
                publicAccount.removeLastTimestamp(dcSet, transaction.timestamp);
            }

        }

        boolean personalized = false;
        TreeMap<String, Stack<Tuple3<Integer, Integer, Integer>>> personalisedData = dcSet.getPersonAddressMap().getItems(key);
        if (personalisedData != null && !personalisedData.isEmpty()) {
            for (Stack<Tuple3<Integer, Integer, Integer>> personalisedDataStack : personalisedData.values()) {
                if (!personalisedDataStack.isEmpty()) {
                    personalized = true;
                    break;
                }
            }
        }

        PublicKeyAccount pkAccount = certifiedPublicKeys.get(0);
        if (!personalized) {
            if (pkAccount.equals(transaction.getCreator()))
                personalized = true;
        }

        if (!personalized) {
            // IT WAS NOT VOUCHED PERSON BEFORE

            PersonCls person;
            if (transaction instanceof IssuePersonRecord) {
                person = (PersonCls) ((IssuePersonRecord)transaction).getItem();
            } else {
                person = (PersonCls) dcSet.getItemPersonMap().get(key);
            }

            BigDecimal personBonus;

            // EMITTE LIA
            transaction.creator.changeBalance(dcSet, true, -AssetCls.LIA_KEY, BigDecimal.ONE, false, false);
            // SUBSTRACT from EMISSION (with minus)
            GenesisBlock.CREATOR.changeBalance(dcSet, false, -AssetCls.LIA_KEY, BigDecimal.ONE, false, true);

            // GIVE GIFT for this PUB_KEY - to PERSON
            BigDecimal issued_FEE_BD_total;

            Account inviter = PersonCls.getInviter(dcSet, key);
            if (inviter != null) {

                // EMITTE LIA
                inviter.changeBalance(dcSet, true, AssetCls.LIA_KEY, BigDecimal.ONE,
                        false, false);
                // SUBSTRACT from EMISSION (with minus)
                GenesisBlock.CREATOR.changeBalance(dcSet, false, AssetCls.LIA_KEY, BigDecimal.ONE, false, true);

                // BONUSES

                personBonus = BlockChain.BONUS_FOR_NEW_PERSON(transaction.height, person);

                issued_FEE_BD_total = personBonus.add(AVATAR_REFERRAL_BONUS.count(dcSet,
                        inviter, true, BlockChain.FEE_ASSET, null,
                        null, transaction.getDBRef(), transaction.getTimestamp()));
            } else {
                personBonus = new BigDecimal(500);

                issued_FEE_BD_total = personBonus;
            }

            pkAccount.changeBalance(dcSet, true, FEE_KEY, personBonus, false, false);
            pkAccount.changeCOMPUStatsBalances(dcSet, true, personBonus, Account.FEE_BALANCE_SIDE_TOTAL_EARNED);

            // ADD to EMISSION (with minus)
            BlockChain.FEE_ASSET_EMITTER.changeBalance(dcSet, false, FEE_KEY, issued_FEE_BD_total, false, true);

            if (transaction.getBlockHeight() > BlockChain.ACTION_ROYALTY_START && !BlockChain.ACTION_ROYALTY_USE_RECIPIENTS) {
                // нужно удалить первую точку тогда для бонусов
                dcSet.getTimeRoyaltyMap().pop(key);
            }

        }

    }

    public void setDC(DCSet dcSet, boolean andUpdateFromState) {
        super.setDC(dcSet, false);

        if (dcSet != null) {
            this.person = (PersonCls) this.dcSet.getItemPersonMap().get(this.key);
        }

        if (false)
            updateFromStateDB();

    }

    @Override
    public ItemCls getItem() {
        if (person == null) {
            person = (PersonCls) dcSet.getItemPersonMap().get(key);
        }
        return this.person;
    }

    // PERSON KEY
    @Override
    public long getKey() {
        return this.key;
    }

    @Override
    public List<PublicKeyAccount> getPublicKeys() {
        return this.certifiedPublicKeys;
    }

    public List<PublicKeyAccount> getCertifiedPublicKeys() {
        return this.certifiedPublicKeys;
    }

    public List<String> getCertifiedPublicKeysB58() {
        List<String> pbKeys = new ArrayList<String>();
        for (PublicKeyAccount key : this.certifiedPublicKeys) {
            pbKeys.add(Base58.encode(key.getPublicKey()));
        }
        return pbKeys;
    }

    public int getAddDay() {
        return this.add_day;
    }

    //////// VIEWS

    public int getPublicKeysSize() {
        return this.typeBytes[2];
    }

    // IT is only PERSONALITY record
    @Override
    public boolean hasPublicText() {
        return !BlockChain.ANONIM_SERT_USE;
    }

    //////////////
    @SuppressWarnings("unchecked")
    @Override
    public void toJsonBody(JSONObject transaction) {
        transaction.put("personKey", this.key);
        transaction.put("pubKeys", this.getCertifiedPublicKeysB58());
        transaction.put("addDays", this.add_day);
    }

    @Override
    public long getInvitedFee() {
        return 0L;
    }

    //VALIDATE

    @Override
    public int getDataLength(int forDeal) {

        int base_len = super.getDataLength(forDeal) + LOAD_LENGTH;

        int accountsSize = this.certifiedPublicKeys.size();
        base_len += accountsSize * PublicKeyAccount.PUBLIC_KEY_LENGTH;
        return this.typeBytes[1] == 1 ? base_len + Transaction.SIGNATURE_LENGTH * accountsSize : base_len;
    }

    //PROCESS/ORPHAN

    //
    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        // ALL Persons by ADMINS
        boolean creatorIsAdmin = false;
        for (String admin : BlockChain.GENESIS_ADMINS) {
            if (creator.equals(admin)) {
                creatorIsAdmin = true;
                checkFlags = checkFlags | NOT_VALIDATE_FLAG_FEE;
                break;
            }
        }
        if (!creatorIsAdmin && creator.isTrustedForIssuePerson(dcSet)) {
            creatorIsAdmin = true;
            checkFlags = checkFlags | NOT_VALIDATE_FLAG_FEE;
        }

        int result = super.isValid(forDeal, checkFlags | NOT_VALIDATE_FLAG_PUBLIC_TEXT);

        // сюда без проверки Персоны приходит
        if (result != VALIDATE_OK)
            return result;

        ///// CREATOR
        if ((checkFlags & NOT_VALIDATE_FLAG_PERSONAL) == 0L && !BlockChain.ANONIM_SERT_USE) {
            if (!creatorIsAdmin && creatorPersonDuration == null) {
                return CREATOR_NOT_PERSONALIZED;
            }
        }

        if (person instanceof PersonsAvatar) {
            errorValue = "avatar can not will have many addresses";
            return Transaction.INVALID_ITEM_OWNER;
        }

        //////// PERSON
        if (!dcSet.getItemPersonMap().contains(this.key)) {
            return Transaction.ITEM_PERSON_NOT_EXIST;
        }

        if (!person.isAlive(this.timestamp))
            return Transaction.ITEM_PERSON_IS_DEAD;

        if (certifiedPublicKeys.size() > 3) {
            errorValue = "list size > 3";
            return INVALID_PUBLIC_KEY;
        }

        ///////// PUBLIC KEYS
        for (PublicKeyAccount publicAccount : this.certifiedPublicKeys) {
            //CHECK IF PERSON PUBLIC KEY IS VALID
            if (!publicAccount.isValid()) {
                errorValue = publicAccount.getPublicKey58();
                return INVALID_PUBLIC_KEY;
            }

            if (creatorIsAdmin || (checkFlags & NOT_VALIDATE_FLAG_PERSONAL) != 0L || BlockChain.ANONIM_SERT_USE)
                continue;

            Tuple4<Long, Integer, Integer, Integer> personDuration = publicAccount.getPersonDuration(dcSet);

            if (personDuration == null) {
                if (this.add_day < 0) {
                    // нельзя снять удостоверение со счета который еще не удостоверен
                    errorValue = "add_day < 0";
                    return CREATOR_NOT_PERSONALIZED;

                } else if (creatorPersonDuration == null
                        || !this.creator.isPersonAlive(height, creatorPersonDuration)
                        || !this.creatorPerson.isAlive(timestamp)
                ) {
                    // нельзя удостоверять других тому у кого уже свой ключ просрочен
                    return CREATOR_NOT_PERSONALIZED;
                }
            } else {
                if (!personDuration.a.equals(this.key)) {
                    // переудостоверить можно только на туже персону что и раньше
                    return INVALID_PERSONALIZY_ANOTHER_PERSON;
                } else if (
                        (!this.creator.isPersonAlive(height, creatorPersonDuration)
                                || !this.creatorPerson.isAlive(timestamp))
                                && !this.key.equals(creatorPersonDuration.a)) {
                    // если этот ключ уже удостоверен, то его изменять может только сам владелец
                    // снять удостоверение ключа может только сам владелец
                    // или продлить только сам владелец может
                    return CREATOR_NOT_PERSONALIZED;
                }
            }
        }

        if (height > BlockChain.START_ISSUE_RIGHTS) {
            Fun.Tuple4<Long, Integer, Integer, Integer> creatorPerson = creator.getPersonDuration(dcSet);
            if (creatorPerson != null) {
                Set<String> thisPersonAddresses = dcSet.getPersonAddressMap().getItems(creatorPerson.a).keySet();

                BigDecimal totalERAOwned = Account.totalForAddresses(dcSet, thisPersonAddresses, AssetCls.ERA_KEY, TransactionAmount.ACTION_SEND);
                BigDecimal totalLIAOwned = Account.totalForAddresses(dcSet, thisPersonAddresses, AssetCls.LIA_KEY, TransactionAmount.ACTION_DEBT);

                int resultERA = BlockChain.VALID_PERSON_CERT_ERA(this, height, totalERAOwned, totalLIAOwned);
                if (resultERA > 0) {
                    return resultERA;
                }
            }
        } else {
            if (creator.getBalanceUSE(RIGHTS_KEY, dcSet).compareTo(BlockChain.MIN_GENERATING_BALANCE_BD) < 0) {
                return Transaction.NOT_ENOUGH_ERA_USE_100;
            }
        }

        return Transaction.VALIDATE_OK;
    }

    @Override
    public void makeItemsKeys() {
        if (isWiped() || person == null) {
            itemsKeys = new Object[][]{};
        }

        if (creatorPersonDuration == null) {
            // Creator is ADMIN
            itemsKeys = new Object[][]{
                    new Object[]{ItemCls.PERSON_TYPE, key, person.getTagsFull()}
            };
        } else {
            itemsKeys = new Object[][]{
                    new Object[]{ItemCls.PERSON_TYPE, key, person.getTagsFull()},
                    new Object[]{ItemCls.PERSON_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()}
            };
        }
    }

    @Override
    public void processBody(Block block, int forDeal) throws TxException {
        super.processBody(block, forDeal);
        processBody(dcSet, block, this, key, certifiedPublicKeys, add_day);
    }

    @Override
    public void orphanBody(Block block, int forDeal) throws TxException {
        orphanBody(dcSet, this, key, certifiedPublicKeys);
        super.orphanBody(block, forDeal);
    }

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        HashSet<Account> accounts = this.getRecipientAccounts();
        accounts.add(this.creator);
        return accounts;
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        HashSet<Account> accounts = new HashSet<Account>(3, 1);
        accounts.addAll(this.certifiedPublicKeys);

        return accounts;
    }

    @Override
    public boolean isInvolved(Account account) {
        if (account.equals(creator)) return true;

        for (PublicKeyAccount publicAccount : this.certifiedPublicKeys) {
            if (publicAccount.equals(account))
                return true;
        }

        return false;
    }


}