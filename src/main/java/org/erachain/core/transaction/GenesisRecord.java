package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import org.erachain.controller.Controller;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;

public abstract class GenesisRecord extends Transaction {

    protected static final int BASE_LENGTH = CRYPTO_TYPE_LENGTH + SIMPLE_TYPE_LENGTH;

    public GenesisRecord(byte type, String NAME_ID, String title, String tags, String message) {
        super(type, NAME_ID, title, tags, message);
    }

    protected GenesisRecord(String type_name,
                            byte[] data) throws Exception {
        super(type_name, data);

    }

    /**
     * final - наследование запрещено - все делаем тут внутри. Наследование делается в toBytesBody, toBytesBD
     *
     * @param forDeal
     * @return
     */
    @Override
    final public byte[] toBytes(int forDeal) {

        if (parseRAW != null) {
            return parseRAW;
        }

        byte[] data = new byte[]{Crypto.NOT_SIGNED, typeBytes[0]};

        // WRITE TITLE && TAGS
        byte[] titleBytes = this.getTitle().getBytes(StandardCharsets.UTF_8);
        byte[] tagsBytes = this.getTags().getBytes(StandardCharsets.UTF_8);
        data = Bytes.concat(data,
                // TITLE SIZE
                new byte[]{(byte) titleBytes.length},
                // TITLE
                titleBytes,
                // TAGS SIZE
                new byte[]{(byte) tagsBytes.length},
                // TAGS
                tagsBytes);

        if (this.dataMessage != null && this.dataMessage.length > 0) {
            byte[] dataSizeBytes = Ints.toByteArray(this.dataMessage.length);
            data = Bytes.concat(data,
                    // WRITE DATA SIZE
                    dataSizeBytes,
                    // WRITE DATA
                    this.dataMessage);

        } else {
            data = Bytes.concat(data,
                    // WRITE EMPTY DATA SIZE
                    new byte[4]);
        }

        data = Bytes.concat(data, toBytesBody(forDeal));

        return (parseRAW = data);

    }

    @Override
    protected byte[] toBytesDB() {
        return null;
    }

    //GETTERS/SETTERS

    public boolean hasPublicText() {
        return false;
    }

    public void generateSignature() {

        byte[] data = toBytes(FOR_NETWORK);

        //DIGEST
        byte[] digest = Crypto.getInstance().digest(data);
        digest = Bytes.concat(digest, digest);

        this.signature = digest;

    }

    @Override
    public PublicKeyAccount getCreator() {
        return GenesisBlock.CREATOR;
    }

    @Override
    public byte[] getSignature() {
        if (this.signature == null)
            generateSignature();

        return this.signature;
    }

    @Override
    public Long getTimestamp() {
        return Controller.getInstance().blockChain.getGenesisTimestamp();
    }


    @Override
    public void orphanBody(Block block, int forDeal) throws TxException {
        throw new TxException("Orphan denied");
    }

    @Override
    public int getDataLength(int forDeal) {
        int base_len = 2 + 2 + DATA_SIZE_LENGTH;
        if (title != null && !title.isEmpty())
            base_len += title.getBytes(StandardCharsets.UTF_8).length;

        if (tags != null && !tags.isEmpty())
            base_len += tags.getBytes(StandardCharsets.UTF_8).length;

        if (dataMessage != null && dataMessage.length > 0)
            base_len += this.dataMessage.length;

        return base_len;
    }

    //VALIDATE

    @Override
    public final boolean isSignatureValid() {
        return Arrays.equals(this.signature, this.getSignature());
    }

    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (typeBytes.length != 1) {
            errorValue = "typeBytes.length = " + typeBytes.length;
            return INVALID_FLAGS;
        }

        if (typeBytes[0] == -1) {
            // не может быть чтобы все флаги были подняты - скорее всего это из JS ошибка
            errorValue = "all = -1";
            return INVALID_FLAGS;
        }

        if (title != null && title.getBytes(StandardCharsets.UTF_8).length > MAX_TITLE_BYTES_LENGTH) {
            errorValue = "bytes: " + title.getBytes(StandardCharsets.UTF_8).length;
            return INVALID_TITLE_LENGTH_MAX;
        }
        if (tags != null && tags.getBytes(StandardCharsets.UTF_8).length > MAX_TITLE_BYTES_LENGTH) {
            errorValue = "bytes: " + tags.getBytes(StandardCharsets.UTF_8).length;
            return INVALID_TAGS_LENGTH_MAX;
        }

        if (this.dataMessage != null) {
            // CHECK DATA SIZE
            if (dataMessage.length > MAX_DATA_BYTES_LENGTH) {
                errorValue = "bytes: " + dataMessage.length;
                return INVALID_DATA_LENGTH;
            }
            if (isEncrypted()) {
                errorValue = "encrypted";
                return INVALID_DATA_FORMAT;
            }
        }

        if (dApp != null) {
            errorValue = "DAPP used";
            return ENCRYPT_DENIED_FOR_DAPP;
        }

        if (exLink != null) {
            errorValue = "exLink denied";
            return INVALID_EX_LINK_TYPE;
        }

        return VALIDATE_OK;

    }

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        return this.getRecipientAccounts();
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        HashSet<Account> accounts = new HashSet<>(1, 1);
        return accounts;
    }

    @Override
    public boolean isInvolved(Account account) {
        return false;
    }

    @Override
    public long calcBaseFee(boolean withFreeProtocol) {
        return 0L;
    }
}
