package org.erachain.core.transaction

import org.json.simple.JSONObject

open class TxException(var msg: String) : Exception(msg) {
    lateinit var tx: Transaction
    constructor() : this("") {
    }
}

class TxSignException() : TxException("Wrong Signature") {
}

class TxParseException(msg: String, val field: String, val help: String?) : TxException(msg) {
    fun updateJson(out: JSONObject) {
        Transaction.updateMapByError2static(out, Transaction.PARSE_ERROR,
            msg + " for field '$field" + if (help == null) "`." else "`. Help: $help", null)
    }
}

class BlockException(msg: String) : TxException(msg) {
}

