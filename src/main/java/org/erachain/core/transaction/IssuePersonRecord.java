package org.erachain.core.transaction;

import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.item.persons.PersonHuman;
import org.json.simple.JSONObject;
import org.mapdb.Fun;

import java.math.BigDecimal;
import java.util.*;

/**
 * typeBytes[POS_TX_FLAGS_1] = 1 - certify public of person key too
 */
public class IssuePersonRecord extends IssueItemRecord {
    public static final byte TYPE_ID = (byte) ISSUE_PERSON_TRANSACTION;
    public static final String TYPE_NAME = "Issue Person";
    /**
     * Нельзя делать большой, так как вся комиссия будет эммитироваться - а значит слишком большой размер будет эммитрировать больше
     */
    static int AND_CERTIFY_MASK = 1;

    public IssuePersonRecord(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, ItemCls.PERSON_TYPE, pubKey, signature, startPos, data, forDeal);
    }

    public IssuePersonRecord(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);
    }

    public IssuePersonRecord(PublicKeyAccount creator, PersonCls person, byte feePow, long timestamp, byte[] signature) {
        super(makeTypeBytes(TYPE_ID), TYPE_NAME, creator, null, null, null, person, feePow, timestamp, signature);
    }

    public IssuePersonRecord(PublicKeyAccount creator, ExLink linkTo, boolean andCertify, PersonCls person, byte feePow, long timestamp) {
        super(makeTypeBytes(TYPE_ID, (byte) 0, andCertify ? (byte) AND_CERTIFY_MASK : (byte) 0), TYPE_NAME, creator,
                null, null, linkTo, person, feePow, timestamp);
    }

    //GETTERS/SETTERS

    @Override
    public long getInvitedFee() {
        return 0L;
    }

    //PARSE CONVERT

    public boolean isAndCertifyPubKey() {
        return !(item instanceof PersonHuman) || (typeBytes[POS_TX_FLAGS_1] & AND_CERTIFY_MASK) != 0;
    }

    @Override
    public List<byte[]> getOtherSignatures() {
        byte[] authorSignature = item.getAuthorSignature();
        if (authorSignature != null) {
            List<byte[]> items = new ArrayList<byte[]>();
            items.add(authorSignature);
            return items;
        }
        return null;
    }

    @Override
    public boolean hasPublicText() {
        return !BlockChain.ANONIM_SERT_USE;
    }

    @Override
    public void toJsonBody(JSONObject transaction) {
        super.toJsonBody(transaction);

        transaction.put("certify", isAndCertifyPubKey());
    }

    //VALIDATE

    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        if (item instanceof PersonHuman) {
            PersonHuman human = (PersonHuman) item;
            if (!Arrays.equals(human.getAuthor().getPublicKey(), creator.getPublicKey())) {
                // OWNER of personal INFO not is CREATOR
                if (human.getAuthorSignature() == null) {
                    return Transaction.ITEM_PERSON_AUTHOR_SIGNATURE_INVALID;
                }
                if (!human.isSignatureValid()) {
                    return Transaction.ITEM_PERSON_AUTHOR_SIGNATURE_INVALID;
                }
            } else {
                // SELF make person
                if (human.getAuthorSignature() != null && !human.isSignatureValid()) {
                    return Transaction.ITEM_PERSON_AUTHOR_SIGNATURE_INVALID;
                }
            }

            if (dcSet.getTransactionFinalMapSigns().contains(human.getAuthorSignature())) {
                return Transaction.ITEM_DUPLICATE;
            }
        } else {
            // все остальные виды должны быть сразу удостоверены на счет
            if (!isAndCertifyPubKey()) {
                return Transaction.MUST_CERTIFY_HERE;
            }

        }

        if (this.item.isNovaItem(this.dcSet) > 0) {
            Fun.Tuple3<Long, Long, byte[]> item = BlockChain.NOVA_PERSONS.get(this.item.getName());
            if (!item.b.equals(this.item.getStartDate())) {
                return INVALID_TIMESTAMP_START;
            }
        }

        if (!item.hasStopDate()) {
            // ALIVE
            Fun.Tuple4<Long, Integer, Integer, Integer> creatorPerson = creator.getPersonDuration(dcSet);
            if (creatorPerson != null) {
                Set<String> thisPersonAddresses = dcSet.getPersonAddressMap().getItems(creatorPerson.a).keySet();

                BigDecimal totalERAOwned = Account.totalForAddresses(dcSet, thisPersonAddresses, AssetCls.ERA_KEY, TransactionAmount.ACTION_SEND);
                BigDecimal totalLIAOwned = Account.totalForAddresses(dcSet, thisPersonAddresses, AssetCls.LIA_KEY, TransactionAmount.ACTION_SEND);

                int resultERA = BlockChain.VALID_PERSON_REG_ERA(this, height, totalERAOwned, totalLIAOwned);
                if (resultERA > 0) {
                    return resultERA;
                }
            }
        }

        if (isAndCertifyPubKey()) {
            if (item.hasStopDate()) {
                return ITEM_PERSON_IS_DEAD;
            } else if (item.getOwner().getPersonDuration(dcSet) != null) {
                return INVALID_PERSONALIZY_ANOTHER_PERSON;
            }
        }

        ////////////// SUPER IS VALID
        // IF BALANCE 0 or more - not check FEE
        boolean checkFeeBalance = creator.getBalance(dcSet, FEE_KEY).a.b.compareTo(BigDecimal.ZERO) < 0;
        int res = super.isValid(forDeal, checkFlags |
                (checkFeeBalance ? 0L : NOT_VALIDATE_FLAG_FEE) | NOT_VALIDATE_FLAG_PUBLIC_TEXT);
        if (res != VALIDATE_OK && res != CREATOR_NOT_PERSONALIZED)
            return res;

        // FIRST PERSONS INSERT as ADMIN
        boolean creatorAdmin = false;
        boolean creatorIsPerson = isCreatorPersonalized();
        if ((checkFlags & NOT_VALIDATE_FLAG_PERSONAL) == 0L && !BlockChain.ANONIM_SERT_USE
                && !creatorIsPerson) {
            // ALL Persons by ADMINS
            for (String admin : BlockChain.GENESIS_ADMINS) {
                if (creator.equals(admin)) {
                    creatorAdmin = true;
                    break;
                }
            }
            if (!creatorAdmin && !creator.isTrustedForIssuePerson(dcSet)) {
                return CREATOR_NOT_PERSONALIZED;
            }
        }

        return res;
    }

    //PROCESS/ORPHAN

    //@Override
    public void processBody(Block block, int forDeal) throws TxException {
        //UPDATE CREATOR
        super.processBody(block, forDeal);

        PersonCls person = (PersonCls) this.item;
        PublicKeyAccount owner = person.getOwner();
        PublicKeyAccount author = person.getAuthor();
        if (!author.equals(creator)) {
            // Это нужно для быстрого поиска по публичному ключу создателя персоны,
            // которая еще не удостоверена вообще
            // но надо понимать что тут будет только последняя запись создания персоны и номер на нее
            this.dcSet.getTransactionFinalMapSigns().put(author.getPublicKey(), person.getKey());
        }
        if (!owner.equals(author)) {
            // Это нужно для быстрого поиска по публичному ключу создателя персоны,
            // которая еще не удостоверена вообще
            // но надо понимать что тут будет только последняя запись создания персоны и номер на нее
            this.dcSet.getTransactionFinalMapSigns().put(owner.getPublicKey(), person.getKey());
        }

        byte[] authorSign = person.getAuthorSignature();
        if (authorSign != null) {
            // for quick search public keys by address - use PUB_KEY from Person DATA owner
            // used in - controller.Controller.getPublicKeyByAddress
            if (!author.equals(creator)) {
                // если создатель персоны и выпускающий - один счет то игнорируем
                long[] authorLastTimestamp = author.getLastTimestamp(this.dcSet);
                if (authorLastTimestamp == null) {
                    // добавим первую точку счете персоны по времени
                    author.setLastTimestamp(new long[]{timestamp, dbRef}, this.dcSet);
                }
            }

            // запомним подпись для поиска потом
            dcSet.getTransactionFinalMapSigns().put(authorSign, dbRef);
        }

        if (isAndCertifyPubKey()) {
            List<PublicKeyAccount> pubKeys = new ArrayList<>();
            pubKeys.add(owner);
            RCertifyPubKeys.processBody(dcSet, block, this, person.getKey(), pubKeys, 1);
        }

    }

    //@Override
    public void orphanBody(Block block, int forDeal) throws TxException {

        PublicKeyAccount owner = item.getOwner();
        PublicKeyAccount author = item.getAuthor();

        if (isAndCertifyPubKey()) {
            List<PublicKeyAccount> pubKeys = new ArrayList<>();
            pubKeys.add(owner);
            RCertifyPubKeys.orphanBody(dcSet, this, item.getKey(), pubKeys);
        }

        byte[] authorSign = item.getAuthorSignature();
        if (authorSign != null) {
            // for quick search public keys by address - use PUB_KEY from Person DATA owner
            // used in - controller.Controller.getPublicKeyByAddress
            if (!author.equals(creator)) {
                // если создатель персоны и выпускающий - один счет то игнорируем
                long[] authorLastTimestamp = author.getLastTimestamp(this.dcSet);
                if (authorLastTimestamp == null) {
                    author.removeLastTimestamp(this.dcSet, timestamp);
                }
            }

            // удалим подпись для поиска
            dcSet.getTransactionFinalMapSigns().delete(authorSign);
        }

        if (!author.equals(creator))
            this.dcSet.getTransactionFinalMapSigns().delete(author.getPublicKey());
        if (!owner.equals(author))
            this.dcSet.getTransactionFinalMapSigns().delete(owner.getPublicKey());

        // ORPHAN SUPER
        super.orphanBody(block, forDeal);

    }

    @Override
    public long calcBaseFee(boolean withFreeProtocol) {

        if (item.isAlive(this.timestamp)) {
            // IF PERSON is LIVE
            return super.calcBaseFee(withFreeProtocol) >> 1;
        }

        // is DEAD
        return super.calcBaseFee(withFreeProtocol);
    }

    @Override
    public List<PublicKeyAccount> getPublicKeys() {
        List<PublicKeyAccount> accounts = super.getPublicKeys();
        PublicKeyAccount inviter = ((PersonCls) item).getInviter();
        if (inviter != null) {
            accounts.add(inviter);
        }
        return accounts;
    }

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        HashSet<Account> accounts = super.getInvolvedAccounts();
        PublicKeyAccount inviter = ((PersonCls) item).getInviter();
        if (inviter != null) {
            accounts.add(inviter);
        }
        return accounts;
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        HashSet<Account> accounts = super.getRecipientAccounts();
        PublicKeyAccount inviter = ((PersonCls) item).getInviter();
        if (inviter != null) {
            accounts.add(inviter);
        }

        return accounts;
    }

    @Override
    public boolean isInvolved(Account account) {
        return super.isInvolved(account) || account.equals(((PersonCls)item).getInviter());
    }
}
