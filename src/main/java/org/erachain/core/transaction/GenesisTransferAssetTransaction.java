package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.block.Block;
import org.erachain.core.block.GenesisBlock;
import org.erachain.datachain.DCSet;
import org.erachain.lang.Lang;
import org.erachain.utils.BigDecimalUtil;
import org.erachain.utils.NumberAsString;
import org.json.simple.JSONObject;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

public class GenesisTransferAssetTransaction extends GenesisRecord {

    private static final byte TYPE_ID = (byte) Transaction.GENESIS_SEND_ASSET_TRANSACTION;
    private static final String NAME_ID = "GENESIS Send Asset";
    private static final int RECIPIENT_LENGTH = TransactionAmount.RECIPIENT_LENGTH;
    private static final int OWNER_LENGTH = RECIPIENT_LENGTH;
    private static final int AMOUNT_LENGTH = TransactionAmount.AMOUNT_LENGTH;

    private static final int BASE_LENGTH = GenesisRecord.BASE_LENGTH + RECIPIENT_LENGTH + KEY_LENGTH + AMOUNT_LENGTH;
    private final Account recipient;
    private final long key;
    private final Account sender;
    private final BigDecimal amount;

    /**
     * for RCalculated
     */
    protected GenesisTransferAssetTransaction(String name, byte[] data) throws Exception {
        super(name, data);

        //CHECK IF WE MATCH BLOCK LENGTH
        if (data.length < BASE_LENGTH) {
            throw new Exception("Data does not match block length: " + data.length + " in " + NAME_ID);
        }

        //READ RECIPIENT
        byte[] recipientBytes = Arrays.copyOfRange(data, parsePos, parsePos + RECIPIENT_LENGTH);
        recipient = new Account(recipientBytes);
        parsePos += RECIPIENT_LENGTH;

        //READ KEY
        byte[] keyBytes = Arrays.copyOfRange(data, parsePos, parsePos + KEY_LENGTH);
        key = Longs.fromByteArray(keyBytes);
        parsePos += KEY_LENGTH;

        //READ AMOUNT
        BigDecimal amount = BigDecimalUtil.fromBytes(data, parsePos);
        parsePos += BigDecimalUtil.BytesLen();

        if (amount.signum() < 0) {
            this.amount = amount.negate();
            // READ SENDER for DEBT
            byte[] senderBytes = Arrays.copyOfRange(data, parsePos, parsePos + OWNER_LENGTH);
            sender = new Account(senderBytes);
            parsePos += OWNER_LENGTH;
        } else {
            sender = null;
            this.amount = amount;
        }

        this.generateSignature();

    }

    protected GenesisTransferAssetTransaction(byte[] data) throws Exception {
        this(NAME_ID, data);
    }

    public GenesisTransferAssetTransaction(Account recipient, long key, BigDecimal amount, String title, String tags, String message) {
        super(TYPE_ID, NAME_ID, title, tags, message);

        this.recipient = recipient;
        this.amount = amount;
        this.key = key;
        this.sender = null;

        this.generateSignature();
    }

    // RENT
    public GenesisTransferAssetTransaction(Account recipient, long key, BigDecimal amount, Account sender, String title, String tags, String message) {
        super(TYPE_ID, NAME_ID, title, tags, message);

        this.recipient = recipient;
        this.amount = amount;
        this.key = key;
        this.sender = sender;

        this.generateSignature();
    }

    // for RCalculated
    public GenesisTransferAssetTransaction(byte type, String name, Account recipient, long key, BigDecimal amount, String title, String tags, String message) {
        super(type, name, title, tags, message);

        assert (amount != null);
        assert (recipient != null);

        this.recipient = recipient;
        this.amount = amount;
        this.key = key;
        this.sender = null;

    }


    //GETTERS/SETTERS

    public Account getSender() {
        return this.sender;
    }

    public Account getRecipient() {
        return this.recipient;
    }

    public String viewActionType() {
        return TransactionAmount.viewTypeName(this.amount, false);
    }

    @Override
    public String viewSubTypeName() {
        return TransactionAmount.viewSubTypeName(sender == null ? Account.BALANCE_POS_OWN : Account.BALANCE_POS_DEBT);
    }

    public String viewSubTypeName(JSONObject langObj) {
        return Lang.T(TransactionAmount.viewSubTypeName(sender == null ? Account.BALANCE_POS_OWN : Account.BALANCE_POS_DEBT), langObj);
    }

    @Override
    public BigDecimal getAmount() {
        return this.amount;
    }

    @Override
    public long getKey() {
        return this.key;
    }

    @Override
    public long getAssetKey() {
        return this.key;
    }

    public void setDC(DCSet dcSet, int forDeal, int blockHeight, int seqNo, boolean andUpdateFromState) {
        super.setDC(dcSet, forDeal, blockHeight, seqNo, false);

        if (false)
            updateFromStateDB();
    }


    @Override
    public BigDecimal getAmount(Account account) {
        if (recipient.equals(account)) {
            return amount;
        }

        return BigDecimal.ZERO;
    }

    @Override
    public String viewAmount() {
        return NumberAsString.formatAsString(this.amount);
    }

    @Override
    public String viewRecipient() {
        return recipient.getPersonAsString();
    }

    //PARSE/CONVERT

    @SuppressWarnings("unchecked")
    @Override
    public void toJsonBody(JSONObject transaction) {
        if (this.sender != null)
            sender.toJsonPersonInfo(transaction, "sender");

        recipient.toJsonPersonInfo(transaction, "recipient");
        transaction.put("asset", this.key);
        transaction.put("amount", this.amount.toPlainString());
    }

    @Override
    public byte[] toBytesBody(int forDeal) {

        byte[] data;

        byte[] keyBytes = Longs.toByteArray(this.key);

        data = Bytes.concat(
                //WRITE RECIPIENT
                this.recipient.getBytes(),
                //WRITE KEY
                keyBytes
        );

        //WRITE AMOUNT

        if (sender != null) {
            data = BigDecimalUtil.toBytes(data, amount.negate());
            //WRITE OWNER
            data = Bytes.concat(data, this.sender.getBytes());
        } else {
            data = BigDecimalUtil.toBytes(data, amount);
        }

        return data;
    }

    @Override
    public int getDataLength(int forDeal) {
        return super.getDataLength(forDeal)
                + RECIPIENT_LENGTH + KEY_LENGTH + AMOUNT_LENGTH
                + (sender == null ? 0 : RECIPIENT_LENGTH);
    }


    //VALIDATE

    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        //CHECK IF RECIPIENT IS VALID ADDRESS
        Tuple2<Account, String> result = Account.tryMakeAccount(this.recipient.getAddress());
        if (result.a == null) {
            return INVALID_ADDRESS;
        }

        if (!BigDecimalUtil.isValid(amount)) {
            errorValue = "max length";
            return Transaction.AMOUNT_LENGHT_SO_LONG;
        }

        if (this.amount.signum() <= 0) {
            return INVALID_AMOUNT;
        }

        if (key <= 0) {
            return INVALID_ITEM_KEY;
        }

        return VALIDATE_OK;
    }

    //PROCESS/ORPHAN

    @Override
    public void processBody(Block block, int forDeal) throws TxException {

        //BigDecimal amount;
        boolean asDebt;
        if (sender != null) {
            asDebt = true;
        } else {
            //amount = this.amount;
            asDebt = false;
        }

        if (asDebt) {
            // THIS is CREDIT
            // UPDATE RECIPIENT DEBT
            this.recipient.changeBalance(this.dcSet, false, Account.BALANCE_POS_DEBT, key, amount,
                    false, false);
        } else {
            // UPDATE RECIPIENT OWN
            this.recipient.changeBalance(this.dcSet, false, Account.BALANCE_POS_OWN, key, amount,
                    false, false);
        }

        //UPDATE TIMESTAMP OF RECIPIENT
        this.recipient.setLastTimestamp(new long[]{this.timestamp, dbRef}, this.dcSet);

        if (this.getKey() == Transaction.RIGHTS_KEY) {
            // PROCESS FORGING DATA
            //// SKIP Genesis Block
            int currentForgingBalance = this.recipient.getForgingBalance(dcSet);
            this.recipient.setForgingData(this.dcSet, 1, currentForgingBalance);
        }

        if (asDebt) {

            // THIS is CREDIT
            this.sender.changeBalance(this.dcSet, false, -Account.BALANCE_POS_DEBT, key, amount,
                    false, false);
            this.dcSet.getCreditAddressesMap().add(
                    new Tuple3<String, Long, String>(
                            this.sender.getAddress(), key,
                            this.recipient.getAddress()),
                    amount);

        } else {
            // CREATOR update
            if (key == FEE_KEY) {
                BlockChain.FEE_ASSET_EMITTER.changeBalance(this.dcSet, true, Account.BALANCE_POS_OWN, key, amount,
                        false, false);

                if (BlockChain.CLONE_MODE) {
                    BigDecimal sideRoyalty = amount.multiply(new BigDecimal("0.05")); // 5%
                    BlockChain.CLONE_ROYALTY_ERACHAIN_ACCOUNT.changeBalance(dcSet, false, Account.BALANCE_POS_OWN, Transaction.FEE_KEY,
                            sideRoyalty, false, false);
                    BlockChain.FEE_ASSET_EMITTER.changeBalance(this.dcSet, true, Account.BALANCE_POS_OWN, Transaction.FEE_KEY,
                            sideRoyalty, false, false);
                }

            } else {
                GenesisBlock.CREATOR.changeBalance(this.dcSet, true, Account.BALANCE_POS_OWN, key, amount,
                        false, false);
            }
        }
    }

    //REST

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        HashSet<Account> accounts = new HashSet<Account>(3, 1);
        if (this.sender != null)
            accounts.add(this.sender);

        accounts.addAll(this.getRecipientAccounts());
        return accounts;
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        HashSet<Account> accounts = new HashSet<Account>();
        accounts.add(this.recipient);
        return accounts;
    }

    @Override
    public boolean isInvolved(Account account) {

        return account.equals(sender)
                || account.equals(recipient);
    }

    //@Override
    public Map<String, Map<Long, BigDecimal>> getAssetAmount() {
        Map<String, Map<Long, BigDecimal>> assetAmount = new LinkedHashMap<>();

        assetAmount = addAssetAmount(assetAmount, this.recipient.getAddress(), this.key, this.amount);

        return assetAmount;
    }

}
