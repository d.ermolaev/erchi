package org.erachain.core.transaction;

import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.block.Block;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetFactory;

import java.math.BigDecimal;

// core.block.Block.isValid(DLSet) - check as false it
public class GenesisIssueAssetTransaction extends GenesisIssueItemRecord {

    private static final byte TYPE_ID = (byte) GENESIS_ISSUE_ASSET_TRANSACTION;
    private static final String NAME_ID = "GENESIS Issue Asset";
    private boolean involvedInWallet;

    public GenesisIssueAssetTransaction(AssetCls asset, String title, String tags, String message) {
        super(TYPE_ID, NAME_ID, asset, title, tags, message);
    }

    public GenesisIssueAssetTransaction(byte[] data) throws Exception {
        super(NAME_ID, data);

        item = AssetFactory.getInstance().parse(parsePos, data, false);
        this.generateSignature();

    }

    //GETTERS/SETTERS

    //PARSE CONVERT

    @Override
    public boolean isInvolved(Account account) {
        if (!this.involvedInWallet) {
            // only one record to wallet for all accounts
            this.involvedInWallet = true;
            return true;
        }

        return false;

    }

    public void processBody(Block block, int forDeal) {

        AssetCls asset = (AssetCls) item;
        if (this.dcSet.getItemAssetMap().size() > 2
                && (!BlockChain.TEST_MODE || item.isNovaItem(dcSet) <= 0)
        )
            // SKIP all base TOKENS
            return;

        super.processBody(block, forDeal);

        long quantity = asset.getQuantity();
        if (quantity > 0L) {
            Account owner = item.getOwner();
            Long assetKey = item.getKey();
            // надо добавить баланс на счет
            owner.changeBalance(dcSet, false, assetKey,
                    new BigDecimal(quantity).setScale(0), false, false);

            // make HOLD balance
            owner.changeBalance(dcSet, false, Account.BALANCE_POS_HOLD, assetKey,
                    new BigDecimal(quantity).setScale(0), false, false);
        }
    }

}
