package org.erachain.core.transaction;

import org.erachain.core.BlockChain;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.polls.PollCls;
import org.json.simple.JSONObject;

import java.math.BigDecimal;

public class IssuePollRecord extends IssueItemRecord {
    public static final byte TYPE_ID = (byte) ISSUE_POLL_TRANSACTION;
    public static final String TYPE_NAME = "Issue Poll";

    public IssuePollRecord(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, ItemCls.POLL_TYPE, pubKey, signature, startPos, data, forDeal);
    }

    public IssuePollRecord(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);
    }

    public IssuePollRecord(PublicKeyAccount creator, ExLink linkTo, PollCls poll, byte feePow, long timestamp) {
        super(makeTypeBytes(TYPE_ID), TYPE_NAME, creator, null, null, linkTo, poll, feePow, timestamp);
    }

    //GETTERS/SETTERS

    //PARSE CONVERT

    //VALIDATE

    //PROCESS/ORPHAN

}
