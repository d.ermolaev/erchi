package org.erachain.core.transaction;

import org.erachain.core.BlockChain;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.statuses.StatusCls;
import org.json.simple.JSONObject;

import java.math.BigDecimal;


public class IssueStatusRecord extends IssueItemRecord {
    public static final byte TYPE_ID = (byte) ISSUE_STATUS_TRANSACTION;
    public static final String TYPE_NAME = "Issue Status";

    public IssueStatusRecord(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, ItemCls.STATUS_TYPE, pubKey, signature, startPos, data, forDeal);
    }

    public IssueStatusRecord(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);
    }

    public IssueStatusRecord(PublicKeyAccount creator, StatusCls status, byte feePow, long timestamp, byte[] signature) {
        super(makeTypeBytes(TYPE_ID), TYPE_NAME, creator, null, null, null, status, feePow, timestamp, signature);
    }

    public IssueStatusRecord(PublicKeyAccount creator, ExLink linkTo, StatusCls status, byte feePow, long timestamp) {
        super(makeTypeBytes(TYPE_ID), TYPE_NAME, creator, null, null, linkTo, status, feePow, timestamp);
    }

    //GETTERS/SETTERS

    //VALIDATE

    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        int result = super.isValid(forDeal, checkFlags);
        return result;
    }

    //PROCESS/ORPHAN

}
