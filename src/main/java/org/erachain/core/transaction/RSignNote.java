package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.crypto.Base58;
import org.erachain.core.exdata.ExData;
import org.erachain.core.exdata.exActions.ExAction;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.exdata.exLink.ExLinkAuthor;
import org.erachain.core.exdata.exLink.ExLinkSource;
import org.erachain.core.item.ItemCls;
import org.erachain.dapp.DAPP;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.TransactionFinalMapSigns;
import org.erachain.gui.library.ASMutableTreeNode;
import org.erachain.lang.Lang;
import org.json.simple.JSONObject;
import org.mapdb.Fun;

import javax.swing.tree.DefaultMutableTreeNode;
import java.math.BigDecimal;
import java.util.*;

/**
 * PROPERTIES:
 * [0] - type
 * [1] - version
 * [2] bits[0] - =1 - has Template (OLD)
 * [3] - < 0 - has DATA
 */
public class RSignNote extends Transaction implements Itemable {

    public static final byte TYPE_ID = (byte) SIGN_NOTE_TRANSACTION;
    public static final String TYPE_NAME = "Note";

    protected final byte[] exDataBytes;
    /**
     * Тут запоминаем откуда данные получили - если из базы то и парсим соответствующе
     */
    protected byte[] dataForDB;
    ExData extendedData;

    public RSignNote(byte[] typeBytes, PublicKeyAccount creator, String title, String tags,
                     byte[] dataMessage, boolean isText, boolean encrypted, ExLink exLink, DAPP dApp, byte feePow,
                     byte[] exDataBytes, byte[] dataForDB, long timestamp) {

        super(typeBytes, TYPE_NAME, creator, title, tags, dataMessage, isText, encrypted,
                exLink, dApp, feePow, timestamp);

        this.exDataBytes = exDataBytes;
        this.dataForDB = dataForDB;
    }

    public RSignNote(byte[] typeBytes, PublicKeyAccount creator, String title, String tags,
                     byte[] dataMessage, boolean isText, boolean encrypted, ExLink exLink, DAPP dApp, byte feePow,
                     byte[] exDataBytes, byte[] dataForDB, long timestamp, byte[] signature) {

        super(typeBytes, TYPE_NAME, creator, title, tags, dataMessage, isText, encrypted,
                exLink, dApp, feePow, timestamp);

        this.exDataBytes = exDataBytes;
        this.dataForDB = dataForDB;
        this.signature = signature;

    }

    public RSignNote(PublicKeyAccount creator, byte feePow, byte[] exDataBytes,
                     byte[] dataForDB, long timestamp, byte[] signature, long seqNo, long feeLong) {
        this(makeTypeBytes(TYPE_ID), creator, null, null, null, false, false, null, null,
                feePow, exDataBytes, dataForDB, timestamp);
        this.signature = signature;
        if (seqNo > 0)
            this.setHeightSeq(seqNo);
        this.fee = BigDecimal.valueOf(feeLong, BlockChain.FEE_SCALE);
    }

    public RSignNote(PublicKeyAccount creator, String title, byte[] dataMess, byte feePow, byte[] exDataBytes) {
        this(makeTypeBytes(TYPE_ID), creator, title, null, dataMess, true, false, null, null,
                feePow, exDataBytes, null, 0L);
    }

    public RSignNote(PublicKeyAccount creator, String title, String tags,
                     byte[] dataMessage, boolean isText, boolean encrypted, ExLink exLink, DAPP dApp, byte feePow,
                     byte[] exDataBytes, long timestamp) {
        super(makeTypeBytes(TYPE_ID), TYPE_NAME, creator, title, tags, dataMessage, isText, encrypted,
                exLink, dApp, feePow, timestamp);

        this.exDataBytes = exDataBytes;
    }

    // asPack
    public RSignNote(byte[] typeBytes, PublicKeyAccount creator, byte[] exDataBytes,
                     byte[] dataForDB, byte[] signature) {
        this(typeBytes, creator, null, null, null, false, false, null, null,
                (byte) 0, exDataBytes, dataForDB, 0L);
        this.signature = signature;
    }

    /**
     * PARSE
     *
     * @param pubKey
     * @param signature
     * @param startPos
     * @param data
     * @param forDeal
     * @throws Exception
     */
    public RSignNote(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, pubKey, signature, startPos, data, forDeal);

        //CHECK IF WE MATCH BLOCK LENGTH
        int test_len;
        if (forDeal == Transaction.FOR_MYPACK) {
            test_len = BASE_LENGTH_AS_MYPACK;
        } else if (forDeal == Transaction.FOR_PACK) {
            test_len = BASE_LENGTH_AS_PACK;
        } else if (forDeal == Transaction.FOR_DB_RECORD) {
            test_len = BASE_LENGTH_AS_DBRECORD;
        } else {
            test_len = BASE_LENGTH;
        }

        if (data.length < test_len) {
            throw new Exception("Data does not match RAW length " + data.length + " < " + test_len);
        }

        //////// BODY PARSE

        //READ DATA SIZE
        byte[] dataSizeBytes = Arrays.copyOfRange(data, parsePos, parsePos + DATA_SIZE_LENGTH);
        int dataSize = Ints.fromByteArray(dataSizeBytes);
        parsePos += DATA_SIZE_LENGTH;

        //READ DATA
        exDataBytes = Arrays.copyOfRange(data, parsePos, parsePos + dataSize);
        parsePos += dataSize;

        if (forDeal == FOR_DB_RECORD) {
            parseDB(data);
        }

    }

    @Deprecated
    // creator, FEE_POWER, data, dbData, timestamp + 10
    public RSignNote(PublicKeyAccount creator, byte feePow, byte[] exDataBytes, byte[] dataForDB, long timestamp) {
        super(makeTypeBytes(TYPE_ID), TYPE_NAME, creator, null, null, null, false, false,
                null, null, feePow, timestamp);
        this.exDataBytes = exDataBytes;
        this.dataForDB = dataForDB;
    }

    @Deprecated
    // creator, FEE_POWER, dbData, timestamp + 10, new byte[64]
    public RSignNote(PublicKeyAccount creator, byte feePow, byte[] exDataBytes, byte[] dataForDB, long timestamp, byte[] signature) {
        super(makeTypeBytes(TYPE_ID), TYPE_NAME, creator, null, null, null, false, false,
                null, null, feePow, timestamp);
        this.exDataBytes = exDataBytes;
        this.dataForDB = dataForDB;
        this.signature = signature;
    }

    @Deprecated
    // creator, feePow, exDataBytes, NTP.getTime()
    public RSignNote(PublicKeyAccount creator, byte feePow, byte[] exDataBytes, long timestamp) {
        super(makeTypeBytes(TYPE_ID), TYPE_NAME, creator, null, null, null, false, false,
                null, null, feePow, timestamp);
        this.exDataBytes = exDataBytes;
    }

    @Override
    public void parseDB(byte[] data) throws Exception {

        super.parseDB(data);

        // ADD local DB-DATA
        //READ DB-DATA SIZE
        byte[] dbDataSizeBytes = Arrays.copyOfRange(data, parsePos, parsePos + DATA_SIZE_LENGTH);
        int dbDataSize = Ints.fromByteArray(dbDataSizeBytes);
        parsePos += DATA_SIZE_LENGTH;

        if (dbDataSize > 0) {
            //READ DB-DATA
            dataForDB = Arrays.copyOfRange(data, parsePos, parsePos + dbDataSize);
            parsePos += dbDataSize;
        }

    }

    @Override
    public byte[] toBytesBody(int forDeal) {

        byte[] data = new byte[0];

        if (this.exDataBytes != null) {

            //WRITE DATA SIZE
            byte[] dataSizeBytes = Ints.toByteArray(this.exDataBytes.length);
            data = Bytes.concat(data, dataSizeBytes);

            //WRITE DATA
            data = Bytes.concat(data, this.exDataBytes);

        }

        return data;
    }

    @Override
    public byte[] toBytesDB() {

        byte[] data = super.toBytesDB();

        if (this.extendedData != null) {
            this.dataForDB = this.extendedData.makeDBData();
        }

        if (this.dataForDB == null || this.dataForDB.length == 0) {
            data = Bytes.concat(data, new byte[DATA_SIZE_LENGTH]);
        } else {
            //WRITE DB-DATA SIZE
            byte[] dataDBSizeBytes = Ints.toByteArray(this.dataForDB.length);
            data = Bytes.concat(data, dataDBSizeBytes);

            //WRITE DB-DATA
            data = Bytes.concat(data, this.dataForDB);
        }

        return data;

    }

    //GETTERS/SETTERS


    @Override
    public void setDC(DCSet dcSet, boolean andUpdateFromState) {
        super.setDC(dcSet, false);


        // LOAD values from EXTERNAL DATA
        parseDataV2WithoutFiles();

        if (andUpdateFromState && !isWiped())
            updateFromStateDB();
    }

    @Override
    public ItemCls getItem() {
        if (extendedData.hasExAction()) {
            return getExAction().getAsset();
        }
        return extendedData.getTemplate();
    }

    @Override
    public void makeItemsKeys() {
        if (isWiped()) {
            itemsKeys = new Object[][]{};
        }

        List<Object> listTags = new ArrayList<>();

        if (creatorPersonDuration != null) {
            // AS PERSON
            listTags.add(new Object[]{ItemCls.PERSON_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()});
            // AS AUTHOR
            listTags.add(new Object[]{ItemCls.AUTHOR_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()});
        }

        if (extendedData.hasExAction()) {
            extendedData.getExAction().updateItemsKeys(listTags);
        }

        if (extendedData.getTemplateKey() != 0L) {
            listTags.add(new Object[]{ItemCls.TEMPLATE_TYPE, extendedData.getTemplateKey()});
        }

        if (extendedData.hasAuthors()) {
            for (ExLinkAuthor author : extendedData.getAuthors()) {
                listTags.add(new Object[]{ItemCls.AUTHOR_TYPE, author.getRef()});
            }
        }

        itemsKeys = listTags.toArray(new Object[][]{});

    }

    @Override
    public ExLink getExLink() {
        // нельзя использовать внутренюю от Трнзакции - так как она начнет по другому байт-код делать и парсить
        return extendedData.getExLink();
    }

    public ExAction getExAction() {
        return extendedData.getExAction();
    }

    public boolean hasExAction() {
        return extendedData.hasExAction();
    }

    public boolean hasLinkRecipients() {
        return extendedData.hasRecipients();
    }

    public Account[] getRecipients() {
        return extendedData.getRecipients();
    }

    public boolean hasAuthors() {
        return extendedData.hasAuthors();
    }

    public boolean hasSources() {
        return extendedData.hasSources();
    }

    public ExLinkAuthor[] getAuthors() {
        return extendedData.getAuthors();
    }

    public ExLinkSource[] getSources() {
        return extendedData.getSources();
    }

    @Override
    public long getKey() {
        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }

        if (extendedData.hasExAction()) {
            return getExAction().getAssetKey();
        }

        return extendedData.getTemplateKey();
    }

    public byte[] getExDataBytes() {
        return this.exDataBytes;
    }

    public String getMessage() {
        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }
        return extendedData.getMessage();
    }

    @Override
    public BigDecimal getAmount() {
        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }

        if (extendedData.hasExAction()) {
            return getExAction().getTotalPay();
        }
        return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal getAmount(Account account) {
        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }

        if (extendedData.hasExAction()) {
            return getExAction().getAmount(account);
        }
        return BigDecimal.ZERO;
    }

    @Override
    public String viewSubTypeName() {
        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }

        if (extendedData.hasExAction()) {
            return Lang.T(getExAction().viewActionType()) + ":" + Lang.T(getExAction().viewType());
        }

        return "";
    }

    @Override
    public String viewSubTypeName(JSONObject langObj) {
        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }

        if (extendedData.hasExAction()) {
            return Lang.T(getExAction().viewActionType(), langObj) + ":" + Lang.T(getExAction().viewType(), langObj);
        }

        return "";
    }

    public DefaultMutableTreeNode viewLinksTree() {

        DefaultMutableTreeNode root = super.viewLinksTree();

        if (extendedData.getTemplate() != null) {
            ASMutableTreeNode item = new ASMutableTreeNode(Lang.T("Template"));
            item.add(new DefaultMutableTreeNode(extendedData.getTemplate()));
            if (root == null) root = new DefaultMutableTreeNode(this);

            root.add(item);
        }

        if (extendedData.hasAuthors()) {
            ASMutableTreeNode item = new ASMutableTreeNode(Lang.T("Authors"));
            for (ExLinkAuthor author : extendedData.getAuthors()) {
                item.add(new DefaultMutableTreeNode(author));
            }
            if (root == null) root = new DefaultMutableTreeNode(this);
            root.add(item);
        }

        if (extendedData.hasSources()) {
            ASMutableTreeNode item = new ASMutableTreeNode(Lang.T("Sources"));
            for (ExLinkSource source : extendedData.getSources()) {
                item.add(new DefaultMutableTreeNode(source));
            }
            if (root == null) root = new DefaultMutableTreeNode(this);
            root.add(item);
        }

        return root;

    }

    public ExData getExData() {
        return this.extendedData;
    }

    public boolean isText() {
        return exDataBytes != null && exDataBytes.length != 0;// || Arrays.equals(this.isText, new byte[1])) ? false : true;
    }

    public boolean isEncrypted() {
        return extendedData.isEncrypted();
    }

    @Override
    public boolean hasPublicText() {
        return extendedData.hasPublicText();
    }

    public boolean isCanSignOnlyRecipients() {
        return extendedData.isCanSignOnlyRecipients();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void toJsonBody(JSONObject transaction) {

        // если из RAW берется JSON - то надо просчитать все
        parseDataFull();

        //ADD CREATOR/SERVICE/DATA
        if (exDataBytes != null && exDataBytes.length > 0) {

            transaction.put("exData", extendedData.toJson());

        }

        if (exDataBytes != null && exDataBytes.length > 0) {
            transaction.put("data64", Base64.getEncoder().encodeToString(this.exDataBytes));
        }

    }


    @Override
    public int getDataLength(int forDeal) {

        int base_len = super.getDataLength(forDeal);

        int add_len = 0;
        if (this.exDataBytes != null && this.exDataBytes.length > 0)
            add_len += DATA_SIZE_LENGTH + this.exDataBytes.length;

        if (forDeal == FOR_DB_RECORD) {
            add_len += DATA_SIZE_LENGTH + (extendedData == null ?
                    dataForDB == null || dataForDB.length == 0 ? 0 : dataForDB.length : extendedData.getLengthDBData());
        }

        return base_len + add_len;
    }

    //PROCESS/ORPHAN

    @Override
    public void processBody(Block block, int forDeal) throws TxException {

        super.processBody(block, forDeal);

        parseDataFull(); // need for take HASHES from FILES
        extendedData.process(this, block);

        byte[][] hashes = extendedData.getAllHashesAsBytes(true);
        if (hashes != null) {
            Long dbKey = makeDBRef(height, seqNo);
            for (byte[] hash : hashes) {
                dcSet.getTransactionFinalMapSigns().put(hash, dbKey);
            }
        }

    }

    @Override
    public void orphanBody(Block block, int forDeal) throws TxException {

        parseDataFull(); // also need for take HASHES from FILES
        extendedData.orphan(this);

        byte[][] hashes = extendedData.getAllHashesAsBytes(true);
        if (hashes != null) {
            for (byte[] hash : hashes) {
                dcSet.getTransactionFinalMapSigns().delete(hash);
            }
        }

        super.orphanBody(block, forDeal);

    }

    //@Override
    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        //CHECK DATA SIZE
        if (exDataBytes == null)
            return INVALID_DATA_LENGTH;

        if (exDataBytes.length > MAX_DATA_BYTES_LENGTH) {
            errorValue = "" + exDataBytes.length;
            return INVALID_DATA_LENGTH;
        }

        // parse with files
        // need for test PUBLIC by files
        parseDataFull();

        int result;
        // комиссия у так уже = 0 - нельзя модифицировать флаг внутри
        if (false) {
            // не учитываем комиссию если размер блока маленький
            result = super.isValid(forDeal, checkFlags | NOT_VALIDATE_FLAG_FEE);
        } else {
            result = super.isValid(forDeal, checkFlags);
        }

        if (result != Transaction.VALIDATE_OK) return result;

        result = extendedData.isValid(this);
        if (result != Transaction.VALIDATE_OK) {
            // errorValue updated in extendedData
            errorValue = extendedData.errorValue;
            return result;
        }

        // только уникальные - так как иначе каждый новый перезатрет поиск старого
        byte[][] allHashes = extendedData.getAllHashesAsBytes(true);
        if (allHashes != null && allHashes.length > 0) {
            TransactionFinalMapSigns map = dcSet.getTransactionFinalMapSigns();
            for (byte[] hash : allHashes) {
                if (map.contains(hash)) {
                    errorValue = Base58.encode(hash);
                    return HASH_ALREADY_EXIST;
                }
            }
        }

        return Transaction.VALIDATE_OK;
    }

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }
        HashSet<Account> accounts = new HashSet<Account>(8, 1);
        accounts.add(this.creator);
        if (extendedData.hasRecipients()) {
            Collections.addAll(accounts, extendedData.getRecipients());
        }
        return accounts;
    }

    public boolean hasRecipients() {
        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }
        return extendedData.hasRecipients();
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }
        HashSet<Account> accounts = new HashSet<>(8, 1);
        if (extendedData.hasRecipients()) {
            Collections.addAll(accounts, extendedData.getRecipients());
        }
        return accounts;
    }

    @Override
    public boolean isInvolved(Account account) {

        if (account.equals(this.creator)) {
            return true;
        }

        if (extendedData == null) {
            parseDataV2WithoutFiles();
        }

        return extendedData.isInvolved(account);
    }

    @Override
    public long calcBaseFee(boolean withFreeProtocol) {

        long long_fee = super.calcBaseFee(withFreeProtocol);
        // дело в том что выше не считается общая комиссия и она как первая транзакция = 0 становится
        if (false)
            return getRoyaltyFee();

        byte[][] allHashes = extendedData.getAllHashesAsBytes(true);

        if (allHashes != null) {
            long_fee += (long) allHashes.length * 100 * BlockChain.FEE_PER_BYTE;
        }

        if (false) {
            long_fee += (long) extendedData.getRecipients().length * 25 * BlockChain.FEE_PER_BYTE;
        }

        if (getExLink() != null)
            long_fee += 100 * BlockChain.FEE_PER_BYTE;

        ExAction exAction = extendedData.getExAction();
        if (exAction != null) {
            long_fee += exAction.getTotalFeeBytes() * BlockChain.FEE_PER_BYTE;
        }

        if (extendedData.hasAuthors()) {
            long_fee += (long) extendedData.getAuthors().length * 100 * BlockChain.FEE_PER_BYTE;
        }

        if (extendedData.hasSources()) {
            long_fee += (long) extendedData.getSources().length * 100 * BlockChain.FEE_PER_BYTE;
        }

        return long_fee + getRoyaltyFee();
    }

    @Override
    public long getRoyaltyFee() {
        return extendedData.getRoyaltyFee();
    }

    public void parseDataV2WithoutFiles() {
        if (extendedData == null) {
            //Version, Title, JSON, Files
            try {
                // здесь нельзя сохранять в parsedData
                extendedData = ExData.parse(getVersion(), this.exDataBytes, false);
                if (dataForDB != null)
                    extendedData.parseDBData(dataForDB);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                Long error = null;
                error++;
            }
        }

        // may by in new FORKED DB - try set if NEW
        if (dcSet != null)
            extendedData.setDC(dcSet);

    }

    public void parseDataFull() {

        if (extendedData == null || !extendedData.isParsedWithFiles()) {
            // если уже парсили или парсили без файлов а надо с файлами

            // version 2
            try {
                extendedData = ExData.parse(getVersion(), this.exDataBytes, true);
                if (dataForDB != null)
                    extendedData.parseDBData(dataForDB);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                Long error = null;
                error++;
            }
        }

        // may by in new FORKED DB - try set if NEW
        if (dcSet != null)
            extendedData.setDC(dcSet);
    }

    public boolean isFavorite() {
        if (Controller.getInstance().doesWalletKeysExists()) {
            return Controller.getInstance().getWallet().dwSet.getDocumentFavoritesSet().contains(this.dbRef);
        }
        return false;
    }

    public Fun.Tuple3<Integer, String, RSignNote> decrypt(Account recipient) {

        Fun.Tuple3<Integer, String, ExData> decryptedExData = extendedData.decrypt(creator, recipient);
        if (decryptedExData.c == null) {
            return new Fun.Tuple3<>(decryptedExData.a, decryptedExData.b, null);
        }

        byte[] exData;
        try {
            exData = decryptedExData.c.toByte();
        } catch (Exception e) {
            return new Fun.Tuple3<>(decryptedExData.a, e.getMessage(), null);
        }

        RSignNote decryptedNote = new RSignNote(typeBytes, creator, title, tags,
                dataMessage, isText, false, null, null,
                feePow, exData,
                dataForDB, timestamp, signature);
        return new Fun.Tuple3<>(decryptedExData.a, decryptedExData.b, decryptedNote);

    }

    public Fun.Tuple2<String, RSignNote> decryptByPassword(byte[] password) {

        Fun.Tuple2<String, ExData> decryptedExData = extendedData.decryptByPassword(password);
        if (decryptedExData.b == null) {
            return new Fun.Tuple2<>(decryptedExData.a, null);
        }

        byte[] exData;
        try {
            exData = decryptedExData.b.toByte();
        } catch (Exception e) {
            return new Fun.Tuple2<>(e.getMessage(), null);
        }

        RSignNote decryptedNote = new RSignNote(typeBytes, creator, title, tags,
                dataMessage, isText, false, null, null,
                feePow, exData,
                dataForDB, timestamp, signature);

        return new Fun.Tuple2<>(null, decryptedNote);

    }

    public Fun.Tuple2<String, RSignNote> decryptByPassword(String password) {
        try {
            byte[] pass = Base58.decode(password);
            return decryptByPassword(pass);
        } catch (Exception e) {
            return new Fun.Tuple2<>(e.getMessage(), null);
        }
    }

    public Fun.Tuple3<Integer, String, byte[]> getPassword(Account recipient) {
        return extendedData.getPassword(creator, recipient);
    }
}
