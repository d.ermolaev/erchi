package org.erachain.core.transaction;

//import java.math.BigInteger;

import org.erachain.core.block.Block;
import org.erachain.core.item.ItemCls;
import org.json.simple.JSONObject;

public class GenesisIssueItemRecord extends GenesisRecord implements Itemable {

    protected ItemCls item;

    public GenesisIssueItemRecord(byte type, String name, ItemCls item, String title, String tags, String message) {
        super(type, name, title, tags, message);

        this.item = item;

        this.generateSignature();

    }

    public GenesisIssueItemRecord(String name, byte[] data) throws Exception {
        super(name, data);
    }

    //GETTERS/SETTERS

    public ItemCls getItem() {
        return this.item;
    }

    public long getAssetKey() {
        return this.getItem().getKey();
    }

    @Override
    public void generateSignature() {

        super.generateSignature();

        // NEED to set an reference
        this.item.setReference(this.signature, dbRef);

    }

    @Override
    public void toJsonBody(JSONObject transaction) {
        transaction.put(this.item.getItemTypeName(), this.item.toJson());
    }

    //PARSE CONVERT

    @Override
    public byte[] toBytesBody(int forDeal) {

        //WRITE ITEM
        return this.item.toBytes(false);

    }

    @Override
    public int getDataLength(int forDeal) {

        return super.getDataLength(forDeal) + this.item.getDataLength(false);
    }

    //VALIDATE
    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        int res = super.isValid(forDeal, 0L);
        if (res != VALIDATE_OK)
            return res;

        return item.isValid();

    }

    //PROCESS/ORPHAN

    @Override
    public void processBody(Block block, int forDeal) {

        //INSERT INTO DATABASE
        this.item.insertToMap(this.dcSet, 0L);

    }

}
