package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.crypto.Base58;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.Order;
import org.erachain.core.item.assets.Trade;
import org.erachain.datachain.DCSet;
import org.json.simple.JSONObject;
import org.mapdb.Fun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

public class CancelOrderTransaction extends Transaction {

    public static final byte TYPE_ID = (byte) CANCEL_ORDER_TRANSACTION;
    public static final String TYPE_NAME = "Cancel Order";
    private static final int ORDER_SIGN_LENGTH = SIGNATURE_LENGTH;
    private static final int LOAD_LENGTH = ORDER_SIGN_LENGTH;

    static Logger LOGGER = LoggerFactory.getLogger(CancelOrderTransaction.class.getName());
    private final byte[] orderSignature;
    private long orderID;


    public CancelOrderTransaction(byte[] typeBytes, PublicKeyAccount creator, String title, String tags,
                                  byte[] orderSignature, byte feePow, long timestamp) {
        super(typeBytes, TYPE_NAME, creator, title, tags, null, null, null, feePow, timestamp);
        this.orderSignature = orderSignature;
    }

    public CancelOrderTransaction(byte[] typeBytes, PublicKeyAccount creator, String title, String tags,
                                  byte[] orderSignature, byte feePow, long timestamp, byte[] signature) {
        this(typeBytes, creator, title, tags, orderSignature, feePow, timestamp);
        this.signature = signature;
    }

    public CancelOrderTransaction(byte[] typeBytes, PublicKeyAccount creator, String title, String tags,
                                  byte[] orderSignature, long orderID, byte feePow, long timestamp, byte[] signature, long seqNo, long feeLong) {
        this(typeBytes, creator, title, tags, orderSignature, feePow, timestamp);
        this.signature = signature;
        this.orderID = orderID;
        this.fee = BigDecimal.valueOf(feeLong, BlockChain.FEE_SCALE);
        if (seqNo > 0)
            this.setHeightSeq(seqNo);
    }

    public CancelOrderTransaction(PublicKeyAccount creator, byte[] orderSignature, byte feePow, long timestamp, byte[] signature) {
        this(makeTypeBytes(TYPE_ID), creator, null, null, orderSignature, feePow, timestamp, signature);
    }

    public CancelOrderTransaction(PublicKeyAccount creator, byte[] orderSignature, byte feePow, long timestamp) {
        this(makeTypeBytes(TYPE_ID), creator, null, null, orderSignature, feePow, timestamp);
    }

    /**
     * NEW PARSE by make new Object from RAW
     *
     * @param data
     * @param forDeal
     * @throws Exception
     */
    public CancelOrderTransaction(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, pubKey, signature, startPos, data, forDeal);

        int test_len;
        if (forDeal == FOR_MYPACK) {
            test_len = BASE_LENGTH_AS_MYPACK + LOAD_LENGTH;
        } else if (forDeal == Transaction.FOR_PACK) {
            test_len = BASE_LENGTH_AS_PACK + LOAD_LENGTH;
        } else if (forDeal == Transaction.FOR_DB_RECORD) {
            test_len = BASE_LENGTH_AS_DBRECORD + LOAD_LENGTH;
        } else {
            test_len = BASE_LENGTH + LOAD_LENGTH;
        }

        if (data.length < test_len) {
            throw new Exception("Data does not match RAW length " + data.length + " < " + test_len);
        }

        //READ ORDER SIGNATURE
        orderSignature = Arrays.copyOfRange(data, parsePos, parsePos + ORDER_SIGN_LENGTH);
        parsePos += ORDER_SIGN_LENGTH;

        if (forDeal == FOR_DB_RECORD) {
            parseDB(data);
        }

    }

    public CancelOrderTransaction(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);

        if (typeBytes[POS_TX_VERSION] > 0) {
            errorMessage.setMsg("invalid `version`");
            throw new TxException("wrong value: " + getVersion());
        }

        //READ ORDER SIGNATURE
        orderSignature = Base58.decode((String) json.get("orderSignature"));

    }

    //GETTERS/SETTERS


    public static void processBody(DCSet dcSet, long dbRef, long orderID, Block block,
                                   String calcMess, boolean ignoreNull) {

        Order order = dcSet.getOrderMap().get(orderID);

        if (order == null) {
            if (ignoreNull)
                return;
            Long error = null;
            error++;
        }

        //DELETE FROM DATABASE FIRST - иначе сработает проверка внутри
        dcSet.getOrderMap().delete(orderID);

        //SET ORPHAN DATA
        dcSet.getCompletedOrderMap().put(orderID, order);

        AssetCls assetHave = dcSet.getItemAssetMap().get(order.getHaveAssetKey());
        AssetCls assetWant = dcSet.getItemAssetMap().get(order.getWantAssetKey());

        // ADD CANCEL as TRADE
        Trade trade = new Trade(Trade.TYPE_CANCEL, dbRef, orderID, order.getHaveAssetKey(), order.getWantAssetKey(),
                order.getAmountWantLeft(), order.getAmountHaveLeft(),
                assetWant.getScale(), assetHave.getScale(), -1);
        //ADD TRADE TO DATABASE
        dcSet.getTradeMap().put(trade);

        //UPDATE BALANCE OF CREATOR
        BigDecimal left = order.getAmountHaveLeft();
        order.getCreator().changeBalance(dcSet, false, Account.BALANCE_POS_OWN, order.getHaveAssetKey(), left,
                null, false,
                // accounting on PLEDGE position
                true, Account.BALANCE_POS_PLEDGE);

        if (block != null) {
            block.addCalculated(order.getCreator(), order.getHaveAssetKey(), left, calcMess, dbRef);
        }
    }

    public static void orphanBody(DCSet dcSet, long dbRef, long orderID, boolean ignoreNull) {

        //REMOVE ORDER DATABASE
        Order order = dcSet.getCompletedOrderMap().get(orderID);

        if (order == null) {
            if (ignoreNull)
                return;
            Long error = null;
            error++;
        }

        if (Transaction.viewDBRef(orderID).equals("776446-1")) {
            boolean debug = true;
        }

        // ROLLBACK CANCEL as TRADE
        dcSet.getTradeMap().delete(new Fun.Tuple2<Long, Long>(dbRef, order.getId()));

        //DELETE ORPHAN DATA FIRST - иначе ошибка будет при добавлении в таблицу ордеров
        dcSet.getCompletedOrderMap().delete(order.getId());

        dcSet.getOrderMap().put(order.getId(), order);

        //REMOVE BALANCE OF CREATOR
        order.getCreator().changeBalance(dcSet, true, Account.BALANCE_POS_OWN, order.getHaveAssetKey(),
                order.getAmountHaveLeft(), null, false,
                // accounting on PLEDGE position
                true, Account.BALANCE_POS_PLEDGE);
    }

    public void setDC(DCSet dcSet, int forDeal, int blockHeight, int seqNo, boolean andUpdateFromState) {
        super.setDC(dcSet, forDeal, blockHeight, seqNo, false);

        if (orderID == 0L) {
            Long createDBRef = this.dcSet.getTransactionFinalMapSigns().get(this.orderSignature);
            if (createDBRef == null && blockHeight > BlockChain.CANCEL_ORDERS_ALL_VALID && height > BlockChain.ALL_VALID_BEFORE) {
                LOGGER.error("ORDER transaction not found: " + Base58.encode(this.orderSignature));
                errorValue = Base58.encode(this.orderSignature);
                if (BlockChain.CHECK_BUGS > 3) {
                    Long error = null;
                    error++;
                }
            }
            this.orderID = createDBRef;
        }

        if (false)
            updateFromStateDB();

    }

    //PARSE CONVERT

    public byte[] getOrderSignature() {
        return this.orderSignature;
    }

    public Long getOrderID() {
        return this.orderID;
    }

    @Override
    public boolean hasPublicText() {
        return false;
    }

    @Override
    public void parseDB(byte[] data) throws Exception {

        super.parseDB(data);

        //READ ORDER ID
        byte[] orderIDBytes = Arrays.copyOfRange(data, parsePos, parsePos + SEQ_NO_LENGTH);
        orderID = Longs.fromByteArray(orderIDBytes);
        parsePos += SEQ_NO_LENGTH;

    }

    @Override
    public byte[] toBytesBody(int forDeal) {
        //WRITE ORDER
        return this.orderSignature;
    }

    @Override
    public byte[] toBytesDB(int forDeal) {
        return Bytes.concat(super.toBytesDB(forDeal),
                // WRITE ORDER ID
                Longs.toByteArray(this.orderID));
    }

    @Override
    public void toJsonBody(JSONObject transaction) {
        transaction.put("orderID", this.orderID);
        transaction.put("orderSignature", Base58.encode(this.orderSignature));
    }

    //VALIDATE
    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        //CHECK IF ORDER EXISTS
        boolean emptyOrder = false;
        if (this.orderID == 0L || !this.dcSet.getOrderMap().contains(this.orderID)) {
            if (this.height > BlockChain.CANCEL_ORDERS_ALL_VALID && height > BlockChain.ALL_VALID_BEFORE) {

                if (true) {
                    if (this.orderID == 0L) {
                        errorValue = "orderID == null";
                        LOGGER.debug("INVALID: " + errorValue);
                    } else {
                        errorValue = "orderID: " + Transaction.viewDBRef(orderID);
                        // 3qUAUPdifyWYg7ABYa5TiWmyssHH1gJtKDatATS6UeKMnSzEwpuPJN5QFKCPHtUWpDYbK7fceFyDGhc51CuhiJ3
                        LOGGER.debug("INVALID: this.sign = " + Base58.encode(signature));
                        LOGGER.debug("INVALID: " + errorValue);
                        LOGGER.debug("INVALID: this.orderSign == " + Base58.encode(orderSignature));
                        if (this.dcSet.getCompletedOrderMap().contains(this.orderID)) {
                            errorValue += " already Completed";
                            LOGGER.debug("INVALID: already Completed");
                        } else {
                            errorValue += " not exist in chain";
                            LOGGER.debug("INVALID: not exist in chain");
                        }
                    }
                }

                return ORDER_DOES_NOT_EXIST;
            } else {
                emptyOrder = true;
            }
        }

        if (!emptyOrder) {
            Order order = this.dcSet.getOrderMap().get(this.orderID);

            //CHECK IF CREATOR IS CREATOR
            if (!order.getCreator().equals(this.creator)) {
                return INVALID_ORDER_CREATOR;
            }
        }

        return super.isValid(forDeal, checkFlags);
    }

    @Override
    public int getDataLength(int forDeal) {
        return super.getDataLength(forDeal) + ORDER_SIGN_LENGTH;
    }

    //PROCESS/ORPHAN

    @Override
    public void processBody(Block block, int forDeal) throws TxException {
        //UPDATE CREATOR
        super.processBody(block, forDeal);

        boolean ignoreNull = height < BlockChain.CANCEL_ORDERS_ALL_VALID || height < BlockChain.ALL_VALID_BEFORE;
        if (this.orderID == 0L) {
            if (ignoreNull)
                return;
            Long error = null;
            error++;
        }

        processBody(dcSet, dbRef, orderID, block, "Cancel Order @" + Transaction.viewDBRef(orderID), ignoreNull);
    }

    @Override
    public void orphanBody(Block block, int forDeal) throws TxException {

        // ORPHAN
        super.orphanBody(block, forDeal);

        boolean ignoreNull = height < BlockChain.CANCEL_ORDERS_ALL_VALID || height < BlockChain.ALL_VALID_BEFORE;
        if (this.orderID == 0L) {
            if (ignoreNull)
                return;
            Long error = null;
            error++;
        }

        orphanBody(dcSet, dbRef, orderID, ignoreNull);

    }

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        HashSet<Account> accounts = new HashSet<>(2, 1);
        accounts.add(this.creator);
        return accounts;
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        return new HashSet<>(1, 1);
    }

    @Override
    public boolean isInvolved(Account account) {
        return account.equals(this.creator);
    }


    public Map<String, Map<Long, BigDecimal>> getAssetAmount() {
        Map<String, Map<Long, BigDecimal>> assetAmount = new LinkedHashMap<>();

        assetAmount = subAssetAmount(assetAmount, this.creator.getAddress(), FEE_KEY, this.fee);

        Order order;

        if (this.dcSet.getCompletedOrderMap().contains(this.orderID)) {
            order = this.dcSet.getCompletedOrderMap().get(this.orderID);
        } else {
            order = this.dcSet.getOrderMap().get(this.orderID);
        }

        assetAmount = addAssetAmount(assetAmount, this.creator.getAddress(), order.getHaveAssetKey(), order.getAmountHave());

        return assetAmount;
    }

}
