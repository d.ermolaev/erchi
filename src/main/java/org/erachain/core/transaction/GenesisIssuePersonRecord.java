package org.erachain.core.transaction;

import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.item.persons.PersonFactory;

public class GenesisIssuePersonRecord extends GenesisIssueItemRecord {
    private static final byte TYPE_ID = (byte) GENESIS_ISSUE_PERSON_TRANSACTION;
    private static final String NAME_ID = "GENESIS Issue Person";

    public GenesisIssuePersonRecord(PersonCls person, String title, String tags, String message) {
        super(TYPE_ID, NAME_ID, person, title, tags, message);
    }

    public GenesisIssuePersonRecord(byte[] data) throws Exception {
        super(NAME_ID, data);

        item = PersonFactory.getInstance().parse(parsePos, data, false);
        this.generateSignature();

    }

}
