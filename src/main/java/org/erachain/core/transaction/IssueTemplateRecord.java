package org.erachain.core.transaction;

import org.erachain.core.BlockChain;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.templates.TemplateCls;
import org.json.simple.JSONObject;

import java.math.BigDecimal;

public class IssueTemplateRecord extends IssueItemRecord {
    public static final byte TYPE_ID = (byte) ISSUE_TEMPLATE_TRANSACTION;
    public static final String TYPE_NAME = "Issue Template";

    public IssueTemplateRecord(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, ItemCls.TEMPLATE_TYPE, pubKey, signature, startPos, data, forDeal);
    }

    public IssueTemplateRecord(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);
    }

    public IssueTemplateRecord(byte[] typeBytes, PublicKeyAccount creator, ExLink linkTo, TemplateCls template,
                               byte feePow, long timestamp) {
        super(typeBytes, TYPE_NAME, creator, null, null, linkTo, template, feePow, timestamp);
    }

    public IssueTemplateRecord(byte[] typeBytes, PublicKeyAccount creator, ExLink linkTo, TemplateCls template,
                               byte feePow, long timestamp, byte[] signature) {
        super(typeBytes, TYPE_NAME, creator, null, null, linkTo, template, feePow, timestamp, signature);
    }

    public IssueTemplateRecord(byte[] typeBytes, PublicKeyAccount creator, ExLink linkTo, TemplateCls template, byte feePow,
                               long timestamp, byte[] signature, long seqNo, long feeLong) {
        super(typeBytes, TYPE_NAME, creator, null, null, linkTo, template, feePow, timestamp, signature);
        if (seqNo > 0)
            this.setHeightSeq(seqNo);
        this.fee = BigDecimal.valueOf(feeLong, BlockChain.FEE_SCALE);
    }

    public IssueTemplateRecord(byte[] typeBytes, PublicKeyAccount creator, ExLink linkTo, TemplateCls template, byte[] signature) {
        super(typeBytes, TYPE_NAME, creator, null, null, linkTo, template, (byte) 0, 0L, signature);
    }

    public IssueTemplateRecord(PublicKeyAccount creator, TemplateCls template, byte feePow, long timestamp, byte[] signature) {
        this(makeTypeBytes(TYPE_ID), creator, null, template, feePow, timestamp, signature);
    }

    public IssueTemplateRecord(PublicKeyAccount creator, TemplateCls template, byte[] signature) {
        this(makeTypeBytes(TYPE_ID), creator, null, template, (byte) 0, 0L, signature);
    }

    public IssueTemplateRecord(PublicKeyAccount creator, ExLink linkTo, TemplateCls template, byte feePow, long timestamp) {
        this(makeTypeBytes(TYPE_ID), creator, linkTo, template, feePow, timestamp);
    }

    public IssueTemplateRecord(PublicKeyAccount creator, TemplateCls template) {
        this(makeTypeBytes(TYPE_ID), creator, null, template, (byte) 0, 0L);
    }

    //GETTERS/SETTERS

    //VALIDATE

    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        int result = super.isValid(forDeal, checkFlags);
        return result;
    }


    //PROCESS/ORPHAN


}
