package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.crypto.Base58;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.polls.PollCls;
import org.erachain.dapp.DAPP;
import org.erachain.datachain.DCSet;
import org.json.simple.JSONObject;
import org.mapdb.Fun;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Vote option > 0 - as PERSON only!
 */
public class VoteOnItemPollTransaction extends Transaction implements Itemable {
    public static final byte TYPE_ID = (byte) VOTE_ON_ITEM_POLL_TRANSACTION;
    public static final String TYPE_NAME = "Vote on Poll";

    private static final int OPTION_SIZE_LENGTH = 4;
    private static final int LOAD_LENGTH = KEY_LENGTH + OPTION_SIZE_LENGTH;
    private final long key;
    private final int option;
    private final boolean personsOnly;

    private PollCls poll;

    /**
     * NEW PARSE by make new Object from RAW
     *
     * @param data
     * @param forDeal
     * @throws Exception
     */
    public VoteOnItemPollTransaction(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, pubKey, signature, startPos, data, forDeal);

        //READ POLL
        byte[] keyBytes = Arrays.copyOfRange(data, parsePos, parsePos + KEY_LENGTH);
        key = Longs.fromByteArray(keyBytes);
        parsePos += KEY_LENGTH;

        //READ OPTION
        byte[] optionBytes = Arrays.copyOfRange(data, parsePos, parsePos + OPTION_SIZE_LENGTH);
        int option = Ints.fromByteArray(optionBytes);
        parsePos += OPTION_SIZE_LENGTH;

        personsOnly = option > 0;
        this.option = Math.abs(option);

        if (forDeal == FOR_DB_RECORD) {
            parseDB(data);
        }

    }

    public VoteOnItemPollTransaction(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);

        if (typeBytes[POS_TX_VERSION] > 0) {
            errorMessage.setMsg("invalid `version`");
            throw new TxException("wrong value: " + getVersion());
        }

        if (!json.containsKey("poll")) {
            throw new TxParseException("VoteOnItemPollTransaction. Empty value", "poll",
                    "Use fields: " + getTXFields());
        }

        errorMessage.setMsg("invalid `poll`");
        this.key = (Long) json.get("poll");
        errorMessage.setMsg("invalid `option`");
        this.option = (int)(long)(Long) json.get("option");

        errorMessage.setMsg("invalid `personsOnly`");
        this.personsOnly = (Boolean) json.get("personsOnly");

    }

    public VoteOnItemPollTransaction(PublicKeyAccount creator, long pollKey, int option, byte feePow) {
        super(makeTypeBytes(TYPE_ID), TYPE_NAME, creator, null, null, null, null, null, feePow, 0L);

        this.key = pollKey;
        this.personsOnly = option > 0;
        this.option = Math.abs(option);
    }

    //GETTERS/SETTERS

    @Override
    public long getKey() {
        return this.key;
    }

    @Override
    public ItemCls getItem() {
        if (poll == null) {
            poll = (PollCls) dcSet.getItemPollMap().get(key);
        }
        return this.poll;
    }

    @Override
    public List<String> getTXFields() {

        String[] array = new String[]{
                "poll:long", "option:int"
        };

        // new ArrayList<> - make modified list!
        List<String> list = new ArrayList<>(Arrays.asList(array));
        list.addAll(0, super.getTXFields());
        return list;
    }

    public void setDC(DCSet dcSet, boolean andUpdateFromState) {
        super.setDC(dcSet, false);

        this.poll = (PollCls) this.dcSet.getItemPollMap().get(this.key);

        if (andUpdateFromState && !isWiped())
            updateFromStateDB();
    }

    public int getOption() {
        return this.option;
    }

    public String viewOption() {
        return ((PollCls) getItem()).viewOption(option);
    }

    @Override
    public String viewTitle() {
        return "##" + viewOption() + " > " + ItemCls.getItemTypeAndKey(ItemCls.POLL_TYPE, key)
                + ":" + getTitle();
    }

    //PARSE CONVERT

    @Override
    public boolean hasPublicText() {
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void toJsonBody(JSONObject transaction) {
        transaction.put("key", this.key);
        transaction.put("option", this.option);
    }

    @Override
    public byte[] toBytesBody(int forDeal) {

        //WRITE POLL KEY
        byte[] keyBytes = Longs.toByteArray(this.key);
        keyBytes = Bytes.ensureCapacity(keyBytes, KEY_LENGTH, 0);

        //WRITE OPTION
        byte[] optionBytes = Ints.toByteArray(personsOnly? this.option : -this.option);
        optionBytes = Bytes.ensureCapacity(optionBytes, OPTION_SIZE_LENGTH, 0);
        return Bytes.concat(keyBytes, optionBytes);

    }

    @Override
    public int getDataLength(int forDeal) {
        return super.getDataLength(forDeal) + LOAD_LENGTH;
    }

    //VALIDATE

    //@Override
    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        //CHECK POLL EXISTS
        if (poll == null) {
            return POLL_NOT_EXISTS;
        }

        if (option <= 0) {
            errorValue = "must be > 0";
            return INVALID_OPTION;
        }

        if (personsOnly && !isCreatorPersonalized()) {
            return CREATOR_NOT_PERSONALIZED;
        }

        //CHECK OPTION EXISTS
        if (this.option > poll.getOptionsSize()) {
            errorValue = "must be <= " + poll.getOptionsSize();
            return INVALID_OPTION;
        }

        return super.isValid(forDeal, checkFlags);

    }

    @Override
    public void makeItemsKeys() {
        if (isWiped()) {
            itemsKeys = new Object[][]{};
        }

        if (creatorPersonDuration == null) {
            itemsKeys = new Object[][]{
                    new Object[]{ItemCls.POLL_TYPE, key, poll.getTagsFull()},
            };
        } else {
            itemsKeys = new Object[][]{
                    new Object[]{ItemCls.PERSON_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()},
                    new Object[]{ItemCls.POLL_TYPE, key, poll.getTagsFull()},
            };
        }
    }

    //PROCESS/ORPHAN

    public void processBody(Block block, int forDeal) throws TxException {
        //UPDATE CREATOR
        super.processBody(block, forDeal);

        //ADD VOTE TO POLL
        this.dcSet.getVoteOnItemPollMap().addItem(this.key, this.option, this.creator.getBytes(),
                this.getHeightSeqNo());

        if (personsOnly) {

            // берем текущее суммарное значение
            Object[] results = dcSet.getVoteOnItemPollResultsMap().get(key);

            if (results == null) {
                results = new Object[poll.getOptionsSize()];
                for (int i = 0; i < results.length; i++) {
                    results[i] = 0L;
                }
            } else {
                // обязательно иначе в форке в памяти хранится ссылка в кэше, а не отдельный объект
                // и результат при переписывании нового значения умножается в разы
                results = results.clone();

            }

            Fun.Tuple2<Long, Long> personKey = new Fun.Tuple2<>(key, creatorPersonDuration.a);
            // берем предыдущий выбор
            Fun.Tuple2<Integer, Long> previousVote = dcSet.getVoteOnItemPollPersonMap().get(personKey);
            if (previousVote != null) {
                // значит надо уменьшить значение для предыдущего
                results[previousVote.a - 1] = (Long) results[previousVote.a - 1] - 1L;

                // запомним для отката текущее и предыдущее значения
                dcSet.getOrphanValuesMap().put(dbRef, new Object[]{option, previousVote.a});

            } else {
                // запомним для отката
                dcSet.getOrphanValuesMap().put(dbRef, new Object[]{option});

            }

            // TODO из ФОРКА результаты дублирует по несколько раз
            results[option - 1] = (Long) results[option - 1] + 1L;

            // запомним выбор персоны
            dcSet.getVoteOnItemPollPersonMap().put(personKey, new Fun.Tuple2<>(option, dbRef));

            // запомним общий результат
            dcSet.getVoteOnItemPollResultsMap().put(key, results);

        }

    }


    //@Override
    @Override
    public void orphanBody(Block block, int forDeal) throws TxException {

        Object[] values = dcSet.getOrphanValuesMap().get(dbRef);

        if (personsOnly) {

        }

        //DELETE VOTE FROM POLL
        this.dcSet.getVoteOnItemPollMap().removeItem(this.key, this.option, this.creator.getBytes());

        //UPDATE CREATOR
        super.orphanBody(block, forDeal);

    }

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        HashSet<Account> accounts = new HashSet<Account>();
        accounts.add(this.creator);
        return accounts;
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        return new HashSet<>(1, 1);
    }

    @Override
    public boolean isInvolved(Account account) {

        return account.equals(this.creator);
    }

}
