package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import org.erachain.core.account.Account;
import org.erachain.core.block.Block;
import org.json.simple.JSONObject;
import org.mapdb.Fun.Tuple3;
import org.mapdb.Fun.Tuple4;
import org.mapdb.Fun.Tuple5;

import java.util.Arrays;
import java.util.HashSet;

public class GenesisCertifyPersonRecord extends GenesisRecord {

    private static final byte TYPE_ID = (byte) Transaction.GENESIS_CERTIFY_PERSON_TRANSACTION;
    private static final String NAME_ID = "GENESIS Certify Person";
    private static final int RECIPIENT_LENGTH = TransactionAmount.RECIPIENT_LENGTH;


    private static final int BASE_LENGTH = GenesisRecord.BASE_LENGTH + RECIPIENT_LENGTH + KEY_LENGTH;

    private final Account recipient;
    private final long key;

    public GenesisCertifyPersonRecord(Account recipient, long key, String title, String tags, String message) {
        super(TYPE_ID, NAME_ID, title, tags, message);
        this.recipient = recipient;
        this.key = key;
        this.generateSignature();
    }

    public GenesisCertifyPersonRecord(byte[] data) throws Exception {
        super(NAME_ID, data);

        //READ RECIPIENT
        byte[] recipientBytes = Arrays.copyOfRange(data, parsePos, parsePos + RECIPIENT_LENGTH);
        recipient = new Account(recipientBytes);
        parsePos += RECIPIENT_LENGTH;

        //READ KEY
        byte[] keyBytes = Arrays.copyOfRange(data, parsePos, parsePos + KEY_LENGTH);
        key = Longs.fromByteArray(keyBytes);
        parsePos += KEY_LENGTH;

        this.generateSignature();
    }

    //GETTERS/SETTERS

    public Account getRecipient() {
        return this.recipient;
    }

    @Override
    public long getKey() {
        return this.key;
    }

    //PARSE/CONVERT

    @SuppressWarnings("unchecked")
    @Override
    public void toJsonBody(JSONObject transaction) {
        //ADD CREATOR/RECIPIENT/AMOUNT/ASSET
        recipient.toJsonPersonInfo(transaction, "recipient");
        transaction.put("person", this.key);
    }

    //@Override
    @Override
    public byte[] toBytesBody(int forDeal) {

        return Bytes.concat(
                //WRITE RECIPIENT
                this.recipient.getBytes(),
                //WRITE KEY
                Longs.toByteArray(this.key));

    }

    @Override
    public int getDataLength(int forDeal) {
        return BASE_LENGTH;
    }


    //VALIDATE

    @Override
    public int isValid(int forDeal, long checkFlags) throws TxException {

        //CHECK IF RECIPIENT IS VALID ADDRESS
        if (!Account.isValidAddress(this.recipient.getBytes())) {
            return INVALID_ADDRESS;
        }

        if (!this.dcSet.getItemPersonMap().contains(this.key)) {
            return Transaction.ITEM_PERSON_NOT_EXIST;
        }

        return VALIDATE_OK;
    }

    //PROCESS/ORPHAN

    @Override
    public void processBody(Block block, int forDeal) {

        //UPDATE RECIPIENT
        Tuple5<Long, Long, byte[], Integer, Integer> itemP =
                new Tuple5<Long, Long, byte[], Integer, Integer>
                        (timestamp, Long.MAX_VALUE, null, height, seqNo);

        // SET ALIVE PERSON for DURATION permanent
        ///db.getPersonStatusMap().addItem(this.key, StatusCls.ALIVE_KEY, itemP);

        // SET PERSON ADDRESS - end date as timestamp
        Tuple4<Long, Integer, Integer, Integer> itemA = new Tuple4<Long, Integer, Integer, Integer>(this.key, Integer.MAX_VALUE, height, seqNo);
        Tuple3<Integer, Integer, Integer> itemA1 = new Tuple3<Integer, Integer, Integer>(0, height, seqNo);
        this.dcSet.getAddressPersonMap().addItem(this.recipient.getBytes(), itemA);
        this.dcSet.getPersonAddressMap().addItem(this.key, this.recipient.getAddress(), itemA1);

        //UPDATE TIMESTAMP OF RECIPIENT
        this.recipient.setLastTimestamp(new long[]{this.timestamp, dbRef}, this.dcSet);
    }

    //REST

    @Override
    public HashSet<Account> getRecipientAccounts() {
        HashSet<Account> accounts = new HashSet<>();
        accounts.add(this.recipient);
        return accounts;
    }

    @Override
    public boolean isInvolved(Account account) {

        return account.equals(recipient);
    }

}
