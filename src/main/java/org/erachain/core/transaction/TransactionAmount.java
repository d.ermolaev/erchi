package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import erchi.core.account.PrivateKeyAccount;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.crypto.AEScrypto;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.dapp.DAPP;
import org.erachain.datachain.DCSet;
import org.erachain.lang.Lang;
import org.erachain.utils.BigDecimalUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * #### BODY FLAGS 1
 * typeBytes[POS_TX_FLAGS_1].7 = -128 - if NO AMOUNT - check sign (NO_AMOUNT_MASK) <br>
 * typeBytes[POS_TX_FLAGS_1].6 = 64 - if backward (CONFISCATE CREDIT, ...) <br>
 * typeBytes[POS_TX_FLAGS_1].5 = 32 - use PACKET instead single Amount+AssetKey (USE_PACKET_MASK) <br>
 * <p>
 */

public class TransactionAmount extends Transaction implements Itemable {

    public static final byte TYPE_ID = (byte) Transaction.SEND_ASSET_TRANSACTION;
    public static final String TYPE_NAME = "Send";
    public static final byte LAST_VERSION = 0;

    /**
     * used in flags1
     */
    public static final byte HAS_AMOUNT_MASK = 1;

    public static final byte HAS_PACKET_MASK = 2; //

    // BALANCES types and ACTION with IT
    // 0 - not used
    public static final int ACTION_SEND = Account.BALANCE_POS_OWN;
    public static final int ACTION_DEBT = Account.BALANCE_POS_DEBT;
    public static final int ACTION_REPAY_DEBT = 100 + ACTION_DEBT; // чисто для другого отображения а так = ACTION_DEBT
    public static final int ACTION_HOLD = Account.BALANCE_POS_HOLD;
    public static final int ACTION_SPEND = Account.BALANCE_POS_SPEND;
    public static final int ACTION_PLEDGE = Account.BALANCE_POS_PLEDGE;
    public static final int ACTION_RESERVED_6 = Account.BALANCE_POS_6;
    public static final int[] ACTIONS_LIST = new int[]{
            ACTION_SEND,
            ACTION_DEBT,
            ACTION_REPAY_DEBT, // чисто для другого отображения а так = ACTION_DEBT
            ACTION_HOLD,
            ACTION_SPEND,
            ACTION_PLEDGE
            // ACTION_RESERVED_6
    };
    public static final String NAME_ACTION_SEND = "SEND";
    public static final String NAME_ACTION_SEND_WAS = "Send # was";
    public static final String NAME_ACTION_DEBT = "CREDIT";
    public static final String NAME_ACTION_DEBT_WAS = "Credit # was";
    public static final String NAME_ACTION_HOLD = "HOLD";
    public static final String NAME_ACTION_HOLD_WAS = "Hold # was";
    public static final String NAME_ACTION_SPEND = "SPEND";
    public static final String NAME_ACTION_SPEND_WAS = "Spend # was";
    public static final String NAME_ACTION_PLEDGE = "PLEDGE";
    public static final String NAME_ACTION_PLEDGE_WAS = "Pledge # was";
    //public static final int AMOUNT_LENGTH = 8;
    public static final int RECIPIENT_LENGTH = Account.ADDRESS_LENGTH;
    protected static final int LOAD_LENGTH = RECIPIENT_LENGTH + 1 + KEY_LENGTH + AMOUNT_LENGTH;
    // + 1 to len for memo
    protected static final int PACKET_ROW_LENGTH = KEY_LENGTH + 5 * (1 + 1 + AMOUNT_LENGTH) + 1;
    protected static final int PACKET_ROW_MEMO_NO = 6;
    static Logger LOGGER = LoggerFactory.getLogger(TransactionAmount.class.getName());
    private static long pointLogg;
    protected Account recipient;
    protected Fun.Tuple4<Long, Integer, Integer, Integer> recipientPersonDuration;
    protected PersonCls recipientPerson;

    protected BigDecimal amount;
    /**
     * actionType - with sign. but balancePosition - only positive
     * see Account.BALANCE_POS_OWN etc. if < 0 as BACKWARD.
     */
    protected int actionType;
    protected long key;
    protected AssetCls asset; // or price Asset for packet
    /**
     * 0: (long) AssetKey, 1: Amount, 2: Price, 3: Discounted Price, 4: Tax as percent, 5: Fee as absolute value, 6: memo, 7: Asset (after setDC())
     */
    protected Object[][] packet;

    public TransactionAmount(PublicKeyAccount pubKey, byte[] signature, int startPos, byte[] data, int forDeal) throws Exception {
        super(TYPE_NAME, pubKey, signature, startPos, data, forDeal);

        int test_len;
        if (forDeal == Transaction.FOR_MYPACK) {
            test_len = BASE_LENGTH_AS_MYPACK;
        } else if (forDeal == Transaction.FOR_PACK) {
            test_len = BASE_LENGTH_AS_PACK;
        } else if (forDeal == Transaction.FOR_DB_RECORD) {
            test_len = BASE_LENGTH_AS_DBRECORD;
        } else {
            test_len = BASE_LENGTH;
        }

        if ((typeBytes[POS_TX_FLAGS_1] & HAS_AMOUNT_MASK) == 0) {
            // without AMOUNT
            test_len -= KEY_LENGTH + 2;
        }

        if (data.length < test_len) {
            throw new Exception("Data does not match RAW length " + data.length + " < " + test_len);
        }

        ///////////////// LOAD

        // READ RECIPIENT
        recipient = new Account(data, parsePos);
        parsePos += Account.ADDRESS_LENGTH;

        if ((typeBytes[POS_TX_FLAGS_1] & HAS_AMOUNT_MASK) != 0) {
            // IF here is AMOUNT
            packet = null;

            actionType = data[parsePos++];

            // READ KEY
            byte[] keyBytes = Arrays.copyOfRange(data, parsePos, parsePos + KEY_LENGTH);
            key = Longs.fromByteArray(keyBytes);
            parsePos += KEY_LENGTH;

            // READ AMOUNT
            amount = BigDecimalUtil.fromBytes(data, parsePos);
            parsePos += BigDecimalUtil.BytesLen();

        } else if ((typeBytes[POS_TX_FLAGS_1] & HAS_PACKET_MASK) != 0L) {
            // IF here is PACKET of AMOUNTTs
            amount = null;

            actionType = data[parsePos++];

            // READ KEY
            byte[] keyBytes = Arrays.copyOfRange(data, parsePos, parsePos + KEY_LENGTH);
            parsePos += KEY_LENGTH;
            key = Longs.fromByteArray(keyBytes);

            // READ AMOUNT
            byte[] packetSizeBytes = Arrays.copyOfRange(data, parsePos, parsePos + Integer.BYTES);
            parsePos += Integer.BYTES;
            int packetSize = Ints.fromByteArray(packetSizeBytes);

            packet = new Object[packetSize][];
            Object[] row;
            byte[] memoBytes;
            for (int count = 0; count < packetSize; count++) {
                row = new Object[8];
                packet[count] = row;

                // READ KEY
                keyBytes = Arrays.copyOfRange(data, parsePos, parsePos + KEY_LENGTH);
                parsePos += KEY_LENGTH;
                row[0] = Longs.fromByteArray(keyBytes);

                //READ AMOUNT
                row[1] = BigDecimalUtil.fromBytes(data, parsePos);
                parsePos += 9;

                //READ PRICE
                row[2] = BigDecimalUtil.fromBytes(data, parsePos);
                parsePos += 9;

                //READ DISCONTED PRICE
                row[3] = BigDecimalUtil.fromBytes(data, parsePos);
                parsePos += 9;

                //READ TAX
                row[4] = BigDecimalUtil.fromBytes(data, parsePos);
                parsePos += 9;

                //READ FEE
                row[5] = BigDecimalUtil.fromBytes(data, parsePos);
                parsePos += 9;

                int lenMemo = data[parsePos++];
                if (lenMemo > 0) {
                    memoBytes = Arrays.copyOfRange(data, parsePos, parsePos + lenMemo);
                    row[PACKET_ROW_MEMO_NO] = new String(memoBytes, StandardCharsets.UTF_8);
                    parsePos += lenMemo;
                }

            }
        } else {
            amount = null;
            packet = null;
        }

    }

    // {"type":31,"creator":"acc:3mU4MydjYUKy9uJPje99CWDpGqzZ","recipient":"acc:4DYi6ArBjXxqZaduhyD2fQQ5bzXg","amount":{"action":1,"assetKey":2,"volume":"0.002"}}
    public TransactionAmount(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, TYPE_NAME, json, errorMessage);

        if (typeBytes[POS_TX_VERSION] > 0) {
            errorMessage.setMsg("invalid `version`");
            throw new TxException("wrong value: " + getVersion());
        }

        if (!json.containsKey("recipient"))
            throw new TxParseException("TransactionAmount. Empty value", "recipient",
                    "Use fields: " + getTXFields());

        errorMessage.setMsg("invalid `recipient`");
        recipient = Account.parseOrPubKey(json.get("recipient").toString());
        if (recipient == null) {
            throw new TxParseException("TransactionAmount. Invalid value", "recipient", "Use address Base58 coding!");
        } else if (json.containsKey("amount")) {
            errorMessage.setMsg("invalid `amount`");
            JSONObject amountJson = (JSONObject) json.get("amount");

            errorMessage.setMsg("invalid `action`");
            actionType = (int) (long) (Long) amountJson.get("action");

            errorMessage.setMsg("invalid `assetKey`");
            key = (Long) amountJson.get("assetKey");

            errorMessage.setMsg("invalid `volume`");
            amount = new BigDecimal(amountJson.get("volume").toString());

            typeBytes[POS_TX_FLAGS_1] |= HAS_AMOUNT_MASK;


        } else if (json.containsKey("packet")) {
            errorMessage.setMsg("invalid `packet`");
            JSONObject packetJson = (JSONObject) json.get("packet");

            errorMessage.setMsg("invalid `priceAssetKey`");
            key = (Long) packetJson.get("priceAssetKey");

            errorMessage.setMsg("invalid `action`");
            actionType = (int)(long)(Long) packetJson.get("action");

            errorMessage.setMsg("invalid `array`");
            JSONArray arrayJson = (JSONArray) packetJson.get("array");

            packet = new Object[arrayJson.size()][];
            int i = 0;
            for (Object rowObj : arrayJson) {
                errorMessage.setMsg("invalid `array` in row " + i);
                JSONArray rowArray = (JSONArray) rowObj;
                packet[i] = new Object[rowArray.size()];
                errorMessage.setMsg("invalid `array` in row " + i + " pos:0 asset key (Long)");
                packet[i][0] = (Long) rowArray.get(0); // asset Key
                errorMessage.setMsg("invalid `array` in row " + i + " pos:1 Volume (BigDecimal as String)");
                packet[i][1] = new BigDecimal(rowArray.get(1).toString()); // Volume
                errorMessage.setMsg("invalid `array` in row " + i + " pos:2 Price (BigDecimal as String)");
                packet[i][2] = new BigDecimal(rowArray.get(2).toString()); // Price
                errorMessage.setMsg("invalid `array` in row " + i + " pos:3 Discounted Price (BigDecimal as String)");
                packet[i][3] = new BigDecimal(rowArray.get(3).toString()); // discounted price
                errorMessage.setMsg("invalid `array` in row " + i + " pos:4 Tax (BigDecimal as String)");
                packet[i][4] = new BigDecimal(rowArray.get(4).toString()); // tax %
                errorMessage.setMsg("invalid `array` in row " + i + " pos:5 Fee (BigDecimal as String)");
                packet[i][5] = new BigDecimal(rowArray.get(5).toString()); // fee
                errorMessage.setMsg("invalid `array` in row " + i + " pos:5 Memo (String)");
                packet[i][6] = rowArray.get(6).toString(); // memo
                i++;
            }

            // SET EXTENDED FLAGS MASK + USE_PACKET_MASK
            typeBytes[POS_TX_FLAGS_1] |= HAS_PACKET_MASK;

        }

    }

    public TransactionAmount(PublicKeyAccount creator,
                             String title, String tags, byte[] dataMessage, boolean isText, boolean encrypted,
                             ExLink exLink, DAPP dapp, byte feePow, Account recipient,
                             int actionType, BigDecimal amount, long key) {
        super(makeTypeBytes(TYPE_ID, LAST_VERSION, (byte) 0), TYPE_NAME, creator, title, tags,
                dataMessage, isText, encrypted,
                exLink, dapp, feePow, 0L);

        this.recipient = recipient;

        if (amount != null && amount.signum() != 0) {
            typeBytes[POS_TX_FLAGS_1] |= HAS_AMOUNT_MASK;
            this.amount = amount;
            this.key = key;

            this.actionType = actionType;

        }


    }

    /**
     * packet
     *
     * @param creator
     * @param title
     * @param tags
     * @param exLink
     * @param dapp
     * @param feePow
     * @param recipient
     * @param packet
     */
    public TransactionAmount(PublicKeyAccount creator,
                             String title, String tags, byte[] dataMessage, boolean isText, boolean encrypted,
                             ExLink exLink, DAPP dapp, byte feePow, Account recipient,
                             int actionType, Long priceAssetKey, Object[][] packet) {
        super(makeTypeBytes(TYPE_ID, LAST_VERSION, (byte) 0), TYPE_NAME, creator, title, tags,
                dataMessage, isText, encrypted, exLink, dapp, feePow, 0L);
        this.recipient = recipient;

        assert (packet != null);

        // SET EXTENDED FLAGS MASK + USE_PACKET_MASK
        typeBytes[POS_TX_FLAGS_1] |= HAS_PACKET_MASK;
        this.packet = packet;
        this.actionType = actionType;
        this.key = priceAssetKey;
    }

    public TransactionAmount(PublicKeyAccount creator, String title, byte[] dataMessage,
                             boolean isText, boolean encrypted, byte feePow,
                             Account recipient, int actionType, BigDecimal amount, long key) {
        this(creator, title, null, dataMessage, isText, encrypted, null, null,
                feePow, recipient, actionType, amount, key);
    }

    public TransactionAmount(PublicKeyAccount creator, String title, byte[] dataMessage,
                             boolean isText, boolean encrypted, byte feePow,
                             Account recipient, int actionType, BigDecimal amount, long key, long timestamp) {
        this(creator, title, null, dataMessage, isText, encrypted, null, null,
                feePow, recipient, actionType, amount, key);
        this.timestamp = timestamp;
    }

    public TransactionAmount(PublicKeyAccount creator, String title, String tags, byte[] dataMessage,
                             boolean isText, boolean encrypted, byte feePow,
                             Account recipient, int actionType, BigDecimal amount, long key, long timestamp,
                             byte[] signature) {
        this(creator, title, tags, dataMessage, isText, encrypted, null, null,
                feePow, recipient, actionType, amount, key);
        this.timestamp = timestamp;
        this.signature = signature;
    }

    public TransactionAmount(PublicKeyAccount creator, String title, String tags,
                             byte[] dataMessage, boolean isText, boolean encrypted,
                             ExLink linkTo, DAPP dapp, byte feePow,
                             Account recipient, int actionType, BigDecimal amount, long key,
                             long timestamp) {
        this(creator,
                title, tags, dataMessage, isText, encrypted,
                linkTo, dapp, feePow, recipient,
                actionType, amount, key);
        this.timestamp = timestamp;

    }

    @Deprecated
    public TransactionAmount(PublicKeyAccount creator, ExLink exLinkk, byte feePow,
                             Account recipient, int actionType, long key, BigDecimal amount,
                             String title, byte[] dataMess, byte[] isText, byte[] encrypted,
                             long timestamp) {
        this(creator, title, null, dataMess,
                isText[0] == 1, encrypted[0] == 1, exLinkk, null,
                feePow, recipient, actionType, amount, key);
        this.timestamp = timestamp;
    }

    @Deprecated
    public TransactionAmount(PublicKeyAccount creator, ExLink exLink, DAPP dApp, byte feePow,
                             Account recipient, int actionType, long key, BigDecimal amount,
                             String title, byte[] dataMess, byte[] isText, byte[] encrypted,
                             long timestamp) {
        this(creator, title, null, dataMess,
                isText[0] == 1, encrypted[0] == 1, exLink, dApp,
                feePow, recipient, actionType, amount, key);
        this.timestamp = timestamp;
    }

    @Deprecated
    public TransactionAmount(byte version, byte flags1, byte prop2, PublicKeyAccount creator, ExLink exLink, DAPP dApp, byte feePow,
                             Account recipient, int actionType, long key, BigDecimal amount,
                             String title, byte[] dataMess, byte[] isText, byte[] encrypted,
                             long timestamp) {
        this(creator, title, null, dataMess,
                isText[0] == 1, encrypted[0] == 1, exLink, dApp,
                feePow, recipient, actionType, amount, key);
        this.typeBytes[POS_TX_FLAGS_1] = flags1;
        this.timestamp = timestamp;
    }

    @Deprecated
    public TransactionAmount(PublicKeyAccount creator, Account recipient, int actionType, long key, BigDecimal amount,
                             String title, byte[] dataMess, byte[] isText, long timestamp) {
        this(creator, title, null, dataMess,
                isText[0] == 1, false, null, null,
                (byte) 0, recipient, actionType, amount, key);
        this.timestamp = timestamp;
    }

    @Deprecated
    public TransactionAmount(PublicKeyAccount creator, Account recipient, int actionType, long key, BigDecimal amount) {
        this(creator, null, null, null,
                false, false, null, null,
                (byte) 0, recipient, actionType, amount, key);
    }

    @Deprecated
    public TransactionAmount(PublicKeyAccount creator, byte feePower, Account recipient, int actionType, long key, BigDecimal amount, long timestamp) {
        this(creator, null, null, null,
                false, false, null, null,
                feePower, recipient, actionType, amount, key);
        this.timestamp = timestamp;
    }

    @Deprecated
    public TransactionAmount(PublicKeyAccount creator, byte feePow,
                             Account recipient, int actionType, long key, BigDecimal amount,
                             long timestamp, byte[] signature) {
        this(creator, null, null, null,
                false, false, null, null,
                feePow, recipient, actionType, amount, key);
        this.timestamp = timestamp;
        this.signature = signature;
    }

    @Deprecated
    public TransactionAmount(byte[] typeBytes, PublicKeyAccount creator, ExLink exLink, DAPP dApp, byte feePow,
                             Account recipient, int actionType, long priceAssetKey, Object[][] packet,
                             String title, byte[] dataMessage, byte[] isText, byte[] encrypted,
                             long timestamp, byte[] signature, long seqNo) {
        super(typeBytes, TYPE_NAME, creator, title, null,
                dataMessage, isText[0] == 1, encrypted[0] == 1, exLink, dApp, feePow, timestamp);
        this.recipient = recipient;

        assert (packet != null);

        typeBytes[POS_TX_FLAGS_1] |= HAS_PACKET_MASK;
        this.packet = packet;
        this.actionType = actionType;
        this.key = priceAssetKey;

        this.signature = signature;
        this.setHeightSeq(seqNo);
    }

    @Deprecated
    public TransactionAmount(PublicKeyAccount creator, byte feePow, Account recipient, int actionType, long key, BigDecimal amount,
                             String title, byte[] dataMessage, byte[] isText, byte[] encrypted,
                             long timestamp, long seqNo, byte[] signature) {
        // PublicKeyAccount creator, String title, byte[] dataMessage,
        //                             boolean isText, boolean encrypted, byte feePow,
        //                             Account recipient, BigDecimal amount, long key
        this(creator, title, dataMessage, isText[0] == 1, encrypted[0] == 1,
                feePow, recipient, actionType, amount, key, timestamp);
        this.signature = signature;
        this.setHeightSeq(seqNo);
    }

    @Override
    public List<String> getTXFields() {

        String[] array = new String[]{
                "recipient:address or pubKey[Base58]", "amount:{action:byte{"
                + Arrays.toString(TransactionAmount.ACTIONS_LIST) + "},assetKey:long,volume:BigDecimal}",
                "packet:{priceAssetKey:long,action:byte{"
                        + Arrays.toString(TransactionAmount.ACTIONS_LIST)
                        + "},array:[assetKey:long,volume:BigDecimal,price:BigDecimal,discount:BigDecimal,tax:BigDecimal,fee:BigDecimal,memo:String]}",
                "TIP: Use String for all BigDecimal"
        };

        // new ArrayList<> - make modified list!
        List<String> list = new ArrayList<>(Arrays.asList(array));
        list.addAll(0, super.getTXFields());
        return list;
    }


    /**
     * // только для передачи в собственность!
     * call only if (balancePosition() == ACTION_SEND && !creator.equals(asset.getOwner()) && !recipient.equals(asset.getOwner())
     * && !BlockChain.ASSET_TRANSFER_PERCENTAGE.isEmpty()
     */
    public static Tuple2<BigDecimal, BigDecimal> calcSendTAX(int height, Long key, AssetCls asset, BigDecimal amount) {

        if (key == null || asset == null || amount == null)
            return null;

        BigDecimal assetFeeMin = BlockChain.ASSET_TRANSFER_PERCENTAGE_MIN(height, key);
        if (assetFeeMin == null) {
            return null;
        }

        BigDecimal feeKoeff = BlockChain.ASSET_TRANSFER_PERCENTAGE(height, key);
        BigDecimal assetFee;
        if (feeKoeff == BigDecimal.ZERO) {
            assetFee = assetFeeMin;
        } else {
            assetFee = amount.abs().multiply(feeKoeff).setScale(asset.getScale(), RoundingMode.UP);
            if (assetFee.compareTo(assetFeeMin) < 0) {
                // USE MINIMAL VALUE
                assetFee = assetFeeMin.setScale(asset.getScale(), RoundingMode.UP);
            }
        }

        BigDecimal burnedFeeKoeff = BlockChain.ASSET_BURN_PERCENTAGE(height, key);
        BigDecimal assetBurned = burnedFeeKoeff == BigDecimal.ZERO ? BigDecimal.ZERO
                : assetFee.multiply(burnedFeeKoeff).setScale(asset.getScale(), RoundingMode.UP);

        return new Tuple2<>(assetFee, assetBurned);

    }

    public static String viewTypeName(BigDecimal amount, boolean isBackward) {
        if (amount == null || amount.signum() == 0)
            return "LETTER";

        if (isBackward) {
            return "backward";
        } else {
            return "SEND";
        }
    }

    public static String viewSubTypeName(int actionType) {

        switch (actionType) {
            case ACTION_SEND:
                return NAME_ACTION_SEND;
            case ACTION_DEBT:
                return NAME_ACTION_DEBT;
            case ACTION_HOLD:
                return NAME_ACTION_HOLD;
            case ACTION_SPEND:
                return NAME_ACTION_SPEND;
            case ACTION_PLEDGE:
                return NAME_ACTION_PLEDGE;
        }

        return "???";

    }

    public static boolean isValidPersonProtect(DCSet dcSet, int height, Account recipient,
                                               boolean creatorIsPerson, long absKey, int actionType,
                                               AssetCls asset) {
        if ((BlockChain.PERSON_SEND_PROTECT && creatorIsPerson && absKey != FEE_KEY && asset.isSendPersonProtected()
                || asset.isAnonimDenied())
                && actionType != ACTION_DEBT && actionType != ACTION_HOLD && actionType != ACTION_SPEND
        ) {
            if (!recipient.isDAppOwned() && !recipient.isPersonAlive(dcSet, height)
                    && !BlockChain.ANONYMASERS.contains(recipient.getAddress())) {

                boolean recipient_admin = false;
                for (String admin : BlockChain.GENESIS_ADMINS) {
                    if (recipient.equals(admin)) {
                        recipient_admin = true;
                        break;
                    }
                }
                return recipient_admin;
            }
        }
        return true;
    }

    public static Fun.Tuple2<Integer, String> isValidAction(
            DCSet dcSet, int height, PublicKeyAccount creator, Tuple2<Integer, PersonCls> creatorPersonDur, byte[] signature,
                                                            int actionType,
                                                            long key, AssetCls asset, BigDecimal amount, Account recipient,
                                                            Tuple2<Integer, PersonCls> recipientPersonDur, BigDecimal fee, BigDecimal assetFee,
                                                            long checkFlags, long timestamp) {

        boolean creatorIsPerson = creatorPersonDur != null;
        if (creatorIsPerson && creatorPersonDur.a < 0)
            return new Fun.Tuple2<>(ITEM_PERSON_IS_DEAD, null);

        int balancePos = Math.abs(actionType);
        if (balancePos > Account.BALANCE_POS_PLEDGE)
            return new Fun.Tuple2<>(INVALID_BALANCE_ACTION, null);

        boolean backward = actionType < 0;

        boolean wrong;

        if (!BigDecimalUtil.isValid(amount)) {
            return new Fun.Tuple2<>(AMOUNT_LENGHT_SO_LONG, "max length");
        }

        if (!asset.isActive(timestamp, creator.equals(asset.getOwner()))) {
            return new Fun.Tuple2<>(INVALID_OUTSIDE_ACTIVITY_PERIOD, asset.errorValue);
        }

        if (asset.isUnique() && !amount.abs().stripTrailingZeros().equals(BigDecimal.ONE)) {
            return new Fun.Tuple2<>(INVALID_AMOUNT, "unique amount 1 != " + amount.toPlainString());
        }

        if (asset.isUnTransferable(asset.getOwner().equals(creator))) {
            return new Fun.Tuple2<>(NOT_TRANSFERABLE_ASSET, null);
        }

        if (!asset.isValidActionType(actionType)) {
            return new Fun.Tuple2<>(INVALID_BALANCE_ACTION, "" + actionType);
        }

        // CHECK IF AMOUNT AND ASSET
        if ((checkFlags & NOT_VALIDATE_FLAG_BALANCE) == 0L
                && amount != null) {

            int amount_sign = amount.signum();
            if (amount_sign != 0) {

                if (amount_sign < 0)
                    return new Fun.Tuple2<>(INVALID_AMOUNT, "negate denied");

                if (key < 0)
                    return new Fun.Tuple2<>(INVALID_ITEM_KEY, "negate denied");

                if (key == AssetCls.LIA_KEY) {
                    return new Fun.Tuple2<>(INVALID_TRANSFER_TYPE, "LIA");
                }

                if (asset == null) {
                    return new Fun.Tuple2<>(ITEM_ASSET_NOT_EXIST, "key: " + key);
                }

                if (!BigDecimalUtil.isValid(amount)) {
                    return new Fun.Tuple2<>(AMOUNT_LENGHT_SO_LONG, "max length");
                }

                if (height > BlockChain.ALL_BALANCES_OK_TO) {

                    int assetType = asset.getClassModel();
                    BigDecimal balance;

                    // условия для особых счетных единиц
                    switch ((int) key) {
                        case 111:
                        case 222:
                        case 333:
                        case 444:
                        case 888:
                        case 999:
                            return new Fun.Tuple2<>(ITEM_ASSET_NOT_EXIST, null);
                        case 555:
                            if (balancePos != ACTION_SEND)
                                return new Fun.Tuple2<>(INVALID_TRANSFER_TYPE, "555 - not spend");

                            if (amount.compareTo(BigDecimal.ZERO.subtract(BigDecimal.ONE)) < 0)
                                return new Fun.Tuple2<>(NO_BALANCE, "< 1");

                            break;
                        case 666:
                            if (balancePos != ACTION_SEND)
                                return new Fun.Tuple2<>(INVALID_TRANSFER_TYPE, "666 - not spend");

                            if (amount.compareTo(BigDecimal.ZERO.subtract(BigDecimal.ONE)) < 0)
                                return new Fun.Tuple2<>(NO_BALANCE, "< 1");

                            break;
                        case 777:
                            if (balancePos != ACTION_SEND)
                                return new Fun.Tuple2<>(INVALID_TRANSFER_TYPE, "777 - not spend");

                            if (amount.compareTo(BigDecimal.ZERO.subtract(BigDecimal.ONE)) < 0)
                                return new Fun.Tuple2<>(NO_BALANCE, "< 1");

                            break;
                    }

                    if (asset.isSelfManaged()) {
                        // учетная единица - само контролируемая
                        if (!creator.equals(asset.getOwner())) {
                            return new Fun.Tuple2<>(CREATOR_NOT_OWNER, "creator != asset owner");
                        }
                        if (creator.equals(recipient)) {
                            return new Fun.Tuple2<>(INVALID_ADDRESS, "Creator equal recipient");
                        }

                        // TRY FEE
                        if ((checkFlags & NOT_VALIDATE_FLAG_FEE) == 0
                                && !BlockChain.isFeeEnough(height, creator)
                                && creator.getForFee(dcSet).compareTo(fee) < 0) {
                            return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);
                        }

                    } else {

                        // самому себе нельзя пересылать
                        if (creator.equals(recipient)
                                && balancePos != ACTION_SPEND) {
                            return new Fun.Tuple2<>(INVALID_ADDRESS, "Creator equal recipient");
                        }

                        // VALIDATE by ASSET TYPE
                        switch (assetType) {
                            // HOLD GOODS, CHECK myself DEBT for CLAIMS
                            case AssetCls.AS_INSIDE_OTHER_CLAIM:
                                break;
                            case AssetCls.AS_ACCOUNTING:
                                //if (balancePos == ACTION_SEND && key >= 1000 && !creator.equals(asset.getOwner())) {
                                //    return INVALID_CREATOR;
                                //}
                                break;
                        }

                        boolean unLimited;
                        // VALIDATE by ACTION
                        switch (balancePos) {
                            // HOLD GOODS, CHECK myself DEBT for CLAIMS
                            case ACTION_HOLD:

                                if (asset.isUnHoldable()) {
                                    return new Fun.Tuple2<>(NOT_HOLDABLE_ASSET, null);
                                }

                                if (backward) {
                                    // if asset is unlimited and me is creator of this
                                    // asset - for RECIPIENT !
                                    unLimited = asset.isUnlimited(recipient, false);

                                    if (!unLimited && (checkFlags & NOT_VALIDATE_FLAG_BALANCE) == 0) {
                                        balance = recipient.getBalance(dcSet, key, balancePos).b;
                                        ////BigDecimal amountOWN = recipient.getBalance(dcSet, key, ACTION_SEND).b;
                                        // amontOWN, balance and amount - is
                                        // negative
                                        if (balance.compareTo(amount.abs()) < 0) {
                                            return new Fun.Tuple2<>(NO_HOLD_BALANCE, null);
                                        }
                                    }
                                } else {
                                    return new Fun.Tuple2<>(INVALID_HOLD_DIRECTION, null);
                                }

                                if ((checkFlags & NOT_VALIDATE_FLAG_FEE) == 0
                                        && !BlockChain.isFeeEnough(height, creator)
                                        && creator.getForFee(dcSet).compareTo(fee) < 0) {
                                    return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);
                                }

                                break;

                            case ACTION_DEBT: // DEBT, CREDIT and BORROW

                                if (asset.isUnDebtable()) {
                                    return new Fun.Tuple2<>(NOT_DEBTABLE_ASSET, null);
                                }

                                // CLAIMS DEBT - only for OWNER except BILL
                                if (asset.isOutsideType()
                                        && assetType != AssetCls.AS_OUTSIDE_BILL
                                        && assetType != AssetCls.AS_OUTSIDE_BILL_EX) {
                                    if (!recipient.equals(asset.getOwner())) {
                                        return new Fun.Tuple2<>(INVALID_CLAIM_DEBT_RECIPIENT, "recipient != asset owner");
                                    } else if (creator.equals(asset.getOwner())) {
                                        return new Fun.Tuple2<>(INVALID_CLAIM_DEBT_CREATOR, "creator == asset owner");
                                    }
                                }

                                if (backward) {

                                    // BACKWARD - BORROW - CONFISCATE CREDIT
                                    Tuple3<String, Long, String> creditKey = new Tuple3<String, Long, String>(
                                            creator.getAddress(), key, recipient.getAddress());
                                    BigDecimal creditAmount = dcSet.getCreditAddressesMap().get(creditKey);
                                    if (creditAmount.compareTo(amount) < 0) {
                                        // NOT ENOUGH DEBT from recipient to THIS creator
                                        return new Fun.Tuple2<>(NO_DEBT_BALANCE, null);
                                    }

                                    // тут проверим и по [В ИСПОЛЬЗОВАНИИ] сколько мы можем забрать
                                    // так как он мог потратить из forFEE - долговые
                                    if (!asset.isUnlimited(recipient, false)
                                            && recipient.getBalanceUSE(key, dcSet)
                                            .compareTo(amount) < 0) {
                                        return new Fun.Tuple2<>(NO_BALANCE, null);
                                    }

                                } else {
                                    // CREDIT - GIVE CREDIT OR RETURN CREDIT

                                    if (!asset.isUnlimited(creator, false)) {

                                        if (creator.getBalanceUSE(key, dcSet)
                                                .compareTo(amount) < 0) {

                                            return new Fun.Tuple2<>(NO_BALANCE, null);
                                        }

                                        Tuple3<String, Long, String> creditKey = new Tuple3<String, Long, String>(
                                                recipient.getAddress(), key, creator.getAddress());
                                        // TRY RETURN
                                        BigDecimal creditAmount = dcSet.getCreditAddressesMap().get(creditKey);
                                        if (creditAmount.compareTo(amount) < 0) {

                                            BigDecimal leftAmount = amount.subtract(creditAmount);
                                            BigDecimal balanceOwn = creator.getBalance(dcSet, key, ACTION_SEND).b; // OWN
                                            // balance
                                            // NOT ENOUGHT DEBT from recipient to
                                            // creator
                                            // TRY CREDITN OWN
                                            if (balanceOwn.compareTo(leftAmount) < 0) {
                                                // NOT ENOUGHT DEBT from recipient to
                                                // creator
                                                return new Fun.Tuple2<>(NO_BALANCE, null);
                                            }
                                        }
                                    }
                                }

                                if ((checkFlags & NOT_VALIDATE_FLAG_FEE) == 0
                                        && !BlockChain.isFeeEnough(height, creator)
                                        && creator.getForFee(dcSet).compareTo(fee) < 0) {
                                    return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);
                                }

                                break;

                            case ACTION_SEND: // SEND ASSET

                                if (key == RIGHTS_KEY) {

                                    // byte[] ss = creator.getAddress();
                                    if (height > BlockChain.FREEZE_FROM
                                            && BlockChain.FOUNDATION_ADDRESSES.contains(creator.getAddress())) {
                                        // LOCK PAYMENTS
                                        wrong = true;
                                        for (String address : BlockChain.TRUE_ADDRESSES) {
                                            if (recipient.equals(address)
                                                // || creator.equals(address)
                                            ) {
                                                wrong = false;
                                                break;
                                            }
                                        }

                                        if (wrong) {
                                            // int balance =
                                            // creator.getBalance(dcSet,
                                            // key, 1).b.intValue();
                                            // if (balance > 3000)
                                            return new Fun.Tuple2<>(INVALID_CREATOR, "freeze");
                                        }
                                    }
                                }

                                // CLAIMS - invalid for backward to CREATOR - need use SPEND instead
                                if (asset.isOutsideType() && recipient.equals(asset.getOwner())) {
                                    // ERROR
                                    return new Fun.Tuple2<>(INVALID_CLAIM_RECIPIENT, "recipient == asset author, try SPEND instead");
                                }


                                if (key == FEE_KEY) {

                                    BigDecimal forSale = creator.getForSale(dcSet, FEE_KEY, height, true);

                                    if ((checkFlags & NOT_VALIDATE_FLAG_FEE) == 0) {
                                        forSale = forSale.subtract(fee);
                                        if (assetFee != null && assetFee.signum() != 0) {
                                            // учтем что еще процент с актива
                                            forSale = forSale.subtract(assetFee);
                                        }
                                    }

                                    if (!BlockChain.isFeeEnough(height, creator)
                                            && forSale.compareTo(amount) < 0) {

                                        /// если это девелоп то не проверяем ниже особые счета
                                        if (BlockChain.CLONE_MODE || BlockChain.TEST_MODE)
                                            return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);

                                        wrong = true;
                                        for (byte[] valid_item : BlockChain.VALID_BAL) {
                                            if (Arrays.equals(signature, valid_item)) {
                                                wrong = false;
                                                break;
                                            }
                                        }

                                        if (wrong)
                                            return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);
                                    }

                                } else {

                                    // if asset is unlimited and me is creator of this asset
                                    unLimited = asset.isUnlimited(creator, false);
                                    if (unLimited) {
                                        // TRY FEE
                                        if ((checkFlags & NOT_VALIDATE_FLAG_FEE) == 0
                                                && !BlockChain.isFeeEnough(height, creator)
                                                && creator.getForFee(dcSet).compareTo(fee) < 0) {
                                            return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);
                                        }

                                    } else {

                                        // ALL OTHER ASSET

                                        // проверим баланс по КОМПУ
                                        if ((checkFlags & NOT_VALIDATE_FLAG_FEE) == 0
                                                && !BlockChain.ERA_COMPU_ALL_UP
                                                && !BlockChain.isFeeEnough(height, creator)
                                                && creator.getForFee(dcSet).compareTo(fee) < 0) {
                                            if (BlockChain.CLONE_MODE || BlockChain.TEST_MODE)
                                                return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);

                                            // TODO: delete wrong check in new CHAIN
                                            // SOME PAYMENTs is WRONG
                                            wrong = true;
                                            for (byte[] valid_item : BlockChain.VALID_BAL) {
                                                if (Arrays.equals(signature, valid_item)) {
                                                    wrong = false;
                                                    break;
                                                }
                                            }

                                            if (wrong)
                                                return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);
                                        }

                                        BigDecimal forSale = creator.getForSale(dcSet, key, height,
                                                true);

                                        if (assetFee != null && assetFee.signum() != 0) {
                                            // учтем что еще процент с актива
                                            forSale = forSale.subtract(assetFee);
                                        }

                                        if (amount.compareTo(forSale) > 0) {
                                            if (BlockChain.CLONE_MODE || BlockChain.TEST_MODE)
                                                return new Fun.Tuple2<>(NO_BALANCE, null);

                                            // TODO: delete wrong check in new CHAIN
                                            // SOME PAYMENTs is WRONG
                                            wrong = true;
                                            for (byte[] valid_item : BlockChain.VALID_BAL) {
                                                if (Arrays.equals(signature, valid_item)) {
                                                    wrong = false;
                                                    break;
                                                }
                                            }

                                            if (wrong)
                                                return new Fun.Tuple2<>(NO_BALANCE, null);
                                        }

                                    }
                                }

                                if (height > BlockChain.FREEZE_FROM) {
                                    String unlock = BlockChain.LOCKED__ADDRESSES.get(creator.getAddress());
                                    if (unlock != null && !recipient.equals(unlock))
                                        return new Fun.Tuple2<>(INVALID_CREATOR, "locked");

                                    Tuple3<String, Integer, Integer> unlockItem = BlockChain.LOCKED__ADDRESSES_PERIOD
                                            .get(creator.getAddress());
                                    if (unlockItem != null && unlockItem.b > height && height < unlockItem.c
                                            && !recipient.equals(unlockItem.a))
                                        return new Fun.Tuple2<>(INVALID_CREATOR, "locked");

                                }

                                break;

                            case ACTION_SPEND: // PRODUCE - SPEND

                                if (asset.isUnSpendable()) {
                                    return new Fun.Tuple2<>(NOT_SPENDABLE_ASSET, null);
                                }

                                if (backward) {
                                    // PRODUCE is denied - only SPEND
                                    return new Fun.Tuple2<>(INVALID_BACKWARD_ACTION, null);
                                } else {

                                    if (asset.isOutsideType() && !recipient.equals(asset.getOwner())) {
                                        return new Fun.Tuple2<>(INVALID_RECEIVER, "recipient != asset owner");
                                    }

                                    // if asset is unlimited and me is creator of this asset
                                    unLimited = asset.isUnlimited(creator, false);

                                    if (!unLimited) {

                                        BigDecimal forSale = creator.getForSale(dcSet, key, height,
                                                false);

                                        if (amount.abs().compareTo(forSale) > 0) {
                                            return new Fun.Tuple2<>(NO_BALANCE, null);
                                        }
                                    }
                                }

                                // TRY FEE
                                if ((checkFlags & NOT_VALIDATE_FLAG_FEE) == 0
                                        && !BlockChain.isFeeEnough(height, creator)
                                        && creator.getForFee(dcSet).compareTo(fee) < 0) {
                                    return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);
                                }

                                break;

                            case ACTION_PLEDGE: // Учесть передачу в залог и возврат из залога

                                // пока отключим
                                if (true) {
                                    return new Fun.Tuple2<>(INVALID_TRANSFER_TYPE, null);
                                }

                                if (asset.isOutsideType()) {
                                    return new Fun.Tuple2<>(INVALID_TRANSFER_TYPE, null);
                                }

                                if (backward) {
                                    if (!asset.getOwner().equals(recipient))
                                        return new Fun.Tuple2<>(INVALID_BACKWARD_ACTION, null);
                                } else {
                                    if (!asset.getOwner().equals(creator))
                                        return new Fun.Tuple2<>(CREATOR_NOT_OWNER, "asset owner != creator");
                                }

                                // if asset is unlimited and me is creator of this
                                // asset
                                unLimited = asset.isUnlimited(creator, false);

                                if (!unLimited) {

                                    BigDecimal forSale = creator.getForSale(dcSet, key, height,
                                            false);

                                    if (amount.abs().compareTo(forSale) > 0) {
                                        return new Fun.Tuple2<>(NO_BALANCE, null);
                                    }
                                }

                                // TRY FEE
                                if ((checkFlags & NOT_VALIDATE_FLAG_FEE) == 0
                                        && !BlockChain.isFeeEnough(height, creator)
                                        && creator.getForFee(dcSet).compareTo(fee) < 0) {
                                    return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);
                                }

                                break;

                            default:
                                return new Fun.Tuple2<>(INVALID_TRANSFER_TYPE, null);
                        }

                        // IF send from PERSON to ANONYMOUS
                        if (!isValidPersonProtect(dcSet, height, recipient,
                                creatorIsPerson, key, actionType,
                                asset))
                            return new Fun.Tuple2<>(RECEIVER_NOT_PERSONALIZED, null);
                    }
                }
            }

        } else {
            // TODO first org.erachain.records is BAD already ((
            // CHECK IF CREATOR HAS ENOUGH FEE MONEY
            if (height > BlockChain.ALL_BALANCES_OK_TO
                    && (checkFlags & NOT_VALIDATE_FLAG_FEE) == 0
                    && !BlockChain.isFeeEnough(height, creator)
                    && creator.getForFee(dcSet).compareTo(fee) < 0) {
                return new Fun.Tuple2<>(NOT_ENOUGH_FEE, null);
            }

        }

        return new Fun.Tuple2<>(VALIDATE_OK, null);
    }

    public int getActionType() {
        return actionType;
    }

    public static void processAction(DCSet dcSet, boolean asOrphan, PublicKeyAccount creator, Account recipient,
                                     int actionType, AssetCls asset, long key, BigDecimal amount,
                                     boolean incomeReverse) {

        // STANDARD ACTION PROCESS
        // UPDATE SENDER
        if (key == 666L) {
            creator.changeBalance(dcSet, asOrphan, actionType, key, amount, asset, false, !incomeReverse);
        } else {
            creator.changeBalance(dcSet, !asOrphan, actionType, key, amount, asset, false, !incomeReverse);
        }
        // UPDATE RECIPIENT
        recipient.changeBalance(dcSet, asOrphan, actionType, key, amount, asset, true, incomeReverse);

        if (actionType == ACTION_DEBT) {
            String creatorStr = creator.getAddress();
            String recipientStr = recipient.getAddress();
            Tuple3<String, Long, String> creditKey = new Tuple3<>(creatorStr, key, recipientStr);
            Tuple3<String, Long, String> creditKeyRecipient = new Tuple3<>(recipientStr, key, creatorStr);

            if (asOrphan) {
                if (actionType < 0) {
                    // CONFISCATE
                    dcSet.getCreditAddressesMap().add(creditKey, amount);
                } else {
                    // in BACK order - RETURN CREDIT << CREDIT
                    // GET CREDIT for left AMOUNT
                    BigDecimal leftAmount = dcSet.getCreditAddressesMap().get(creditKey);
                    if (leftAmount.compareTo(amount) < 0) {
                        dcSet.getCreditAddressesMap().sub(creditKey, leftAmount);
                        // RETURN my DEBT and make reversed DEBT
                        dcSet.getCreditAddressesMap().add(creditKeyRecipient, amount.subtract(leftAmount));
                    } else {
                        // ONLY RETURN CREDIT
                        dcSet.getCreditAddressesMap().sub(creditKey, amount);
                    }
                }
            } else {
                if (actionType < 0) {
                    // CONFISCATE
                    dcSet.getCreditAddressesMap().sub(creditKey, amount);
                } else {
                    // CREDIT or RETURN CREDIT
                    BigDecimal creditAmount = dcSet.getCreditAddressesMap().get(creditKeyRecipient);
                    if (creditAmount.compareTo(amount) >= 0) {
                        // ALL CREDIT RETURN
                        dcSet.getCreditAddressesMap().sub(creditKeyRecipient, amount);
                    } else {
                        // update creditAmount to 0
                        BigDecimal leftAmount;
                        if (creditAmount.signum() != 0) {
                            dcSet.getCreditAddressesMap().sub(creditKeyRecipient, creditAmount);
                            // GET CREDIT for left AMOUNT
                            leftAmount = amount.subtract(creditAmount);
                        } else {
                            leftAmount = amount;
                        }

                        dcSet.getCreditAddressesMap().add(creditKey, leftAmount);
                    }
                }
            }
        }

        if (actionType == ACTION_SEND && asset.isChangeDebtBySendActions()) {
            // если это актив который должен поменять и балансы Долговые то
            // тут не важно какое направление и какой остаток - все одинаково - учетный же

            processAction(dcSet, asOrphan, creator, recipient, -ACTION_DEBT,
                    asset, key, amount, incomeReverse);
        } else if (actionType == ACTION_SPEND && asset.isChangeDebtBySpendActions()) {
            // если это актив в Требованием Исполнения - то подтверждение Исполнения уменьшит и Требование Исполнения
            // Но ПОЛУЧАТЕЛЬ - у нас создатель Актива

            // смотрим какой там долг (он отрицательный)
            BigDecimal debtBalance = creator.getBalance(dcSet, key, ACTION_DEBT).b;
            // и берем наибольший из них (там оба отрицательные) - так чтобы если Требование меньше Чем, текущее Действие - чтобы в минус не ушло
            debtBalance = debtBalance.max(amount);

            if (debtBalance.signum() != 0) {
                processAction(dcSet, !asOrphan, creator, recipient, -ACTION_DEBT,
                        asset, key, debtBalance.negate(), incomeReverse);
            }
        }

    }

    public static void processActionBody(DCSet dcSet, boolean asOrphan, Block block, long dbRef,
                                         PublicKeyAccount creator, Account recipient,
                                         int actionType, BigDecimal amount,
                                         long key, AssetCls asset, BigDecimal assetFEE) {

        // BACKWARD - CONFISCATE
        int posBal = Math.abs(actionType);
        boolean incomeReverse = posBal == ACTION_HOLD;

        // STANDARD ACTION ORPHAN
        processAction(dcSet, asOrphan, creator, recipient, actionType, asset, key, amount, incomeReverse);

        if (key == RIGHTS_KEY && block != null) {
            block.addForgingInfoUpdate(recipient);
        }

        if (assetFEE != null && assetFEE.signum() != 0) {
            ///
            // in proc: this.creator.changeBalance(dcSet, !backward, backward, absKey, this.assetFEE.a, false, false, !incomeReverse);
            // in orph: this.creator.changeBalance(dcSet, backward, backward, absKey, this.assetFEE.a, false, false, !incomeReverse);
            /// USE: (asOrphan ^ !backward) for both actions
            creator.changeBalance(dcSet, asOrphan, actionType, key, assetFEE, asset, false, !incomeReverse);

            if (block != null && !asOrphan) {
                block.addCalculated(creator, key,
                        assetFEE.negate(), "Asset Fee", dbRef);
            }
        }

    }

    @Override
    public void setDC(DCSet dcSet, boolean andUpdateFromState) {
        super.setDC(dcSet, false);
        if (BlockChain.TEST_DB == 0 && recipient != null) {
            recipientPersonDuration = recipient.getPersonDuration(dcSet);
            if (recipientPersonDuration != null) {
                recipientPerson = (PersonCls) dcSet.getItemPersonMap().get(recipientPersonDuration.a);
            }
        }

        if (this.key != 0L) {
            this.asset = this.dcSet.getItemAssetMap().get(this.getKey());
        }
        if (packet != null) {
            for (Object[] row : packet) {
                row[7] = this.dcSet.getItemAssetMap().get((Long) row[0]);
            }
        }

        if (false)
            updateFromStateDB();

    }

    @Override
    public void setDC(DCSet dcSet, int forDeal, int blockHeight, int seqNo, boolean andUpdateFromState) {
        super.setDC(dcSet, forDeal, blockHeight, seqNo, false);

        if (false)
            updateFromStateDB();
    }

    public Account getRecipient() {
        return this.recipient;
    }

    @Override
    public long getKey() {
        return this.key;
    }

    @Override
    public long getAssetKey() {
        return this.key;
    }

    @Override
    public ItemCls getItem() {
        return this.asset;
    }

    public boolean hasPacket() {
        return packet != null;
    }

    /*
     * ************** VIEW
     */

    public Object[][] getPacket() {
        return packet;
    }

    @Override
    public void makeItemsKeys() {

        if (isWiped()) {
            itemsKeys = new Object[0][0];

        } else {

            // запомним что тут две сущности
            if (key != 0) {
                if (creatorPersonDuration != null) {
                    if (recipientPersonDuration != null) {
                        itemsKeys = new Object[][]{
                                new Object[]{ItemCls.PERSON_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()},
                                new Object[]{ItemCls.PERSON_TYPE, recipientPersonDuration.a, recipientPerson.getTagsFull()},
                                new Object[]{ItemCls.ASSET_TYPE, getKey(), asset.getTagsFull()}
                        };
                    } else {
                        itemsKeys = new Object[][]{
                                new Object[]{ItemCls.PERSON_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()},
                                new Object[]{ItemCls.ASSET_TYPE, getKey(), asset.getTagsFull()}
                        };
                    }
                } else {
                    if (recipientPersonDuration != null) {
                        itemsKeys = new Object[][]{
                                new Object[]{ItemCls.PERSON_TYPE, recipientPersonDuration.a, recipientPerson.getTagsFull()},
                                new Object[]{ItemCls.ASSET_TYPE, getKey(), asset.getTagsFull()}
                        };
                    } else {
                        itemsKeys = new Object[][]{
                                new Object[]{ItemCls.ASSET_TYPE, getKey(), asset.getTagsFull()}
                        };
                    }
                }
            } else {
                if (creatorPersonDuration != null) {
                    if (recipientPersonDuration != null) {
                        itemsKeys = new Object[][]{
                                new Object[]{ItemCls.PERSON_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()},
                                new Object[]{ItemCls.PERSON_TYPE, recipientPersonDuration.a, recipientPerson.getTagsFull()},
                        };
                    } else {
                        itemsKeys = new Object[][]{
                                new Object[]{ItemCls.PERSON_TYPE, creatorPersonDuration.a, creatorPerson.getTagsFull()},
                        };
                    }
                } else {
                    if (recipientPersonDuration != null) {
                        itemsKeys = new Object[][]{
                                new Object[]{ItemCls.PERSON_TYPE, recipientPersonDuration.a, recipientPerson.getTagsFull()},
                        };
                    } else {
                        // need initalize in all cases
                        itemsKeys = new Object[0][0];
                    }
                }
            }

            // TODO добавить список в накладной
            if (packet != null) {
                Object[][] itemsKeysFull;
                if (itemsKeys.length > 0) {
                    itemsKeysFull = new Object[itemsKeys.length + packet.length][];
                    System.arraycopy(itemsKeys, 0, itemsKeysFull, 0, itemsKeys.length);
                    for (int count = 0; count < packet.length; count++) {
                        itemsKeysFull[itemsKeys.length + count] = new Object[]{ItemCls.ASSET_TYPE, packet[count][0], ((AssetCls) packet[count][7]).getTagsFull()};
                    }
                } else {
                    itemsKeysFull = new Object[packet.length][];
                    for (int count = 0; count < packet.length; count++) {
                        itemsKeysFull[count] = new Object[]{ItemCls.ASSET_TYPE, packet[count][0], ((AssetCls) packet[count][7]).getTagsFull()};
                    }
                }

                itemsKeys = itemsKeysFull;
            }
        }

    }

    @Override
    public AssetCls getAsset() {
        return this.asset;
    }

    @Override
    public BigDecimal getAmount() {
        // return this.amount == null? BigDecimal.ZERO: this.amount;
        return this.amount;
    }

    @Override
    public BigDecimal getAmount(Account account) {
        if (this.amount == null)
            return BigDecimal.ZERO;

        if (this.creator.equals(account)) {
            // IF SENDER
            return amount.negate();
        } else if (this.recipient.equals(account)) {
            // IF RECIPIENT
            return amount;
        }

        return BigDecimal.ZERO;
    }

    public Tuple2<BigDecimal, BigDecimal> calcSendTAX(Long key, AssetCls asset, BigDecimal amount) {
        return calcSendTAX(height, key, asset, amount);
    }

    @Override
    public long calcBaseFee(boolean withFreeProtocol) {

        if (creator == null)
            return 0L;

        long long_fee = super.calcBaseFee(withFreeProtocol);

        // TODO packet
        if (packet != null) {
            long_fee += 25L * packet.length * BlockChain.FEE_PER_BYTE;
        }

        assetFEE = null;
        assetsPacketFEE = null;

        // ПРОЦЕНТЫ в любом случае посчитаем - даже если халявная транзакция
        // только для передачи в собственность!
        if (actionType == ACTION_SEND && !BlockChain.ASSET_TRANSFER_PERCENTAGE_MIN_TAB.isEmpty()) {
            if (packet != null) {
                assetsPacketFEE = new HashMap<>();
                Tuple2<BigDecimal, BigDecimal> assetFee;
                for (Object[] row : packet) {
                    assetFee = calcSendTAX((Long) row[0], (AssetCls) row[7], (BigDecimal) row[1]);
                    if (assetFee != null)
                        assetsPacketFEE.put((AssetCls) row[7], assetFee);
                }

            } else if (hasAmount() && !creator.equals(asset.getOwner()) && !recipient.equals(asset.getOwner())) {
                assetFEE = calcSendTAX(key, asset, amount);
                if (assetFEE != null) {
                    long_fee -= 256L;
                    if (long_fee < 0L)
                        long_fee = 0L;
                }
            }
        }

        return long_fee;
    }

    public boolean hasAmount() {
        return amount != null && amount.signum() != 0 || packet != null;
    }

    // BACKWARD AMOUNT
    public boolean isBackward() {
        return actionType < 0;
    }

    // PARSE/CONVERT

    @Override
    public String viewRecipient() {
        return recipient.getPersonAsString();
    }

    @Override
    public String viewTypeName() {
        return viewTypeName(packet == null ? this.amount : BigDecimal.ONE, isBackward());
    }

    @Override
    public String viewSubTypeName() {
        if (packet == null && (amount == null || amount.signum() == 0)
                || asset == null) // в телеграммах может быть несуществующий актив - он без проверки идет - чтобы не падал JSON
            return "";

        return viewSubTypeName(actionType);
    }

    @Override
    public String viewSubTypeName(JSONObject langObj) {
        if (packet == null && (amount == null || amount.signum() == 0))
            return "";

        return Lang.T(viewSubTypeName(actionType), langObj);
    }

    @Override
    public String viewAmount() {

        if (hasPacket())
            return "package";

        if (amount == null || amount.signum() == 0)
            return "";

        if (this.amount.signum() < 0) {
            return this.amount.negate().toPlainString();
        } else {
            return this.amount.toPlainString();
        }
    }

    @Override
    public String viewFullTypeName() {
        return viewActionType();
    }

    public String viewActionType() {
        if (hasAmount()) {
            return Account.balancePositionName(actionType)
                    + (isBackward() ? " backward" : "");
        } else
            return "Mail";

    }

    @Override
    public byte[] toBytesBody(int forDeal) {

        // WRITE RECIPIENT
        byte[] data;

        if (this.amount != null) {
            // WRITE ACTION TYPE
            // WRITE KEY
            // WRITE AMOUNT
            data = Bytes.concat(this.recipient.getBytes(), new byte[]{(byte) actionType}, Longs.toByteArray(this.key),
                    BigDecimalUtil.toBytes(amount));

        } else if (packet != null) {
            // WRITE ACTION TYPE
            // WRITE PRICE ASSET KEY
            // WRITE PACKET SIZE
            data = Bytes.concat(new byte[]{(byte) actionType}, Longs.toByteArray(this.key),
                    Ints.toByteArray(packet.length));

            byte[][] memoBytes = new byte[packet.length][];
            int count = 0;
            int additionalLen = 0;
            for (Object[] row : packet) {
                if (row[PACKET_ROW_MEMO_NO] == null) {
                    memoBytes[count++] = new byte[0];
                } else {
                    additionalLen += (memoBytes[count++] = ((String) row[PACKET_ROW_MEMO_NO]).getBytes(StandardCharsets.UTF_8)).length;
                }
            }

            byte[] buff = new byte[packet.length * PACKET_ROW_LENGTH + additionalLen];
            int pos = 0;
            count = 0;
            for (Object[] row : packet) {
                /**
                 * 0: (long) AssetKey, 1: Amount, 2: Price, 3: Discounted Price, 4: Tax as percent, 5: Fee as absolute value, 6: memo, 7: Asset (after setDC())
                 */

                // WRITE ASSET KEY
                System.arraycopy(Longs.toByteArray((Long) row[0]), 0, buff, pos, Long.BYTES);
                pos += Long.BYTES;

                // WRITE AMOUNT
                BigDecimalUtil.toBytes(buff, pos, (BigDecimal) row[1]);
                pos += 9;

                // WRITE PRICE
                if (row[2] != null && ((BigDecimal) row[2]).signum() != 0)
                    BigDecimalUtil.toBytes(buff, pos, (BigDecimal) row[2]);
                pos += 9;

                // WRITE DISCOUNT PRICE
                if (row[3] != null && ((BigDecimal) row[3]).signum() != 0)
                    BigDecimalUtil.toBytes(buff, pos, (BigDecimal) row[3]);
                pos += 9;

                // WRITE TAX
                if (row[4] != null && ((BigDecimal) row[4]).signum() != 0)
                    BigDecimalUtil.toBytes(buff, pos, (BigDecimal) row[4]);
                pos += 9;

                // WRITE FEE
                if (row[5] != null && ((BigDecimal) row[5]).signum() != 0)
                    BigDecimalUtil.toBytes(buff, pos, (BigDecimal) row[5]);
                pos += 9;

                // WRITE MEMO LEN
                buff[pos++] = (byte) memoBytes[count].length;
                // WRITE MEMO
                System.arraycopy(memoBytes[count], 0, buff, pos, memoBytes[count].length);
                pos += memoBytes[count++].length;
            }

            data = Bytes.concat(data, buff);

        } else {
            data = this.recipient.getBytes();
        }


        return data;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void toJsonBody(JSONObject transaction) {

        recipient.toJsonPersonInfo(transaction, "recipient");
        if (amount != null && amount.signum() != 0) {
            JSONObject amountJson = new JSONObject();
            transaction.put("amount", amountJson);

            amountJson.put("action", actionType);
            amountJson.put("assetKey", this.getKey());
            if (asset == null) {
                setDC(DCSet.getInstance(), false);
            }
            amountJson.put("asset", asset.toJsonInfo());

            amountJson.put("volume", this.amount.toPlainString());
            amountJson.put("actionName", viewActionType());

        } else if (packet != null) {
            JSONObject packetJson = new JSONObject();
            transaction.put("amount", packetJson);

            packetJson.put("priceAssetKey", this.getKey());
            if (asset == null) {
                setDC(DCSet.getInstance(), false);
            }
            asset.toJsonInfo(packetJson, "priceAsset");

            packetJson.put("action", actionType);
            packetJson.put("actionName", viewSubTypeName(actionType));

            JSONArray packetArray = new JSONArray();
            for (Object[] row : packet) {
                JSONArray rowArray = new JSONArray();
                rowArray.add(row[0]); // asset Key
                rowArray.add(((BigDecimal) row[1]).stripTrailingZeros().toPlainString()); // volume
                rowArray.add(row[2] == null ? "0" : ((BigDecimal) row[2]).stripTrailingZeros().toPlainString()); // price
                rowArray.add(row[3] == null ? "0" : ((BigDecimal) row[3]).stripTrailingZeros().toPlainString()); // discounted price
                rowArray.add(row[4] == null ? "0" : ((BigDecimal) row[4]).stripTrailingZeros().toPlainString()); // tax %
                rowArray.add(row[5] == null ? "0" : ((BigDecimal) row[5]).stripTrailingZeros().toPlainString()); // fee
                rowArray.add(row[6] == null ? "" : row[6]); // memo
                rowArray.add(((AssetCls) row[7]).toJsonInfo()); // asset
                packetArray.add(rowArray);
            }
            packetJson.put("array", packetArray);

        }
    }

    @Override
    public HashSet<Account> getInvolvedAccounts() {
        HashSet<Account> accounts = new HashSet<Account>(3, 1);
        if (this.creator != null)
            accounts.add(this.creator);

        accounts.addAll(this.getRecipientAccounts());
        return accounts;
    }

    @Override
    public boolean hasRecipients() {
        return true;
    }

    @Override
    public HashSet<Account> getRecipientAccounts() {
        HashSet<Account> accounts = new HashSet<Account>(2, 1);
        accounts.add(this.recipient);
        return accounts;
    }

    @Override
    public boolean isInvolved(Account account) {
        return account.equals(creator)
                || account.equals(recipient);
    }

    @Override
    public int getDataLength(int forDeal) {

        int base_len = super.getDataLength(forDeal);

        if (amount != null) {
            base_len += LOAD_LENGTH;

        } else if (packet != null) {
            base_len += LOAD_LENGTH;

            int additionalLen = 0;
            for (Object[] row : packet) {
                if (row[PACKET_ROW_MEMO_NO] == null)
                    continue;

                additionalLen += ((String) row[PACKET_ROW_MEMO_NO]).getBytes(StandardCharsets.UTF_8).length;
            }

            return base_len - AMOUNT_LENGTH + Integer.BYTES + packet.length * PACKET_ROW_LENGTH + additionalLen;

        } else
            base_len += RECIPIENT_LENGTH;

        return base_len;

    }

    // TODO запоминать пароль
    @Override
    public void encrypt(PrivateKeyAccount privateKeyAccount) {
        if (dataMessage == null || dataMessage.length == 0 || encryptingDone)
            return;

        encryptingDone = true;

        byte[] password = Crypto.getInstance().createSeed(Crypto.HASH_LENGTH);

        try {
            dataMessage = AEScrypto.aesEncrypt(dataMessage, password);
            byte[] passwordEncrypted = AEScrypto.dataEncrypt(password, privateKeyAccount.getPrivateKey(),
                    Controller.getInstance().getPublicKey(recipient));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public int isValid(int forDeal, long checkFlags) throws TxException {

        if (height < BlockChain.ALL_VALID_BEFORE) {
            return VALIDATE_OK;
        }

        int isValidRes = super.isValid(forDeal, checkFlags);
        if (isValidRes != VALIDATE_OK)
            return isValidRes;

        // CHECK IF RECIPIENT IS VALID ADDRESS
        if (!Account.isValidAddress(this.recipient.getBytes())) {
            return INVALID_RECEIVER;
        }

        //////////////////////////////
        // CHECK IF AMOUNT AND ASSET
        if (actionType == 0 && (amount != null || packet != null)) {
            errorValue = "(action == 0 and amount != null OR packet != null";
            return INVALID_AMOUNT;
        }
        if (actionType != 0 && amount == null && packet == null) {
            errorValue = "(action != 0 and amount == null and packet == null";
            return INVALID_AMOUNT;
        }

        if ((typeBytes[POS_TX_FLAGS_1] & HAS_AMOUNT_MASK) == 0 && (typeBytes[POS_TX_FLAGS_1] & HAS_PACKET_MASK) == 0) {
            if (amount != null) {
                errorValue = "HAS_PACKET_MASK and amount != null";
                return INVALID_AMOUNT;
            } else if (packet != null) {
                errorValue = "HAS_PACKET_MASK and packet != null";
                return INVALID_PACKET_SIZE;
            }
        } else if ((typeBytes[POS_TX_FLAGS_1] & HAS_AMOUNT_MASK) != 0) {
            if (amount == null) {
                errorValue = "HAS_AMOUNT_MASK and amount == null";
                return INVALID_AMOUNT;
            } else if (packet != null) {
                errorValue = "HAS_AMOUNT_MASK and packet != null";
                return INVALID_PACKET_SIZE;
            }
        } else if ((typeBytes[POS_TX_FLAGS_1] & HAS_PACKET_MASK) != 0) {
            if (amount != null) {
                errorValue = "HAS_PACKET_MASK and amount != null";
                return INVALID_AMOUNT;
            } else if (packet == null) {
                errorValue = "HAS_PACKET_MASK and packet == null";
                return INVALID_PACKET_SIZE;
            }
        }

        if (this.amount != null) {
            if (key <= 0)
                return INVALID_ITEM_KEY;

            BigDecimal assetFee;
            if (assetFEE == null) {
                assetFee = null;
            } else
                assetFee = assetFEE.a;

            Fun.Tuple2<Integer, PersonCls> creatorPersonDur = creatorPersonDuration == null? null : Account.getPerson(creatorPerson, height, creatorPersonDuration);
            Fun.Tuple2<Integer, PersonCls> recipientPersonDur = recipientPersonDuration == null? null : Account.getPerson(recipientPerson, height, recipientPersonDuration);

            Fun.Tuple2<Integer, String> result = isValidAction(dcSet, height, creator, creatorPersonDur, signature, actionType, key, asset, amount, recipient,
                    recipientPersonDur, fee, assetFee, checkFlags, timestamp);
            if (result.a != VALIDATE_OK) {
                errorValue = result.b;
                return result.a;
            }

        } else if (packet != null) {
            if (key <= 0)
                return INVALID_ITEM_KEY;

            if (packet.length == 0) {
                errorValue = "=0";
                return INVALID_PACKET_SIZE;
            } else if (packet.length > 1000) {
                errorValue = "> " + 1000;
                return INVALID_PACKET_SIZE;
            } else if (actionType < ACTION_SEND || actionType > Account.BALANCE_POS_PLEDGE) {
                return INVALID_BALANCE_POS;
            } else if (asset == null) {
                errorValue = "" + key;
                return ITEM_ASSET_NOT_EXIST;
            }

            int count = 0;
            byte[] temp;
            Long rowAssetKey;

            Fun.Tuple2<BigDecimal, BigDecimal> rowAssetFEE;

            Fun.Tuple2<Integer, PersonCls> creatorPersonDur = creatorPersonDuration == null? null : Account.getPerson(creatorPerson, height, creatorPersonDuration);
            Fun.Tuple2<Integer, PersonCls> recipientPersonDur = recipientPersonDuration == null? null : Account.getPerson(recipientPerson, height, recipientPersonDuration);

            HashSet<Long> keys = new HashSet<>();
            for (Object[] row : packet) {
                rowAssetKey = (Long) row[0];
                if (rowAssetKey == null) {
                    errorValue = "[" + count + "] = null";
                    return INVALID_ITEM_KEY;
                } else if (row[7] == null) {
                    errorValue = "[" + count + "] : " + rowAssetKey;
                    return ITEM_ASSET_NOT_EXIST;
                } else if (row[1] == null || ((BigDecimal) row[1]).signum() <= 0) {
                    errorValue = "Amount[" + count + "] = " + row[1];
                    return INVALID_AMOUNT;
                } else if (row[2] != null && ((BigDecimal) row[2]).signum() < 0) {
                    errorValue = "Price[" + count + "] = " + row[2];
                    return INVALID_AMOUNT;
                } else if (row[3] != null && ((BigDecimal) row[3]).signum() < 0) {
                    errorValue = "DiscontedPrice[" + count + "] = " + row[3];
                    return INVALID_AMOUNT;
                } else if (row[PACKET_ROW_MEMO_NO] != null && ((String) row[PACKET_ROW_MEMO_NO]).length() > 0) {
                    temp = ((String) row[PACKET_ROW_MEMO_NO]).getBytes(StandardCharsets.UTF_8);
                    if (temp.length > 255) {
                        errorValue = "Memo[" + count + "].length > 255";
                        return INVALID_MESSAGE_LENGTH;
                    }
                }

                if (keys.contains(rowAssetKey)) {
                    errorValue = "[" + count + "] : " + rowAssetKey;
                    return ITEM_DUPLICATE_KEY;
                } else {
                    keys.add(rowAssetKey);
                }

                // GET ROW ASSET FEE
                if (assetsPacketFEE == null)
                    rowAssetFEE = null;
                else
                    rowAssetFEE = assetsPacketFEE.get(rowAssetKey);

                Fun.Tuple2<Integer, String> result = isValidAction(dcSet, height, creator, creatorPersonDur, signature, actionType, rowAssetKey,
                        (AssetCls) row[7], (BigDecimal) row[1], recipient,
                        recipientPersonDur, fee, rowAssetFEE == null ? null : rowAssetFEE.a, checkFlags, timestamp);

                if (result.a != VALIDATE_OK) {
                    errorValue = "[" + count + "] : " + result.b;
                    return result.a;
                }

                count++;
            }

        } else {

            // CHECK IF CREATOR HAS ENOUGH FEE MONEY
            if (height > BlockChain.ALL_BALANCES_OK_TO
                    && (checkFlags & NOT_VALIDATE_FLAG_FEE) == 0L
                    && !BlockChain.isFeeEnough(height, creator)
                    && this.creator.getForFee(dcSet).compareTo(this.fee) < 0) {
                return NOT_ENOUGH_FEE;
            }

        }

        return VALIDATE_OK;
    }

    @Override
    public void processBody(Block block, int forDeal) throws TxException {

        super.processBody(block, forDeal);

        if (this.amount == null) {
            if (packet == null)
                return;

            BigDecimal assetFeeRow;
            BigDecimal amountRow;
            Long assetKeyRow;
            // ROW:
            // 0: (long) AssetKey, 1: Amount, 2: Price, 3: Discounted Price, 4: Tax as percent, 5: Fee as absolute value, 6: memo, 7: Asset (after setDC())
            for (Object[] row : packet) {

                // see core.exdata.exActions.ExFilteredPays.makeFilterPayList
                assetKeyRow = (Long) row[0];
                amountRow = (BigDecimal) row[1];


                if (assetsPacketFEE != null && assetsPacketFEE.containsKey((AssetCls) row[7]))
                    assetFeeRow = assetsPacketFEE.get((AssetCls) row[7]).a;
                else
                    assetFeeRow = null;

                // STANDARD ACTION PROCESS
                processActionBody(dcSet, false, block, dbRef, creator, recipient,
                        actionType, amountRow,
                        assetKeyRow, (AssetCls) row[7], assetFeeRow);

            }

            return;

        }

        int amount_sign = this.amount.compareTo(BigDecimal.ZERO);
        if (amount_sign == 0)
            return;

        // BACKWARD - CONFISCATE
        boolean backward = isBackward();
        boolean incomeReverse = actionType == ACTION_HOLD;

        // STANDARD ACTION PROCESS
        processAction(dcSet, false, creator, recipient, actionType, asset, key, amount, incomeReverse);

        if (key == RIGHTS_KEY && block != null) {
            block.addForgingInfoUpdate(this.recipient);
        }

        if (assetFEE != null && assetFEE.a.signum() != 0) {
            // учтем что он еще заплатил комиссию с суммы
            this.creator.changeBalance(dcSet, !backward, actionType, key, this.assetFEE.a, asset, false, !incomeReverse);
            if (block != null) {
                block.addCalculated(this.creator, key,
                        this.assetFEE.a.negate(), "Asset Fee", this.dbRef);
            }
        }

    }

    @Override
    public void orphanBody(Block block, int forDeal) throws TxException {

        super.orphanBody(block, forDeal);

        if (this.amount == null) {
            if (packet == null)
                return;

            boolean backward = isBackward();
            BigDecimal assetFeeRow;
            BigDecimal amountRow;
            Long assetKeyRow;
            // ROW:
            // 0: (long) AssetKey, 1: Amount, 2: Price, 3: Discounted Price, 4: Tax as percent, 5: Fee as absolute value, 6: memo, 7: Asset (after setDC())
            for (Object[] row : packet) {

                // see core.exdata.exActions.ExFilteredPays.makeFilterPayList

                assetKeyRow = (Long) row[0];
                amountRow = (BigDecimal) row[1];


                if (assetsPacketFEE.containsKey((AssetCls) row[7]))
                    assetFeeRow = assetsPacketFEE.get((AssetCls) row[7]).a;
                else
                    assetFeeRow = null;

                // STANDARD ACTION PROCESS
                processActionBody(dcSet, true, block, dbRef, creator, recipient,
                        actionType, amountRow,
                        assetKeyRow, (AssetCls) row[7], assetFeeRow);

            }

            return;
        }

        int amount_sign = this.amount.compareTo(BigDecimal.ZERO);
        if (amount_sign == 0)
            return;

        // BACKWARD - CONFISCATE
        boolean backward = isBackward();
        boolean incomeReverse = actionType == ACTION_HOLD;

        // STANDARD ACTION ORPHAN
        processAction(dcSet, true, creator, recipient, actionType, asset, key, amount, incomeReverse);

        if (key == RIGHTS_KEY && block != null) {
            block.addForgingInfoUpdate(this.recipient);
        }

        if (assetFEE != null && assetFEE.a.signum() != 0) {
            this.creator.changeBalance(dcSet, backward, actionType, key, this.assetFEE.a, asset, false, !incomeReverse);
        }

    }

    public Map<String, Map<Long, BigDecimal>> getAssetAmount() {
        Map<String, Map<Long, BigDecimal>> assetAmount = new LinkedHashMap<>();

        if (this.amount != null) {

            assetAmount = subAssetAmount(assetAmount, this.creator.getAddress(), FEE_KEY, this.fee);

            assetAmount = subAssetAmount(assetAmount, this.creator.getAddress(), this.key, this.amount);
            assetAmount = addAssetAmount(assetAmount, this.recipient.getAddress(), this.key, this.amount);
        }

        return assetAmount;
    }

}
