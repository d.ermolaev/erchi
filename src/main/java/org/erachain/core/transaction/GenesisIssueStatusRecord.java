package org.erachain.core.transaction;

import org.erachain.core.item.statuses.StatusCls;
import org.erachain.core.item.statuses.StatusFactory;


public class GenesisIssueStatusRecord extends GenesisIssueItemRecord {

    private static final byte TYPE_ID = (byte) GENESIS_ISSUE_STATUS_TRANSACTION;
    private static final String NAME_ID = "GENESIS Issue Status";

    public GenesisIssueStatusRecord(StatusCls status, String title, String tags, String message) {
        super(TYPE_ID, NAME_ID, status, title, tags, message);
    }

    public GenesisIssueStatusRecord(byte[] data) throws Exception {
        super(NAME_ID, data);

        item = StatusFactory.getInstance().parse(parsePos, data, false);
        this.generateSignature();

    }

    //GETTERS/SETTERS

    //PARSE CONVERT

}
