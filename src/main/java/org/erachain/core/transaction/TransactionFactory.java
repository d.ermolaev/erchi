package org.erachain.core.transaction;


import com.google.common.primitives.Bytes;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.GenesisBlock;
import org.erachain.core.crypto.Crypto;
import org.json.simple.JSONObject;

import java.util.Arrays;

import static org.erachain.core.transaction.Transaction.FOR_NETWORK;
import static org.erachain.core.transaction.Transaction.FOR_PACK;

public class TransactionFactory {

    private static TransactionFactory instance;

    private TransactionFactory() {

    }

    public static TransactionFactory getInstance() {
        if (instance == null) {
            instance = new TransactionFactory();
        }

        return instance;
    }

    public Transaction parse(byte[] data, int forDeal) throws Exception {

        //READ SIGNATURE TYPE
        byte signType = data[0];

        if (signType == Crypto.NOT_SIGNED) {
            // здесь нет подписи и создателя - без проверки делаем обычным образом парсинг по старинке

            int txsType = Byte.toUnsignedInt(data[1]);
            switch (txsType) {
                case Transaction.CALCULATED_TRANSACTION:
                    //PARSE CALCULATED
                    return new RCalculated(data);

                case Transaction.GENESIS_SEND_ASSET_TRANSACTION:
                    //PARSE TRANSFER ASSET TRANSACTION
                    return new GenesisTransferAssetTransaction(data);

                case Transaction.GENESIS_ISSUE_ASSET_TRANSACTION:
                    //PARSE GENESIS TRANSACTION
                    return new GenesisIssueAssetTransaction(data);

                case Transaction.GENESIS_ISSUE_PERSON_TRANSACTION:
                    //PARSE ISSUE PERSON TRANSACTION
                    return new GenesisIssuePersonRecord(data);

                case Transaction.GENESIS_ISSUE_TEMPLATE_TRANSACTION:
                    //PARSE ISSUE PLATE TRANSACTION
                    return new GenesisIssueTemplateRecord(data);

                case Transaction.GENESIS_ISSUE_STATUS_TRANSACTION:
                    //PARSE ISSUE STATUS TRANSACTION
                    return new GenesisIssueStatusRecord(data);

                case Transaction.GENESIS_CERTIFY_PERSON_TRANSACTION:
                    //PARSE TRANSFER ASSET TRANSACTION
                    return new GenesisCertifyPersonRecord(data);

                case Transaction.GENESIS_ISSUE_UNION_TRANSACTION:
                case Transaction.GENESIS_SIGN_NOTE_TRANSACTION:
                case Transaction.GENESIS_ASSIGN_STATUS_TRANSACTION:
                case Transaction.GENESIS_ADOPT_UNION_TRANSACTION:
                    //PARSE TRANSFER ASSET TRANSACTION
                    // TODO GenesisTransferStatusTransaction
                    // not ready return GenesisTransferStatusTransaction.Parse(data);
                    throw new TxException("Invalid transaction type: " + txsType);

            }

            throw new TxException("Invalid transaction type: " + txsType);
        }

        // тут парсинг новый - сперва публичный ключ потом подпись и дальше остальное
        byte[] pubKey;
        byte[] signature;
        PublicKeyAccount creator;

        if (signType == Crypto.ED25519_SYSTEM) {
            // Ed25519

            // RETRIEVE PUB KEY
            pubKey = new byte[Crypto.HASH_LENGTH];
            System.arraycopy(data, 1, pubKey, 0, Crypto.HASH_LENGTH);

            // RETRIEVE SIGNATURE
            signature = new byte[Crypto.SIGNATURE_LENGTH];
            System.arraycopy(data, 1 + Crypto.HASH_LENGTH, signature, 0, Crypto.SIGNATURE_LENGTH);

            if (forDeal == FOR_NETWORK || forDeal == FOR_PACK) {
                // это пришло из сети - проверяем подпись!

                // чтобы из других цепочек не срабатывало
                // при этом получаем новый массив байт и в нем можно чистить Подпись без восстановления
                BlockChain blockchain = Controller.getInstance().blockChain;
                byte[] genesisSignature;
                if (blockchain == null)
                    genesisSignature = new GenesisBlock().getSignature();
                else
                    genesisSignature = blockchain.getGenesisBlock().getSignature();
                byte[] dataForCheck = Bytes.concat(data, genesisSignature);

                // ERASE SIGNATURE
                System.arraycopy(Crypto.EMPTY_SIGNATURE, 0, dataForCheck, 1 + Crypto.HASH_LENGTH, Crypto.SIGNATURE_LENGTH);

                // CHECK SIGN
                if (!Crypto.getInstance().verify(pubKey, signature, dataForCheck)) {
                    throw new TxSignException();
                }
            }

        } else {
            throw new TxException("Wrong signature system: " + signType);
        }

        creator = new PublicKeyAccount(signType, pubKey);
        int startPos = 1 + creator.getParsePosWithSign();
        int txsType = Byte.toUnsignedInt(data[startPos]);

        switch (txsType) {
            case Transaction.SIGN_NOTE_TRANSACTION:
                //PARSE PAYMENT TRANSACTION
                return new RSignNote(creator, signature, startPos, data, forDeal);

            case Transaction.VOTE_ON_ITEM_POLL_TRANSACTION:
                //PARSE CREATE ITEM POLL VOTE
                return new VoteOnItemPollTransaction(creator, signature, startPos, data, forDeal);

            case Transaction.CREATE_ORDER_TRANSACTION:
                //PARSE ORDER CREATION TRANSACTION
                return CreateOrderTransaction.Parse(data, forDeal);

            case Transaction.CANCEL_ORDER_TRANSACTION:
                return new CancelOrderTransaction(creator, signature, startPos, data, forDeal);

            case Transaction.CHANGE_ORDER_TRANSACTION:
                //PARSE UPDATE CREATION TRANSACTION
                return ChangeOrderTransaction.Parse(data, forDeal);

            case Transaction.ISSUE_ASSET_SERIES_TRANSACTION:
                //PARSE ISSUE ASSET SERIES TRANSACTION
                return IssueAssetSeriesTransaction.Parse(data, forDeal);

            case Transaction.SEND_ASSET_TRANSACTION:
                // PARSE MESSAGE TRANSACTION
                return new TransactionAmount(creator, signature, startPos, data, forDeal);

            case Transaction.HASHES_RECORD:
                // PARSE ACCOUNTING TRANSACTION V3
                return RHashes.Parse(data, forDeal);

            case Transaction.SIGN_TRANSACTION:
                //PARSE CERTIFY PERSON TRANSACTION
                return RVouch.Parse(data, forDeal);

            case Transaction.SET_STATUS_TO_ITEM_TRANSACTION:
                //PARSE CERTIFY PERSON TRANSACTION
                return RSetStatusToItem.Parse(data, forDeal);

            case Transaction.SET_UNION_TO_ITEM_TRANSACTION:
                //PARSE CERTIFY PERSON TRANSACTION
                return RSetUnionToItem.Parse(data, forDeal);

            case Transaction.CERTIFY_PUB_KEYS_TRANSACTION:
                //PARSE CERTIFY PERSON TRANSACTION
                return new RCertifyPubKeys(creator, signature, startPos, data, forDeal);

            case Transaction.ISSUE_ASSET_TRANSACTION:
                //PARSE ISSUE ASSET TRANSACTION
                return new IssueAssetTransaction(creator, signature, startPos, data, forDeal);

            case Transaction.ISSUE_TEMPLATE_TRANSACTION:
                //PARSE ISSUE PLATE TRANSACTION
                return new IssueTemplateRecord(creator, signature, startPos, data, forDeal);

            case Transaction.ISSUE_PERSON_TRANSACTION:
                //PARSE ISSUE PERSON TRANSACTION
                return new IssuePersonRecord(creator, signature, startPos, data, forDeal);

            case Transaction.ISSUE_POLL_TRANSACTION:
                //PARSE ISSUE POLL TRANSACTION
                return new IssuePollRecord(creator, signature, startPos, data, forDeal);

            case Transaction.ISSUE_STATUS_TRANSACTION:
                //PARSE ISSUE PLATE TRANSACTION
                return new IssueStatusRecord(creator, signature, startPos, data, forDeal);

        }

        throw new TxException("Invalid transaction type: " + txsType);

    }

    public Transaction parseJson(JSONObject json, TxException errorMessage) throws Exception {

        if (!json.containsKey("type"))
            throw new TxParseException("Empty value",
                    "type",
                    "Use that values: " + Arrays.asList(Transaction.getTransactionTypes(true)));

        int txsType = (int) (long) (Long) json.get("type");

        switch (txsType) {
            case Transaction.CANCEL_ORDER_TRANSACTION:
                return new CancelOrderTransaction(json, errorMessage);
            case Transaction.ISSUE_ASSET_TRANSACTION:
                return new IssueAssetTransaction(json, errorMessage);
            case Transaction.ISSUE_TEMPLATE_TRANSACTION:
                return new IssueTemplateRecord(json, errorMessage);
            case Transaction.ISSUE_PERSON_TRANSACTION:
                return new IssuePersonRecord(json, errorMessage);
            case Transaction.ISSUE_POLL_TRANSACTION:
                return new IssuePollRecord(json, errorMessage);
            case Transaction.ISSUE_STATUS_TRANSACTION:
                return new IssueStatusRecord(json, errorMessage);
            case Transaction.SEND_ASSET_TRANSACTION:
                return new TransactionAmount(json, errorMessage);
            case Transaction.CERTIFY_PUB_KEYS_TRANSACTION:
                return new RCertifyPubKeys(json, errorMessage);
            case Transaction.CREATE_ORDER_TRANSACTION:
                return new CreateOrderTransaction(json, errorMessage);
            case Transaction.VOTE_ON_ITEM_POLL_TRANSACTION:
                return new VoteOnItemPollTransaction(json, errorMessage);

            case Transaction.SIGN_NOTE_TRANSACTION:
            case Transaction.CHANGE_ORDER_TRANSACTION:
            case Transaction.ISSUE_ASSET_SERIES_TRANSACTION:
            case Transaction.HASHES_RECORD:
            case Transaction.SIGN_TRANSACTION:
            case Transaction.SET_STATUS_TO_ITEM_TRANSACTION:
            case Transaction.SET_UNION_TO_ITEM_TRANSACTION:
                break;
        }

        throw new TxParseException("Invalid value: " + txsType,
                "type",
                "Use that values: " + Arrays.asList(Transaction.getTransactionTypes(true)));

    }

}
