package org.erachain.core.transaction;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import org.erachain.controller.Controller;
import org.erachain.core.account.Account;
import org.erachain.core.block.Block;
import org.erachain.lang.Lang;
import org.json.simple.JSONObject;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * ## typeBytes
 * 0 - record type
 * 1 - record version
 * 2 - property 1
 * 3 = property 2
 * <br>
 * flags - is reference to Parent transaction
 */
public class RCalculated extends GenesisTransferAssetTransaction {

    private static final byte TYPE_ID = (byte) Transaction.CALCULATED_TRANSACTION;
    private static final String NAME_ID = "_protocol_";
    private long txReference;

    public RCalculated(Account recipient, long key,
                       BigDecimal amount, String title, long txReference, long seqNo) {
        super(TYPE_ID, NAME_ID, recipient, key, amount, title, null, null);

        this.txReference = txReference;

        if (seqNo > 0)
            this.setHeightSeq(seqNo);

    }

    public RCalculated(Account recipient, long key,
                       BigDecimal amount, String title, String tags, String message,
                       long txReference, long seqNo) {
        super(TYPE_ID, NAME_ID, recipient, key, amount, title, tags, message);

        this.txReference = txReference;

        if (seqNo > 0)
            this.setHeightSeq(seqNo);

    }


    public RCalculated(byte[] data) throws Exception {
        super(NAME_ID, data);

        //READ REFERENCE
        byte[] referBytes = Arrays.copyOfRange(data, parsePos, parsePos + TIMESTAMP_LENGTH);
        txReference = Longs.fromByteArray(referBytes);
        parsePos += TIMESTAMP_LENGTH;

        //READ SEQ_NO
        byte[] seqNoBytes = Arrays.copyOfRange(data, parsePos, parsePos + TIMESTAMP_LENGTH);
        setHeightSeq(Longs.fromByteArray(seqNoBytes));
        parsePos += TIMESTAMP_LENGTH;

    }


    // GETTERS/SETTERS

    @Override
    public int hashCode() {
        return Long.hashCode(dbRef);
    }

    @Override
    public boolean equals(Object transaction) {
        if (transaction instanceof RCalculated)
            return dbRef == ((Transaction) transaction).getDBRef();
        return false;
    }

    @Override
    public Long getTimestamp() {
        return (this.timestamp = Controller.getInstance().blockChain.getTimestamp(this.height) + seqNo);
    }

    @Override
    public String viewTypeName() {
        return NAME_ID;
    }

    @Override
    public String viewSignature() {
        return "calculated_" + viewHeightSeq();
    }

    @Override
    public String viewSubTypeName() {
        return "";
    }

    public String viewSubTypeName(JSONObject langObj) {
        return "";
    }

    public String viewFullTypeName(JSONObject langObj) {
        return viewTypeName();
    }

    @Override
    public String viewFullTypeName() {
        return viewTypeName();
    }

    @Override
    public String viewActionType() {
        return "";
    }

    @Override
    public boolean hasPublicText() {
        return false;
    }

    public String viewTxReference() {
        return Transaction.viewDBRef(txReference);
    }

    @Override
    public String viewTitle() {
        return this.title + " (@" + viewTxReference() + ")";
    }

    @Override
    public String viewTitle(JSONObject langObj) {
        return Lang.T(this.title, langObj) + " (@" + viewTxReference() + ")";
    }

    // PARSE/CONVERT

    @Override
    public long calcBaseFee(boolean withFreeProtocol) {
        return 0L;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void toJsonBody(JSONObject transaction) {
        transaction.put("timestamp", Controller.getInstance().blockChain.getTimestamp(this.height));
    }

    // CONVERT
    @Override
    public byte[] toBytesBody(int forDeal) {

        ////////// LOAD

        byte[] data;

        // HEAD SIZE & HEAD
        data = Bytes.concat(super.toBytesBody(forDeal),
                Longs.toByteArray(txReference),
                Longs.toByteArray(dbRef));

        return data;
    }

    //PROCESS/ORPHAN

    @Override
    public void processBody(Block block, int forDeal) throws TxException {
        throw new TxException("Processing denied");
    }

    @Override
    public int getDataLength(int forDeal) {

        return super.getDataLength(forDeal)
                /// LOAD
                + 2 * TIMESTAMP_LENGTH;
    }

}