package org.erachain.core.account;

import com.google.common.primitives.Bytes;
import org.erachain.core.crypto.Base32;
import org.erachain.core.crypto.Base58;
import org.erachain.core.crypto.Crypto;
import org.erachain.core.crypto.Ed25519;
import org.erachain.core.transaction.TxException;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;

import static org.erachain.core.crypto.Crypto.SIGNATURE_LENGTH;

public class PublicKeyAccount extends Account {

    static Logger LOGGER = LoggerFactory.getLogger(PublicKeyAccount.class.getName());

    public static final int PUBLIC_KEY_LENGTH = Crypto.HASH_LENGTH;

    protected byte system;
    protected byte[] publicKey;
    protected String publicKey58;

    /**
     * parse from system + pub key bytes
     *
     * @param publicKey
     */
    public PublicKeyAccount(byte[] publicKey) {
        super(publicKey[0], Arrays.copyOfRange(publicKey, 1, publicKey.length));
        this.publicKey = Arrays.copyOfRange(publicKey, 1, publicKey.length);
        this.system = publicKey[0];
    }

    public PublicKeyAccount(byte system, byte[] publicKey) {
        super(system, publicKey);
        this.publicKey = publicKey;
        this.system = system;
    }

    public PublicKeyAccount(byte[] data, int parsePos) {
        this(data[parsePos], parse(data, parsePos));
    }

    public PublicKeyAccount(byte[] data, int parsePos, byte system) {
        this(system, parse(system, data, parsePos));
    }

    public PublicKeyAccount(String publicKey) {
        this(Base58.decode(publicKey), 0);
    }

    public static byte[] parse(byte system, byte[] data, int parsePos) {

        if (system == Crypto.ED25519_SYSTEM) {
            return Arrays.copyOfRange(data, parsePos, parsePos + PUBLIC_KEY_LENGTH);
        } else if (system == Crypto.DAPP_SYSTEM) {
            return Arrays.copyOfRange(data, parsePos, parsePos + PUBLIC_KEY_LENGTH);
        }

        return null;
    }

    public static byte[] parse(byte[] data, int parsePos) {

        int type = data[parsePos++];

        if (type == Crypto.ED25519_SYSTEM) {
            return Arrays.copyOfRange(data, parsePos, parsePos + PUBLIC_KEY_LENGTH);
        } else if (type == Crypto.DAPP_SYSTEM) {
            return Arrays.copyOfRange(data, parsePos, parsePos + PUBLIC_KEY_LENGTH);
        }

        return null;
    }

    /**
     * Throw TxsException. `erc:` - as prefix for Erchi blockchain
     * acc:ADDRESS - ass account Base58
     * pub:PUB KEY = as public key Base 58
     * SEED:ACCOUNT_SEED of account pair (private and public keys) - ACCOUNT SEED - not private key! Because it is not same for ED25519 on JavaScript and Java - but SEED is SAME
     * MASTER:MASTER_SEED = master SEED of Wallet
     *
     * @param address
     */
    public static PublicKeyAccount parse(String address) throws TxException {
        String[] split = address.split(":");
        int shift = 0;
        if (split.length == 3) {
            if (!split[0].equals("erc"))
                throw new TxException("not Erchi blockchain, Use `erc:pub:PUBLIC_KEY`");
            if (!split[1].equals("pub"))
                throw new TxException("not Erchi pub key, Use `pub:PUBLIC_KEY`");
            shift = 2;
        } else if (split.length == 2) {
            if (!split[0].equals("pub"))
                throw new TxException("not Erchi pub key, Use `pub:PUBLIC_KEY`");
            shift = 1;
        }

        if (!isValidPublicKey(split[shift]))
            throw new TxException("not Erchi pub key, Use `pub:PUBLIC_KEY`");

        return new PublicKeyAccount(split[shift]);
    }

    public static PublicKeyAccount makeForDApp(byte[] publicKey) {
        return new PublicKeyAccount(Crypto.DAPP_SYSTEM, publicKey);
    }

    //CHECK IF IS VALID PUBLIC KEY and MAKE NEW
    public static boolean isValidPublicKey(byte system, byte[] publicKey) {
        if (publicKey == null) return false;

        if (system == Crypto.ED25519_SYSTEM)
            return publicKey.length == PUBLIC_KEY_LENGTH;
        if (system == Crypto.DAPP_SYSTEM)
            return publicKey.length == PUBLIC_KEY_LENGTH;

        return false;
    }

    public static boolean validLenPubKey(String publicKey) {
        return publicKey.length() >= PublicKeyAccount.PUBLIC_KEY_LENGTH
                && publicKey.length() < PublicKeyAccount.PUBLIC_KEY_LENGTH + 7 + (PublicKeyAccount.PUBLIC_KEY_LENGTH >> 1);
    }

    public static boolean isValidPublicKey(String publicKey) {

        if (publicKey == null || publicKey.isEmpty()) {
            return false;
        }

        byte[] pk = null;
        if (publicKey.startsWith("+")) {
            // BASE.32 from  BANK
            publicKey = publicKey.substring(1);
            if (Base32.isExtraSymbols(publicKey)) {
                return false;
            }
            pk = Base32.decode(publicKey, PUBLIC_KEY_LENGTH + 1);
        } else {

            String[] split = publicKey.split(":");
            int shift = 0;
            if (split.length == 3) {
                if (!split[0].equals("erc"))
                    return false;
                if (!split[1].equals("pub"))
                    return false;
                shift = 2;
            } else if (split.length == 2) {
                if (!split[0].equals("pub"))
                    return false;
                shift = 1;
            }

            publicKey = split[shift];

            if (Base58.isExtraSymbols(publicKey)) {
                return false;
            }

            pk = Base58.decode(publicKey);

        }

        byte system = pk[0];
        pk = Arrays.copyOfRange(pk, 1, pk.length);

        return isValidPublicKey(system, pk);
    }

    public byte[] parseSign(byte[] data, int parsePos) {

        if (system == Crypto.ED25519_SYSTEM) {
            return Arrays.copyOfRange(data, parsePos, parsePos + SIGNATURE_LENGTH);
        } else if (system == Crypto.DAPP_SYSTEM) {
            return Arrays.copyOfRange(data, parsePos, parsePos + SIGNATURE_LENGTH);
        }

        return null;
    }

    public int getLength() {
        if (system == Crypto.ED25519_SYSTEM)
            return PUBLIC_KEY_LENGTH + 1;
        if (system == Crypto.DAPP_SYSTEM)
            return PUBLIC_KEY_LENGTH + 1;
        else
            return 0;
    }

    public int getSignLength() {
        if (system == Crypto.ED25519_SYSTEM)
            return SIGNATURE_LENGTH;
        if (system == Crypto.DAPP_SYSTEM)
            return SIGNATURE_LENGTH;
        else
            return 0;
    }

    public byte getSystem() {
        return system;
    }

    public byte[] getPublicKey() {
        return publicKey;
    }

    /**
     * System + pubKey bytes
     *
     * @return
     */
    public byte[] getPublicKeyFull() {
        return Bytes.concat(new byte[]{system}, publicKey);
    }

    @Override
    public final JSONObject toJson() {
        JSONObject json = super.toJson();
        json.put("system", system);
        json.put("pubkey", getPublicKey58());
        return json;
    }

    @Override
    public final void toJsonPersonInfo(Map json, String keyName) {

        super.toJsonPersonInfo(json, keyName);
        json.put(keyName + "_pubkey", getPublicKey58());

    }

    public String getPublicKey58() {
        if (publicKey58 == null) {
            publicKey58 = Base58.encode(Bytes.concat(new byte[]{system}, publicKey));
        }
        return publicKey58;
    }

    public String getPublicKey58Full() {
        return "pub:" + getPublicKey58();
    }

    public String getBase32() {
        return Base32.encode(Bytes.concat(new byte[]{system}, publicKey));
    }

    public boolean isValid() {
        return isValidPublicKey(system, this.publicKey);
    }

    /**
     * forparse in transaction with Signature
     *
     * @return
     */
    public int getParsePosWithSign() {
        if (system == Crypto.ED25519_SYSTEM) {
            return SIGNATURE_LENGTH + PUBLIC_KEY_LENGTH;
        }
        return -1;
    }

    /**
     * for parsing only pub key
     *
     * @return
     */
    public int getParsePos() {
        if (system == Crypto.ED25519_SYSTEM || system == Crypto.DAPP_SYSTEM) {
            return 1 + PUBLIC_KEY_LENGTH;
        }
        return -1;
    }

    public byte[] getEmptySignature() {
        if (system == Crypto.ED25519_SYSTEM) {
            return new byte[SIGNATURE_LENGTH];
        }
        return null;

    }

    public void updateSign(byte[] txRaw, byte[] signature) {
        if (system == Crypto.ED25519_SYSTEM) {
            System.arraycopy(signature, 0, txRaw, 1 + PUBLIC_KEY_LENGTH, SIGNATURE_LENGTH);
        }
    }

    public boolean verify(byte[] signature, byte[] message) {
        try {
            if (system == Crypto.ED25519_SYSTEM) {
                //VERIFY SIGNATURE
                return Ed25519.verify(signature, message, publicKey);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return false;
    }

}
