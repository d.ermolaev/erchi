package org.erachain.core.item.statuses;

import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.transaction.TxException;
import org.json.simple.JSONObject;

public class Status extends StatusCls {

    private static final byte TYPE_ID = STATUS;

    public Status(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);
    }

    public Status(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, json, errorMessage);
    }

    public Status(PublicKeyAccount author, String name, byte[] icon, byte[] image, String description, boolean unique) {
        super(TYPE_ID, author, name, icon, image, description, unique);
    }

    //GETTERS/SETTERS

    public String getTypeClassName() {
        return "status";
    }

    public int getMinNameLen() {
        return 12;
    }

}
