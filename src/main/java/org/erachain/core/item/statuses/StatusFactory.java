package org.erachain.core.item.statuses;

import org.erachain.core.item.polls.PollCls;
import org.erachain.core.transaction.TxException;
import org.erachain.core.transaction.TxParseException;
import org.json.simple.JSONObject;

import java.util.Arrays;

import static org.erachain.core.item.ItemCls.POS_CLASS;

public class StatusFactory {

    private static StatusFactory instance;

    private StatusFactory() {

    }

    public static StatusFactory getInstance() {
        if (instance == null) {
            instance = new StatusFactory();
        }

        return instance;
    }

    public StatusCls parse(int startPos, byte[] data, boolean includeReference) throws Exception {
        //READ TYPE
        int typeClass = data[startPos + POS_CLASS];

        switch (typeClass) {
            case StatusCls.STATUS:

                //PARSE SIMPLE STATUS
                return new Status(startPos, data, includeReference);

            case StatusCls.TITLE:

                //
                //return Status.parse(data, includeReference);
        }

        throw new TxParseException("StatusCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{StatusCls.STATUS}));
    }

    public StatusCls parse(int typeClass, JSONObject json, TxException errorMessage) throws Exception {

        switch (typeClass) {
            case StatusCls.STATUS:

                //PARSE SIMPLE STATUS
                return new Status(json, errorMessage);

            case StatusCls.TITLE:
        }

        throw new TxParseException("StatusCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{StatusCls.STATUS}));

    }

}
