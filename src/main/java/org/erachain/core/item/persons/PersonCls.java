package org.erachain.core.item.persons;

import com.google.common.primitives.Bytes;
import lombok.SneakyThrows;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.item.ItemCls;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.ItemAssetBalanceMap;
import org.erachain.datachain.ItemMap;
import org.erachain.datachain.TransactionFinalMap;
import org.erachain.lang.Lang;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple2;

import java.math.BigDecimal;
import java.util.*;


//birthLatitude -90..90; birthLongitude -180..180
public abstract class PersonCls extends ItemCls {

    public static final byte TYPE_ID = ItemCls.PERSON_TYPE;

    public static final long MIN_START_KEY_OLD = 0L;
    public static final int HUMAN = 1;
    public static final int AVATAR = 2;
    public static final int UNION = 3;

    private static final int PERSON_WITH_INVITER_MASK = 1;

    private static final int MIN_IMAGE_LENGTH = 1 << 13;
    public static int MAX_IMAGE_LENGTH = 1 << 16;

    final protected PublicKeyAccount inviter;

    public PersonCls(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);

        if ((typeBytes[POS_TYPE_FLAGS_1] & PERSON_WITH_INVITER_MASK) != 0) {
            inviter = new PublicKeyAccount(data, parsedPos);
            parsedPos += inviter.getLength();
        } else {
            inviter = null;
        }

    }

    public PersonCls(byte typeClass, JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, typeClass, json, errorMessage);

        errorMessage.setMsg("invalid `item.inviter`");
        if (json.containsKey("inviter")) {
            typeBytes[POS_TYPE_FLAGS_1] |= PERSON_WITH_INVITER_MASK;
            inviter = PublicKeyAccount.parse((String) json.get("inviter"));
        } else {
            inviter = null;
        }

    }

    protected PersonCls(int personClass, int classModel, PublicKeyAccount inviter, PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                        int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                        String description, Long startDate, Long stopDate) {
        super(makeTypeBytes(TYPE_ID, personClass, classModel), author, owner, name, tags, iconType, iconAsURL, icon,
                imageType, imageAsURL, image, description, startDate, stopDate);

        if (inviter != null) {
            typeBytes[POS_TYPE_FLAGS_1] |= PERSON_WITH_INVITER_MASK;
        }
        this.inviter = inviter;

    }

    //GETTERS/SETTERS

    public static BigDecimal getBalance(long personKey, long assetKey, int pos, int side) {

        Set<String> addresses = DCSet.getInstance().getPersonAddressMap().getItems(personKey).keySet();

        ItemAssetBalanceMap map = DCSet.getInstance().getAssetBalanceMap();

        // тут переключение внутри цикла идет - так же слишком ресурсно
        BigDecimal sum = addresses.stream()
                .map((adr) -> Account.getBytesFromAddress(adr))
                .map((key) -> map.get(key, assetKey))
                .map((balances) -> {
                    switch (pos) {
                        case 1:
                            switch (side) {
                                case Account.BALANCE_SIDE_DEBIT:
                                    return balances.a.a;
                                case Account.BALANCE_SIDE_LEFT:
                                    return balances.a.b;
                                case Account.BALANCE_SIDE_CREDIT:
                                    return balances.a.a.subtract(balances.a.b);
                                default:
                                    return BigDecimal.ZERO;
                            }
                        case 2:
                            switch (side) {
                                case Account.BALANCE_SIDE_DEBIT:
                                    return balances.b.a;
                                case Account.BALANCE_SIDE_LEFT:
                                    return balances.b.b;
                                case Account.BALANCE_SIDE_CREDIT:
                                    return balances.b.a.subtract(balances.b.b);
                                default:
                                    return BigDecimal.ZERO;
                            }
                        case 3:
                            switch (side) {
                                case Account.BALANCE_SIDE_DEBIT:
                                    return balances.c.a;
                                case Account.BALANCE_SIDE_LEFT:
                                    return balances.c.b;
                                case Account.BALANCE_SIDE_CREDIT:
                                    return balances.c.a.subtract(balances.c.b);
                                default:
                                    return BigDecimal.ZERO;
                            }
                        case 4:
                            switch (side) {
                                case Account.BALANCE_SIDE_DEBIT:
                                    return balances.d.a;
                                case Account.BALANCE_SIDE_LEFT:
                                    return balances.d.b;
                                case Account.BALANCE_SIDE_CREDIT:
                                    return balances.d.a.subtract(balances.d.b);
                                default:
                                    return BigDecimal.ZERO;
                            }
                        case 5:
                            switch (side) {
                                case Account.BALANCE_SIDE_DEBIT:
                                    return balances.e.a;
                                case Account.BALANCE_SIDE_LEFT:
                                    return balances.e.b;
                                case Account.BALANCE_SIDE_CREDIT:
                                    return balances.e.a.subtract(balances.e.b);
                                default:
                                    return BigDecimal.ZERO;
                            }
                        default:
                            return BigDecimal.ZERO;
                    }
                })
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return sum;

    }

    @SneakyThrows
    public static BigDecimal getTotalBalance(DCSet dcSet, Long personKey, Long assetKey, int parsedPos) {
        TreeMap<String, Stack<Fun.Tuple3<Integer, Integer, Integer>>> addresses
                = dcSet.getPersonAddressMap().getItems(personKey);

        if (addresses.isEmpty())
            return null;

        TransactionFinalMap transactionsMap = dcSet.getTransactionFinalMap();
        BigDecimal balanceTotal = BigDecimal.ZERO;

        for (String address : addresses.keySet()) {
            Account account = new Account(address);
            Tuple2<BigDecimal, BigDecimal> balance = account.getBalanceForAction(assetKey, parsedPos);
            if (balance != null)
                balanceTotal = balanceTotal.add(balance.b);
        }

        return balanceTotal;
    }


    @Override
    public long START_KEY() {
        if (Transaction.parseHeightDBRef(dbRef) > BlockChain.START_KEY_UP)
            return BlockChain.START_KEY_UP_ITEMS;
        //return START_KEY_UP_ITEMS;

        return START_KEY_OLD;
    }

    @Override
    public long MIN_START_KEY() {
        if (Transaction.parseHeightDBRef(dbRef) > BlockChain.START_KEY_UP)
            return BlockChain.START_KEY_UP_ITEMS;
        //return START_KEY_UP_ITEMS;

        return MIN_START_KEY_OLD;
    }

    public String getItemTypeName() {
        return "person";
    }

    @Override
    public List<String> getItemFields() {

        String[] array = new String[]{
                "inviter:pubKey[Base58]=null"
        };

        // new ArrayList<> - make modified list!
        List<String> list = new ArrayList<>(Arrays.asList(array));
        list.addAll(0, super.getItemFields());
        return list;
    }

    @Override
    public int getImageMAXLength() {
        return MAX_IMAGE_LENGTH;
    }

    public int getImageMINLength() {
        return MIN_IMAGE_LENGTH;
    }

    @Override
    public HashMap getNovaItems() {
        return BlockChain.NOVA_PERSONS;
    }

    public Set<String> getPubKeys(DCSet dcSet) {
        return dcSet.getPersonAddressMap().getItems(this.getKey()).keySet();
    }

    public static PublicKeyAccount getInviter(DCSet dcSet, Long itemKey) {
        PersonCls item = (PersonCls) dcSet.getItemPersonMap().get(itemKey);
        return item.inviter;
    }

    public PublicKeyAccount getInviter() {
        return this.inviter;
    }

    // DB
    public ItemMap getDBMap(DCSet db) {
        return db.getItemPersonMap();
    }

    public static JSONArray toJsonAddresses(Long key) {

        TreeMap<String, Stack<Fun.Tuple3<Integer, Integer, Integer>>> addresses = DCSet.getInstance().getPersonAddressMap().getItems(key);
        TransactionFinalMap transactionsMap = DCSet.getInstance().getTransactionFinalMap();

        JSONArray jsonItems = new JSONArray();
        for (String address : addresses.keySet()) {

            Stack<Fun.Tuple3<Integer, Integer, Integer>> stack = addresses.get(address);
            if (stack == null || stack.isEmpty()) {
                continue;
            }

            Fun.Tuple3<Integer, Integer, Integer> item = stack.peek();
            Transaction transactionIssue = transactionsMap.get(item.b, item.c);

            Map accountJSON = new LinkedHashMap();
            accountJSON.put("address", address);
            accountJSON.put("to_date", item.a * 86400000L);
            accountJSON.put("verifier", transactionIssue.getCreator().getAddress());
            if (transactionIssue.getCreator().getPerson() != null) {
                accountJSON.put("verifier_key", transactionIssue.getCreator().getPerson().b.getKey());
                accountJSON.put("verifier_name", transactionIssue.getCreator().getPerson().b.viewName());
            } else {
                accountJSON.put("verifier_key", "");
                accountJSON.put("verifier_name", "");
            }

            jsonItems.add(accountJSON);

        }
        return jsonItems;
    }

    @Override
    public int isValid() {
        if (isIconAsURL()) {
            // нельзя делать ссылку на иконку у Персон
            errorValue = "URL type denied";
            return Transaction.INVALID_ICON_TYPE;
        }
        if (isImageAsURL()) {
            // нельзя делать ссылку на фотку у Персон
            errorValue = "URL type denied";
            return Transaction.INVALID_IMAGE_TYPE;
        }

        if (startDate == null)
            return Transaction.INVALID_ITEM_START_DATE;

        if (inviter != null) {
            if (inviter.equals(author)) {
                errorValue = "equals Author";
                return Transaction.INVALID_PERSON_INVITER;
            } else if (inviter.equals(owner)) {
                errorValue = "equals Owner";
                return Transaction.INVALID_PERSON_INVITER;
            }
        }

        return super.isValid();
    }

    public boolean isHuman() {
        return typeBytes[0] == HUMAN;
    }

    public boolean isUnion() {
        return typeBytes[0] == UNION;
    }

    public byte[] toBytes(boolean includeReference) {
        if (inviter == null)
            return super.toBytes(includeReference);

        return Bytes.concat(super.toBytes(includeReference),
                new byte[]{inviter.getSystem()}, inviter.getPublicKey());
    }

    @Override
    public int getDataLength(boolean includeReference) {
        return super.getDataLength(includeReference)
                + (inviter == null? 0 : inviter.getLength());
    }

    @Override
    public JSONObject toJson() {

        JSONObject personJSON = super.toJson();
        if (inviter != null)
            inviter.toJsonPersonInfo(personJSON, "inviter");

        return personJSON;

    }

    @Override
    public JSONObject jsonForExplorerInfo(DCSet dcSet, JSONObject langObj, boolean forPrint) {

        JSONObject itemJson = super.jsonForExplorerInfo(dcSet, langObj, forPrint);

        if (inviter != null) {
            itemJson.put("Label_Inviter", Lang.T("Inviter", langObj));
            inviter.toJsonPersonInfo(itemJson, "inviter");
        }

        return itemJson;
    }

}
