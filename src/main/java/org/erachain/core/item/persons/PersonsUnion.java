package org.erachain.core.item.persons;

import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;
import org.json.simple.JSONObject;

public class PersonsUnion extends PersonCls {

    private static final byte TYPE_ID = UNION;


    public PersonsUnion(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);
    }

    public PersonsUnion(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, json, errorMessage);
    }

    public PersonsUnion(int unionModel, PublicKeyAccount inviter, PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                        int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                        String description, Long startDate, Long stopDate) {
        super(TYPE_ID, unionModel, inviter, author, owner, name, tags, iconType, iconAsURL, icon,
                imageType, imageAsURL, image, description, startDate, stopDate);
    }

    //GETTERS/SETTERS

    public String getTypeClassName() {
        return "union";
    }

    public int getMinNameLen() {
        return 3;
    }

    @Override
    public boolean isUniqueName() {
        return true;
    }

}
