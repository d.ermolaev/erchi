package org.erachain.core.item.persons;


import com.google.common.primitives.Bytes;
import erchi.core.account.PrivateKeyAccount;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.crypto.Base58;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;
import org.erachain.datachain.DCSet;
import org.erachain.lang.Lang;
import org.erachain.utils.ByteArrayUtils;
import org.json.simple.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PersonHuman extends PersonCls {

    public static final int MAX_DESCRIPTION_LENGTH = 1 << 15;

    /**
     * set typeBytes[POS_CLASS_FLAGS_1]
     */
    private static final int PERSON_WITH_SIGN_MASK = 1;
    private static final byte TYPE_ID = HUMAN;
    public static String[] GENDERS_LIST = {"Male", "Female", "-"};

    public static final int GENDER_LENGTH = 1;
    public static final int HEIGHT_LENGTH = 1;
    protected static final int RACE_SIZE_LENGTH = 1;
    protected static final int LATITUDE_LENGTH = 4;
    protected static final int SKIN_COLOR_SIZE_LENGTH = 1;
    protected static final int EYE_COLOR_SIZE_LENGTH = 1;
    protected static final int HAIR_COLOR_SIZE_LENGTH = 1;
    protected static final int BASE_LENGTH = GENDER_LENGTH + RACE_SIZE_LENGTH + LATITUDE_LENGTH * 2
            + SKIN_COLOR_SIZE_LENGTH + EYE_COLOR_SIZE_LENGTH + HAIR_COLOR_SIZE_LENGTH
            + HEIGHT_LENGTH;

    protected byte gender; //
    protected String race;
    protected float birthLatitude;
    protected float birthLongitude;
    protected String skinColor; // First Name|Middle Name|Last Name
    protected String eyeColor; // First Name|Middle Name|Last Name
    protected String hairColor; // First Name|Middle Name|Last Name
    protected byte height;

    // for personal data author - his signature
    protected byte[] authorSignature;

    public PersonHuman(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);

        //READ GENDER
        gender = data[parsedPos];
        parsedPos++;

        //READ RACE
        int raceLength = Byte.toUnsignedInt(data[parsedPos]);
        parsedPos++;

        byte[] raceBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + raceLength);
        race = new String(raceBytes, StandardCharsets.UTF_8);
        parsedPos += raceLength;

        //READ BIRTH LATITUDE
        birthLatitude = ByteArrayUtils.ByteArray2float(Arrays.copyOfRange(data, parsedPos, parsedPos + LATITUDE_LENGTH));
        parsedPos += LATITUDE_LENGTH;

        //READ BIRTH LONGITUDE
        birthLongitude = ByteArrayUtils.ByteArray2float(Arrays.copyOfRange(data, parsedPos, parsedPos + LATITUDE_LENGTH));
        parsedPos += LATITUDE_LENGTH;

        //READ SKIN COLOR LENGTH
        int skinColorLength = Byte.toUnsignedInt(data[parsedPos]);
        parsedPos++;

        byte[] skinColorBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + skinColorLength);
        skinColor = new String(skinColorBytes, StandardCharsets.UTF_8);
        parsedPos += skinColorLength;

        //READ EYE COLOR LENGTH
        int eyeColorLength = Byte.toUnsignedInt(data[parsedPos]);
        parsedPos++;

        byte[] eyeColorBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + eyeColorLength);
        eyeColor = new String(eyeColorBytes, StandardCharsets.UTF_8);
        parsedPos += eyeColorLength;

        //READ HAIR COLOR LENGTH
        int hairСolorLength = Byte.toUnsignedInt(data[parsedPos]);
        parsedPos++;

        byte[] hairСolorBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + hairСolorLength);
        hairColor = new String(hairСolorBytes, StandardCharsets.UTF_8);
        parsedPos += hairСolorLength;

        //READ HEIGHT
        height = data[parsedPos];
        parsedPos++;

        if ((typeBytes[POS_CLASS_FLAGS_1] & PERSON_WITH_SIGN_MASK) != 0) {
            //READ SIGNATURE
            authorSignature = author.parseSign(data, parsedPos);
            parsedPos += authorSignature.length;
        } else {
            authorSignature = null;
        }

    }

    public PersonHuman(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, json, errorMessage);

        gender = (Byte) json.getOrDefault("gender", 0);
        race = (String) json.getOrDefault("race", "");
        birthLatitude = (Float) json.getOrDefault("birthLatitude", 0.0);
        birthLongitude = (Float) json.getOrDefault("birthLongitude", 0.0);

        skinColor = (String) json.getOrDefault("skinColor", "");
        eyeColor = (String) json.getOrDefault("eyeColor", "");
        hairColor = (String) json.getOrDefault("hairColor", "");
        height = (byte) (int) (long) (Long) json.getOrDefault("height", 0L);

        String authorSignatureStr = (String) json.get("authorSignature");
        if (authorSignatureStr != null) {
            typeBytes[POS_CLASS_FLAGS_1] |= PERSON_WITH_SIGN_MASK;
            this.authorSignature = Base58.decode(authorSignatureStr);
        }

    }

    public PersonHuman(PublicKeyAccount inviter, PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                       int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                       String description, Long startDate, Long stopDate,
                       byte gender, String race, float birthLatitude, float birthLongitude,
                       String skinColor, String eyeColor, String hairColor, int height,
                       byte[] authorSignature) {
        super(TYPE_ID, 0, inviter, author, owner, name, tags, iconType, iconAsURL, icon,
                imageType, imageAsURL, image, description, startDate, stopDate);
        this.gender = gender;
        this.race = race == null ? "" : race;
        this.birthLatitude = birthLatitude;
        this.birthLongitude = birthLongitude;
        this.skinColor = skinColor == null ? "" : skinColor;
        this.eyeColor = eyeColor == null ? "" : eyeColor;
        this.hairColor = hairColor == null ? "" : hairColor;
        this.height = (byte) height;

        if (authorSignature != null) {
            typeBytes[POS_CLASS_FLAGS_1] |= PERSON_WITH_SIGN_MASK;
            this.authorSignature = authorSignature;
        }

    }

    public PersonHuman(PublicKeyAccount author, String name, byte[] icon, byte[] image, String description,
                       Long birthday, Long deathday,
                       byte gender, String race, float birthLatitude, float birthLongitude,
                       String skinColor, String eyeColor, String hairColor, int height,
                       byte[] authorSignature) {
        this(author, author, author, name, null, 0, false, icon, 0, false, image, description,
                birthday, deathday,
                gender, race, birthLatitude, birthLongitude,
                skinColor, eyeColor, hairColor, height, authorSignature);
    }

    //GETTERS/SETTERS
    public byte getGender() {
        return this.gender;
    }

    public String getRace() {
        return this.race;
    }

    public float getBirthLatitude() {
        return this.birthLatitude;
    }

    public float getBirthLongitude() {
        return this.birthLongitude;
    }

    public String getSkinColor() {
        return this.skinColor;
    }

    public String getEyeColor() {
        return this.eyeColor;
    }

    public String getHairColor() {
        return this.hairColor;
    }

    public int getHumanHeight() {
        return Byte.toUnsignedInt(this.height);
    }

    @Override
    public byte[] getAuthorSignature() {
        return authorSignature;
    }

    public boolean isSigned() {
        return authorSignature != null;
    }

    public String getTypeClassName() {
        return "human";
    }

    public int getMinNameLen() {
        return 5;
    }

    @Override
    public List<String> getItemFields() {

        String[] array = new String[]{
                "gender:int", "race", "birthLatitude:float=0.0", "birthLongitude:float=0.0",
                "skinColor", "eyeColor", "hairColor", "height:byte", "authorSignature:Base58"
        };

        // new ArrayList<> - make modified list!
        List<String> list = new ArrayList<>(Arrays.asList(array));
        list.addAll(0, super.getItemFields());
        return list;
    }

    // to BYTES
    public byte[] toBytes(boolean includeReference) {

        // WRITE GENDER
        // WRITE RACE
        byte[] raceBytes = this.race.getBytes(StandardCharsets.UTF_8);
        byte[] data = Bytes.concat(super.toBytes(includeReference),
                new byte[]{gender},
                new byte[]{(byte) raceBytes.length});

        //WRITE RACE
        data = Bytes.concat(data, raceBytes);

        //WRITE BIRTH_LATITUDE
        byte[] birthLatitudeBytes = ByteArrayUtils.float2ByteArray(this.birthLatitude);
        //birthdayBytes = Bytes.ensureCapacity(birthdayBytes, LATITUDE_LENGTH, 0);
        data = Bytes.concat(data, birthLatitudeBytes);

        //WRITE BIRTH_LONGITUDE
        byte[] birthLongitudeBytes = ByteArrayUtils.float2ByteArray(this.birthLongitude);
        //birthdayBytes = Bytes.ensureCapacity(birthdayBytes, LATITUDE_LENGTH, 0);
        data = Bytes.concat(data, birthLongitudeBytes);

        //WRITE SKIN COLOR SIZE
        byte[] skinColorBytes = this.skinColor.getBytes(StandardCharsets.UTF_8);
        data = Bytes.concat(data, new byte[]{(byte) skinColorBytes.length});

        //WRITE SKIN COLOR
        data = Bytes.concat(data, skinColorBytes);

        //WRITE EYE COLOR SIZE
        byte[] eyeColorBytes = this.eyeColor.getBytes(StandardCharsets.UTF_8);
        data = Bytes.concat(data, new byte[]{(byte) eyeColorBytes.length});

        //WRITE EYE COLOR
        data = Bytes.concat(data, eyeColorBytes);

        //WRITE HAIR COLOR SIZE
        byte[] hairColorBytes = this.hairColor.getBytes(StandardCharsets.UTF_8);
        data = Bytes.concat(data, new byte[]{(byte) hairColorBytes.length});

        //WRITE HAIR COLOR
        data = Bytes.concat(data, hairColorBytes);

        //WRITE HEIGHT
        data = Bytes.concat(data, new byte[]{this.height});

        if (authorSignature != null) {
            data = Bytes.concat(data, this.authorSignature);
            // for signature
        }

        return data;
    }

    @Override
    public int getDataLength(boolean includeReference) {
        return super.getDataLength(includeReference)
                + BASE_LENGTH
                + race.getBytes(StandardCharsets.UTF_8).length
                + skinColor.getBytes(StandardCharsets.UTF_8).length
                + eyeColor.getBytes(StandardCharsets.UTF_8).length
                + hairColor.getBytes(StandardCharsets.UTF_8).length
                + (authorSignature == null ? 0 : authorSignature.length);
    }

    @Override
    public String toString(DCSet db) {
        long key = this.getKey();
        return "[" + (key < 1 ? "?" : key) + (this.typeBytes[0] == HUMAN ? "" : "U") + "]"
                + this.name // + "♥"
                ///+ DateTimeFormat.timestamptoString(birthday, "dd-MM-YY", "UTC")
                ;
    }

    @Override
    public String getShort(DCSet db) {
        long key = this.getKey();
        return "[" + (key < 1 ? "?" : key) + (this.typeBytes[0] == HUMAN ? "" : "U") + "]"
                + this.name.substring(0, Math.min(this.name.length(), 20)) //"♥"
                //+ DateTimeFormat.timestamptoString(birthday, "dd-MM-YY", "UTC")
                ;
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject toJson() {

        JSONObject personJSON = super.toJson();

        // ADD DATA
        personJSON.put("birthday", this.startDate);
        personJSON.put("deathday", this.stopDate);
        personJSON.put("gender", this.gender);
        personJSON.put("race", this.race);
        personJSON.put("birthLatitude", this.birthLatitude);
        personJSON.put("birthLongitude", this.birthLongitude);
        personJSON.put("skinColor", this.skinColor);
        personJSON.put("eyeColor", this.eyeColor);
        personJSON.put("hairColor", this.hairColor);
        personJSON.put("height", Byte.toUnsignedInt(this.height));

        if (this.authorSignature != null) {
            personJSON.put("authorSignature", Base58.encode(this.authorSignature));
        }

        return personJSON;
    }

    @Override
    public JSONObject jsonForExplorerPage(JSONObject langObj, Object[] args) {

        JSONObject json = super.jsonForExplorerPage(langObj, args);
        json.put("birthday", startDate);

        if (isImageAsURL()) {
            json.put("imageURL", getImageURL());
            json.put("imageType", getImageType());
            json.put("imageMediaType", getImageMediaType().toString());
        }

        return json;

    }

    @Override
    public JSONObject jsonForExplorerInfo(DCSet dcSet, JSONObject langObj, boolean forPrint) {

        JSONObject itemJson = super.jsonForExplorerInfo(dcSet, langObj, forPrint);
        itemJson.put("Label_TXCreator", Lang.T("Registrar", langObj));
        itemJson.put("Label_Authorship", Lang.T("Authorship", langObj));
        itemJson.put("Label_Born", Lang.T("Birthday", langObj));
        itemJson.put("Label_Bornplace", Lang.T("Birthplace", langObj));
        itemJson.put("Label_Gender", Lang.T("Gender", langObj));

        itemJson.put("birthday", viewStartDate());
        itemJson.put("birthLatitude", getBirthLatitude());
        itemJson.put("birthLongitude", getBirthLongitude());
        if (!isAlive(0L)) {
            itemJson.put("deathday", viewStopDate());
            itemJson.put("Label_dead", Lang.T("Deathday", langObj));

        }

        String gender = Lang.T("Man", langObj);
        if (getGender() == 0) {
            gender = Lang.T("Man", langObj);
        } else if (getGender() == 1) {
            gender = Lang.T("Woman", langObj);
        } else {
            gender = Lang.T("-", langObj);
        }
        itemJson.put("gender", gender);


        if (!forPrint) {
        }

        return itemJson;
    }

    //
    public void sign(PrivateKeyAccount author) {

        if (!Arrays.equals(author.getPublicKey(), this.author.getPublicKey())) {
            typeBytes[POS_CLASS_FLAGS_1] &= ~PERSON_WITH_SIGN_MASK;
            authorSignature = null;
            return;
        }

        // setup FLAG for verify signature after PARSE
        typeBytes[POS_CLASS_FLAGS_1] |= PERSON_WITH_SIGN_MASK;

        // USE SUPER CLASS DATA ONLY!
        byte[] data = super.toBytes(false);
        this.authorSignature = author.sign(data);

    }

    public boolean isSignatureValid() {

        if (this.authorSignature == null || this.authorSignature.length != author.getSignLength()
                || Arrays.equals(this.authorSignature, author.getEmptySignature()))
            return false;

        // USE SUPER CLASS DATA ONLY!
        byte[] data = super.toBytes(false);
        return author.verify(this.authorSignature, data);
    }

    @Override
    public int isValid() {

        int result = super.isValid();
        if (result != Transaction.VALIDATE_OK) {
            return result;
        }

        // FOR PERSONS need LIMIT DESCRIPTION because it may be make with 0 COMPU balance
        int descriptionLength = getDescription().getBytes(StandardCharsets.UTF_8).length;
        if (descriptionLength > MAX_DESCRIPTION_LENGTH) {
            return Transaction.INVALID_DESCRIPTION_LENGTH_MAX;
        }
        // birthLatitude -90..90; birthLongitude -180..180
        if (getBirthLatitude() > 90 || getBirthLatitude() < -90) {
            return Transaction.ITEM_PERSON_LATITUDE_ERROR;
        }
        if (getBirthLongitude() > 180 || getBirthLongitude() < -180) {
            return Transaction.ITEM_PERSON_LONGITUDE_ERROR;
        }
        if (getRace().getBytes(StandardCharsets.UTF_8).length > 255) {
            return Transaction.ITEM_PERSON_RACE_ERROR;
        }
        if (getGender() < 0 || getGender() > 2) {
            return Transaction.ITEM_PERSON_GENDER_ERROR;
        }
        if (getSkinColor().getBytes(StandardCharsets.UTF_8).length > 255) {
            return Transaction.ITEM_PERSON_SKIN_COLOR_ERROR;
        }
        if (getEyeColor().getBytes(StandardCharsets.UTF_8).length > 255) {
            return Transaction.ITEM_PERSON_EYE_COLOR_ERROR;
        }
        if (getHairColor().getBytes(StandardCharsets.UTF_8).length > 255) {
            return Transaction.ITEM_PERSON_HAIR_COLOR_ERROR;
        }

        if (getHumanHeight() > 255) {
            return Transaction.ITEM_PERSON_HEIGHT_ERROR;
        }

        if (startDate == null) {
            // IF PERSON is LIVE

            if (getImage() == null) {
                return Transaction.INVALID_IMAGE_LENGTH_MIN;
            }
            int len = getImage().length;
            if (len < getImageMINLength()) {
                errorValue = "" + len + " < " + getImageMINLength();
                return Transaction.INVALID_IMAGE_LENGTH_MIN;
            }

        } else {
            // person is DIE - any PHOTO
        }

        return Transaction.VALIDATE_OK;

    }

}
