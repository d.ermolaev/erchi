package org.erachain.core.item.persons;

import org.erachain.core.item.ItemCls;
import org.erachain.core.transaction.TxException;
import org.erachain.core.transaction.TxParseException;
import org.json.simple.JSONObject;

import java.util.Arrays;

import static org.erachain.core.item.ItemCls.POS_CLASS;

public class PersonFactory {

    private static PersonFactory instance;

    private PersonFactory() {

    }

    public static PersonFactory getInstance() {
        if (instance == null) {
            instance = new PersonFactory();
        }

        return instance;
    }

    public PersonCls parse(int startPos, byte[] data, boolean includeReference) throws Exception {
        //READ TYPE
        int typeClass = data[startPos + POS_CLASS];

        switch (typeClass) {
            case PersonCls.HUMAN:
                return new PersonHuman(startPos, data, includeReference);

            case PersonCls.AVATAR:
                return new PersonsAvatar(startPos, data, includeReference);

            case PersonCls.UNION:
                return new PersonsUnion(startPos, data, includeReference);
        }

        throw new TxParseException("UserCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{PersonCls.HUMAN, PersonCls.AVATAR //, PersonCls.UNION
                }));
    }

    public PersonCls parse(int typeClass, JSONObject json, TxException errorMessage) throws Exception {

        switch (typeClass) {
            case PersonCls.HUMAN:
                return new PersonHuman(json, errorMessage);

            case PersonCls.AVATAR:
                return new PersonsAvatar(json, errorMessage);

            case PersonCls.UNION:
                return new PersonsUnion(json, errorMessage);
        }

        throw new TxParseException("UserCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{PersonCls.HUMAN, PersonCls.AVATAR //, PersonCls.UNION
                }));

    }

}
