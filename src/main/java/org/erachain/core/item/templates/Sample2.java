package org.erachain.core.item.templates;

//import java.math.BigDecimal;

import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.transaction.TxException;
import org.json.simple.JSONObject;

public class Sample2 extends TemplateCls {

    private static final byte TYPE_ID = SAMPLE;

    public Sample2(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);
    }

    public Sample2(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, json, errorMessage);
    }

    public Sample2(PublicKeyAccount author, String name, byte[] icon, byte[] image, String description) {
        super(TYPE_ID, author, name, icon, image, description);
    }

    public Sample2(byte[] typeBytes, PublicKeyAccount author, String name, byte[] icon, byte[] image, String description) {
        super(typeBytes, author, name, icon, image, description);
    }

    //PARSE

    //GETTERS/SETTERS
    public String getTypeClassName() {
        return "sample";
    }

    public int getMinNameLen() {
        return 12;
    }

}
