package org.erachain.core.item.templates;

import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.transaction.TxException;
import org.json.simple.JSONObject;

//import org.slf4j.LoggerFactory;


public class Template extends TemplateCls {

    private static final byte TYPE_ID = PLATE;

    public Template(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);
    }

    public Template(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, json, errorMessage);
    }

    public Template(PublicKeyAccount author, String name, byte[] icon, byte[] image, String description) {
        super(TYPE_ID, author, name, icon, image, description);
    }

    //GETTERS/SETTERS
    public String getTypeClassName() {
        return "plate";
    }

    public int getMinNameLen() {
        return 12;
    }

}
