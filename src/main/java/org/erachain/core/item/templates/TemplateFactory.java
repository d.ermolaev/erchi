package org.erachain.core.item.templates;

import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.transaction.TxParseException;
import org.erachain.core.transaction.TxException;
import org.json.simple.JSONObject;

import java.util.Arrays;

import static org.erachain.core.item.ItemCls.POS_CLASS;

public class TemplateFactory {

    private static TemplateFactory instance;

    private TemplateFactory() {

    }

    public static TemplateFactory getInstance() {
        if (instance == null) {
            instance = new TemplateFactory();
        }

        return instance;
    }

    public TemplateCls parse(int startPos, byte[] data, boolean includeReference) throws Exception {
        //READ TYPE
        int typeClass = data[startPos + POS_CLASS];

        switch (typeClass) {
            case TemplateCls.PLATE:

                //PARSE SIMPLE PLATE
                return new Template(startPos, data, includeReference);

            case TemplateCls.SAMPLE:

                //
                //return Template.parse(data, includeReference);

            case TemplateCls.PAPER:

                //
                //return Template.parse(data, includeReference);
        }

        throw new TxParseException("TemplateCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{TemplateCls.PLATE}));
    }

    public TemplateCls parse(int typeClass, JSONObject json, TxException errorMessage) throws Exception {

        switch (typeClass) {
            case TemplateCls.PLATE:

                //PARSE SIMPLE PLATE
                return new Template(json, errorMessage);

            case TemplateCls.SAMPLE:
            case TemplateCls.PAPER:
        }

        throw new TxParseException("TemplateCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{TemplateCls.PLATE}));

    }

}
