package org.erachain.core.item.polls;

import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.transaction.TxException;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.VoteOnItemPollMap;
import org.erachain.utils.Pair;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONObject;
import org.mapdb.Fun;

import java.math.BigDecimal;
import java.util.*;

public class SimplePoll extends PollCls {

    private static final byte TYPE_ID = SIMPLE_VOTING;
    private static final List<String > OPTIONS = Arrays.asList("Yes", "No");
    private static final List<String > OPTIONS_VIEW = Arrays.asList("1: Yes", "2: No");

    public SimplePoll(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);
    }

    public SimplePoll(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, json, errorMessage);
    }

    public SimplePoll(PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                      int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                      String description, Long startDate, Long stopDate) {
        super(TYPE_ID, author, owner, name,
                tags, iconType, iconAsURL, icon, imageType, imageAsURL, image,
                description, startDate, stopDate);
    }


    //GETTERS/SETTERS
    public String getTypeClassName() {
        return "simple";
    }

    @Override
    public List<String> getOptions() {
        return OPTIONS;
    }

    @Override
    public int getOptionsSize() {
        return 2;
    }

    @Override
    public List<String> viewOptions() {
        return OPTIONS_VIEW;
    }

    @Override
    public BigDecimal getTotalVotes(DCSet dcSet) {
        Object[] results = dcSet.getVoteOnItemPollResultsMap().get(key);
        BigDecimal total = BigDecimal.ZERO;
        for (Object item: results) {
            total = total.add(new BigDecimal(item.toString()));
        }

        return total;
    }

    @Override
    public BigDecimal getTotalVotes(DCSet dcSet, long assetKey) {
        return getTotalVotes(dcSet);
    }

    @Override
    public Fun.Tuple4<Integer, long[], BigDecimal, BigDecimal[]> votesWithPersons(DCSet dcSet, long assetKey, int balanceparsedPos) {

        int optionsSize = getOptionsSize();
        long[] personVotes = new long[optionsSize];
        int personsTotal = 0;
        BigDecimal votesSum;
        BigDecimal[] optionVotes = new BigDecimal[optionsSize];

        Object[] results = dcSet.getVoteOnItemPollResultsMap().get(key);
        if (results == null) {
            results = new Object[optionsSize];
        }

        for (int i = 0; i < optionsSize; i++) {
            Object item = results[i];

            if (item == null) {
                personVotes[i] = 0L;
                optionVotes[i] = BigDecimal.ZERO;
                continue;
            }

            personVotes[i] = (Long) item;
            personsTotal += (Long) item;
            optionVotes[i] = new BigDecimal((Long) item);

        }

        votesSum = new BigDecimal(personsTotal);

        return new Fun.Tuple4<>(personsTotal, personVotes, votesSum, optionVotes);
    }

}
