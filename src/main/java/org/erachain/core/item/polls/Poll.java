package org.erachain.core.item.polls;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.transaction.TxException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Poll extends PollCls {

    private static final byte TYPE_ID = VARIANT_VOTING;

    protected static final int OPTIONS_SIZE_LENGTH = 4;
    protected static final int BASE_LENGTH = OPTIONS_SIZE_LENGTH;
    private final List<String> options;

    public Poll(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);

        //READ OPTIONS SIZE
        byte[] optionsLengthBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + OPTIONS_SIZE_LENGTH);
        int optionsLength = Ints.fromByteArray(optionsLengthBytes);
        parsedPos += OPTIONS_SIZE_LENGTH;

        //READ OPTIONS
        int nameLength;
        options = new ArrayList<String>();
        for (int i = 0; i < optionsLength; i++) {

            nameLength = Byte.toUnsignedInt(data[parsedPos]);
            parsedPos++;

            byte[] optionBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + nameLength);
            String option = new String(optionBytes, StandardCharsets.UTF_8);
            parsedPos += nameLength;

            options.add(option);
        }

    }

    public Poll(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, json, errorMessage);

        JSONArray optionsArray = (JSONArray) json.get("options");

        options = new ArrayList<String>();
        for (Object item : optionsArray) {
            options.add((String) item);
        }

    }

    public Poll(PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                String description, Long startDate, Long stopDate, List<String> options) {
        super(TYPE_ID, author, owner, name,
                tags, iconType, iconAsURL, icon, imageType, imageAsURL, image,
                description, startDate, stopDate);
        this.options = options;

    }


    //GETTERS/SETTERS
    public String getTypeClassName() {
        return "poll";
    }

    @Override
    public List<String> getOptions() {
        return this.options;
    }

    @Override
    public int getOptionsSize() {
        return options.size();
    }

    @Override
    public List<String> viewOptions() {
        List<String> result = new ArrayList<>();
        int count = 0;
        for (String option : this.options) {
            result.add(++count + ": " + option);
        }
        return result;
    }

    public byte[] toBytes(boolean includeReference) {

        byte[] data = super.toBytes(includeReference);

        //WRITE OPTIONS SIZE
        byte[] optionsLengthBytes = Ints.toByteArray(getOptionsSize());
        data = Bytes.concat(data, optionsLengthBytes);

        //WRITE OPTIONS
        for (String option : getOptions()) {

            //WRITE NAME SIZE
            byte[] optionBytes = option.getBytes(StandardCharsets.UTF_8);
            data = Bytes.concat(data, new byte[]{(byte) optionBytes.length});

            //WRITE NAME
            data = Bytes.concat(data, optionBytes);
        }

        return data;
    }


    public int getDataLength(boolean includeReference) {
        int length = super.getDataLength(includeReference) + BASE_LENGTH;

        for (String option : getOptions()) {
            length += 1 + option.getBytes(StandardCharsets.UTF_8).length;
        }

        return length;

    }

}
