package org.erachain.core.item.polls;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import org.erachain.core.BlockChain;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.item.ItemCls;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.ItemMap;
import org.erachain.datachain.VoteOnItemPollMap;
import org.erachain.lang.Lang;
import org.erachain.utils.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple3;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.*;

public abstract class PollCls extends ItemCls {

    public static final byte TYPE_ID = ItemCls.POLL_TYPE;

    public static final int SIMPLE_VOTING = 1;
    public static final int VARIANT_VOTING = 2;

    public static final int INITIAL_FAVORITES = 0;

    public PollCls(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);
    }

    public PollCls(byte typeClass, JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, typeClass, json, errorMessage);
    }

    public PollCls(int tClass, PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                   int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                   String description, Long startDate, Long stopDate) {
        super(makeTypeBytes(TYPE_ID, tClass, 0), author, owner, name,
                tags, iconType, iconAsURL, icon, imageType, imageAsURL, image,
                description, startDate, stopDate);
    }

    //GETTERS/SETTERS

    @Override
    public long START_KEY() {
        if (Transaction.parseHeightDBRef(dbRef) > BlockChain.START_KEY_UP)
            return BlockChain.START_KEY_UP_ITEMS;

        return START_KEY_OLD;
    }

    @Override
    public long MIN_START_KEY() {
        if (Transaction.parseHeightDBRef(dbRef) > BlockChain.START_KEY_UP)
            return BlockChain.START_KEY_UP_ITEMS;

        return MIN_START_KEY_OLD;
    }

    public String getItemTypeName() {
        return "poll";
    }

    public int getMinNameLen() {
        return 12;
    }

    abstract public List<String> getOptions();

    abstract public int getOptionsSize();

    public List<String> getItemFields() {

        String[] array = new String[]{
                "options:[<String>]"
        };

        // new ArrayList<> - make modified list!
        List<String> list = new ArrayList<>(Arrays.asList(array));
        list.addAll(0, super.getItemFields());
        return list;
    }

    abstract public List<String> viewOptions();

    @Override
    public ItemMap getDBMap(DCSet dc) {
        return dc.getItemPollMap();
    }

    public boolean hasVotes(DCSet dc) {
        return dc.getVoteOnItemPollMap().hasVotes(this.key);
    }

    public BigDecimal getTotalVotes(DCSet dcSet) {
        return getTotalVotes(dcSet, AssetCls.FEE_KEY);
    }

    public BigDecimal getTotalVotes(DCSet dcSet, long assetKey) {
        BigDecimal votesSum = BigDecimal.ZERO;
        VoteOnItemPollMap map = dcSet.getVoteOnItemPollMap();
        NavigableSet<Tuple3> optionVoteKeys;
        Account voter;

        optionVoteKeys = map.getVotes(this.key);
        for (Tuple3<Long, Integer, byte[]> key : optionVoteKeys) {
            voter = new Account(key.c);
            votesSum = votesSum.add(voter.getBalanceUSE(assetKey));
        }

        return votesSum;
    }

    public BigDecimal getTotalVotes(DCSet dcSet, long assetKey, int option) {
        BigDecimal votesSum = BigDecimal.ZERO;
        VoteOnItemPollMap map = dcSet.getVoteOnItemPollMap();
        NavigableSet<Tuple3> optionVoteKeys;
        Account voter;

        optionVoteKeys = map.getVotes(this.key);
        for (Tuple3<Long, Integer, byte[]> key : optionVoteKeys) {
            if (option != key.b)
                continue;

            voter = new Account(key.c);
            votesSum = votesSum.add(voter.getBalanceUSE(assetKey));
        }

        return votesSum;
    }


    public List<Pair<Account, Integer>> getVotes(DCSet dcSet) {
        List<Pair<Account, Integer>> votes = new ArrayList<Pair<Account, Integer>>();

        VoteOnItemPollMap map = dcSet.getVoteOnItemPollMap();
        NavigableSet<Tuple3> optionVoteKeys;
        Pair<Account, Integer> vote;
        Account voter;

        optionVoteKeys = map.getVotes(this.key);
        for (Tuple3<Long, Integer, byte[]> key : optionVoteKeys) {
            voter = new Account(key.c);
            vote = new Pair<Account, Integer>(voter, key.b);
            votes.add(vote);
        }

        return votes;
    }

    /**
     * список всех персон голосующих
     *
     * @param dcSet
     * @return
     */
    public List<Pair<Account, Integer>> getPersonVotes(DCSet dcSet) {
        List<Pair<Account, Integer>> votes = new ArrayList<Pair<Account, Integer>>();

        VoteOnItemPollMap map = dcSet.getVoteOnItemPollMap();
        NavigableSet<Tuple3> optionVoteKeys;
        Pair<Account, Integer> vote;
        Account voter;

        optionVoteKeys = map.getVotes(this.key);
        for (Tuple3<Long, Integer, byte[]> key : optionVoteKeys) {
            voter = new Account(key.c);
            if (voter.isPersonAlive(dcSet, 0)) {
                vote = new Pair<Account, Integer>(voter, key.b);
                votes.add(vote);
            }
        }

        return votes;
    }

    /**
     * тут ошибка так как при переголосвании не учитывается повторное голосование - используй votesWithPersons
     *
     * @param dcSet
     * @return
     */
    public List<Long> getPersonCountVotes(DCSet dcSet) {


        List<Long> votes = new ArrayList<>(getOptionsSize());
        for (int i = 0; i < getOptionsSize(); i++) {
            votes.add(0L);
        }

        VoteOnItemPollMap map = dcSet.getVoteOnItemPollMap();
        NavigableSet<Tuple3> optionVoteKeys;
        Pair<Account, Integer> vote;
        Account voter;

        optionVoteKeys = map.getVotes(this.key);
        for (Tuple3<Long, Integer, byte[]> key : optionVoteKeys) {
            voter = new Account(key.c);
            Integer optionNo = key.b;
            if (voter.isPersonAlive(dcSet, 0)) {
                Long count = votes.get(optionNo - 1);
                votes.add(optionNo - 1, count + 1L);
            }
        }

        return votes;
    }

    /**
     * Можно задавать как номер актива так и позицию баланса. Если позиция = 0 то берем Имею + Долг
     *
     * @param dcSet
     * @param assetKey
     * @param balanceparsedPos
     * @return
     */
    public Fun.Tuple4<Integer, long[], BigDecimal, BigDecimal[]> votesWithPersons(DCSet dcSet, long assetKey, int balanceparsedPos) {

        int optionsSize = getOptionsSize();
        long[] personVotes = new long[optionsSize];
        int personsTotal = 0;

        BigDecimal[] optionVotes = new BigDecimal[optionsSize];
        for (int i = 0; i < optionVotes.length; i++) {
            optionVotes[i] = BigDecimal.ZERO;
        }

        BigDecimal votesSum = BigDecimal.ZERO;

        Set personsVotedSet = new HashSet<Long>();
        Iterable<Pair<Account, Integer>> votes = getVotes(dcSet);
        Iterator iterator = votes.iterator();
        while (iterator.hasNext()) {

            Pair<Account, Integer> item = (Pair<Account, Integer>) iterator.next();

            int option = item.getB();

            Account voter = item.getA();
            Fun.Tuple4<Long, Integer, Integer, Integer> personInfo = voter.getPersonDuration(dcSet);

            // запретим голосовать много раз разными счетами одной персоне
            if (personInfo != null
                    && !personsVotedSet.contains(personInfo.a)) {
                personVotes[option - 1]++;
                personsTotal++;

                // запомним что он голосовал
                personsVotedSet.add(personInfo.a);
            }

            BigDecimal votesVol;
            if (balanceparsedPos > 0) {
                votesVol = voter.getBalanceForAction(dcSet, assetKey, balanceparsedPos).b;
            } else {
                votesVol = voter.getBalanceUSE(assetKey, dcSet);
            }

            optionVotes[option - 1] = optionVotes[option - 1].add(votesVol);
            votesSum = votesSum.add(votesVol);

        }

        return new Fun.Tuple4<>(personsTotal, personVotes, votesSum, optionVotes);
    }

    public long getPersonCountTotalVotes(DCSet dcSet) {
        long votes = 0L;

        VoteOnItemPollMap map = dcSet.getVoteOnItemPollMap();
        NavigableSet<Tuple3> optionVoteKeys;
        Pair<Account, Integer> vote;
        Account voter;

        optionVoteKeys = map.getVotes(this.key);
        for (Tuple3<Long, Integer, byte[]> key : optionVoteKeys) {
            voter = new Account(key.c);
            if (voter.isPersonAlive(dcSet, 0)) {
                ++votes;
            }
        }

        return votes;
    }

    public List<Pair<Account, Integer>> getVotes(DCSet dcSet, List<Account> accounts) {
        List<Pair<Account, Integer>> votes = new ArrayList<Pair<Account, Integer>>();

        VoteOnItemPollMap map = dcSet.getVoteOnItemPollMap();
        NavigableSet<Tuple3> optionVoteKeys; // <Long, Integer, byte[]>
        Pair<Account, Integer> vote;
        Account voter;

        optionVoteKeys = map.getVotes(this.key);
        for (Tuple3<Long, Integer, byte[]> key : optionVoteKeys) {
            for (Account account : accounts) {
                if (account.equals(key.c)) {
                    vote = new Pair<Account, Integer>(account, key.b);
                    votes.add(vote);
                }
            }
        }

        return votes;
    }

    public int getOption(String option) {

        int i = 0;
        for (String pollOption : getOptions()) {
            if (pollOption.equals(option)) {
                return i;
            }

            i++;
        }

        return -1;
    }

    public String viewOption(int option) {
        return option + ": " + getOptions().get(option - 1);
    }

    //OTHER

    @SuppressWarnings("unchecked")
    public JSONObject toJson() {

        JSONObject pollJSON = super.toJson();

        JSONArray jsonOptions = new JSONArray();
        for (String option : getOptions()) {
            jsonOptions.add(option);
        }

        pollJSON.put("options", jsonOptions);
        pollJSON.put("totalVotes", getTotalVotes(DCSet.getInstance()).toPlainString());

        return pollJSON;
    }

    public JSONObject jsonForExplorerPage(JSONObject langObj, Object[] args) {

        JSONObject json = super.jsonForExplorerPage(langObj, args);

        json.put("optionsCount", getOptionsSize());
        json.put("totalVotes", getTotalVotes(DCSet.getInstance()).toPlainString());

        return json;
    }

    public JSONObject jsonForExplorerInfo(DCSet dcSet, JSONObject langObj, boolean forPrint) {

        JSONObject itemJson = super.jsonForExplorerInfo(dcSet, langObj, forPrint);
        itemJson.put("Label_Poll", Lang.T("Poll", langObj));


        if (!forPrint) {
        }

        return itemJson;
    }

}
