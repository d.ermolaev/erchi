package org.erachain.core.item.polls;

import org.erachain.core.item.templates.TemplateCls;
import org.erachain.core.transaction.TxException;
import org.erachain.core.transaction.TxParseException;
import org.json.simple.JSONObject;

import java.util.Arrays;

import static org.erachain.core.item.ItemCls.POS_CLASS;

public class PollFactory {

    private static PollFactory instance;

    private PollFactory() {

    }

    public static PollFactory getInstance() {
        if (instance == null) {
            instance = new PollFactory();
        }

        return instance;
    }

    public PollCls parse(int startPos, byte[] data, boolean includeReference) throws Exception {
        //READ TYPE
        int typeClass = data[startPos + POS_CLASS];

        switch (typeClass) {
            case PollCls.SIMPLE_VOTING:
                //PARSE SIMPLE POLL
                return new SimplePoll(startPos, data, includeReference);
            case PollCls.VARIANT_VOTING:
                //PARSE VARIANT VOTING
                return new Poll(startPos, data, includeReference);

        }

        throw new TxParseException("PollCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{PollCls.SIMPLE_VOTING, PollCls.VARIANT_VOTING}));

    }

    public PollCls parse(int typeClass, JSONObject json, TxException errorMessage) throws Exception {

        switch (typeClass) {
            case PollCls.SIMPLE_VOTING:
                return new SimplePoll(json, errorMessage);
            case PollCls.VARIANT_VOTING:
                return new Poll(json, errorMessage);

        }

        throw new TxParseException("PollCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{PollCls.SIMPLE_VOTING, PollCls.VARIANT_VOTING}));

    }

}
