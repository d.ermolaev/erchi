package org.erachain.core.item.statements;

import org.erachain.core.account.PublicKeyAccount;

public class Note extends StatementCls {

    private static final int TYPE_ID = NOTE;

    public Note(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);
    }

    public Note(PublicKeyAccount author, String name, byte[] icon, byte[] image, String description) {
        super(TYPE_ID, author, name, icon, image, description);
    }

    public Note(byte[] typeBytes, PublicKeyAccount author, String name, byte[] icon, byte[] image, String description) {
        super(typeBytes, author, name, icon, image, description);
    }

    //GETTERS/SETTERS

    public String getTypeClassName() {
        return "note";
    }

    public int getMinNameLen() {
        return 12;
    }

}
