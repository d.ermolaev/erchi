package org.erachain.core.item.statements;

import static org.erachain.core.item.ItemCls.POS_CLASS;

public class StatementFactory {

    private static StatementFactory instance;

    private StatementFactory() {

    }

    public static StatementFactory getInstance() {
        if (instance == null) {
            instance = new StatementFactory();
        }

        return instance;
    }

    public StatementCls parse(int startPos, byte[] data, boolean includeReference) throws Exception {
        //READ TYPE
        int type = data[startPos + POS_CLASS];

        switch (type) {
            case StatementCls.NOTE:

                //PARSE SIMPLE STATUS
                return new Note(startPos, data, includeReference);

            //case StatementCls.TITLE:

            //
            //return Status.parse(data, includeReference);
        }

        throw new Exception("Invalid Statement type: " + type);
    }

}
