package org.erachain.core.item;

import org.erachain.core.item.assets.AssetFactory;
import org.erachain.core.item.persons.PersonFactory;
import org.erachain.core.item.polls.PollFactory;
import org.erachain.core.item.statuses.StatusFactory;
import org.erachain.core.item.templates.TemplateFactory;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;
import org.erachain.core.transaction.TxParseException;
import org.json.simple.JSONObject;

import java.util.Arrays;

public class ItemFactory {

    private static ItemFactory instance;

    private ItemFactory() {

    }

    public static ItemFactory getInstance() {
        if (instance == null) {
            instance = new ItemFactory();
        }

        return instance;
    }

    public ItemCls parse(int itemType, int startPos, byte[] data, boolean includeReference) throws Exception {

        switch (itemType) {
            case ItemCls.ASSET_TYPE:
                return AssetFactory.getInstance().parse(startPos, data, includeReference);
            case ItemCls.TEMPLATE_TYPE:
                return TemplateFactory.getInstance().parse(startPos, data, includeReference);
            case ItemCls.PERSON_TYPE:
            case ItemCls.AUTHOR_TYPE:
                return PersonFactory.getInstance().parse(startPos, data, includeReference);
            case ItemCls.POLL_TYPE:
                return PollFactory.getInstance().parse(startPos, data, includeReference);
            case ItemCls.STATUS_TYPE:
                return StatusFactory.getInstance().parse(startPos, data, includeReference);
        }

        throw new TxParseException("Invalid value: " + itemType,
                "itemType",
                "Use that values: " + Arrays.asList(ItemCls.getItemTypes()));

    }

    public ItemCls parse(int itemType, JSONObject json, TxException errorMessage) throws Exception {

        if (!json.containsKey("typeClass"))
            throw new TxParseException("Empty value", "typeClass", null);

        errorMessage.setMsg("invalid `item.typeClass`");
        int typeClass = (int) (long) (Long) json.get("typeClass");

        switch (itemType) {
            case ItemCls.PERSON_TYPE:
            case ItemCls.AUTHOR_TYPE:
                return PersonFactory.getInstance().parse(typeClass, json, errorMessage);
            case ItemCls.ASSET_TYPE:
                return AssetFactory.getInstance().parse(typeClass, json, errorMessage);
            case ItemCls.TEMPLATE_TYPE:
                return TemplateFactory.getInstance().parse(typeClass, json, errorMessage);
            case ItemCls.POLL_TYPE:
                return PollFactory.getInstance().parse(typeClass, json, errorMessage);
            case ItemCls.STATUS_TYPE:
                return StatusFactory.getInstance().parse(typeClass, json, errorMessage);
        }

        throw new TxParseException("Invalid value: " + itemType,
                "itemType",
                "Use that values: " + Arrays.asList(ItemCls.getItemTypes()));

    }

}
