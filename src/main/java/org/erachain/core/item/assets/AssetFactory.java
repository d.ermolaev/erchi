package org.erachain.core.item.assets;

import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.transaction.TxException;
import org.erachain.core.transaction.TxParseException;
import org.json.simple.JSONObject;

import java.util.Arrays;

import static org.erachain.core.item.ItemCls.POS_CLASS;

public class AssetFactory {

    private static AssetFactory instance;

    private AssetFactory() {

    }

    public static AssetFactory getInstance() {
        if (instance == null) {
            instance = new AssetFactory();
        }

        return instance;
    }

    public AssetCls parse(int startPos, byte[] data, boolean includeReference) throws Exception {
        //READ TYPE
        int typeClass = data[startPos + POS_CLASS];

        switch (typeClass) {
            case AssetCls.VENTURE:
                return new AssetVenture(startPos, data, includeReference);

            case AssetCls.UNIQUE:
                return new AssetUnique(startPos, data, includeReference);

            case AssetCls.UNIQUE_COPY:
                return new AssetUniqueSeriesCopy(startPos, data, includeReference);

            case AssetCls.NAME:
                //return RegisterNameTransaction.Parse(data, includeReference);

        }

        throw new TxParseException("AssetCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{AssetCls.VENTURE, AssetCls.UNIQUE, AssetCls.UNIQUE_COPY}));
    }

    public AssetCls parse(int typeClass, JSONObject json, TxException errorMessage) throws Exception {
        //READ TYPE

        switch (typeClass) {
            case AssetCls.VENTURE:
                return new AssetVenture(json, errorMessage);

            case AssetCls.UNIQUE:
                return new AssetUnique(json, errorMessage);

            case AssetCls.UNIQUE_COPY:
                return new AssetUniqueSeriesCopy(json, errorMessage);

            case AssetCls.NAME:
                //return RegisterNameTransaction.Parse(data, includeReference);

        }

        throw new TxParseException("AssetCls. Invalid value: " + typeClass, "typeClass",
                "Use that values: " + Arrays.asList(new Integer[]{AssetCls.VENTURE, AssetCls.UNIQUE, AssetCls.UNIQUE_COPY}));

    }

}
