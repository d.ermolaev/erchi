package org.erachain.core.item.assets;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.exdata.exLink.ExLinkAddress;
import org.erachain.core.transaction.TxException;
import org.erachain.datachain.DCSet;
import org.erachain.lang.Lang;
import org.json.simple.JSONObject;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple5;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//import java.math.BigDecimal;
//import com.google.common.primitives.Bytes;

public class AssetVenture extends AssetCls {

    protected static final int SCALE_LENGTH = 1;
    protected static final int QUANTITY_LENGTH = 8;

    private static final byte TYPE_ID = VENTURE;

    /**
     * 0 - unlimited for owner.
     * If < 0 - all but not owner may sold it on exchange for owner - as Auction
     */
    protected long quantity;
    /**
     * +8 ... -23 = 32 диапазон. положительные - округляют целые числа
     */
    protected int scale;

    public AssetVenture(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);

        //READ QUANTITY
        byte[] quantityBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + QUANTITY_LENGTH);
        quantity = Longs.fromByteArray(quantityBytes);
        parsedPos += QUANTITY_LENGTH;

        //READ SCALE
        byte[] scaleBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + SCALE_LENGTH);
        scale = scaleBytes[0];
        parsedPos += SCALE_LENGTH;

    }

    public AssetVenture(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, json, errorMessage);

        //READ QUANTITY
        errorMessage.setMsg("invalid `quantity`");
        quantity = (Long) json.getOrDefault("quantity", 0L);

        //READ SCALE
        errorMessage.setMsg("invalid `scale`");
        scale = (byte)(long)(Long) json.getOrDefault("scale", 0L);

    }

    public AssetVenture(int assetModel, PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                        int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                        String description, Long startDate, Long stopDate,
                        ExLinkAddress[] dexAwards, boolean isUnTransferable, boolean isAnonimDenied,
                        int scale, long quantity) {
        super(TYPE_ID, assetModel, author, owner, name, tags, iconType, iconAsURL, icon,
                imageType, imageAsURL, image, description, startDate, stopDate,
                dexAwards, isUnTransferable, isAnonimDenied);
        this.quantity = quantity;
        this.scale = (byte) scale;
    }

    public AssetVenture(int assetModel, PublicKeyAccount author, String name,
                        byte[] icon, byte[] image, String description, int scale, long quantity) {
        this(assetModel, author, author, name, null, 0, false, icon,
                0, false, image, description, null, null,
                null, false, false, scale, quantity);
    }

    //GETTERS/SETTERS
    @Override
    public String getTypeClassName() {
        return "common";
    }

    /**
     * 0 - unlimited for owner.
     * If < 0 - all but not owner may sold it on exchange for owner - as Auction.
     */
    @Override
    public long getQuantity() {
        return this.quantity;
    }

    public int getScale() {
        return this.scale;
    }

    @Override
    public List<String> getItemFields() {

        String[] array = new String[]{
                "quantity:long=0", "scale:byte=0"
        };

        // new ArrayList<> - make modified list!
        List<String> list = new ArrayList<>(Arrays.asList(array));
        list.addAll(0, super.getItemFields());
        return list;
    }

    @Override
    public BigDecimal getReleased(DCSet dcSet) {
        if (quantity > 0) {
            Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>>
                    bals = this.getOwner().getBalance(this.getKey());
            return new BigDecimal(quantity).subtract(bals.a.b).stripTrailingZeros();
        } else {
            Tuple5<Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>, Tuple2<BigDecimal, BigDecimal>>
                    bals = this.getOwner().getBalance(this.getKey());
            return bals.a.b.negate().stripTrailingZeros();
        }
    }

    @Override
    public BigDecimal getReleased() {
        return getReleased(DCSet.getInstance());
    }

    /**
     * Без ограничений - только если это счетная единица или сам владелец без ограничений
     *
     * @param address
     * @param notAccounting
     * @return
     */
    @Override
    public boolean isUnlimited(Account address, boolean notAccounting) {
        return !notAccounting && isAccounting() || getQuantity() == 0L && author.equals(address);
    }

    @Override
    public boolean isUnique() {
        return quantity == 1L;
    }

    @Override
    public byte[] toBytes(boolean includeReference) {
        return Bytes.concat(super.toBytes(includeReference),
                Longs.toByteArray(this.quantity),
                new byte[]{(byte) scale});
    }

    public String makeHTMLView() {

        String text = super.makeHTMLHeadView();
        text += Lang.T("Quantity") + ":&nbsp;" + getQuantity() + ", "
                + Lang.T("Scale") + ":&nbsp;" + getScale() + "<br>";
        text += super.makeHTMLFootView(true);

        return text;

    }

    @Override
    public int getDataLength(boolean includeReference) {
        int len = super.getDataLength(includeReference);
        return len + SCALE_LENGTH + QUANTITY_LENGTH;
    }

}
