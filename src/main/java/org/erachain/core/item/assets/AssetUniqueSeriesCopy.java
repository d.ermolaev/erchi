package org.erachain.core.item.assets;

//import java.math.BigDecimal;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;
import org.erachain.controller.Controller;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.exdata.exLink.ExLinkAddress;
import org.erachain.core.transaction.IssueAssetSeriesTransaction;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;
import org.erachain.datachain.DCSet;
import org.erachain.lang.Lang;
import org.erachain.webAPI.PreviewMaker;
import org.json.simple.JSONObject;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * копия - в ней нет данных при парсинге - наполняется только после loadExtData()
 */
public class AssetUniqueSeriesCopy extends AssetUnique {

    protected static final int BASE_LENGTH = Long.BYTES + 2 * Short.BYTES;
    private static final int TYPE_ID = UNIQUE_COPY;
    private final long origKey;
    private final int total;
    private final int index;
    AssetCls original;

    public AssetUniqueSeriesCopy(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);

        //READ ORIGINAL ASSET KEY
        byte[] origKeyBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + Long.BYTES);
        origKey = Longs.fromByteArray(origKeyBytes);
        parsedPos += Long.BYTES;

        //READ TOTAL
        byte[] totalBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + Short.BYTES);
        total = Shorts.fromByteArray(totalBytes);
        parsedPos += Short.BYTES;

        //READ INDEX
        byte[] indexBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + Short.BYTES);
        index = Shorts.fromByteArray(indexBytes);
        parsedPos += Short.BYTES;

    }

    public AssetUniqueSeriesCopy(JSONObject json, TxException errorMessage) throws Exception {
        super(json, errorMessage);

        //READ ORIGINAL ASSET KEY
        origKey = (Long) json.get("origKey");

        //READ TOTAL
        total = (Integer) json.get("total");

        //READ INDEX
        index = (Short) json.get("index");

    }

    public AssetUniqueSeriesCopy(PublicKeyAccount author, String name, String tags,
                                 int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                                 String description, Long startDate, Long stopDate,
                                 ExLinkAddress[] dexAwards, boolean isUnTransferable, boolean isAnonimDenied,
                                 long origKey, int total, int index) {
        super(UNIQUE_COPY, AssetCls.AS_NON_FUNGIBLE, author, author, name, tags, iconType, iconAsURL, icon,
                imageType, imageAsURL, image, description, startDate, stopDate,
                dexAwards, isUnTransferable, isAnonimDenied);
        typeBytes[POS_CLASS] = TYPE_ID;

        this.origKey = origKey;
        this.total = total;
        this.index = index;

    }

    /**
     * Первая копия - содержит обертку полностью, все остальные только номера/ Оригинал тут вообще не используем.
     *
     * @param scin
     * @param origKey
     * @param total
     * @param index
     * @return
     */
    public static AssetUniqueSeriesCopy makeCopy(Transaction issueTX, AssetCls scin, long origKey, int total, int index) {
        String copyName = scin.getName() + " #" + index + "/" + total;

        AssetUniqueSeriesCopy copy = null;

        return copy;
    }

    @Override
    public int hashCode() {
        return (reference == null ? 0 : Ints.fromByteArray(reference)) + index;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AssetUniqueSeriesCopy) {
            AssetUniqueSeriesCopy item = (AssetUniqueSeriesCopy) obj;
            if (this.reference != null && item.reference != null)
                if (Arrays.equals(this.reference, item.reference))
                    return item.index == index;
        }
        return false;
    }

    // GETTERS/SETTERS
    @Override
    public String getTypeClassName() {
        return "series";
    }

    public long getOrigKey() {
        return origKey;
    }

    @Override
    public List<String> getItemFields() {

        String[] array = new String[]{
                "origKey:long", "total:int", "index:short"
        };

        // new ArrayList<> - make modified list!
        List<String> list = new ArrayList<>(Arrays.asList(array));
        list.addAll(0, super.getItemFields());
        return list;
    }

    public AssetCls getOriginal(DCSet dcSet) {
        if (origKey == 0) return null;

        if (original == null) {
            original = dcSet.getItemAssetMap().get(origKey);
        }
        return original;
    }

    @Override
    public ImageIcon getImageIcon() {
        if (index == 1)
            return super.getImageIcon();

        AssetCls firstAsset = DCSet.getInstance().getItemAssetMap().get(key - index + 1);
        return firstAsset.getImageIcon();

    }

    //@Override
    public byte[] getIcon1() {
        if (icon != null)
            return icon;

        if (origKey == 0) return null;

        getOriginal(DCSet.getInstance());

        return original.getIcon();

    }

    //@Override
    public byte[] getImage1() {
        if (origKey == 0) return null;

        if (image != null)
            return image;

        getOriginal(DCSet.getInstance());

        return original.getImage();

    }

    public boolean hasOriginal() {
        return origKey > 0;
    }

    public int getTotal() {
        return total;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public byte[] toBytes(boolean includeReference) {

        byte[] parentData = super.toBytes(includeReference);

        byte[] data = new byte[BASE_LENGTH];

        System.arraycopy(Longs.toByteArray(origKey), 0, data, 0, Long.BYTES);
        System.arraycopy(Shorts.toByteArray((short) total), 0, data, 8, Short.BYTES);
        System.arraycopy(Shorts.toByteArray((short) index), 0, data, 10, Short.BYTES);

        return Bytes.concat(parentData, data);
    }

    public int getDataLength(boolean includeReference) {
        return super.getDataLength(includeReference) + BASE_LENGTH;
    }

    //OTHER

    public JSONObject toJson() {

        // тут же referenceTx определяется
        JSONObject assetJSON = super.toJson();
        assetJSON.put("index", index);
        assetJSON.put("total", total);

        if (origKey > 0) {
            // ADD DATA of ORIGINAL
            JSONObject originalJson = new JSONObject();
            originalJson.put("key", origKey);

            IssueAssetSeriesTransaction issueTX = (IssueAssetSeriesTransaction) referenceTx;
            if (issueTX.getDCSet() == null) {
                issueTX.setDC(Controller.getInstance().getDCSet(), false);
            }
            AssetCls original = issueTX.getOrigAsset();


            String iconURL = original.getIconURL();
            if (iconURL != null) {
                originalJson.put("iconURL", iconURL);
                originalJson.put("iconType", original.getIconType());
                //originalJson.put("iconTypeName", original.getIconTypeName());
                originalJson.put("iconMediaType", original.getIconMediaType().toString());
            }

            String imageURL = original.getImageURL();
            if (imageURL != null) {
                originalJson.put("imageURL", imageURL);
                originalJson.put("imageType", original.getImageType());
                //originalJson.put("imageTypeName", original.getImageTypeName());
                originalJson.put("imageMediaType", original.getImageMediaType().toString());
                originalJson.put("imagePreviewMediaType", PreviewMaker.getPreviewType(original).toString());
            }

            assetJSON.put("original", originalJson);
        }

        return assetJSON;

    }

    @Override
    public JSONObject jsonForExplorerInfo(DCSet dcSet, JSONObject langObj, boolean forPrint) {
        JSONObject itemJson = super.jsonForExplorerInfo(dcSet, langObj, forPrint);
        itemJson.put("Label_Original_Asset", Lang.T("Original Asset", langObj));
        itemJson.put("Label_Series", Lang.T("Series", langObj));

        return itemJson;
    }

    public String makeHTMLView() {

        String text = super.makeHTMLHeadView();
        if (origKey > 0) {
            text += Lang.T("Original Asset") + ":&nbsp;" + origKey + "<br>";
        }
        text += Lang.T("Series") + ":&nbsp;" + total + ", "
                + Lang.T("Index") + ":&nbsp;" + index + "<br>";
        text += super.makeHTMLFootView(true);

        return text;

    }

}
