package org.erachain.core.item.assets;

import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.exdata.exLink.ExLinkAddress;
import org.erachain.core.transaction.TxException;
import org.erachain.dapp.epoch.DogePlanet;
import org.erachain.datachain.DCSet;
import org.erachain.webAPI.WebResource;
import org.json.simple.JSONObject;
import org.mapdb.Fun;

import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

public class AssetUnique extends AssetCls {

    private static final byte TYPE_ID = UNIQUE;

    public AssetUnique(int startPos, byte[] data, boolean includeReference) throws Exception {
        super(startPos, data, includeReference);
    }

    public AssetUnique(JSONObject json, TxException errorMessage) throws Exception {
        super(TYPE_ID, json, errorMessage);
    }

    /**
     * for sub Class AssetUniqueSeriesCopy
     *
     * @param type       - UNIQUE_COPY
     * @param assetModel
     */
    public AssetUnique(int type, int assetModel, PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                       int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                       String description, Long startDate, Long stopDate,
                       ExLinkAddress[] dexAwards, boolean isUnTransferable, boolean isAnonimDenied) {
        super(type, assetModel, author, owner, name, tags, iconType, iconAsURL, icon,
                imageType, imageAsURL, image, description, startDate, stopDate,
                dexAwards, isUnTransferable, isAnonimDenied);
    }

    public AssetUnique(int assetModel, PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                       int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                       String description, Long startDate, Long stopDate,
                       ExLinkAddress[] dexAwards, boolean isUnTransferable, boolean isAnonimDenied) {
        super(TYPE_ID, assetModel, author, owner, name, tags, iconType, iconAsURL, icon,
                imageType, imageAsURL, image, description, startDate, stopDate,
                dexAwards, isUnTransferable, isAnonimDenied);
    }

    protected AssetUnique(PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                          int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                          String description, Long startDate, Long stopDate,
                          ExLinkAddress[] dexAwards, boolean isUnTransferable, boolean isAnonimDenied) {
        super(TYPE_ID, AssetCls.AS_NON_FUNGIBLE, author, owner, name, tags, iconType, iconAsURL, icon,
                imageType, imageAsURL, image, description, startDate, stopDate,
                dexAwards, isUnTransferable, isAnonimDenied);
    }

    public AssetUnique(int assetModel, PublicKeyAccount author, String name, byte[] icon, byte[] image, String description) {
        this(assetModel, author, author, name, null, 0, false, icon, 0, false, image,
                description, null, null, null, false, false);
    }

    //GETTERS/SETTERS

    @Override
    public String getImageURL() {
        if (author.equals(DogePlanet.MAKER))
            return DogePlanet.getImageURL(this);

        return super.getImageURL();

    }

    @Override
    public MediaType getImageMediaType() {
        if (author.equals(DogePlanet.MAKER))
            return WebResource.TYPE_ARRAY;

        return super.getImageMediaType();
    }

    @Override
    public String getTypeClassName() {
        return "unique";
    }

    @Override
    public long getQuantity() {
        return 1L;
    }

    @Override
    public boolean isUnique() {
        return true;
    }

    @Override
    public int getScale() {
        return 0;
    }

    @Override
    public boolean isUnlimited(Account address, boolean notAccounting) {
        return false;
    }

    @Override
    public BigDecimal getReleased(DCSet dcSet) {
        Fun.Tuple5<Fun.Tuple2<BigDecimal, BigDecimal>, Fun.Tuple2<BigDecimal, BigDecimal>, Fun.Tuple2<BigDecimal, BigDecimal>, Fun.Tuple2<BigDecimal, BigDecimal>, Fun.Tuple2<BigDecimal, BigDecimal>>
                bals = this.getOwner().getBalance(this.getKey());
        return BigDecimal.ONE.subtract(bals.a.b).stripTrailingZeros();
    }

    @Override
    public BigDecimal getReleased() {
        return getReleased(DCSet.getInstance());
    }

    //OTHER

}
