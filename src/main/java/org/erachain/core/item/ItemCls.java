package org.erachain.core.item;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;
import org.erachain.controller.Controller;
import org.erachain.core.BlockChain;
import org.erachain.core.Jsonable;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.crypto.Base58;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.transaction.RSetStatusToItem;
import org.erachain.core.transaction.Transaction;
import org.erachain.core.transaction.TxException;
import org.erachain.core.transaction.TxParseException;
import org.erachain.web.scanner.ExplorerJsonLine;
import org.erachain.web.scanner.WebTransactionsHTML;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.ItemMap;
import org.erachain.dbs.IteratorCloseable;
import org.erachain.gui.Iconable;
import org.erachain.gui.library.Library;
import org.erachain.lang.Lang;
import org.erachain.settings.Settings;
import org.erachain.utils.DateTimeFormat;
import org.erachain.utils.Pair;
import org.erachain.webAPI.PreviewMaker;
import org.erachain.webAPI.WebResource;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple6;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.*;

public abstract class ItemCls implements Iconable, ExplorerJsonLine, Jsonable {

    public final static long START_KEY_OLD = 1L << 14;
    public static final long MIN_START_KEY_OLD = 1000L;

    // ITEM TYPES
    public static final int PERSON_TYPE = 1;
    public static final int ASSET_TYPE = 2;
    public static final int STATUS_TYPE = 3;
    public static final int TEMPLATE_TYPE = 4;
    public static final int POLL_TYPE = 5;
    public static final int STATEMENT_TYPE = 6;
    public static final int AUTHOR_TYPE = 21;

    public static final int MEDIA_TYPE_IMG = 0;
    public static final int MEDIA_TYPE_VIDEO = 1;
    public static final int MEDIA_TYPE_AUDIO = 2;
    public static final int MEDIA_TYPE_FRAME = 10; // POST
    //public static final int MIN_NAME_LENGTH = 10;
    public static final int MAX_NAME_LENGTH = Transaction.MAX_TITLE_BYTES_LENGTH;
    public static final int MAX_IMAGE_LENGTH = BlockChain.MAIN_MODE ? 1500000 : Transaction.MAX_DATA_BYTES_LENGTH >> 1;

    public static final int POS_COMM_VERS = 0;
    public static final int POS_COMM_FLAGS_1 = 1;
    public static final int POS_TYPE = 2;
    public static final int POS_TYPE_FLAGS_1 = 3;
    public static final int POS_CLASS = 4;
    public static final int POS_CLASS_FLAGS_1 = 5;
    public static final int POS_MODEL = 6;
    public static final int POS_MODEL_FLAGS_1 = 7;
    /**
     * 0 - common version. Другая версия может иметь другую структуру тут, с числом флагов другим например.
     * 1 - common flags. Флаги общие для всех
     * 2 - item type (asset, status, person etc.)
     * 3 - type flags. Флаги для типов общие
     * 4 - class (assetUnique, assetVenture, ...)
     * 5 - class flags. Флаги для классов общие
     * 6 - model - модель поведения сущности, например движимый или недвижимый и т.д. По-сути это под-под-вид
     * Модель поведения можно выделить в новый класс - если там довольно сложная логика
     * 7 - model flags. Флаги для модели
     */
    protected static final int TYPE_LENGTH = 8;
    protected static final int NAME_SIZE_LENGTH = 1;
    protected static final int ICON_SIZE_LENGTH = 2;
    public static final int MAX_ICON_LENGTH = (int) Math.pow(256, ICON_SIZE_LENGTH) - 1;
    protected static final int IMAGE_SIZE_LENGTH = 4;
    protected static final int APP_DATA_LENGTH = Integer.BYTES;
    protected static final int DESCRIPTION_SIZE_LENGTH = 4;
    protected static final int REFERENCE_LENGTH = Transaction.SIGNATURE_LENGTH;
    protected static final int DBREF_LENGTH = Transaction.DBREF_LENGTH;
    protected static final int BASE_LENGTH = TYPE_LENGTH
            + 2 * NAME_SIZE_LENGTH + 1 + ICON_SIZE_LENGTH + 1 + IMAGE_SIZE_LENGTH + DESCRIPTION_SIZE_LENGTH;
    protected static final int TIMESTAMP_LENGTH = Transaction.TIMESTAMP_LENGTH;
    /**
     * добавочные флаги по знаку из Размер Картинки
     */

    //protected static final int APP_DATA_MASK = 1;
    /**
     * Флаг который показывает что есть еще данные вычисленные для бзы данных
     */
    protected static final int DB_DATA_MASK = 1 << 30;
    // typeBytes[POS_COMM_FLAGS_1]
    protected static final byte START_MASK = (byte) 1;
    protected static final byte STOP_MASK = (byte) 2;
    protected static final byte ITEM_HAS_URL_MASK = (byte) -128;

    static Logger LOGGER = LoggerFactory.getLogger(ItemCls.class.getName());

    final protected byte[] typeBytes;
    /**
     * Это тип сущности. Person, Asset? Status etc
     */
    final protected int type;
    /**
     * Это Класс типа - по сути под-тип. Например, VENTURE или UNIQUE для активов. Или Human Union Avatar для персон
     */
    final protected int typeClass;
    /**
     * ЭтО Модель Класса типа - по сути под-тип под-типа = под-под-тип. Например, движимое-недвижимое имущество у активов
     */
    final protected int classModel;

    /**
     * Need Public Key - for chech signature on data
     */
    final protected PublicKeyAccount author;
    protected PublicKeyAccount owner;

    /**
     * 0-1 - байты переключателей для включения обработчиков супер-класса и суб-классов
     * Если супер класс включился - он берет 8 байт на свои флаги и 2 байта на свойства Иконки И картинки
     */
    //final protected byte[] appData;
    //protected long flags;
    final protected String name;
    /**
     * first tad - TICKER for Exchange or NikName
     */
    final protected String tags;
    final protected int iconType;
    final protected boolean iconAsURL;
    final protected byte[] icon;
    final protected int imageType;
    final protected boolean imageAsURL;
    final protected byte[] image;
    final protected String description;
    final protected Long startDate;
    final protected Long stopDate;
    public Transaction referenceTx = null;
    public String errorValue;

    // DB VALUES
    protected int parsedPos;
    /**
     * this is signature of issued record
     */
    protected byte[] reference = null;
    protected long key = 0;
    protected long dbRef;

    public ItemCls(int startPos, byte[] data, boolean includeReference) throws Exception {

        parsedPos = startPos;

        // READ TYPEs and FLAGS
        typeBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + TYPE_LENGTH);
        parsedPos += TYPE_LENGTH;

        type = typeBytes[POS_TYPE];
        typeClass = typeBytes[POS_CLASS];
        classModel = typeBytes[POS_MODEL];

        //READ BYTES
        byte systemAuthor = data[parsedPos++];
        byte systemOwner = data[parsedPos++];
        int lenName = Byte.toUnsignedInt(data[parsedPos++]);
        int lenTags = Byte.toUnsignedInt(data[parsedPos++]);
        int iconType = data[parsedPos++];
        int imageType = data[parsedPos++];

        ///////// DATA

        author = new PublicKeyAccount(data, parsedPos, systemAuthor);
        parsedPos += author.getLength() - 1;

        owner = new PublicKeyAccount(data, parsedPos, systemOwner);
        parsedPos += owner.getLength() - 1;

        //READ NAME
        name = new String(Arrays.copyOfRange(data, parsedPos, parsedPos + lenName), StandardCharsets.UTF_8);
        parsedPos += lenName;

        //READ TAGS
        tags = new String(Arrays.copyOfRange(data, parsedPos, parsedPos + lenTags), StandardCharsets.UTF_8);
        parsedPos += lenTags;

        //READ ICON
        if (iconType < 0) {
            iconAsURL = true;
            iconType &= ~ITEM_HAS_URL_MASK;
        } else {
            iconAsURL = false;
        }
        this.iconType = iconType;

        byte[] iconLengthBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + ICON_SIZE_LENGTH);
        int iconLength = (iconLengthBytes[0] & 0xFF) << 8 | (iconLengthBytes[1] & 0xFF);
        parsedPos += ICON_SIZE_LENGTH;

        // !!! Проверяем по максимуму протокола - по супер классу ItemCls. Локальные ограничения в isValid тут
        if (iconLength < 0 || iconLength > ItemCls.MAX_ICON_LENGTH) {
            throw new TxParseException("Invalid icon length - " + iconLength + " on parse in pos: " + parsedPos, "iconLengthBytes", null);
        }

        icon = Arrays.copyOfRange(data, parsedPos, parsedPos + iconLength);
        parsedPos += iconLength;

        //READ IMAGE
        if (imageType < 0) {
            imageAsURL = true;
            imageType &= ~ITEM_HAS_URL_MASK;
        } else {
            imageAsURL = false;
        }
        this.imageType = imageType;

        byte[] imageLengthBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + IMAGE_SIZE_LENGTH);
        int imageLength = Ints.fromByteArray(imageLengthBytes);
        parsedPos += IMAGE_SIZE_LENGTH;

        // !!! Проверяем по максимуму протокола - по супер классу ItemCls. Локальные ограничения в isValid тут
        if (imageLength < 0 || imageLength > ItemCls.MAX_IMAGE_LENGTH) {
            throw new TxParseException("Invalid image length " + imageLength + " on parse in pos: " + parsedPos, "imageLengthBytes", null);
        }

        image = Arrays.copyOfRange(data, parsedPos, parsedPos + imageLength);
        parsedPos += imageLength;

        //READ DESCRIPTION
        byte[] descriptionLengthBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + DESCRIPTION_SIZE_LENGTH);
        int descriptionLength = Ints.fromByteArray(descriptionLengthBytes);
        parsedPos += DESCRIPTION_SIZE_LENGTH;

        if (descriptionLength < 0 || descriptionLength > BlockChain.MAX_REC_DATA_BYTES) {
            throw new TxParseException("Invalid description length" + descriptionLength + " on parse in pos: " + parsedPos, "descriptionLengthBytes", null);
        }

        byte[] descriptionBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + descriptionLength);
        description = new String(descriptionBytes, StandardCharsets.UTF_8);
        parsedPos += descriptionLength;

        if ((typeBytes[POS_COMM_FLAGS_1] & START_MASK) != 0) {
            startDate = Longs.fromByteArray(Arrays.copyOfRange(data, parsedPos, parsedPos + Long.BYTES));
            parsedPos += Long.BYTES;
        } else
            startDate = null;

        if ((typeBytes[POS_COMM_FLAGS_1] & STOP_MASK) != 0) {
            stopDate = Longs.fromByteArray(Arrays.copyOfRange(data, parsedPos, parsedPos + Long.BYTES));
            parsedPos += Long.BYTES;
        } else
            stopDate = null;

        if (includeReference) {
            //READ REFERENCE
            reference = Arrays.copyOfRange(data, parsedPos, parsedPos + REFERENCE_LENGTH);
            parsedPos += REFERENCE_LENGTH;

            //READ SEQNO
            byte[] dbRefBytes = Arrays.copyOfRange(data, parsedPos, parsedPos + DBREF_LENGTH);
            dbRef = Longs.fromByteArray(dbRefBytes);
            parsedPos += DBREF_LENGTH;
        }

    }

    public ItemCls(byte itemType, byte typeClass, JSONObject json, TxException errorMessage) throws Exception {

        type = itemType;
        this.typeClass = typeClass;
        errorMessage.setMsg("invalid `item.classModel`");
        classModel = (byte) (int) (long) (Long) json.getOrDefault("classModel", 0L);
        typeBytes = makeTypeBytes(type, this.typeClass, classModel);

        errorMessage.setMsg("invalid `item.commVer`");
        typeBytes[POS_COMM_VERS] = (byte) (int) (long) (Long) json.getOrDefault("commVer", 0L);

        // !! FLAGS make only INSIDE!! errorMessage.setMsg("invalid `item.commFlags1`");
        //                typeBytes[POS_COMM_FLAGS_1] = (byte)(int)(long)(Long) json.getOrDefault("commFlags1", 0L);

        // !! FLAGS make only INSIDE!! errorMessage.setMsg("invalid `item.typeFlags1`");
        //                 typeBytes[POS_TYPE_FLAGS_1] = (byte)(int)(long)(Long) json.getOrDefault("typeFlags1", 0L);

        // !! FLAGS make only INSIDE!! errorMessage.setMsg("invalid `item.classFlags1`");
        //                  typeBytes[POS_CLASS_FLAGS_1] = (byte)(int)(long)(Long) json.getOrDefault("classFlags1", 0L);

        // !! FLAGS make only INSIDE!! errorMessage.setMsg("invalid `item.modelFlags1`");
        //                 typeBytes[POS_MODEL_FLAGS_1] = (byte)(int)(long)(Long) json.getOrDefault("modelFlags1", 0L);

        errorMessage.setMsg("invalid `item.name`. Used fields: " + getItemFields());
        name = (String) json.get("name");
        if (name == null || name.isEmpty())
            throw new TxException("Null");

        errorMessage.setMsg("invalid `item.tags`");
        tags = json.containsKey("tags") ? json.get("tags").toString().trim().toLowerCase() : "";

        PublicKeyAccount authorTmp = null;
        PublicKeyAccount ownerTmp = null;

        errorMessage.setMsg("invalid `item.author`");
        if (json.containsKey("author"))
            authorTmp = PublicKeyAccount.parse((String) json.get("author"));

        errorMessage.setMsg("invalid `item.owner`");
        if (json.containsKey("owner"))
            ownerTmp = PublicKeyAccount.parse((String) json.get("owner"));

        if (authorTmp == null)
            if (ownerTmp == null) authorTmp = errorMessage.getTx().getCreator();
            else authorTmp = ownerTmp;

        if (ownerTmp == null)
            ownerTmp = authorTmp;

        author = authorTmp;
        owner = ownerTmp;

        ////////////////// ICON
        errorMessage.setMsg("invalid `item.iconType`");
        byte iconTypeInt = (byte)(int)(long)(Long) json.getOrDefault("iconType", 0L);
        errorMessage.setMsg("invalid `item.iconURL`");
        String iconURL = (String) json.get("iconURL");
        if (iconURL == null) {
            errorMessage.setMsg("invalid `item.iconData`");
            String iconStr = (String) json.get("iconData");
            if (iconStr == null)
                icon = new byte[0];
            else {
                errorMessage.setMsg("invalid `item.iconData` as Base64");
                icon = Base64.getDecoder().decode(iconStr);
            }

            iconAsURL = false;
            iconType = iconTypeInt;

        } else {
            errorMessage.setMsg("invalid `item.iconURL` for UTF-8");
            icon = iconURL.getBytes(StandardCharsets.UTF_8);
            iconType = iconTypeInt & ~ITEM_HAS_URL_MASK;
            iconAsURL = true;
        }

        int iconLength = icon.length;
        // !!! Проверяем по максимуму протокола - по супер классу ItemCls. Локальные ограничения в isValid тут
        if (iconLength < 0 || iconLength > ItemCls.MAX_ICON_LENGTH) {
            throw new Exception("Invalid icon length - " + iconLength);
        }

        /////////////////// IMAGE
        errorMessage.setMsg("invalid `item.imageType`");
        byte imageTypeInt = (byte)(int)(long)(Long) json.getOrDefault("imageType", 0L);
        errorMessage.setMsg("invalid `item.imageURL`");
        String imageURL = (String) json.get("imageURL");
        if (imageURL == null) {
            errorMessage.setMsg("invalid `item.imageData`");
            String imageStr = (String) json.get("imageData");
            if (imageStr == null)
                image = new byte[0];
            else {
                errorMessage.setMsg("invalid `item.imageData` as Base64");
                image = Base64.getDecoder().decode(imageStr);
            }

            imageAsURL = false;
            imageType = imageTypeInt;

        } else {
            errorMessage.setMsg("invalid `item.imageURL` for UTF-8");
            image = imageURL.getBytes(StandardCharsets.UTF_8);
            imageType = imageTypeInt & ~ITEM_HAS_URL_MASK;
            imageAsURL = true;
        }

        int imageLength = image.length;

        // !!! Проверяем по максимуму протокола - по супер классу ItemCls. Локальные ограничения в isValid тут
        if (imageLength < 0 || imageLength > ItemCls.MAX_IMAGE_LENGTH) {
            throw new Exception("Invalid image length - " + imageLength);
        }


        errorMessage.setMsg("invalid `item.description`");
        description = (String) json.getOrDefault("description", "");
        byte[] descriptionBytes = description.getBytes(StandardCharsets.UTF_8);
        int descriptionLength = descriptionBytes.length;
        if (descriptionLength < 0 || descriptionLength > BlockChain.MAX_REC_DATA_BYTES) {
            throw new Exception("Invalid description length" + descriptionLength);
        }

        errorMessage.setMsg("invalid `item.startDate`");
        startDate = (Long) json.get("startDate");
        errorMessage.setMsg("invalid `item.stopDate`");
        stopDate = (Long) json.get("stopDate");

    }

    public ItemCls(byte[] typeBytes, PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                   int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                   String description, Long startDate, Long stopDate) {
        this.typeBytes = typeBytes;
        type = typeBytes[POS_TYPE];
        typeClass = typeBytes[POS_CLASS];
        classModel = typeBytes[POS_MODEL];

        if (owner == null && author != null)
            owner = author;
        else if (author == null && owner != null)
            author = owner;

        assert(author != null);
        assert(owner != null);

        this.author = author;
        this.owner = owner;



        this.name = name.trim();
        this.tags = tags == null ? "" : tags.trim().toLowerCase();

        this.iconType = iconType;
        this.iconAsURL = iconAsURL;
        this.icon = icon == null ? new byte[0] : icon;

        this.imageType = imageType;
        this.imageAsURL = imageAsURL;
        this.image = image == null ? new byte[0] : image;

        this.description = description == null ? "" : description;

        this.startDate = startDate;
        this.stopDate = stopDate;
    }

    public ItemCls(byte[] typeBytes, PublicKeyAccount author, PublicKeyAccount owner, String name, String tags,
                   int iconType, boolean iconAsURL, byte[] icon, int imageType, boolean imageAsURL, byte[] image,
                   String description, String startDate, String stopDate) {

        this.typeBytes = typeBytes;
        type = typeBytes[POS_TYPE];
        typeClass = typeBytes[POS_CLASS];
        classModel = typeBytes[POS_MODEL];

        this.author = author;
        this.owner = owner == null ? author : owner;

        this.name = name.trim();
        this.tags = tags == null ? "" : tags.trim().toLowerCase();

        this.iconType = iconType;
        this.iconAsURL = iconAsURL;
        this.icon = icon == null ? new byte[0] : icon;

        this.imageType = imageType;
        this.imageAsURL = imageAsURL;
        this.image = image == null ? new byte[0] : image;

        this.description = description == null ? "" : description;

        if (startDate.length() < 11) startDate += " 00:01:01";
        this.startDate = Timestamp.valueOf(startDate).getTime();

        if (stopDate != null && stopDate.length() < 11) stopDate += " 00:01:01";
        this.stopDate = stopDate == null ? Long.MIN_VALUE : Timestamp.valueOf(stopDate).getTime();

    }

    public ItemCls(byte[] typeBytes, PublicKeyAccount author, String name,
                   byte[] icon, byte[] image, String description) {
        this.typeBytes = typeBytes;
        type = typeBytes[POS_TYPE];
        typeClass = typeBytes[POS_CLASS];
        classModel = typeBytes[POS_MODEL];

        this.author = (owner = author);
        this.name = name.trim();
        this.tags = "";

        this.iconType = 0;
        this.iconAsURL = false;
        this.icon = icon == null ? new byte[0] : icon;

        this.imageType = 0;
        this.imageAsURL = false;
        this.image = image == null ? new byte[0] : image;

        this.description = description == null ? "" : description;

        this.startDate = null;
        this.stopDate = null;
    }

    /**
     * Создает стандартные байты типа для текущей версии протокола - для GUI и нового создания сущностей.
     * Если новая версия структуры typeBytes появляется - тут переделать надо
     *
     * @param type
     * @param subType
     * @param model
     * @return
     */
    public static byte[] makeTypeBytes(int type, int subType, int model) {
        return new byte[]{0, 0, (byte) type, 0, (byte) subType, 0, (byte) model, 0};
    }

    public static Pair<Integer, Long> resolveDateFromStr(String str, Long defaultVol) {
        if (str.length() == 0) return new Pair<Integer, Long>(0, defaultVol);
        else if (str.length() == 1) {
            if (str == "+")
                return new Pair<Integer, Long>(0, Long.MAX_VALUE);
            else if (str == "-")
                return new Pair<Integer, Long>(0, Long.MIN_VALUE);
            else
                return new Pair<Integer, Long>(0, defaultVol);
        } else {
            try {
                Long date = Long.parseLong(str);
                return new Pair<Integer, Long>(0, date);
            } catch (Exception e) {
                return new Pair<Integer, Long>(-1, 0L);
            }
        }
    }

    public static Pair<Integer, Integer> resolveEndDayFromStr(String str, Integer defaultVol) {
        if (str.length() == 0) return new Pair<Integer, Integer>(0, defaultVol);
        else if (str.length() == 1) {
            if (str == "+")
                return new Pair<Integer, Integer>(0, Integer.MAX_VALUE);
            else if (str == "-")
                return new Pair<Integer, Integer>(0, Integer.MIN_VALUE);
            else
                return new Pair<Integer, Integer>(0, defaultVol);
        } else {
            try {
                Integer date = Integer.parseInt(str);
                return new Pair<Integer, Integer>(0, date);
            } catch (Exception e) {
                return new Pair<Integer, Integer>(-1, 0);
            }
        }
    }

    public static ItemCls getItem(DCSet db, int type, long key) {
        return db.getItem_Map(type).get(key);
    }

    public static Integer[] getItemTypes() {

        Integer[] list = new Integer[]{
                PERSON_TYPE,
                ASSET_TYPE,
                STATUS_TYPE,
                TEMPLATE_TYPE,
                POLL_TYPE,

        };

        return list;
    }

    public boolean isUniqueName() {
        return false;
    }

    public List<String> getItemFields() {

        String[] list = new String[]{
                "classModel:int", "commVer:int",
                "author:pubKey[Base58]=(owner or TX creator)",
                "owner:pubKey[Base58]=(author or TX creator)",
                "name",
                "tags", "iconType:byte", "iconURL", "iconData:Base64", "imageType:byte", "imageURL", "imageData:Base64",
                "description", "startDate:Long=null", "stopDate:Long=null"
        };

        return Arrays.asList(list);
    }

    public static long getStartKey(int itemType, long startKey, long minStartKey) {
        if (!BlockChain.CLONE_MODE)
            return minStartKey;

        long startKeyUser = BlockChain.startKeys[itemType];

        if (startKeyUser == 0) {
            return startKey;
        } else if (startKeyUser < minStartKey) {
            return (BlockChain.startKeys[itemType] = minStartKey);
        }
        return startKeyUser;
    }

    public static Long timeToLong(String date) {
        if (date == null)
            return null;

        if (date.length() < 11)
            date += " 00:01:01";

        return Timestamp.valueOf(date).getTime();
    }

    public static String viewMediaType(int mediaType) {
        switch (mediaType) {
            case MEDIA_TYPE_IMG:
                return "img";
            case MEDIA_TYPE_VIDEO:
                return "video";
            case MEDIA_TYPE_AUDIO:
                return "audio";
            case MEDIA_TYPE_FRAME:
                return "frame";
            default:
                return "unknown";
        }
    }

    public static MediaType getMediaType(int mediaType, byte[] media) {
        if (mediaType == ItemCls.MEDIA_TYPE_IMG) {
            if (media != null && media.length > 20) {
                byte[] header = new byte[20];
                System.arraycopy(media, 0, header, 0, 20);
                String typeName = new String(header).trim();
                if (typeName.contains("PNG")) {
                    return WebResource.TYPE_PNH;
                } else if (typeName.contains("GIF")) {
                    return WebResource.TYPE_GIF;
                } else {
                    return WebResource.TYPE_JPEG;
                }
            } else {
                return WebResource.TYPE_IMAGE;
            }

        } else if (mediaType == ItemCls.MEDIA_TYPE_VIDEO) {
            return WebResource.TYPE_VIDEO;
        } else if (mediaType == ItemCls.MEDIA_TYPE_AUDIO) {
            return WebResource.TYPE_AUDIO;
        }
        return null;
    }

    /**
     * При поиске будет в нижний регистр перевернуто. Поэтому тут нельзя использовать маленькие буквы/
     * TT - тип трнзакции - используется в Transaction.tags
     *
     * @param itemType
     * @return
     */
    public static String getItemTypeAndKey(int itemType) {
        switch (itemType) {
            case ItemCls.ASSET_TYPE:
                return "A";
            //case ItemCls.IMPRINT_TYPE:
            //    return "I";
            case ItemCls.PERSON_TYPE:
                return "P";
            case ItemCls.POLL_TYPE:
                return "V"; // Vote
            //case ItemCls.UNION_TYPE:
            //    return "U";
            case ItemCls.STATEMENT_TYPE:
                return "N"; // NOTE
            case ItemCls.STATUS_TYPE:
                return "S";
            case ItemCls.TEMPLATE_TYPE:
                return "T"; // TEMPLATE
            case ItemCls.AUTHOR_TYPE:
                return "PA"; // for quick search
            default:
                return "x";

        }
    }

    public static String getItemTypeAndKey(int itemType, Object itemKey) {
        return "@" + getItemTypeAndKey(itemType) + itemKey.toString();
    }

    public static String getItemTypeAndTag(int itemType, Object tag) {
        return "@" + getItemTypeAndKey(itemType) + tag.toString();
    }

    public static String getItemTypeName(int itemType) {
        switch (itemType) {
            case ItemCls.ASSET_TYPE:
                return "ASSET";
            //case ItemCls.IMPRINT_TYPE:
            //    return "IMPRINT";
            case ItemCls.PERSON_TYPE:
            case ItemCls.AUTHOR_TYPE:
                return "PERSON";
            case ItemCls.POLL_TYPE:
                return "POLL"; // Opinion
            //case ItemCls.UNION_TYPE:
            //    return "UNION";
            case ItemCls.STATEMENT_TYPE:
                return "STATEMENT"; // TeXT
            case ItemCls.STATUS_TYPE:
                return "STATUS";
            case ItemCls.TEMPLATE_TYPE:
                return "TEMPLATE"; // TeMPLATE
            default:
                return null;

        }
    }

    public byte[] getAuthorSignature() {
        return null;
    }

    public static int getItemTypeByName(String itemTypeName) {
        String type = itemTypeName.toLowerCase();

        if (type.startsWith("asset")) {
            return ItemCls.ASSET_TYPE;
            //} else if (type.startsWith("imprint")) {
            //    return ItemCls.IMPRINT_TYPE;
        } else if (type.startsWith("person") || type.startsWith("author")) {
            return ItemCls.PERSON_TYPE;
        } else if (type.startsWith("poll")) {
            return ItemCls.POLL_TYPE;
        } else if (type.startsWith("statement")) {
            return ItemCls.STATEMENT_TYPE;
        } else if (type.startsWith("status")) {
            return ItemCls.STATUS_TYPE;
        } else if (type.startsWith("template")) {
            return ItemCls.TEMPLATE_TYPE;
            //} else if (type.startsWith("union")) {
            //    return ItemCls.UNION_TYPE;
        }

        return -1;

    }

    public static void makeJsonLitePage(DCSet dcSet, int itemType, long start, int pageSize,
                                        Map output, boolean showPerson, boolean descending) {

        ItemMap map = dcSet.getItem_Map(itemType);
        ItemCls element;
        long size = map.getLastKey();

        if (start < 1 || start > size && size > 0) {
            start = size;
        }
        output.put("start", start);
        output.put("pageSize", pageSize);
        output.put("listSize", size);

        JSONArray array = new JSONArray();

        long key = 0;
        try (IteratorCloseable<Long> iterator = map.getIterator(start, descending)) {
            while (iterator.hasNext() && pageSize-- > 0) {
                key = iterator.next();
                element = map.get(key);
                if (element != null) {
                    array.add(element.toJsonLite());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        output.put("pageItems", array);
        output.put("lastKey", key);

    }

    public byte[] toBytes(boolean includeReference) {

        byte[] nameBytes = this.name.getBytes(StandardCharsets.UTF_8);
        byte[] tagsBytes = this.tags.getBytes(StandardCharsets.UTF_8);

        byte[] iconLengthBytes = Shorts.toByteArray((short) this.icon.length);
        byte iconType = (byte) this.iconType;
        if (iconAsURL)
            iconType |= ITEM_HAS_URL_MASK;

        byte[] imageLengthBytes = Ints.toByteArray(this.image.length);
        byte imageType = (byte) this.imageType;
        if (imageAsURL)
            imageType |= ITEM_HAS_URL_MASK;

        byte[] startDateBytes;
        if (startDate != null) {
            typeBytes[POS_COMM_FLAGS_1] |= START_MASK;
            startDateBytes = Longs.toByteArray(startDate);
        } else
            startDateBytes = new byte[0];

        byte[] stopDateBytes;
        if (stopDate != null) {
            typeBytes[POS_COMM_FLAGS_1] |= STOP_MASK;
            stopDateBytes = Longs.toByteArray(stopDate);
        } else
            stopDateBytes = new byte[0];

        byte[] descriptionBytes = this.description.getBytes(StandardCharsets.UTF_8);
        int descriptionLength = descriptionBytes.length;
        byte[] descriptionLengthBytes = Ints.toByteArray(descriptionLength);

        byte[] data = Bytes.concat(typeBytes,
                new byte[]{author.getSystem(), owner.getSystem(),
                        (byte) nameBytes.length, (byte) tagsBytes.length, iconType, imageType},
                author.getPublicKey(), owner.getPublicKey(),
                nameBytes, tagsBytes,
                iconLengthBytes,
                icon,
                imageLengthBytes,
                image,
                descriptionLengthBytes,
                descriptionBytes,
                startDateBytes,
                stopDateBytes
        );


        if (includeReference) {
            //WRITE REFERENCE and refDB
            data = Bytes.concat(data, this.reference, Longs.toByteArray(this.dbRef));
        }

        return data;
    }

    public int getDataLength(boolean includeReference) {
        return BASE_LENGTH
                + author.getLength() + owner.getLength()
                + name.getBytes(StandardCharsets.UTF_8).length
                + tags.getBytes(StandardCharsets.UTF_8).length
                + icon.length
                + image.length
                + description.getBytes(StandardCharsets.UTF_8).length
                + (startDate == null ? 0 : Long.BYTES)
                + (stopDate == null ? 0 : Long.BYTES)
                + (includeReference ? REFERENCE_LENGTH + DBREF_LENGTH : 0);
    }

    public boolean isAlive(Long onThisTime) {

        if (stopDate == null
                || onThisTime == null
                || startDate == null
                || startDate < onThisTime && stopDate > onThisTime)
            return true;

        return this.stopDate > onThisTime;

    }

    /**
     * load fields after get or delete in DB
     * У некоторых типов сущностей (актив.уникальный.серия AssetUniqueSeries) нужно подгружать данные из базы.
     * Так же при создании вторичных ключей надо вызывать это.
     */
    //public void loadExtData(ItemMap itemMap) {
    //}

    public int getIconMAXLength() {
        return MAX_ICON_LENGTH;
    }
    //public abstract FavoriteItemMap getDBFavoriteMap();

    public int getImageMAXLength() {
        return MAX_IMAGE_LENGTH;
    }

    public int isValid() {

        // TEST ALL BYTES for database FIELD
        if (name.getBytes(StandardCharsets.UTF_8).length > ItemCls.MAX_NAME_LENGTH) {
            errorValue = "" + name.getBytes(StandardCharsets.UTF_8).length + " > " + ItemCls.MAX_NAME_LENGTH;
            return Transaction.INVALID_NAME_LENGTH_MAX;
        }

        if (author == null) {
            errorValue = "Empty value denied";
            return Transaction.INVALID_ITEM_AUTHOR;
        } else if (owner == null) {
            errorValue = "Empty value denied";
            return Transaction.INVALID_ITEM_OWNER;
        }

        if (icon != null && icon.length > getIconMAXLength()) {
            errorValue = "" + icon.length + " > " + getIconMAXLength();
            return Transaction.INVALID_ICON_LENGTH_MAX;
        } else if (image != null && image.length > getImageMAXLength()) {
            errorValue = "" + image.length + " > " + getImageMAXLength();
            return Transaction.INVALID_IMAGE_LENGTH_MAX;
        }

        if (tags != null && tags.getBytes(StandardCharsets.UTF_8).length > 255) {
            return Transaction.INVALID_TAGS_LENGTH_MAX;
        }

        //CHECK DESCRIPTION LENGTH
        int descriptionLength = description == null ? 0 : description.getBytes(StandardCharsets.UTF_8).length;
        if (descriptionLength > Transaction.MAX_DATA_BYTES_LENGTH) {
            errorValue = "" + descriptionLength + " > " + Transaction.MAX_DATA_BYTES_LENGTH;
            return Transaction.INVALID_DESCRIPTION_LENGTH_MAX;
        }

        if (iconAsURL && (icon == null || icon.length > 512)) {
            errorValue = "icon";
            return Transaction.INVALID_URL_LENGTH;
        }

        if (imageAsURL && (image == null || image.length > 512)) {
            errorValue = "image";
            return Transaction.INVALID_URL_LENGTH;
        }

        if (iconType == MEDIA_TYPE_AUDIO) {
            errorValue = "!=audio";
            return Transaction.INVALID_ICON_TYPE;
        }

        if (startDate != null && stopDate != null && startDate > stopDate)
            return Transaction.INVALID_ITEM_STOP_DATE;

        return Transaction.VALIDATE_OK;
    }

    public final int getItemType() {
        return type;
    }

    public final int getTypeClass() {
        return typeClass;
    }

    public int getClassModel() {
        return this.classModel;
    }

    public abstract long START_KEY();

    public abstract long MIN_START_KEY();

    public long getStartKey() {
        return getStartKey(getItemType(), START_KEY(), MIN_START_KEY());
    }

    public abstract int getMinNameLen();

    public abstract String getItemTypeName();

    public abstract String getTypeClassName();

    public abstract ItemMap getDBMap(DCSet db);

    public byte[] getTypeBytes() {
        return this.typeBytes;
    }

    public PublicKeyAccount getAuthor() {
        return this.author;
    }

    public PublicKeyAccount getOwner() {
        return this.owner;
    }

    public void setOwner(PublicKeyAccount newOwner) {
        this.owner = newOwner;
    }

    public String getName() {
        return this.name;
    }

    public String getShortName() {
        String[] words = this.viewName().split(" ");
        String result = "";
        for (String word : words) {
            if (word.length() > 6) {
                result += word.substring(0, 5) + ".";
            } else {
                result += word + " ";
            }
            if (result.length() > 25)
                break;
        }

        return result.trim();

    }

    // TODO - get from TAGS first
    public String getTickerName() {
        String[] words = getName().split("|");
        String name = words[0].trim();
        if (name.length() > 6) {
            name = name.substring(0, 6);
        }
        return name;

    }

    public String getTags() {
        return tags;
    }

    /**
     * доп метки для поиска данной сущности или её типа
     *
     * @return
     */
    public String[] getTagsFull() {
        if (tags != null && !tags.isEmpty()) {
            String[] array = tags.toLowerCase().split(",");
            for (int i = 0; i < array.length; i++) {
                array[i] = array[i].trim();
            }
            return array;
        }
        return null;
    }

    public void putTagsFull(JSONObject json) {
        String[] tagsArray = getTagsFull();
        if (tagsArray == null || tagsArray.length == 0)
            return;

        JSONArray tagsJson = new JSONArray();
        Collections.addAll(tagsJson, tagsArray);
        json.put("tagsFull", tagsJson);
    }

    public long getFlags() {
        return Longs.fromByteArray(new byte[]{typeBytes[POS_COMM_FLAGS_1], typeBytes[POS_TYPE_FLAGS_1],
                typeBytes[POS_CLASS_FLAGS_1], typeBytes[POS_MODEL_FLAGS_1]});
    }

    public String getImageTypeExt() {
        if (imageType == ItemCls.MEDIA_TYPE_VIDEO) {
            return "mp4";
        } else if (imageType == ItemCls.MEDIA_TYPE_AUDIO) {
            return "mp3";
        }
        if (image.length > 20) {
            byte[] header = new byte[20];
            System.arraycopy(image, 0, header, 0, 20);
            String typeName = new String(header).trim();
            if (typeName.contains("PNG")) {
                return "png";
            } else if (typeName.contains("GIF")) {
                return "gif";
            }
        }
        return "jpg";
    }

    public MediaType getImageMediaType() {
        return getMediaType(imageType, isImageAsURL() ? null : image);
    }

    public byte[] getIcon() {
        return this.icon;
    }

    @Override
    public ImageIcon getImageIcon() {
        byte[] icon = getIcon();
        if (icon == null || icon.length == 0)
            return null;

        if (isIconAsURL()) {
            URL url = null;
            try {
                url = new URL(new String(icon, StandardCharsets.UTF_8));
                return new ImageIcon(url);
            } catch (Exception e) {
                return null;
            }
        } else
            return new ImageIcon(icon);
    }

    public int getIconType() {
        return iconType;
    }

    public MediaType getIconMediaType() {
        return getMediaType(iconType, isIconAsURL() ? null : icon);
    }

    public boolean isIconAsURL() {
        return iconAsURL;
    }

    public String getIconURL() {
        if (isIconAsURL()) {
            // внешняя ссылка - обработаем ее
            return new String(getIcon(), StandardCharsets.UTF_8);
        } else if (getIcon() != null && getIcon().length > 0) {
            return "/api" + getItemTypeName() + "/icon/" + key;
        }

        return null;
    }

    public byte[] getImage() {
        return this.image;
    }

    public int getImageType() {
        return imageType;
    }

    public boolean isImageAsURL() {
        return imageAsURL;
    }

    public boolean hasStartDate() {
        return startDate != null;
    }

    public boolean hasStopDate() {
        return stopDate != null;
    }

    public Long getStartDate() {
        return startDate;
    }

    public Long getStopDate() {
        return stopDate;
    }

    public String viewStartDate() {
        return this.startDate == null ? "--" : DateTimeFormat.timestamptoString(this.startDate, Settings.getInstance().getBirthTimeFormat(), "UTC");
    }

    public String viewStopDate() {
        return this.stopDate == null ? "--" : DateTimeFormat.timestamptoString(this.stopDate, Settings.getInstance().getBirthTimeFormat(), "UTC");
    }

    public boolean isActive(long timestamp, boolean forInit) {
        if (!forInit && startDate != null && timestamp < startDate) {
            errorValue = "< " + viewStartDate();
            return false;
        }
        if (stopDate != null && timestamp >= stopDate) {
            errorValue = ">= " + viewStopDate();
            return false;
        }
        return true;
    }

    public String getImageURL() {
        if (isImageAsURL()) {
            // внешняя ссылка - обработаем ее
            return new String(getImage(), StandardCharsets.UTF_8);
        } else if (getImage() != null && getImage().length > 0) {
            return "/api" + getItemTypeName() + "/image/" + key;
        }

        return null;
    }

    public long getKey() {
        return this.key;
    }

    public void setKey(long key) {
        this.key = key;
    }

    public long resolveKey(DCSet db) {

        if (this.reference == null || BlockChain.isWiped(this.reference))
            return 0L;

        return this.key;
    }

    public String getItemTypeAndKey() {
        return getItemTypeAndKey(getItemType(), key);
    }

    public long getHeight(DCSet db) {
        //INSERT INTO DATABASE
        ItemMap dbMap = this.getDBMap(db);
        long key = dbMap.getLastKey();
        return key;
    }

    public long getDBref() {
        return dbRef;
    }

    public void resetKey() {
        this.key = 0;
    }

    public String viewName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public String viewDescription() {
        return this.description;
    }

    public byte[] getReference() {
        return this.reference;
    }

    /**
     * Тут может быть переопределена повторно - если транзакция валялась в неподтвержденных и была уже проверена
     * ранее. Это не страшно
     *
     * @param signature
     * @param dbRef
     */
    public void setReference(byte[] signature, long dbRef) {
        this.reference = signature;
        this.dbRef = dbRef;
    }

    public Transaction getIssueTransaction(DCSet dcSet) {
        return dcSet.getTransactionFinalMap().get(this.reference);
    }

    public boolean isConfirmed() {
        return isConfirmed(DCSet.getInstance());
    }

    public boolean isConfirmed(DCSet db) {
        return key != 0;
    }

    public int getConfirmations(DCSet db) {

        // CHECK IF IN UNCONFIRMED TRANSACTION

        if (!isConfirmed())
            return 0;

        Long dbRef = db.getTransactionFinalMapSigns().get(this.reference);
        if (dbRef == null)
            return 0;

        int height = Transaction.parseHeightDBRef(dbRef);

        return 1 + db.getBlockMap().size() - height;

    }

    public boolean isFavorite() {
        return Controller.getInstance().isItemFavorite(this);
    }

    @Override
    public int hashCode() {
        return (getItemType() << 30) + (int) key + (reference == null ? 0 : Ints.fromByteArray(reference));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ItemCls) {
            ItemCls item = (ItemCls) obj;
            if (item.getItemType() == getItemType()) {
                if (key != 0 && item.key == key
                        || this.reference != null && item.reference != null
                        && Arrays.equals(this.reference, item.reference))
                    /// сущности по смарт-контрактам, в момент когда еще нет Номера, могут совпадать reference
                    /// но это разные сущности! надо имя сравнивать
                    return item.name.equals(name);
            }
        }
        return false;
    }

    public String toString(DCSet db) {
        long key = this.getKey();
        return (key < getStartKey() ? "" : "[" + key + "] ") + this.viewName();
    }

    public String toString(DCSet db, byte[] data) {
        String str = this.toString(db);

        Tuple6<Long, Long, byte[], byte[], Long, byte[]> tuple = RSetStatusToItem.unpackData(data);

        if (str.contains("%1") && tuple.a != null)
            str = str.replace("%1", tuple.a.toString());
        else
            str = str.replace("%1", "");

        if (str.contains("%2") && tuple.b != null)
            str = str.replace("%2", tuple.b.toString());
        else
            str = str.replace("%2", "");

        if (str.contains("%3") && tuple.c != null)
            str = str.replace("%3", new String(tuple.c, StandardCharsets.UTF_8));
        else
            str = str.replace("%3", "");

        if (str.contains("%4") && tuple.d != null)
            str = str.replace("%4", new String(tuple.d, StandardCharsets.UTF_8));
        else
            str = str.replace("%4", "");

        if (str.contains("%D") && tuple.f != null)
            str = str.replace("%D", new String(tuple.f, StandardCharsets.UTF_8));
        else
            str = str.replace("%D", "");

        return str;
    }

    public String toStringNoKey(byte[] data) {
        String str = name;

        Tuple6<Long, Long, byte[], byte[], Long, byte[]> tuple = RSetStatusToItem.unpackData(data);

        if (str.contains("%1") && tuple.a != null)
            str = str.replace("%1", tuple.a.toString());
        if (str.contains("%2") && tuple.b != null)
            str = str.replace("%2", tuple.b.toString());
        if (str.contains("%3") && tuple.c != null)
            str = str.replace("%3", new String(tuple.c, StandardCharsets.UTF_8));
        if (str.contains("%4") && tuple.d != null)
            str = str.replace("%4", new String(tuple.d, StandardCharsets.UTF_8));
        if (str.contains("%D") && tuple.f != null)
            str = str.replace("%D", new String(tuple.f, StandardCharsets.UTF_8));

        return str;
    }

    @Override
    public String toString() {
        return toString(DCSet.getInstance());
    }

    public String getShort(DCSet db) {
        return this.viewName().substring(0, Math.min(this.viewName().length(), 30));
    }

    public String getShort() {
        return getShort(DCSet.getInstance());
    }

    public void toJsonInfo(Map json, String keyName) {
        json.put(keyName + "_key", getKey());
        json.put(keyName + "_name", viewName());
        json.put(keyName + "_icon", getImageURL());
        json.put(keyName + "_iconMediaType", getIconMediaType().toString());
    }

    public JSONObject toJsonInfo() {
        JSONObject json = new JSONObject();
        json.put("key", getKey());
        json.put("name", viewName());
        json.put("icon", getImageURL());
        json.put("iconMediaType", getIconMediaType().toString());
        return json;
    }

    public JSONObject toJsonLite() {

        JSONObject itemJSON = new JSONObject();

        itemJSON.put("itemType", type);
        itemJSON.put("typeClass", typeClass);
        itemJSON.put("classModel", classModel);
        itemJSON.put("commVer", typeBytes[POS_COMM_VERS]);
        itemJSON.put("commFlags1", typeBytes[POS_COMM_FLAGS_1]);
        itemJSON.put("typeFlags1", typeBytes[POS_TYPE_FLAGS_1]);
        itemJSON.put("classFlags1", typeBytes[POS_CLASS_FLAGS_1]);
        itemJSON.put("modelFlags1", typeBytes[POS_MODEL_FLAGS_1]);

        itemJSON.put("itemTypeName", this.getItemTypeName());
        itemJSON.put("typeClassName", this.getTypeClassName());

        author.toJsonPersonInfo(itemJSON, "author");
        owner.toJsonPersonInfo(itemJSON, "owner");

        itemJSON.put("startKey", this.getStartKey());

        itemJSON.put("key", this.getKey());

        itemJSON.put("name", this.name);
        if (tags != null && !tags.isEmpty())
            itemJSON.put("tags", this.tags);

        String iconURL = getIconURL();
        if (iconURL != null) {
            itemJSON.put("iconURL", getIconURL());
            itemJSON.put("iconType", getIconType());
            itemJSON.put("iconMediaType", getIconMediaType().toString());
        }

        return itemJSON;
    }

    @SuppressWarnings("unchecked")
    public JSONObject toJson() {

        JSONObject itemJSON = toJsonLite();

        putTagsFull(itemJSON);

        itemJSON.put("charKey", getItemTypeAndKey());

        // ADD DATA
        itemJSON.put("itemCharKey", getItemTypeAndKey());
        itemJSON.put("description", viewDescription());
        itemJSON.put("isConfirmed", this.isConfirmed());

        if (reference != null) {
            itemJSON.put("txSignature", Base58.encode(this.reference));

            Long txSeqNo = DCSet.getInstance().getTransactionFinalMapSigns().get(this.reference);
            if (txSeqNo != null) {
                // если транзакция еще не подтверждена - чтобы ошибок не было при отображении в блокэксплорере
                itemJSON.put("txSeqNo", Transaction.viewDBRef(txSeqNo));
                if (referenceTx == null)
                    referenceTx = DCSet.getInstance().getTransactionFinalMap().get(txSeqNo);

                if (referenceTx != null) {
                    PublicKeyAccount creatorTX = referenceTx.getCreator();
                    creatorTX.toJsonPersonInfo(itemJSON, "txCreator");
                    itemJSON.put("txTimestamp", referenceTx.getTimestamp());
                    itemJSON.put("blockTimestamp", Controller.getInstance().blockChain.getTimestamp(referenceTx.getBlockHeight()));
                }
            }
        }

        String imageURL = getImageURL();
        if (imageURL != null) {
            itemJSON.put("imageURL", imageURL);
            itemJSON.put("imageType", getImageType());
            itemJSON.put("imageMediaType", getImageMediaType().toString());
            itemJSON.put("imagePreviewMediaType", PreviewMaker.getPreviewType(this).toString());
        }

        if (startDate != null)
            itemJSON.put("startDate", startDate);
        if (stopDate != null)
            itemJSON.put("stopDate", stopDate);

        return itemJSON;
    }

    @SuppressWarnings("unchecked")
    public JSONObject toJsonData() {

        JSONObject itemJSON = new JSONObject();

        // ADD DATA
        if (isIconAsURL()) {
            itemJSON.put("iconURL", getIconURL());
            itemJSON.put("iconType", getIconType());
            itemJSON.put("iconMediaType", getIconMediaType().toString());
        }

        if (isImageAsURL()) {
            itemJSON.put("imageURL", getImageURL());
            itemJSON.put("imageType", getImageType());
            itemJSON.put("imageMediaType", getImageMediaType().toString());
        }

        return itemJSON;
    }

    /**
     * JSON for Scanner lists
     *
     * @param langObj
     * @param args
     * @return
     */
    public JSONObject jsonForExplorerPage(JSONObject langObj, Object[] args) {

        JSONObject itemJSON = new JSONObject();
        itemJSON.put("key", this.getKey());
        itemJSON.put("nameOrig", getName());
        itemJSON.put("name", this.viewName());
        putTagsFull(itemJSON);

        itemJSON.put("item_type", this.getItemTypeName());

        if (description != null && !description.isEmpty()) {
            if (viewDescription().length() > 100) {
                itemJSON.put("description", viewDescription().substring(0, 100));
            } else {
                itemJSON.put("description", viewDescription());
            }
        } else {
            itemJSON.put("description", "");
        }

        author.toJsonPersonInfo(itemJSON, "author");

        String iconURL = getIconURL();
        if (iconURL != null) {
            itemJSON.put("iconURL", iconURL);
            itemJSON.put("iconMediaType", getIconMediaType().toString());
        }

        return itemJSON;
    }

    public JSONObject jsonForExplorerInfo(DCSet dcSet, JSONObject langObj, boolean forPrint) {

        JSONObject itemJson = toJson();

        if (getKey() > 0 && getKey() < getStartKey()) {
            itemJson.put("description", Lang.T(viewDescription(), langObj));
        }

        itemJson.put("Label_Author", Lang.T("Author", langObj));
        itemJson.put("Label_Owner", Lang.T("Owner", langObj));
        //itemJson.put("Label_Pubkey", Lang.T("Public Key", langObj));
        itemJson.put("Label_TXCreator", Lang.T("Creator", langObj));
        itemJson.put("Label_Number", Lang.T("Number", langObj));
        itemJson.put("Label_TXIssue", Lang.T("Transaction of Issue", langObj));
        itemJson.put("Label_DateIssue", Lang.T("Issued Date", langObj));
        itemJson.put("Label_Signature", Lang.T("Signature", langObj));
        itemJson.put("Label_Actions", Lang.T("Actions", langObj));
        itemJson.put("Label_RAW", Lang.T("Bytecode", langObj));
        itemJson.put("Label_Print", Lang.T("Print", langObj));
        itemJson.put("Label_Description", Lang.T("Description", langObj));
        itemJson.put("Label_seqNo", Lang.T("Номер", langObj));
        itemJson.put("Label_SourceText", Lang.T("Source Text # исходный текст", langObj));
        itemJson.put("Label_ValidityPeriod", Lang.T("Validity period", langObj));

        itemJson.put("author", this.getAuthor().getAddress());
        Fun.Tuple2<Integer, PersonCls> authorPerson = this.getAuthor().getPerson();
        if (authorPerson != null) {
            itemJson.put("author_person", authorPerson.b.getName());
            itemJson.put("author_person_key", authorPerson.b.getKey());
            itemJson.put("author_person_image_url", authorPerson.b.getImageURL());
            itemJson.put("author_person_image_media_type", authorPerson.b.getImageMediaType().toString());

        }

        itemJson.put("owner", this.getOwner().getAddress());
        Fun.Tuple2<Integer, PersonCls> ownerPerson = this.getOwner().getPerson();
        if (ownerPerson != null) {
            itemJson.put("owner_person", ownerPerson.b.getName());
            itemJson.put("owner_person_key", ownerPerson.b.getKey());
            itemJson.put("owner_person_image_url", ownerPerson.b.getImageURL());
            itemJson.put("owner_person_image_media_type", ownerPerson.b.getImageMediaType().toString());

        }

        if (referenceTx != null) {
            if (referenceTx.getCreator() != null) {
                itemJson.put("tx_creator_person", referenceTx.viewCreator());
            }

            WebTransactionsHTML.getAppLink(itemJson, referenceTx, langObj);
            WebTransactionsHTML.getVouches(itemJson, referenceTx, langObj);
            WebTransactionsHTML.getLinks(itemJson, referenceTx, langObj);

        }

        return itemJson;
    }

    public String makeHTMLView() {

        String text = makeHTMLHeadView();
        text += makeHTMLFootView(true);

        return text;
    }

    public String makeHTMLHeadView() {

        String text = "[" + getKey() + "]" + Lang.T("Name") + ":&nbsp;" + viewName() + "<br>";
        return text;

    }

    public String makeHTMLFootView(boolean andLabel) {

        String text = andLabel ? Lang.T("Description") + ":<br>" : "";
        if (getKey() > 0 && getKey() < START_KEY()) {
            text += Library.to_HTML(Lang.T(viewDescription())) + "<br>";
        } else {
            text += Library.to_HTML(viewDescription()) + "<br>";
        }

        return text;

    }

    public HashMap getNovaItems() {
        return new HashMap<String, Fun.Tuple3<Long, Long, byte[]>>();
    }

    public byte[] getNovaItemCreator(Object item) {
        return ((Fun.Tuple3<Integer, Long, byte[]>) item).c;
    }

    public Long getNovaItemKey(Object item) {
        return ((Fun.Tuple3<Long, Long, byte[]>) item).a;
    }

    /**
     * @param dcSet
     * @return key если еще не добавлен, -key если добавлен и 0 - если это не НОВА
     */
    public long isNovaItem(DCSet dcSet) {
        Object item = getNovaItems().get(this.name);
        if (item != null && author.equals(getNovaItemCreator(item))) {
            ItemMap dbMap = this.getDBMap(dcSet);
            Long key = getNovaItemKey(item);
            if (dbMap.contains(key)) {
                return -key;
            } else {
                return key;
            }
        }

        return 0L;
    }

    //
    public long insertToMap(DCSet db, long startKey) {
        //INSERT INTO DATABASE
        ItemMap dbMap = this.getDBMap(db);

        long newKey;

        long novaKey = this.isNovaItem(db);

        if (novaKey > 0) {

            // INSERT WITH NOVA KEY
            newKey = novaKey;
            dbMap.put(newKey, this);

            // если в Генесиз вносим NOVA ASSET - пересчитаем и Размер
            if (dbMap.getLastKey() < newKey) {
                dbMap.setLastKey(newKey);
            }

        } else {

            // INSERT WITH NEW KEY
            newKey = dbMap.getLastKey();
            if (newKey < startKey) {
                // IF this not GENESIS issue - start from startKey
                dbMap.setLastKey(startKey);
            }
            newKey = dbMap.incrementPut(this);

        }

        this.key = newKey;

        return key;
    }

    public long deleteFromMap(DCSet db, long startKey) {
        //DELETE FROM DATABASE

        long thisKey = this.getKey();

        ItemMap map = this.getDBMap(db);
        if (thisKey > startKey) {
            map.decrementDelete(thisKey);

            if (BlockChain.CHECK_BUGS > 1
                    && map.getLastKey() != thisKey - 1 && !BlockChain.isNovaAsset(thisKey)) {
                LOGGER.error("After delete KEY: " + key + " != map.value.key - 1: " + map.getLastKey());
                Long error = null;
                error++;
            }

        } else {
            map.delete(thisKey);
        }

        return thisKey;

    }

}
