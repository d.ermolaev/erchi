package org.erachain.core;

import com.google.common.primitives.Bytes;
import erchi.core.account.PrivateKeyAccount;
import lombok.extern.slf4j.Slf4j;
import org.erachain.controller.Controller;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.core.block.Block;
import org.erachain.core.crypto.Base58;
import org.erachain.core.exdata.exLink.ExLink;
import org.erachain.core.item.assets.AssetCls;
import org.erachain.core.item.assets.AssetVenture;
import org.erachain.core.item.assets.Order;
import org.erachain.core.item.persons.PersonCls;
import org.erachain.core.item.persons.PersonHuman;
import org.erachain.core.item.polls.PollCls;
import org.erachain.core.item.statuses.StatusCls;
import org.erachain.core.item.templates.TemplateCls;
import org.erachain.core.transaction.*;
import org.erachain.dapp.DAPP;
import org.erachain.datachain.DCSet;
import org.erachain.datachain.TransactionMap;
import org.erachain.dbs.IteratorCloseable;
import org.erachain.ntp.NTP;
import org.erachain.utils.Pair;
import org.erachain.utils.TransactionTimestampComparator;
import org.mapdb.DB;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * класс для создания транзакций в текущем кошельке
 * Использует форкнутую базу цепочки
 * <p>
 * TODO: - поидее его надо перенести в модуль Кошелек - ибо без кошелька он не будет пахать.
 */
@Slf4j
public class TransactionCreator {
    private final AtomicInteger seqNo = new AtomicInteger();
    private DCSet fork;
    private Block lastBlock;
    private int blockHeight;

    // must be a SYNCHRONIZED
    private synchronized void checkUpdate() {
        //CHECK IF WE ALREADY HAVE A FORK
        if (this.lastBlock == null || this.fork == null) {
            updateFork();
        } else {
            //CHECK IF WE NEED A NEW FORK
            if (!Arrays.equals(this.lastBlock.getSignature(), Controller.getInstance().getLastBlock().getSignature())) {
                updateFork();
            }
        }
    }

    private synchronized void updateFork() {
        //CREATE NEW FORK
        if (this.fork != null) {
            // закроем сам файл базы - закрывать DCSet.fork - не нужно - он сам очистится
            this.fork.close();
        }

        // создаем в памяти базу - так как она на 1 блок только нужна - а значит много памяти не возьмет
        DB database = DCSet.makeDBinMemory();
        this.fork = DCSet.getInstance().fork(database, "updateFork");

        //UPDATE LAST BLOCK
        this.lastBlock = Controller.getInstance().getLastBlock();
        this.blockHeight = this.fork.getBlockMap().size() + 1;
        this.seqNo.set(0); // reset sequence number

        //SCAN UNCONFIRMED TRANSACTIONS FOR TRANSACTIONS WHERE ACCOUNT IS CREATOR OF
        TransactionMap transactionTab = this.fork.getTransactionTab();
        List<Transaction> accountTransactions = new ArrayList<Transaction>();
        Transaction transaction;

        // здесь нужен протокольный итератор!
        try (IteratorCloseable<Long> iterator = transactionTab.getIterator()) {
            List<Account> accountMap = Controller.getInstance().getWalletAccounts();

            while (iterator.hasNext()) {
                transaction = transactionTab.get(iterator.next());

                if (accountMap.contains(transaction.getCreator())) {
                    accountTransactions.add(transaction);
                }
            }
        } catch (IOException e) {
        }

        //SORT THEM BY TIMESTAMP
        Collections.sort(accountTransactions, new TransactionTimestampComparator());

        //VALIDATE AND PROCESS THOSE TRANSACTIONS IN FORK for recalc last reference
        for (Transaction transactionAccount : accountTransactions) {

            try {
                transactionAccount.setDC(this.fork, Transaction.FOR_NETWORK, this.blockHeight, this.seqNo.incrementAndGet());
                if (transactionAccount.isValid(Transaction.FOR_NETWORK, 0L) == Transaction.VALIDATE_OK) {
                    transactionAccount.process(null, Transaction.FOR_NETWORK);
                } else {
                    //THE TRANSACTION BECAME INVALID LET
                    this.fork.getTransactionTab().delete(transactionAccount);
                }
                // CLEAR for HEAP
                transactionAccount.resetDCSet();
            } catch (java.lang.Throwable e) {
                if (e instanceof java.lang.IllegalAccessError) {
                    // налетели на закрытую таблицу
                } else {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

    public DCSet getFork() {
        if (this.fork == null) {
            updateFork();
        }
        return this.fork;
    }

    public long getReference(PublicKeyAccount creator) {
        this.checkUpdate();
        return 0L;
    }

    public Transaction createIssueAssetTransaction(PrivateKeyAccount creator, ExLink linkTo, AssetCls asset, int feePow) {
        //CHECK FOR UPDATES
        // all unconfirmed org.erachain.records insert in FORK for calc last account REFERENCE
        this.checkUpdate();

        //TIME
        long time = NTP.getTime();

        asset.setKey(this.fork.getItemAssetMap().getLastKey() + 1L);

        //CREATE ISSUE ASSET TRANSACTION
        IssueAssetTransaction issueAssetTransaction = new IssueAssetTransaction(creator, linkTo, asset, (byte) feePow, time);
        issueAssetTransaction.sign(creator, Transaction.FOR_NETWORK);
        issueAssetTransaction.setDC(this.fork, Transaction.FOR_NETWORK, this.blockHeight, this.seqNo.incrementAndGet());

        return issueAssetTransaction;
    }

    // TODO use tryInsertTransaction
    @Deprecated
    public Transaction r_Send(PrivateKeyAccount creator, String title, String tags, byte[] message,
                              byte[] isText, byte[] encryptMessage, ExLink linkTo, DAPP dapp, int feePow, Account recipient,
                              int actionType, BigDecimal amount, long key) {

        this.checkUpdate();

        Transaction messageTx;

        long timestamp = NTP.getTime();

        //CREATE MESSAGE TRANSACTION
        messageTx = new TransactionAmount(creator, title, tags, message, isText[0] > 0, encryptMessage[0] > 0,
                linkTo, dapp, (byte) feePow, recipient, actionType, amount, key, timestamp);
        messageTx.sign(creator, Transaction.FOR_NETWORK);
        messageTx.setDC(this.fork, Transaction.FOR_NETWORK, this.blockHeight, this.seqNo.incrementAndGet());

        return messageTx;
    }

    public void signAndUpdateTransaction(Transaction transaction, PrivateKeyAccount creator) {

        transaction.setTimestamp(NTP.getTime());
        transaction.sign(creator, Transaction.FOR_NETWORK);

        this.checkUpdate();

        transaction.setDC(this.fork, Transaction.FOR_NETWORK, this.blockHeight, this.seqNo.incrementAndGet());

    }

    public Integer afterCreate(Transaction transaction, int forDeal, boolean tryFree, boolean notRelease) {
        //CHECK IF PAYMENT VALID

        if (!transaction.isSignatureValid())
            return Transaction.INVALID_SIGNATURE;

        boolean incremented = false;
        if (!this.fork.equals(transaction.getDCSet())) {
            incremented = true;
            transaction.setDC(this.fork, forDeal, this.blockHeight, this.seqNo.incrementAndGet());
        }

        int valid = 0;
        try {
            valid = transaction.isValid(forDeal, tryFree ? Transaction.NOT_VALIDATE_FLAG_FEE : 0L);
        } catch (TxException e) {
            throw new RuntimeException(e);
        }

        if (valid == Transaction.VALIDATE_OK) {

            if (forDeal > Transaction.FOR_PACK) {

                if (notRelease) {
                    if (incremented)
                        this.seqNo.decrementAndGet();
                } else {
                    //PROCESS IN FORK
                    try {
                        transaction.process(null, forDeal);
                    } catch (TxException e) {
                        throw new RuntimeException(e);
                    }
                    Controller.getInstance().onTransactionCreate(transaction);
                }
            }
        } else {
            if (incremented)
                this.seqNo.decrementAndGet();
        }


        //RETURN
        return valid;
    }

    public Integer afterCreateRaw(Transaction transaction, int forDeal, boolean notRelease) {
        this.checkUpdate();
        return this.afterCreate(transaction, forDeal, false, notRelease);
    }

    public Transaction r_Hashes(PrivateKeyAccount creator, ExLink exLink, int feePow,
                                String urlStr, String dataStr, String[] hashes58) {

        this.checkUpdate();

        Transaction transaction;

        long timestamp = NTP.getTime();

        byte[] url = urlStr.getBytes(StandardCharsets.UTF_8);
        byte[] data = dataStr.getBytes(StandardCharsets.UTF_8);

        byte[][] hashes = new byte[hashes58.length][32];
        for (int i = 0; i < hashes58.length; i++) {
            hashes[i] = Bytes.ensureCapacity(Base58.decode(hashes58[i]), 32, 0);
        }

        //CREATE MESSAGE TRANSACTION
        transaction = new RHashes(creator, exLink, (byte) feePow, url, data, hashes, timestamp);
        transaction.sign(creator, Transaction.FOR_NETWORK);
        transaction.setDC(this.fork, Transaction.FOR_NETWORK, this.blockHeight, this.seqNo.incrementAndGet());

        return transaction;
    }

    public Transaction r_Hashes(PrivateKeyAccount creator, ExLink exLink, int feePow,
                                String urlStr, String dataStr, String hashesStr) {

        String[] hashes58;
        if (hashesStr.length() > 0) {
            hashes58 = hashesStr.split("[-, ]");
        } else {
            hashes58 = new String[0];
        }
        return r_Hashes(creator, exLink, feePow,
                urlStr, dataStr, hashes58);
    }

}
