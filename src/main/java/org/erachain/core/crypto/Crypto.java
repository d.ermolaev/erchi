package org.erachain.core.crypto;

import erchi.core.account.PrivateKeyAccount;
import org.erachain.core.account.Account;
import org.erachain.core.account.PublicKeyAccount;
import org.erachain.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Crypto {

    public static final int HASH_LENGTH = 32;
    public static final int SIGNATURE_LENGTH = 2 * HASH_LENGTH;
    public static final byte[] EMPTY_SIGNATURE = new byte[SIGNATURE_LENGTH];

    public static final byte ADDRESS_VERSION = 15;

    public static final byte NOT_SIGNED = 0; // not used signatures
    public static final byte ED25519_SYSTEM = 100;
    public static final byte DAPP_SYSTEM = 1;

    static final byte[] EMPTY_PUB_KEY = new byte[PublicKeyAccount.PUBLIC_KEY_LENGTH];
    static Logger LOGGER = LoggerFactory.getLogger(Crypto.class.getName());
    private static Crypto instance;
    private final SecureRandom random;

    private Crypto() {

        //RANDOM
        this.random = new SecureRandom();

    }

    public static Crypto getInstance() {
        if (instance == null) {
            instance = new Crypto();
        }

        return instance;
    }

    public byte[] digest(byte[] input) {
        try {
            //SHA256
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            return sha256.digest(input);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public byte[] digest512(byte[] input) {
        try {
            //SHA256
            MessageDigest sha512 = MessageDigest.getInstance("SHA-512");
            return sha512.digest(input);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public byte[] doubleDigest(byte[] input) {
        //DOUBLE SHA256
        return this.digest(this.digest(input));
    }

    public Pair<byte[], byte[]> createKeyPair(byte[] seed) {
        return Ed25519.createKeyPair(seed);
    }

    public String getAddressFromShort(byte[] addressShort) {
        return Account.getAddress(addressShort);
    }


    public byte[] sign(PrivateKeyAccount account, byte[] message) {
        try {
            //GET SIGNATURE
            return Ed25519.sign(account.getKeyPair(), message);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new byte[64];
        }
    }

    public boolean verify(byte[] publicKey, byte[] signature, byte[] message) {
        try {
            //VERIFY SIGNATURE
            return Ed25519.verify(signature, message, publicKey);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }

    // make SEEd with length without empty symbols in Base58
    public byte[] createSeed(int length) {
        byte[] seed = new byte[length];
        this.random.nextBytes(seed);
        return seed;

    }

    @Deprecated
    public enum SignType {
        Ed25519(ED25519_SYSTEM),
        unknown(0x0F);

        private final int value;

        SignType(final int value) {
            this.value = value;
        }

        public static Crypto.SignType getCode(final byte value) {
            for (final Crypto.SignType code : Crypto.SignType.values()) {
                if (code.value == value) {
                    return code;
                }
            }
            throw new IllegalArgumentException(
                    "Illegal value provided for Code (" + value + ").");
        }

        /**
         * Returns the byte value of the enumerations value.
         *
         * @return byte representation
         */
        public int getValue() {
            return value;
        }
    }

}
