package erchi.core.account

import com.google.common.primitives.Bytes
import org.erachain.core.account.PublicKeyAccount
import org.erachain.core.crypto.Base58
import org.erachain.core.crypto.Crypto
import org.erachain.utils.Pair

class PrivateKeyAccount(system: Byte, val seed: ByteArray) : PublicKeyAccount(
    system,
    Crypto.getInstance().createKeyPair(
        seed
    ).b
) {

    val keyPair: Pair<ByteArray, ByteArray> = Crypto.getInstance().createKeyPair(seed)

    val privateKey: ByteArray
        get() = keyPair.a

    fun getSeed58(): String {
        return "SECRET:" + Base58.encode(Bytes.concat(byteArrayOf(system), seed))
    }

    fun sign(data: ByteArray): ByteArray {
        if (system == Crypto.ED25519_SYSTEM)
            return Crypto.getInstance().sign(this, data)

        return Crypto.getInstance().sign(this, data)
    }
}